# Android Timezone service #

This is a service for android that can be used by other apps to convert from lat/lon to a time zone id.

It is a wrapper around https://sites.google.com/a/edval.biz/www/mapping-lat-lng-s-to-timezones 2km file, which has been modified to improve performance on initialization.

The provided code is available under GPL: https://www.gnu.org/copyleft/gpl.html

The latest stable version van be installed from the play store: https://play.google.com/store/apps/details?id=pukanito.at.gmail.com.pftimezoneservice

## Usage ##

In your manifest file you need to add a permission:

```
#!xml

<uses-permission android:name="pukanito.at.gmail.com.pftimezoneservice.PFTZService" />
```

In your activity or service you can add something like:

```
#!java

public void resolve(final double latitude, final double longitude) {
    ResultReceiver receiver = new ResolverResultReceiver(new Handler());
    Intent intent = new Intent("pukanito.at.gmail.com.pftimezoneservice.RESOLVE");
    intent.setPackage("pukanito.at.gmail.com.pftimezoneservice");
    intent.putExtra("receiver", receiverForSending(receiver));
    intent.putExtra("latitude", latitude);
    intent.putExtra("longitude", longitude);
    PackageManager manager = getPackageManager();
    List<ResolveInfo> infos = manager.queryIntentServices(intent, 0);
    if (infos != null && infos.size() > 0)
        startService(intent);
    else {
        // Time zone resolver service not present.
        ...
    }
}

/**
 * From: http://stackoverflow.com/questions/5743485/android-resultreceiver-across-packages
 *       Robert Tupelo-Schneck.
 */
private static ResultReceiver receiverForSending(ResultReceiver actualReceiver) {
    Parcel parcel = Parcel.obtain();
    actualReceiver.writeToParcel(parcel,0);
    parcel.setDataPosition(0);
    ResultReceiver receiverForSending = ResultReceiver.CREATOR.createFromParcel(parcel);
    parcel.recycle();
    return receiverForSending;
}

private class ResolverResultReceiver extends ResultReceiver {

    public ResolverResultReceiver(final Handler handler) {
        super(handler);
    }

    @Override
    protected void onReceiveResult(final int resultCode, final Bundle resultData) {
        final String tzid = resultData.getString("tzid");
        // Do something with the tzid.
        ...
    }
}

```