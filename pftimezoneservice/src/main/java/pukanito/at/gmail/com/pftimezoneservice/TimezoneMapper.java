/*
 This file is part of PFTimezoneService.

 PFTimezoneService is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PFTimezoneService is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with PFTimezoneService.  If not, see <http://www.gnu.org/licenses/>.

 The original code was written by Tim Cooper:   tim@edval.com.au
 https://sites.google.com/a/edval.biz/www/mapping-lat-lng-s-to-timezones
 This code is available under GPL:   https://www.gnu.org/copyleft/gpl.html

 The original code has been modified to perform on Android.
 Copyright 2015 Yves Han.
*/

package pukanito.at.gmail.com.pftimezoneservice;

public class TimezoneMapper {

    public static String latLngToTimezoneString(double lat, double lng)
    {
        String tzId = timezoneStrings[getTzInt(lat,lng)];
        return tzId;
    }

	static String[] timezoneStrings = {
	"unknown",
	"America/Dominica",
	"America/St_Vincent",
	"Australia/Lord_Howe",
	"Asia/Kashgar",
	"Pacific/Wallis",
	"Europe/Berlin",
	"America/Manaus",
	"Asia/Jerusalem",
	"America/Phoenix",
	"Australia/Darwin",
	"Asia/Seoul",
	"Africa/Gaborone",
	"Indian/Chagos",
	"America/Argentina/Mendoza",
	"Asia/Hong_Kong",
	"America/Godthab",
	"Africa/Dar_es_Salaam",
	"Pacific/Majuro",
	"America/Port-au-Prince",
	"America/Montreal",
	"Atlantic/Reykjavik",
	"America/Panama",
	"America/Sitka",
	"Asia/Ho_Chi_Minh",
	"America/Danmarkshavn",
	"Asia/Jakarta",
	"America/Boise",
	"Asia/Baghdad",
	"Africa/El_Aaiun",
	"Europe/Zagreb",
	"America/Santiago",
	"America/Merida",
	"Africa/Nouakchott",
	"America/Bahia_Banderas",
	"Australia/Perth",
	"Asia/Sakhalin",
	"Asia/Vladivostok",
	"Africa/Bissau",
	"America/Los_Angeles",
	"Asia/Rangoon",
	"America/Belize",
	"Asia/Harbin",
	"Australia/Currie",
	"Pacific/Pago_Pago",
	"America/Vancouver",
	"Asia/Magadan",
	"Asia/Tbilisi",
	"Asia/Yerevan",
	"Europe/Tallinn",
	"Pacific/Johnston",
	"Asia/Baku",
	"America/North_Dakota/New_Salem",
	"Europe/Vilnius",
	"America/Indiana/Petersburg",
	"Asia/Tehran",
	"America/Inuvik",
	"Europe/Lisbon",
	"Europe/Vatican",
	"Pacific/Chatham",
	"Antarctica/Macquarie",
	"America/Araguaina",
	"Asia/Thimphu",
	"Atlantic/Madeira",
	"America/Coral_Harbour",
	"Pacific/Funafuti",
	"Indian/Mahe",
	"Australia/Adelaide",
	"Africa/Freetown",
	"Atlantic/South_Georgia",
	"Africa/Accra",
	"America/North_Dakota/Beulah",
	"America/Jamaica",
	"America/Scoresbysund",
	"America/Swift_Current",
	"Europe/Tirane",
	"Asia/Ashgabat",
	"America/Moncton",
	"Europe/Vaduz",
	"Australia/Eucla",
	"America/Montserrat",
	"America/Glace_Bay",
	"Atlantic/Stanley",
	"Africa/Bujumbura",
	"Africa/Porto-Novo",
	"America/Argentina/Rio_Gallegos",
	"America/Grenada",
	"Asia/Novokuznetsk",
	"America/Argentina/Catamarca",
	"America/Indiana/Indianapolis",
	"America/Indiana/Tell_City",
	"America/Curacao",
	"America/Miquelon",
	"America/Detroit",
	"America/Menominee",
	"Asia/Novosibirsk",
	"Africa/Lagos",
	"Indian/Cocos",
	"America/Yakutat",
	"Europe/Volgograd",
	"Asia/Qatar",
	"Indian/Antananarivo",
	"Pacific/Marquesas",
	"America/Grand_Turk",
	"Asia/Khandyga",
	"America/North_Dakota/Center",
	"Pacific/Guam",
	"Pacific/Pitcairn",
	"America/Cambridge_Bay",
	"Asia/Bahrain",
	"America/Kentucky/Monticello",
	"Arctic/Longyearbyen",
	"Africa/Cairo",
	"Australia/Hobart",
	"Pacific/Galapagos",
	"Asia/Oral",
	"America/Dawson_Creek",
	"Africa/Mbabane",
	"America/Halifax",
	"Pacific/Tongatapu",
	"Asia/Aqtau",
	"Asia/Hovd",
	"Africa/Nairobi",
	"Asia/Ulaanbaatar",
	"Indian/Christmas",
	"Asia/Taipei",
	"Australia/Melbourne",
	"America/Argentina/Salta",
	"Australia/Broken_Hill",
	"America/Argentina/Tucuman",
	"America/Kentucky/Louisville",
	"Asia/Jayapura",
	"Asia/Macau",
	"America/Ojinaga",
	"America/Nome",
	"Pacific/Wake",
	"Europe/Andorra",
	"America/Iqaluit",
	"America/Kralendijk",
	"Europe/Jersey",
	"Asia/Ust-Nera",
	"Asia/Yakutsk",
	"America/Yellowknife",
	"America/Fortaleza",
	"Asia/Irkutsk",
	"America/Tegucigalpa",
	"Europe/Zaporozhye",
	"Pacific/Fiji",
	"Pacific/Tarawa",
	"Africa/Asmara",
	"Asia/Dhaka",
	"Asia/Pyongyang",
	"Europe/Athens",
	"America/Resolute",
	"Africa/Brazzaville",
	"Africa/Libreville",
	"Atlantic/St_Helena",
	"Europe/Samara",
	"America/Adak",
	"America/Argentina/Jujuy",
	"America/Chicago",
	"Africa/Sao_Tome",
	"Europe/Bratislava",
	"Asia/Riyadh",
	"America/Lima",
	"America/New_York",
	"America/Pangnirtung",
	"Asia/Samarkand",
	"America/Port_of_Spain",
	"Africa/Johannesburg",
	"Pacific/Port_Moresby",
	"America/Bahia",
	"Europe/Zurich",
	"America/St_Barthelemy",
	"Asia/Nicosia",
	"Europe/Kaliningrad",
	"America/Anguilla",
	"Europe/Ljubljana",
	"Asia/Yekaterinburg",
	"Africa/Kampala",
	"America/Rio_Branco",
	"Africa/Bamako",
	"America/Goose_Bay",
	"Europe/Moscow",
	"Africa/Conakry",
	"America/Chihuahua",
	"Europe/Warsaw",
	"Pacific/Palau",
	"Europe/Mariehamn",
	"Africa/Windhoek",
	"America/La_Paz",
	"America/Recife",
	"America/Mexico_City",
	"Asia/Amman",
	"America/Tijuana",
	"America/Metlakatla",
	"Pacific/Midway",
	"Europe/Simferopol",
	"Europe/Budapest",
	"Pacific/Apia",
	"America/Paramaribo",
	"Africa/Malabo",
	"Africa/Ndjamena",
	"Asia/Choibalsan",
	"America/Antigua",
	"Europe/Istanbul",
	"Africa/Blantyre",
	"Australia/Sydney",
	"Asia/Dushanbe",
	"Europe/Belgrade",
	"Asia/Karachi",
	"Europe/Luxembourg",
	"Europe/Podgorica",
	"Australia/Lindeman",
	"Africa/Bangui",
	"Asia/Aden",
	"Pacific/Chuuk",
	"Asia/Brunei",
	"Indian/Comoro",
	"America/Asuncion",
	"Europe/Prague",
	"America/Cayman",
	"Pacific/Pohnpei",
	"America/Atikokan",
	"Pacific/Norfolk",
	"Africa/Dakar",
	"America/Argentina/Buenos_Aires",
	"America/Edmonton",
	"America/Barbados",
	"America/Santo_Domingo",
	"Asia/Bishkek",
	"Asia/Kuwait",
	"Pacific/Efate",
	"Indian/Mauritius",
	"America/Aruba",
	"Australia/Brisbane",
	"Indian/Kerguelen",
	"Pacific/Kiritimati",
	"America/Toronto",
	"Asia/Qyzylorda",
	"Asia/Aqtobe",
	"America/Eirunepe",
	"Europe/Isle_of_Man",
	"America/Blanc-Sablon",
	"Pacific/Honolulu",
	"America/Montevideo",
	"Asia/Tashkent",
	"Pacific/Kosrae",
	"America/Indiana/Winamac",
	"America/Argentina/La_Rioja",
	"Africa/Mogadishu",
	"Asia/Phnom_Penh",
	"Africa/Banjul",
	"America/Creston",
	"Europe/Brussels",
	"Asia/Gaza",
	"Atlantic/Bermuda",
	"America/Indiana/Knox",
	"America/El_Salvador",
	"America/Managua",
	"Africa/Niamey",
	"Europe/Monaco",
	"Africa/Ouagadougou",
	"Pacific/Easter",
	"Atlantic/Canary",
	"Asia/Vientiane",
	"Europe/Bucharest",
	"Africa/Lusaka",
	"Asia/Kathmandu",
	"Africa/Harare",
	"Asia/Bangkok",
	"Europe/Rome",
	"Africa/Lome",
	"America/Denver",
	"Indian/Reunion",
	"Europe/Kiev",
	"Europe/Vienna",
	"America/Guadeloupe",
	"America/Argentina/Cordoba",
	"Asia/Manila",
	"Asia/Tokyo",
	"America/Nassau",
	"Pacific/Enderbury",
	"Atlantic/Azores",
	"America/Winnipeg",
	"Europe/Dublin",
	"Asia/Kuching",
	"America/Argentina/Ushuaia",
	"Asia/Colombo",
	"Asia/Krasnoyarsk",
	"America/St_Johns",
	"Asia/Shanghai",
	"Pacific/Kwajalein",
	"Africa/Kigali",
	"Europe/Chisinau",
	"America/Noronha",
	"Europe/Guernsey",
	"Europe/Paris",
	"America/Guyana",
	"Africa/Luanda",
	"Africa/Abidjan",
	"America/Tortola",
	"Europe/Malta",
	"Europe/London",
	"Pacific/Guadalcanal",
	"Pacific/Gambier",
	"America/Thule",
	"America/Rankin_Inlet",
	"America/Regina",
	"America/Indiana/Vincennes",
	"America/Santarem",
	"Africa/Djibouti",
	"Pacific/Tahiti",
	"Europe/San_Marino",
	"America/Argentina/San_Luis",
	"Africa/Ceuta",
	"Asia/Singapore",
	"America/Campo_Grande",
	"Africa/Tunis",
	"Europe/Copenhagen",
	"Asia/Pontianak",
	"Asia/Dubai",
	"Africa/Khartoum",
	"Europe/Helsinki",
	"America/Whitehorse",
	"America/Maceio",
	"Africa/Douala",
	"Asia/Kuala_Lumpur",
	"America/Martinique",
	"America/Sao_Paulo",
	"America/Dawson",
	"Africa/Kinshasa",
	"Europe/Riga",
	"Africa/Tripoli",
	"Europe/Madrid",
	"America/Nipigon",
	"Pacific/Fakaofo",
	"Europe/Skopje",
	"America/St_Thomas",
	"Africa/Maseru",
	"Europe/Sofia",
	"America/Porto_Velho",
	"America/St_Kitts",
	"Africa/Casablanca",
	"Asia/Hebron",
	"Asia/Dili",
	"America/Argentina/San_Juan",
	"Asia/Almaty",
	"Europe/Sarajevo",
	"America/Boa_Vista",
	"Africa/Addis_Ababa",
	"Indian/Mayotte",
	"Africa/Lubumbashi",
	"Atlantic/Cape_Verde",
	"America/Lower_Princes",
	"Europe/Oslo",
	"Africa/Monrovia",
	"Asia/Muscat",
	"America/Thunder_Bay",
	"America/Juneau",
	"Pacific/Rarotonga",
	"Atlantic/Faroe",
	"America/Cayenne",
	"America/Cuiaba",
	"Africa/Maputo",
	"Asia/Anadyr",
	"Asia/Kabul",
	"America/Santa_Isabel",
	"Asia/Damascus",
	"Pacific/Noumea",
	"America/Anchorage",
	"Asia/Kolkata",
	"Pacific/Niue",
	"Asia/Kamchatka",
	"America/Matamoros",
	"Europe/Stockholm",
	"America/Havana",
	"Pacific/Auckland",
	"America/Rainy_River",
	"Asia/Omsk",
	"Africa/Algiers",
	"America/Guayaquil",
	"Indian/Maldives",
	"Asia/Makassar",
	"America/Monterrey",
	"Europe/Amsterdam",
	"America/St_Lucia",
	"Europe/Uzhgorod",
	"America/Indiana/Marengo",
	"Pacific/Saipan",
	"America/Bogota",
	"America/Indiana/Vevay",
	"America/Guatemala",
	"America/Puerto_Rico",
	"America/Marigot",
	"Africa/Juba",
	"America/Costa_Rica",
	"America/Caracas",
	"Pacific/Nauru",
	"Europe/Minsk",
	"America/Belem",
	"America/Cancun",
	"America/Hermosillo",
	"Asia/Chongqing",
	"Asia/Beirut",
	"Europe/Gibraltar",
	"Asia/Urumqi",
	"America/Mazatlan"
	};

	private static int getTzInt(double lat, double lng)
	{
	 if (lng < 7.634444)
	  return call49(lat,lng);
	 else
	  if (lng < 107.186142)
	   if (lng < 43.199085)
	    if (lat < 58.313641)
	     return call29(lat,lng);
	    else
	     return call30(lat,lng);
	   else
	    if (lat < 19.567249)
	     return call41(lat,lng);
	    else
	     if (lng < 77.698914)
	      if (lat < 44.271465)
	       if (lng < 54.132832)
	        if (lat < 27.843779)
	         if (lat < 24.802000)
	          if (lng < 53.340973)
	           if (lat < 24.373695)
	            if (lng < 52.652027)
	             if (lng < 52.034306)
	              {
	              if (poly[0].contains(lat,lng)) return 163;
	              else return 321;
	              }
	             else
	              if (lat < 23.585572)
	               {
	               if (poly[1].contains(lat,lng)) return 321;
	               else return 163;
	               }
	              else
	               return 321;
	            else
	             if (lat < 22.930712)
	              {
	              if (poly[2].contains(lat,lng)) return 321;
	              else return 163;
	              }
	             else
	              return 321;
	           else
	            if (lng < 51.511971)
	             {
	             if (poly[3].contains(lat,lng)) return 163;
	             else return 100;
	             }
	            else
	             return 321;
	          else
	           if (lat < 22.848626)
	            if (lat < 19.710944)
	             {
	             if (poly[4].contains(lat,lng)) return 357;
	             else return 163;
	             }
	            else
	             {
	             if (poly[5].contains(lat,lng)) return 321;
	             else return 163;
	             }
	           else
	            return 321;
	         else
	          if (lng < 50.359833)
	           return 163;
	          else
	           if (lng < 51.254749)
	            if (lng < 50.664471)
	             if (lat < 25.509583)
	              return 163;
	             else
	              return 109;
	            else
	             if (lng < 50.828693)
	              if (lat < 25.615926)
	               if (lng < 50.817749)
	                if (lat < 25.046631)
	                 if (lng < 50.779838)
	                  return 100;
	                 else
	                  return 163;
	                else
	                 if (lat < 25.523945)
	                  return 100;
	                 else
	                  {
	                  if (poly[6].contains(lat,lng)) return 100;
	                  else return 109;
	                  }
	               else
	                return 100;
	              else
	               return 109;
	             else
	              return 100;
	           else
	            if (lat < 26.166555)
	             if (lng < 52.427582)
	              return 100;
	             else
	              return 321;
	            else
	             return 55;
	        else
	         if (lng < 49.143944)
	          return call32(lat,lng);
	         else
	          if (lat < 36.860363)
	           return 55;
	          else
	           if (lng < 50.815176)
	            if (lat < 40.000000)
	             if (lat < 37.608088)
	              return 55;
	             else
	              return 51;
	            else
	             if (lng < 50.607834)
	              return 51;
	             else
	              return 120;
	           else
	            if (lat < 39.656750)
	             if (lat < 37.340805)
	              if (lat < 36.947498)
	               return 55;
	              else
	               {
	               if (poly[7].contains(lat,lng)) return 76;
	               else return 55;
	               }
	             else
	              return 76;
	            else
	             if (lat < 40.805862)
	              return 76;
	             else
	              {
	              if (poly[8].contains(lat,lng)) return 76;
	              else return 120;
	              }
	       else
	        if (lat < 24.585056)
	         if (lng < 67.529053)
	          if (lng < 59.836582)
	           if (lng < 56.023945)
	            if (lat < 24.426750)
	             if (lat < 24.076361)
	              {
	              if (poly[9].contains(lat,lng)) return 163;
	              if (poly[10].contains(lat,lng)) return 321;
	              else return 357;
	              }
	             else
	              if (lng < 54.479195)
	               return 321;
	              else
	               {
	               if (poly[11].contains(lat,lng)) return 357;
	               else return 321;
	               }
	            else
	             if (lng < 54.564610)
	              return 321;
	             else
	              {
	              if (poly[12].contains(lat,lng)) return 357;
	              else return 321;
	              }
	           else
	            return 357;
	          else
	           return 210;
	         else
	          if (lat < 23.647362)
	           return 371;
	          else
	           if (lat < 23.780416)
	            if (lng < 68.187114)
	             return 210;
	            else
	             return 371;
	           else
	            if (lng < 68.191277)
	             return 210;
	            else
	             if (lat < 23.877695)
	              return 371;
	             else
	              {
	              if (poly[13].contains(lat,lng)) return 210;
	              else return 371;
	              }
	        else
	         return call34(lat,lng);
	      else
	       if (lat < 69.612137)
	        if (lat < 66.434448)
	         return call36(lat,lng);
	        else
	         if (lng < 61.114918)
	          return 183;
	         else
	          if (lng < 66.210541)
	           if (lng < 61.193863)
	            return 183;
	           else
	            if (lat < 67.696091)
	             {
	             if (poly[14].contains(lat,lng)) return 178;
	             else return 183;
	             }
	            else
	             {
	             if (poly[15].contains(lat,lng)) return 183;
	             else return 178;
	             }
	          else
	           return 178;
	       else
	        if (lng < 66.471832)
	         return 183;
	        else
	         if (lat < 73.526054)
	          return 178;
	         else
	          if (lat < 78.236320)
	           return 183;
	          else
	           return 289;
	     else
	      if (lat < 72.816780)
	       return call40(lat,lng);
	      else
	       return 289;
	  else
	   return call48(lat,lng);
	}

	private static int call0(double lat, double lng)
	{
	 if (lat < -31.846668)
	  if (lng < -64.471046)
	   if (lat < -41.999168)
	    {
	    if (poly[16].contains(lat,lng)) return 31;
	    if (poly[17].contains(lat,lng)) return 127;
	    else return 88;
	    }
	   else
	    if (lat < -36.922918)
	     if (lng < -68.456551)
	      if (lat < -39.461043)
	       if (lng < -70.449303)
	        if (lat < -40.730106)
	         {
	         if (poly[18].contains(lat,lng)) return 127;
	         else return 31;
	         }
	        else
	         {
	         if (poly[19].contains(lat,lng)) return 31;
	         else return 127;
	         }
	       else
	        return 127;
	      else
	       if (lng < -70.449303)
	        {
	        if (poly[20].contains(lat,lng)) return 31;
	        else return 127;
	        }
	       else
	        {
	        if (poly[21].contains(lat,lng)) return 14;
	        else return 127;
	        }
	     else
	      {
	      if (poly[22].contains(lat,lng)) return 14;
	      else return 127;
	      }
	    else
	     if (lng < -68.456551)
	      if (lat < -34.384793)
	       if (lng < -70.449303)
	        {
	        if (poly[23].contains(lat,lng)) return 14;
	        if (poly[24].contains(lat,lng)) return 127;
	        else return 31;
	        }
	       else
	        {
	        if (poly[25].contains(lat,lng)) return 31;
	        if (poly[26].contains(lat,lng)) return 31;
	        if (poly[27].contains(lat,lng)) return 127;
	        else return 14;
	        }
	      else
	       if (lng < -70.236777)
	        {
	        if (poly[28].contains(lat,lng)) return 14;
	        if (poly[29].contains(lat,lng)) return 346;
	        if (poly[30].contains(lat,lng)) return 346;
	        else return 31;
	        }
	       else
	        if (lat < -33.115731)
	         {
	         if (poly[31].contains(lat,lng)) return 31;
	         else return 14;
	         }
	        else
	         {
	         if (poly[32].contains(lat,lng)) return 31;
	         if (poly[33].contains(lat,lng)) return 31;
	         if (poly[34].contains(lat,lng)) return 346;
	         else return 14;
	         }
	     else
	      if (lat < -34.384793)
	       {
	       if (poly[35].contains(lat,lng)) return 127;
	       if (poly[36].contains(lat,lng)) return 278;
	       if (poly[37].contains(lat,lng)) return 314;
	       else return 14;
	       }
	      else
	       if (lng < -66.463799)
	        if (lat < -33.115731)
	         {
	         if (poly[38].contains(lat,lng)) return 314;
	         else return 14;
	         }
	        else
	         {
	         if (poly[39].contains(lat,lng)) return 14;
	         if (poly[40].contains(lat,lng)) return 249;
	         if (poly[41].contains(lat,lng)) return 314;
	         else return 346;
	         }
	       else
	        {
	        if (poly[42].contains(lat,lng)) return 249;
	        if (poly[43].contains(lat,lng)) return 278;
	        else return 314;
	        }
	  else
	   if (lng < -62.365894)
	    if (lat < -42.072571)
	     return 88;
	    else
	     {
	     if (poly[44].contains(lat,lng)) return 127;
	     if (poly[45].contains(lat,lng)) return 278;
	     else return 226;
	     }
	   else
	    if (lat < -38.766533)
	     return 226;
	    else
	     {
	     if (poly[46].contains(lat,lng)) return 278;
	     else return 226;
	     }
	 else
	  if (lng < -70.599426)
	   return 31;
	  else
	   if (lat < -25.156712)
	    if (lng < -66.452066)
	     if (lat < -28.501690)
	      if (lng < -68.525746)
	       if (lat < -30.174179)
	        {
	        if (poly[47].contains(lat,lng)) return 346;
	        else return 31;
	        }
	       else
	        {
	        if (poly[48].contains(lat,lng)) return 31;
	        if (poly[49].contains(lat,lng)) return 249;
	        else return 346;
	        }
	      else
	       {
	       if (poly[50].contains(lat,lng)) return 88;
	       if (poly[51].contains(lat,lng)) return 346;
	       else return 249;
	       }
	     else
	      if (lng < -68.525746)
	       if (lat < -26.829201)
	        {
	        if (poly[52].contains(lat,lng)) return 88;
	        if (poly[53].contains(lat,lng)) return 249;
	        if (poly[54].contains(lat,lng)) return 346;
	        else return 31;
	        }
	       else
	        {
	        if (poly[55].contains(lat,lng)) return 88;
	        if (poly[56].contains(lat,lng)) return 88;
	        if (poly[57].contains(lat,lng)) return 88;
	        else return 31;
	        }
	      else
	       if (lat < -26.829201)
	        {
	        if (poly[58].contains(lat,lng)) return 31;
	        if (poly[59].contains(lat,lng)) return 249;
	        else return 88;
	        }
	       else
	        {
	        if (poly[60].contains(lat,lng)) return 31;
	        if (poly[61].contains(lat,lng)) return 31;
	        if (poly[62].contains(lat,lng)) return 31;
	        if (poly[63].contains(lat,lng)) return 127;
	        else return 88;
	        }
	    else
	     if (lat < -28.501690)
	      {
	      if (poly[64].contains(lat,lng)) return 249;
	      if (poly[65].contains(lat,lng)) return 278;
	      else return 88;
	      }
	     else
	      if (lng < -64.378386)
	       if (lat < -26.829201)
	        {
	        if (poly[66].contains(lat,lng)) return 88;
	        if (poly[67].contains(lat,lng)) return 278;
	        else return 129;
	        }
	       else
	        {
	        if (poly[68].contains(lat,lng)) return 88;
	        if (poly[69].contains(lat,lng)) return 127;
	        if (poly[70].contains(lat,lng)) return 278;
	        else return 129;
	        }
	      else
	       {
	       if (poly[71].contains(lat,lng)) return 127;
	       else return 278;
	       }
	   else
	    if (lng < -64.158066)
	     if (lng < -67.378746)
	      {
	      if (poly[72].contains(lat,lng)) return 127;
	      if (poly[73].contains(lat,lng)) return 190;
	      else return 31;
	      }
	     else
	      if (lat < -23.378345)
	       if (lng < -65.768406)
	        {
	        if (poly[74].contains(lat,lng)) return 31;
	        if (poly[75].contains(lat,lng)) return 159;
	        if (poly[76].contains(lat,lng)) return 159;
	        else return 127;
	        }
	       else
	        {
	        if (poly[77].contains(lat,lng)) return 159;
	        else return 127;
	        }
	      else
	       if (lng < -65.768406)
	        {
	        if (poly[78].contains(lat,lng)) return 31;
	        if (poly[79].contains(lat,lng)) return 127;
	        if (poly[80].contains(lat,lng)) return 190;
	        else return 159;
	        }
	       else
	        if (lat < -22.489161)
	         {
	         if (poly[81].contains(lat,lng)) return 159;
	         if (poly[82].contains(lat,lng)) return 190;
	         else return 127;
	         }
	        else
	         {
	         if (poly[83].contains(lat,lng)) return 127;
	         if (poly[84].contains(lat,lng)) return 127;
	         if (poly[85].contains(lat,lng)) return 159;
	         else return 190;
	         }
	    else
	     {
	     if (poly[86].contains(lat,lng)) return 190;
	     if (poly[87].contains(lat,lng)) return 219;
	     if (poly[88].contains(lat,lng)) return 278;
	     else return 127;
	     }
	}

	private static int call1(double lat, double lng)
	{
	 if (lng < -62.304707)
	  if (lng < -73.131889)
	   if (lng < -109.229095)
	    if (lng < -140.592087)
	     if (lng < -157.873383)
	      if (lng < -177.880646)
	       return 377;
	      else
	       if (lat < -22.136345)
	        return 119;
	       else
	        return 360;
	     else
	      return 312;
	    else
	     if (lng < -134.878342)
	      return 305;
	     else
	      if (lng < -124.772850)
	       return 107;
	      else
	       return 263;
	   else
	    return 31;
	  else
	   if (lng < -72.442055)
	    return 31;
	   else
	    return call0(lat,lng);
	 else
	  if (lng < -51.151619)
	   if (lat < -38.793318)
	    return 226;
	   else
	    if (lat < -33.256744)
	     if (lng < -56.669033)
	      if (lng < -58.403419)
	       if (lng < -60.270836)
	        if (lat < -33.279449)
	         {
	         if (poly[89].contains(lat,lng)) return 278;
	         else return 226;
	         }
	        else
	         {
	         if (poly[90].contains(lat,lng)) return 226;
	         else return 278;
	         }
	       else
	        if (lat < -34.247784)
	         return 226;
	        else
	         {
	         if (poly[91].contains(lat,lng)) return 226;
	         if (poly[92].contains(lat,lng)) return 245;
	         if (poly[93].contains(lat,lng)) return 245;
	         else return 278;
	         }
	      else
	       if (lat < -34.154116)
	        if (lng < -58.238857)
	         return 226;
	        else
	         if (lat < -34.711240)
	          {
	          if (poly[94].contains(lat,lng)) return 245;
	          else return 226;
	          }
	         else
	          return 245;
	       else
	        if (lat < -34.020496)
	         if (lng < -58.370483)
	          return 226;
	         else
	          return 245;
	        else
	         return 245;
	     else
	      if (lng < -55.917015)
	       return 245;
	      else
	       {
	       if (poly[95].contains(lat,lng)) return 329;
	       else return 245;
	       }
	    else
	     if (lng < -53.073933)
	      if (lat < -32.746323)
	       if (lng < -58.113934)
	        if (lng < -58.179478)
	         if (lat < -33.112217)
	          if (lng < -58.380772)
	           return 245;
	          else
	           return 278;
	         else
	          if (lng < -58.244583)
	           return 278;
	          else
	           {
	           if (poly[96].contains(lat,lng)) return 245;
	           else return 278;
	           }
	        else
	         if (lat < -33.078617)
	          return 245;
	         else
	          if (lng < -58.151073)
	           {
	           if (poly[97].contains(lat,lng)) return 245;
	           else return 278;
	           }
	          else
	           if (lat < -33.051277)
	            return 245;
	           else
	            {
	            if (poly[98].contains(lat,lng)) return 278;
	            else return 245;
	            }
	       else
	        if (lng < -58.083050)
	         return 245;
	        else
	         {
	         if (poly[99].contains(lat,lng)) return 329;
	         else return 245;
	         }
	      else
	       if (lat < -27.173150)
	        if (lng < -57.689320)
	         {
	         if (poly[100].contains(lat,lng)) return 219;
	         if (poly[101].contains(lat,lng)) return 245;
	         else return 278;
	         }
	        else
	         if (lat < -29.959736)
	          if (lng < -55.381626)
	           if (lat < -31.353029)
	            return 245;
	           else
	            if (lng < -56.535473)
	             {
	             if (poly[102].contains(lat,lng)) return 278;
	             if (poly[103].contains(lat,lng)) return 329;
	             else return 245;
	             }
	            else
	             {
	             if (poly[104].contains(lat,lng)) return 245;
	             else return 329;
	             }
	          else
	           {
	           if (poly[105].contains(lat,lng)) return 329;
	           else return 245;
	           }
	         else
	          if (lng < -55.381626)
	           if (lat < -28.566443)
	            {
	            if (poly[106].contains(lat,lng)) return 329;
	            else return 278;
	            }
	           else
	            if (lng < -56.535473)
	             {
	             if (poly[107].contains(lat,lng)) return 278;
	             else return 219;
	             }
	            else
	             {
	             if (poly[108].contains(lat,lng)) return 219;
	             if (poly[109].contains(lat,lng)) return 329;
	             if (poly[110].contains(lat,lng)) return 329;
	             else return 278;
	             }
	          else
	           {
	           if (poly[111].contains(lat,lng)) return 278;
	           if (poly[112].contains(lat,lng)) return 278;
	           if (poly[113].contains(lat,lng)) return 278;
	           else return 329;
	           }
	       else
	        if (lng < -57.689320)
	         if (lat < -24.386564)
	          if (lng < -59.997013)
	           return 278;
	          else
	           if (lat < -25.779857)
	            {
	            if (poly[114].contains(lat,lng)) return 278;
	            else return 219;
	            }
	           else
	            {
	            if (poly[115].contains(lat,lng)) return 278;
	            else return 219;
	            }
	         else
	          if (lng < -59.997013)
	           {
	           if (poly[116].contains(lat,lng)) return 278;
	           else return 219;
	           }
	          else
	           {
	           if (poly[117].contains(lat,lng)) return 278;
	           if (poly[118].contains(lat,lng)) return 317;
	           else return 219;
	           }
	        else
	         if (lat < -24.386564)
	          if (lng < -55.381626)
	           {
	           if (poly[119].contains(lat,lng)) return 278;
	           if (poly[120].contains(lat,lng)) return 278;
	           else return 219;
	           }
	          else
	           if (lat < -25.779857)
	            if (lng < -54.227779)
	             {
	             if (poly[121].contains(lat,lng)) return 219;
	             else return 278;
	             }
	            else
	             if (lat < -26.476503)
	              {
	              if (poly[122].contains(lat,lng)) return 329;
	              if (poly[123].contains(lat,lng)) return 329;
	              else return 278;
	              }
	             else
	              {
	              if (poly[124].contains(lat,lng)) return 329;
	              else return 278;
	              }
	           else
	            {
	            if (poly[125].contains(lat,lng)) return 219;
	            if (poly[126].contains(lat,lng)) return 278;
	            else return 329;
	            }
	         else
	          if (lng < -55.381626)
	           if (lat < -22.993271)
	            {
	            if (poly[127].contains(lat,lng)) return 317;
	            else return 219;
	            }
	           else
	            {
	            if (poly[128].contains(lat,lng)) return 219;
	            else return 317;
	            }
	          else
	           {
	           if (poly[129].contains(lat,lng)) return 219;
	           if (poly[130].contains(lat,lng)) return 329;
	           else return 317;
	           }
	     else
	      if (lat < -30.006798)
	       return 329;
	      else
	       {
	       if (poly[131].contains(lat,lng)) return 317;
	       else return 329;
	       }
	  else
	   if (lng < -40.978226)
	    return 329;
	   else
	    return 156;
	}

	private static int call2(double lat, double lng)
	{
	 if (lat < 12.137099)
	  if (lat < 7.110721)
	   if (lat < 0.280717)
	    if (lng < -89.239861)
	     return 114;
	    else
	     if (lng < -79.895477)
	      if (lat < -3.404815)
	       if (lng < -80.838730)
	        return 164;
	       else
	        if (lng < -80.230945)
	         if (lat < -3.453752)
	          if (lat < -3.949543)
	           {
	           if (poly[132].contains(lat,lng)) return 381;
	           else return 164;
	           }
	          else
	           {
	           if (poly[133].contains(lat,lng)) return 381;
	           else return 164;
	           }
	         else
	          {
	          if (poly[134].contains(lat,lng)) return 164;
	          else return 381;
	          }
	        else
	         if (lat < -3.437323)
	          if (lat < -4.285207)
	           if (lng < -80.136307)
	            {
	            if (poly[135].contains(lat,lng)) return 381;
	            else return 164;
	            }
	           else
	            {
	            if (poly[136].contains(lat,lng)) return 381;
	            else return 164;
	            }
	          else
	           if (lat < -3.478404)
	            {
	            if (poly[137].contains(lat,lng)) return 164;
	            else return 381;
	            }
	           else
	            {
	            if (poly[138].contains(lat,lng)) return 164;
	            else return 381;
	            }
	         else
	          return 381;
	      else
	       return 381;
	     else
	      if (lat < -9.017232)
	       return 164;
	      else
	       if (lng < -79.723465)
	        if (lat < -4.393893)
	         {
	         if (poly[139].contains(lat,lng)) return 381;
	         else return 164;
	         }
	        else
	         return 381;
	       else
	        if (lat < -4.368257)
	         {
	         if (poly[140].contains(lat,lng)) return 381;
	         else return 164;
	         }
	        else
	         if (lng < -77.101025)
	          {
	          if (poly[141].contains(lat,lng)) return 164;
	          else return 381;
	          }
	         else
	          if (lat < -2.043770)
	           {
	           if (poly[142].contains(lat,lng)) return 381;
	           else return 164;
	           }
	          else
	           if (lng < -75.789804)
	            {
	            if (poly[143].contains(lat,lng)) return 164;
	            if (poly[144].contains(lat,lng)) return 390;
	            if (poly[145].contains(lat,lng)) return 390;
	            if (poly[146].contains(lat,lng)) return 390;
	            else return 381;
	            }
	           else
	            if (lat < -0.881526)
	             {
	             if (poly[147].contains(lat,lng)) return 381;
	             else return 164;
	             }
	            else
	             if (lng < -75.134194)
	              {
	              if (poly[148].contains(lat,lng)) return 164;
	              if (poly[149].contains(lat,lng)) return 390;
	              else return 381;
	              }
	             else
	              {
	              if (poly[150].contains(lat,lng)) return 390;
	              else return 164;
	              }
	   else
	    if (lat < 2.667005)
	     if (lat < 1.664773)
	      if (lng < -89.933998)
	       return 114;
	      else
	       if (lat < 1.439020)
	        if (lng < -78.833519)
	         return 381;
	        else
	         if (lng < -77.054873)
	          {
	          if (poly[151].contains(lat,lng)) return 390;
	          else return 381;
	          }
	         else
	          if (lng < -76.706748)
	           {
	           if (poly[152].contains(lat,lng)) return 381;
	           else return 390;
	           }
	          else
	           {
	           if (poly[153].contains(lat,lng)) return 381;
	           else return 390;
	           }
	       else
	        return 390;
	     else
	      return 390;
	    else
	     if (lng < -87.027443)
	      return 396;
	     else
	      return 390;
	  else
	   if (lng < -81.257545)
	    if (lng < -82.555992)
	     if (lat < 9.845403)
	      if (lng < -83.620537)
	       return 396;
	      else
	       {
	       if (poly[154].contains(lat,lng)) return 22;
	       else return 396;
	       }
	     else
	      if (lat < 10.119060)
	       return 396;
	      else
	       if (lat < 10.124827)
	        return 396;
	       else
	        {
	        if (poly[155].contains(lat,lng)) return 259;
	        else return 396;
	        }
	    else
	     return 22;
	   else
	    if (lng < -78.151619)
	     return 22;
	    else
	     if (lng < -77.174110)
	      if (lat < 8.686544)
	       {
	       if (poly[156].contains(lat,lng)) return 390;
	       else return 22;
	       }
	      else
	       return 22;
	     else
	      return 390;
	 else
	  if (lng < -83.153893)
	   if (lat < 17.817400)
	    if (lng < -88.027069)
	     if (lng < -88.175591)
	      if (lng < -92.937019)
	       return 192;
	      else
	       if (lng < -90.279968)
	        if (lng < -90.371544)
	         if (lat < 15.832155)
	          {
	          if (poly[157].contains(lat,lng)) return 192;
	          else return 392;
	          }
	         else
	          if (lng < -91.654282)
	           {
	           if (poly[158].contains(lat,lng)) return 392;
	           else return 192;
	           }
	          else
	           if (lat < 16.824778)
	            {
	            if (poly[159].contains(lat,lng)) return 392;
	            else return 192;
	            }
	           else
	            {
	            if (poly[160].contains(lat,lng)) return 392;
	            else return 192;
	            }
	        else
	         return 392;
	       else
	        if (lat < 15.718479)
	         if (lat < 14.435086)
	          {
	          if (poly[161].contains(lat,lng)) return 145;
	          if (poly[162].contains(lat,lng)) return 392;
	          if (poly[163].contains(lat,lng)) return 392;
	          else return 258;
	          }
	         else
	          {
	          if (poly[164].contains(lat,lng)) return 145;
	          if (poly[165].contains(lat,lng)) return 258;
	          else return 392;
	          }
	        else
	         if (lng < -88.627838)
	          {
	          if (poly[166].contains(lat,lng)) return 392;
	          else return 41;
	          }
	         else
	          if (lat < 16.102887)
	           return 41;
	          else
	           return 392;
	     else
	      if (lat < 15.787296)
	       {
	       if (poly[167].contains(lat,lng)) return 258;
	       else return 145;
	       }
	      else
	       return 41;
	    else
	     if (lat < 16.027033)
	      if (lng < -87.692162)
	       if (lat < 13.312460)
	        return 258;
	       else
	        {
	        if (poly[168].contains(lat,lng)) return 258;
	        if (poly[169].contains(lat,lng)) return 258;
	        if (poly[170].contains(lat,lng)) return 258;
	        else return 145;
	        }
	      else
	       if (lng < -87.547157)
	        if (lat < 13.082987)
	         return 259;
	        else
	         return 145;
	       else
	        if (lng < -87.386246)
	         if (lat < 13.031934)
	          return 259;
	         else
	          return 145;
	        else
	         if (lng < -85.270069)
	          if (lat < 14.082066)
	           if (lng < -86.328157)
	            {
	            if (poly[171].contains(lat,lng)) return 145;
	            else return 259;
	            }
	           else
	            {
	            if (poly[172].contains(lat,lng)) return 145;
	            else return 259;
	            }
	          else
	           {
	           if (poly[173].contains(lat,lng)) return 259;
	           else return 145;
	           }
	         else
	          if (lat < 14.063700)
	           return 259;
	          else
	           if (lng < -84.211981)
	            {
	            if (poly[174].contains(lat,lng)) return 259;
	            else return 145;
	            }
	           else
	            {
	            if (poly[175].contains(lat,lng)) return 145;
	            else return 259;
	            }
	     else
	      if (lng < -87.468124)
	       return 41;
	      else
	       return 145;
	   else
	    if (lng < -87.539442)
	     if (lng < -90.983063)
	      if (lng < -96.048988)
	       if (lng < -103.944931)
	        if (lng < -106.203979)
	         return 407;
	        else
	         if (lat < 21.374973)
	          {
	          if (poly[176].contains(lat,lng)) return 34;
	          if (poly[177].contains(lat,lng)) return 407;
	          else return 192;
	          }
	         else
	          {
	          if (poly[178].contains(lat,lng)) return 192;
	          else return 407;
	          }
	       else
	        return 192;
	      else
	       if (lng < -91.509560)
	        {
	        if (poly[179].contains(lat,lng)) return 32;
	        if (poly[180].contains(lat,lng)) return 32;
	        else return 192;
	        }
	       else
	        if (lat < 18.103054)
	         {
	         if (poly[181].contains(lat,lng)) return 192;
	         else return 32;
	         }
	        else
	         return 32;
	     else
	      if (lng < -88.015625)
	       if (lat < 18.646767)
	        if (lng < -88.080856)
	         if (lng < -89.425028)
	          {
	          if (poly[182].contains(lat,lng)) return 401;
	          else return 32;
	          }
	         else
	          if (lat < 18.496557)
	           {
	           if (poly[183].contains(lat,lng)) return 401;
	           else return 41;
	           }
	          else
	           return 401;
	        else
	         if (lat < 18.171311)
	          return 41;
	         else
	          return 401;
	       else
	        {
	        if (poly[184].contains(lat,lng)) return 401;
	        if (poly[185].contains(lat,lng)) return 401;
	        if (poly[186].contains(lat,lng)) return 401;
	        else return 32;
	        }
	      else
	       if (lat < 18.167719)
	        return 41;
	       else
	        if (lat < 19.379875)
	         return 401;
	        else
	         {
	         if (poly[187].contains(lat,lng)) return 32;
	         else return 401;
	         }
	    else
	     return 401;
	  else
	   if (lat < 19.974030)
	    if (lng < -79.727272)
	     if (lng < -82.645966)
	      return 259;
	     else
	      if (lat < 13.380502)
	       return 390;
	      else
	       return 221;
	    else
	     if (lng < -76.180321)
	      if (lat < 18.526976)
	       return 72;
	      else
	       return 376;
	     else
	      if (lng < -75.874138)
	       return 376;
	      else
	       if (lng < -75.169226)
	        {
	        if (poly[188].contains(lat,lng)) return 165;
	        else return 376;
	        }
	       else
	        if (lng < -75.159943)
	         return 165;
	        else
	         {
	         if (poly[189].contains(lat,lng)) return 376;
	         else return 165;
	         }
	   else
	    return 376;
	}

	private static int call3(double lat, double lng)
	{
	 if (lng < -67.557743)
	  if (lat < -10.505639)
	   if (lng < -71.018164)
	    return 164;
	   else
	    if (lat < -12.835115)
	     {
	     if (poly[190].contains(lat,lng)) return 190;
	     else return 164;
	     }
	    else
	     if (lng < -69.287953)
	      {
	      if (poly[191].contains(lat,lng)) return 164;
	      if (poly[192].contains(lat,lng)) return 190;
	      else return 180;
	      }
	     else
	      {
	      if (poly[193].contains(lat,lng)) return 164;
	      if (poly[194].contains(lat,lng)) return 180;
	      else return 190;
	      }
	  else
	   if (lat < -5.502251)
	    if (lng < -71.018164)
	     if (lat < -8.003945)
	      if (lng < -72.748374)
	       {
	       if (poly[195].contains(lat,lng)) return 180;
	       else return 164;
	       }
	      else
	       {
	       if (poly[196].contains(lat,lng)) return 180;
	       else return 164;
	       }
	     else
	      if (lng < -72.748374)
	       if (lat < -6.753098)
	        {
	        if (poly[197].contains(lat,lng)) return 180;
	        if (poly[198].contains(lat,lng)) return 241;
	        else return 164;
	        }
	       else
	        {
	        if (poly[199].contains(lat,lng)) return 241;
	        else return 164;
	        }
	      else
	       {
	       if (poly[200].contains(lat,lng)) return 241;
	       else return 180;
	       }
	    else
	     {
	     if (poly[201].contains(lat,lng)) return 7;
	     if (poly[202].contains(lat,lng)) return 164;
	     if (poly[203].contains(lat,lng)) return 241;
	     else return 180;
	     }
	   else
	    if (lng < -71.018164)
	     if (lat < -3.000556)
	      if (lng < -72.748374)
	       {
	       if (poly[204].contains(lat,lng)) return 241;
	       else return 164;
	       }
	      else
	       if (lat < -4.251403)
	        if (lng < -71.883269)
	         {
	         if (poly[205].contains(lat,lng)) return 241;
	         else return 164;
	         }
	        else
	         {
	         if (poly[206].contains(lat,lng)) return 241;
	         else return 164;
	         }
	       else
	        return 164;
	     else
	      if (lng < -72.748374)
	       if (lat < -1.749709)
	        {
	        if (poly[207].contains(lat,lng)) return 390;
	        else return 164;
	        }
	       else
	        {
	        if (poly[208].contains(lat,lng)) return 164;
	        else return 390;
	        }
	      else
	       {
	       if (poly[209].contains(lat,lng)) return 390;
	       else return 164;
	       }
	    else
	     if (lat < -3.000556)
	      if (lng < -69.287953)
	       if (lat < -4.251403)
	        {
	        if (poly[210].contains(lat,lng)) return 7;
	        if (poly[211].contains(lat,lng)) return 164;
	        if (poly[212].contains(lat,lng)) return 164;
	        else return 241;
	        }
	       else
	        if (lng < -70.153058)
	         {
	         if (poly[213].contains(lat,lng)) return 241;
	         if (poly[214].contains(lat,lng)) return 390;
	         else return 164;
	         }
	        else
	         {
	         if (poly[215].contains(lat,lng)) return 164;
	         if (poly[216].contains(lat,lng)) return 241;
	         if (poly[217].contains(lat,lng)) return 390;
	         else return 7;
	         }
	      else
	       return 7;
	     else
	      if (lng < -69.287953)
	       if (lat < -1.749709)
	        {
	        if (poly[218].contains(lat,lng)) return 7;
	        if (poly[219].contains(lat,lng)) return 164;
	        else return 390;
	        }
	       else
	        {
	        if (poly[220].contains(lat,lng)) return 390;
	        else return 7;
	        }
	      else
	       return 7;
	 else
	  if (lat < -8.000557)
	   if (lng < -63.538338)
	    if (lat < -11.582574)
	     {
	     if (poly[221].contains(lat,lng)) return 341;
	     else return 190;
	     }
	    else
	     if (lng < -65.548040)
	      if (lat < -9.791566)
	       {
	       if (poly[222].contains(lat,lng)) return 7;
	       if (poly[223].contains(lat,lng)) return 180;
	       if (poly[224].contains(lat,lng)) return 341;
	       if (poly[225].contains(lat,lng)) return 341;
	       if (poly[226].contains(lat,lng)) return 341;
	       if (poly[227].contains(lat,lng)) return 341;
	       if (poly[228].contains(lat,lng)) return 341;
	       if (poly[229].contains(lat,lng)) return 341;
	       else return 190;
	       }
	      else
	       {
	       if (poly[230].contains(lat,lng)) return 7;
	       if (poly[231].contains(lat,lng)) return 180;
	       if (poly[232].contains(lat,lng)) return 190;
	       if (poly[233].contains(lat,lng)) return 190;
	       if (poly[234].contains(lat,lng)) return 190;
	       if (poly[235].contains(lat,lng)) return 190;
	       if (poly[236].contains(lat,lng)) return 190;
	       else return 341;
	       }
	     else
	      if (lat < -9.791566)
	       {
	       if (poly[237].contains(lat,lng)) return 190;
	       else return 341;
	       }
	      else
	       if (lng < -64.543189)
	        {
	        if (poly[238].contains(lat,lng)) return 7;
	        if (poly[239].contains(lat,lng)) return 190;
	        else return 341;
	        }
	       else
	        {
	        if (poly[240].contains(lat,lng)) return 341;
	        else return 7;
	        }
	   else
	    if (lat < -11.582574)
	     if (lng < -61.678474)
	      {
	      if (poly[241].contains(lat,lng)) return 341;
	      else return 190;
	      }
	     else
	      if (lat < -13.373583)
	       if (lng < -60.748543)
	        {
	        if (poly[242].contains(lat,lng)) return 190;
	        if (poly[243].contains(lat,lng)) return 363;
	        else return 341;
	        }
	       else
	        {
	        if (poly[244].contains(lat,lng)) return 190;
	        if (poly[245].contains(lat,lng)) return 341;
	        else return 363;
	        }
	      else
	       {
	       if (poly[246].contains(lat,lng)) return 363;
	       else return 341;
	       }
	    else
	     if (lng < -61.678474)
	      {
	      if (poly[247].contains(lat,lng)) return 341;
	      else return 7;
	      }
	     else
	      if (lat < -9.791566)
	       {
	       if (poly[248].contains(lat,lng)) return 341;
	       else return 363;
	       }
	      else
	       {
	       if (poly[249].contains(lat,lng)) return 7;
	       if (poly[250].contains(lat,lng)) return 363;
	       else return 341;
	       }
	  else
	   if (lng < -61.221546)
	    {
	    if (poly[251].contains(lat,lng)) return 349;
	    else return 7;
	    }
	   else
	    {
	    if (poly[252].contains(lat,lng)) return 349;
	    else return 7;
	    }
	}

	private static int call4(double lat, double lng)
	{
	 if (lng < -59.818611)
	  if (lat < -0.498862)
	   return call3(lat,lng);
	  else
	   if (lng < -60.590443)
	    if (lng < -62.334740)
	     if (lng < -68.406662)
	      if (lat < 3.058463)
	       {
	       if (poly[253].contains(lat,lng)) return 7;
	       else return 390;
	       }
	      else
	       {
	       if (poly[254].contains(lat,lng)) return 397;
	       else return 390;
	       }
	     else
	      if (lat < 3.058463)
	       if (lng < -65.370701)
	        if (lat < 1.279800)
	         {
	         if (poly[255].contains(lat,lng)) return 390;
	         if (poly[256].contains(lat,lng)) return 397;
	         else return 7;
	         }
	        else
	         if (lng < -66.888681)
	          if (lat < 2.169132)
	           {
	           if (poly[257].contains(lat,lng)) return 7;
	           if (poly[258].contains(lat,lng)) return 397;
	           if (poly[259].contains(lat,lng)) return 397;
	           else return 390;
	           }
	          else
	           {
	           if (poly[260].contains(lat,lng)) return 397;
	           else return 390;
	           }
	         else
	          {
	          if (poly[261].contains(lat,lng)) return 390;
	          else return 397;
	          }
	       else
	        if (lat < 1.279800)
	         {
	         if (poly[262].contains(lat,lng)) return 349;
	         if (poly[263].contains(lat,lng)) return 397;
	         if (poly[264].contains(lat,lng)) return 397;
	         else return 7;
	         }
	        else
	         if (lng < -63.852720)
	          {
	          if (poly[265].contains(lat,lng)) return 7;
	          if (poly[266].contains(lat,lng)) return 7;
	          if (poly[267].contains(lat,lng)) return 349;
	          else return 397;
	          }
	         else
	          {
	          if (poly[268].contains(lat,lng)) return 7;
	          if (poly[269].contains(lat,lng)) return 397;
	          else return 349;
	          }
	      else
	       if (lng < -65.370701)
	        {
	        if (poly[270].contains(lat,lng)) return 390;
	        else return 397;
	        }
	       else
	        if (lat < 4.837125)
	         if (lng < -63.852720)
	          {
	          if (poly[271].contains(lat,lng)) return 397;
	          else return 349;
	          }
	         else
	          {
	          if (poly[272].contains(lat,lng)) return 397;
	          else return 349;
	          }
	        else
	         return 397;
	    else
	     if (lat < 4.950161)
	      if (lat < -0.489444)
	       if (lng < -61.116394)
	        {
	        if (poly[273].contains(lat,lng)) return 7;
	        else return 349;
	        }
	       else
	        {
	        if (poly[274].contains(lat,lng)) return 7;
	        else return 349;
	        }
	      else
	       {
	       if (poly[275].contains(lat,lng)) return 349;
	       else return 397;
	       }
	     else
	      {
	      if (poly[276].contains(lat,lng)) return 298;
	      if (poly[277].contains(lat,lng)) return 349;
	      else return 397;
	      }
	   else
	    if (lat < 3.459039)
	     if (lat < 0.229444)
	      {
	      if (poly[278].contains(lat,lng)) return 7;
	      else return 349;
	      }
	     else
	      if (lat < 3.349273)
	       {
	       if (poly[279].contains(lat,lng)) return 349;
	       else return 298;
	       }
	      else
	       {
	       if (poly[280].contains(lat,lng)) return 298;
	       else return 349;
	       }
	    else
	     if (lat < 3.485937)
	      {
	      if (poly[281].contains(lat,lng)) return 298;
	      else return 349;
	      }
	     else
	      if (lat < 3.608901)
	       {
	       if (poly[282].contains(lat,lng)) return 298;
	       else return 349;
	       }
	      else
	       {
	       if (poly[283].contains(lat,lng)) return 298;
	       else return 349;
	       }
	 else
	  if (lng < -56.305687)
	   if (lat < 2.304270)
	    if (lat < -2.231925)
	     if (lat < -8.698259)
	      {
	      if (poly[284].contains(lat,lng)) return 7;
	      if (poly[285].contains(lat,lng)) return 310;
	      else return 363;
	      }
	     else
	      if (lat < -5.465092)
	       if (lng < -58.062149)
	        {
	        if (poly[286].contains(lat,lng)) return 310;
	        if (poly[287].contains(lat,lng)) return 363;
	        else return 7;
	        }
	       else
	        {
	        if (poly[288].contains(lat,lng)) return 7;
	        if (poly[289].contains(lat,lng)) return 363;
	        else return 310;
	        }
	      else
	       {
	       if (poly[290].contains(lat,lng)) return 7;
	       else return 310;
	       }
	    else
	     if (lat < 0.036172)
	      {
	      if (poly[291].contains(lat,lng)) return 310;
	      else return 7;
	      }
	     else
	      if (lng < -58.062149)
	       {
	       if (poly[292].contains(lat,lng)) return 7;
	       if (poly[293].contains(lat,lng)) return 310;
	       if (poly[294].contains(lat,lng)) return 349;
	       else return 298;
	       }
	      else
	       {
	       if (poly[295].contains(lat,lng)) return 200;
	       if (poly[296].contains(lat,lng)) return 310;
	       else return 298;
	       }
	   else
	    if (lng < -59.516312)
	     if (lat < 3.472700)
	      if (lat < 3.427237)
	       {
	       if (poly[297].contains(lat,lng)) return 349;
	       else return 298;
	       }
	      else
	       {
	       if (poly[298].contains(lat,lng)) return 349;
	       else return 298;
	       }
	     else
	      if (lat < 3.512545)
	       {
	       if (poly[299].contains(lat,lng)) return 349;
	       else return 298;
	       }
	      else
	       if (lat < 3.939903)
	        {
	        if (poly[300].contains(lat,lng)) return 349;
	        else return 298;
	        }
	       else
	        {
	        if (poly[301].contains(lat,lng)) return 349;
	        else return 298;
	        }
	    else
	     if (lat < 4.460028)
	      {
	      if (poly[302].contains(lat,lng)) return 200;
	      else return 298;
	      }
	     else
	      {
	      if (poly[303].contains(lat,lng)) return 200;
	      else return 298;
	      }
	  else
	   if (lat < -9.477882)
	    {
	    if (poly[304].contains(lat,lng)) return 310;
	    if (poly[305].contains(lat,lng)) return 363;
	    else return 400;
	    }
	   else
	    if (lat < -1.736668)
	     if (lat < -5.607275)
	      if (lng < -53.959818)
	       return 310;
	      else
	       if (lat < -7.542579)
	        {
	        if (poly[306].contains(lat,lng)) return 400;
	        else return 310;
	        }
	       else
	        {
	        if (poly[307].contains(lat,lng)) return 400;
	        else return 310;
	        }
	     else
	      {
	      if (poly[308].contains(lat,lng)) return 400;
	      else return 310;
	      }
	    else
	     if (lat < 2.133939)
	      if (lng < -53.959818)
	       {
	       if (poly[309].contains(lat,lng)) return 200;
	       if (poly[310].contains(lat,lng)) return 362;
	       if (poly[311].contains(lat,lng)) return 400;
	       else return 310;
	       }
	      else
	       if (lat < 0.198636)
	        {
	        if (poly[312].contains(lat,lng)) return 400;
	        else return 310;
	        }
	       else
	        {
	        if (poly[313].contains(lat,lng)) return 310;
	        else return 400;
	        }
	     else
	      if (lng < -53.959818)
	       if (lat < 4.069243)
	        if (lng < -55.132752)
	         {
	         if (poly[314].contains(lat,lng)) return 310;
	         else return 200;
	         }
	        else
	         if (lat < 3.101591)
	          if (lng < -54.546285)
	           {
	           if (poly[315].contains(lat,lng)) return 310;
	           if (poly[316].contains(lat,lng)) return 400;
	           else return 200;
	           }
	          else
	           {
	           if (poly[317].contains(lat,lng)) return 200;
	           if (poly[318].contains(lat,lng)) return 400;
	           if (poly[319].contains(lat,lng)) return 400;
	           else return 362;
	           }
	         else
	          {
	          if (poly[320].contains(lat,lng)) return 362;
	          else return 200;
	          }
	       else
	        {
	        if (poly[321].contains(lat,lng)) return 362;
	        else return 200;
	        }
	      else
	       {
	       if (poly[322].contains(lat,lng)) return 400;
	       else return 362;
	       }
	}

	private static int call5(double lat, double lng)
	{
	 if (lng < -7.365113)
	  if (lng < -12.454523)
	   if (lat < 8.580436)
	    return 68;
	   else
	    if (lat < 9.868772)
	     {
	     if (poly[323].contains(lat,lng)) return 184;
	     else return 68;
	     }
	    else
	     if (lat < 12.540651)
	      if (lng < -12.887652)
	       {
	       if (poly[324].contains(lat,lng)) return 225;
	       else return 184;
	       }
	      else
	       {
	       if (poly[325].contains(lat,lng)) return 225;
	       else return 184;
	       }
	     else
	      {
	      if (poly[326].contains(lat,lng)) return 33;
	      else return 225;
	      }
	  else
	   if (lat < 10.000000)
	    if (lng < -11.496679)
	     if (lat < 7.561782)
	      return 68;
	     else
	      {
	      if (poly[327].contains(lat,lng)) return 184;
	      else return 68;
	      }
	    else
	     if (lat < 5.327682)
	      {
	      if (poly[328].contains(lat,lng)) return 300;
	      else return 356;
	      }
	     else
	      if (lat < 7.663841)
	       if (lng < -9.430896)
	        {
	        if (poly[329].contains(lat,lng)) return 68;
	        if (poly[330].contains(lat,lng)) return 184;
	        else return 356;
	        }
	       else
	        if (lat < 6.495762)
	         {
	         if (poly[331].contains(lat,lng)) return 300;
	         else return 356;
	         }
	        else
	         if (lng < -8.398005)
	          {
	          if (poly[332].contains(lat,lng)) return 184;
	          if (poly[333].contains(lat,lng)) return 184;
	          if (poly[334].contains(lat,lng)) return 300;
	          if (poly[335].contains(lat,lng)) return 300;
	          else return 356;
	          }
	         else
	          {
	          if (poly[336].contains(lat,lng)) return 184;
	          if (poly[337].contains(lat,lng)) return 356;
	          else return 300;
	          }
	      else
	       if (lng < -9.430896)
	        if (lat < 8.831921)
	         if (lng < -10.463788)
	          {
	          if (poly[338].contains(lat,lng)) return 184;
	          if (poly[339].contains(lat,lng)) return 356;
	          else return 68;
	          }
	         else
	          {
	          if (poly[340].contains(lat,lng)) return 68;
	          if (poly[341].contains(lat,lng)) return 184;
	          if (poly[342].contains(lat,lng)) return 184;
	          else return 356;
	          }
	        else
	         {
	         if (poly[343].contains(lat,lng)) return 68;
	         else return 184;
	         }
	       else
	        if (lat < 8.831921)
	         if (lng < -8.398005)
	          {
	          if (poly[344].contains(lat,lng)) return 356;
	          if (poly[345].contains(lat,lng)) return 356;
	          if (poly[346].contains(lat,lng)) return 356;
	          else return 184;
	          }
	         else
	          {
	          if (poly[347].contains(lat,lng)) return 184;
	          else return 300;
	          }
	        else
	         {
	         if (poly[348].contains(lat,lng)) return 300;
	         else return 184;
	         }
	   else
	    if (lat < 10.486805)
	     if (lng < -7.646448)
	      {
	      if (poly[349].contains(lat,lng)) return 184;
	      if (poly[350].contains(lat,lng)) return 300;
	      else return 181;
	      }
	     else
	      if (lat < 10.362350)
	       {
	       if (poly[351].contains(lat,lng)) return 181;
	       else return 300;
	       }
	      else
	       {
	       if (poly[352].contains(lat,lng)) return 300;
	       else return 181;
	       }
	    else
	     if (lat < 15.945997)
	      if (lat < 13.216401)
	       if (lng < -9.909818)
	        if (lat < 11.851603)
	         return 184;
	        else
	         if (lng < -11.182171)
	          if (lat < 12.534002)
	           {
	           if (poly[353].contains(lat,lng)) return 181;
	           if (poly[354].contains(lat,lng)) return 225;
	           else return 184;
	           }
	          else
	           {
	           if (poly[355].contains(lat,lng)) return 225;
	           else return 181;
	           }
	         else
	          {
	          if (poly[356].contains(lat,lng)) return 184;
	          else return 181;
	          }
	       else
	        if (lat < 11.851603)
	         {
	         if (poly[357].contains(lat,lng)) return 184;
	         else return 181;
	         }
	        else
	         {
	         if (poly[358].contains(lat,lng)) return 184;
	         else return 181;
	         }
	      else
	       if (lng < -9.909818)
	        if (lat < 14.581199)
	         {
	         if (poly[359].contains(lat,lng)) return 225;
	         else return 181;
	         }
	        else
	         if (lng < -11.182171)
	          {
	          if (poly[360].contains(lat,lng)) return 33;
	          if (poly[361].contains(lat,lng)) return 225;
	          else return 181;
	          }
	         else
	          {
	          if (poly[362].contains(lat,lng)) return 181;
	          else return 33;
	          }
	       else
	        {
	        if (poly[363].contains(lat,lng)) return 181;
	        else return 33;
	        }
	     else
	      return 33;
	 else
	  if (lat < 1.701323)
	   if (lng < 5.635208)
	    if (lat < -8.678568)
	     return 156;
	    else
	     return 201;
	   else
	    return 161;
	  else
	   if (lat < 15.082593)
	    if (lng < -0.462076)
	     if (lat < 9.742030)
	      if (lng < -3.913594)
	       {
	       if (poly[364].contains(lat,lng)) return 262;
	       if (poly[365].contains(lat,lng)) return 262;
	       if (poly[366].contains(lat,lng)) return 262;
	       else return 300;
	       }
	      else
	       if (lat < 7.239377)
	        {
	        if (poly[367].contains(lat,lng)) return 300;
	        else return 70;
	        }
	       else
	        {
	        if (poly[368].contains(lat,lng)) return 70;
	        if (poly[369].contains(lat,lng)) return 262;
	        else return 300;
	        }
	     else
	      if (lng < -3.913594)
	       if (lat < 12.412311)
	        if (lng < -5.639354)
	         if (lat < 11.077171)
	          if (lng < -6.502234)
	           {
	           if (poly[370].contains(lat,lng)) return 300;
	           else return 181;
	           }
	          else
	           {
	           if (poly[371].contains(lat,lng)) return 181;
	           else return 300;
	           }
	         else
	          return 181;
	        else
	         if (lat < 11.077171)
	          if (lng < -4.776474)
	           {
	           if (poly[372].contains(lat,lng)) return 181;
	           if (poly[373].contains(lat,lng)) return 300;
	           else return 262;
	           }
	          else
	           {
	           if (poly[374].contains(lat,lng)) return 300;
	           if (poly[375].contains(lat,lng)) return 300;
	           else return 262;
	           }
	         else
	          {
	          if (poly[376].contains(lat,lng)) return 262;
	          else return 181;
	          }
	       else
	        {
	        if (poly[377].contains(lat,lng)) return 262;
	        else return 181;
	        }
	      else
	       if (lat < 12.412311)
	        if (lng < -2.187835)
	         {
	         if (poly[378].contains(lat,lng)) return 70;
	         if (poly[379].contains(lat,lng)) return 300;
	         else return 262;
	         }
	        else
	         {
	         if (poly[380].contains(lat,lng)) return 262;
	         else return 70;
	         }
	       else
	        {
	        if (poly[381].contains(lat,lng)) return 181;
	        else return 262;
	        }
	    else
	     if (lat < 4.509286)
	      return 96;
	     else
	      if (lng < 2.405395)
	       if (lat < 11.173301)
	        if (lng < -0.273275)
	         {
	         if (poly[382].contains(lat,lng)) return 262;
	         else return 70;
	         }
	        else
	         if (lat < 8.347418)
	          if (lat < 6.934477)
	           if (lng < 1.066060)
	            {
	            if (poly[383].contains(lat,lng)) return 272;
	            else return 70;
	            }
	           else
	            {
	            if (poly[384].contains(lat,lng)) return 70;
	            if (poly[385].contains(lat,lng)) return 272;
	            else return 84;
	            }
	          else
	           {
	           if (poly[386].contains(lat,lng)) return 70;
	           if (poly[387].contains(lat,lng)) return 84;
	           else return 272;
	           }
	         else
	          if (lat < 9.760359)
	           {
	           if (poly[388].contains(lat,lng)) return 70;
	           if (poly[389].contains(lat,lng)) return 84;
	           else return 272;
	           }
	          else
	           if (lng < 1.066060)
	            if (lat < 10.466830)
	             {
	             if (poly[390].contains(lat,lng)) return 70;
	             if (poly[391].contains(lat,lng)) return 84;
	             else return 272;
	             }
	            else
	             {
	             if (poly[392].contains(lat,lng)) return 70;
	             if (poly[393].contains(lat,lng)) return 84;
	             if (poly[394].contains(lat,lng)) return 84;
	             if (poly[395].contains(lat,lng)) return 262;
	             else return 272;
	             }
	           else
	            {
	            if (poly[396].contains(lat,lng)) return 262;
	            if (poly[397].contains(lat,lng)) return 262;
	            if (poly[398].contains(lat,lng)) return 272;
	            else return 84;
	            }
	       else
	        if (lat < 13.127947)
	         if (lng < 0.971660)
	          return 262;
	         else
	          if (lat < 12.150624)
	           {
	           if (poly[399].contains(lat,lng)) return 260;
	           if (poly[400].contains(lat,lng)) return 262;
	           else return 84;
	           }
	          else
	           {
	           if (poly[401].contains(lat,lng)) return 84;
	           if (poly[402].contains(lat,lng)) return 262;
	           else return 260;
	           }
	        else
	         {
	         if (poly[403].contains(lat,lng)) return 181;
	         if (poly[404].contains(lat,lng)) return 262;
	         else return 260;
	         }
	      else
	       if (lat < 9.795939)
	        {
	        if (poly[405].contains(lat,lng)) return 84;
	        else return 96;
	        }
	       else
	        if (lat < 12.439266)
	         if (lng < 5.019920)
	          if (lat < 11.117603)
	           {
	           if (poly[406].contains(lat,lng)) return 96;
	           else return 84;
	           }
	          else
	           {
	           if (poly[407].contains(lat,lng)) return 84;
	           if (poly[408].contains(lat,lng)) return 96;
	           else return 260;
	           }
	         else
	          return 96;
	        else
	         {
	         if (poly[409].contains(lat,lng)) return 96;
	         else return 260;
	         }
	   else
	    if (lng < -5.327115)
	     if (lat < 16.333138)
	      {
	      if (poly[410].contains(lat,lng)) return 33;
	      else return 181;
	      }
	     else
	      {
	      if (poly[411].contains(lat,lng)) return 181;
	      else return 33;
	      }
	    else
	     if (lng < 1.153665)
	      {
	      if (poly[412].contains(lat,lng)) return 260;
	      if (poly[413].contains(lat,lng)) return 380;
	      else return 181;
	      }
	     else
	      if (lng < 4.394054)
	       if (lat < 18.243891)
	        {
	        if (poly[414].contains(lat,lng)) return 181;
	        else return 260;
	        }
	       else
	        {
	        if (poly[415].contains(lat,lng)) return 260;
	        if (poly[416].contains(lat,lng)) return 380;
	        else return 181;
	        }
	      else
	       {
	       if (poly[417].contains(lat,lng)) return 380;
	       else return 260;
	       }
	}

	private static int call6(double lat, double lng)
	{
	 if (lat < 9.581140)
	  if (lat < 6.615787)
	   if (lng < -50.743057)
	    if (lng < -51.613949)
	     if (lat < -15.164592)
	      if (lng < -68.172417)
	       if (lat < -21.296675)
	        {
	        if (poly[418].contains(lat,lng)) return 190;
	        else return 31;
	        }
	       else
	        if (lng < -71.325500)
	         return 164;
	        else
	         if (lat < -18.230633)
	          {
	          if (poly[419].contains(lat,lng)) return 164;
	          if (poly[420].contains(lat,lng)) return 190;
	          else return 31;
	          }
	         else
	          if (lng < -69.748959)
	           {
	           if (poly[421].contains(lat,lng)) return 31;
	           if (poly[422].contains(lat,lng)) return 31;
	           else return 164;
	           }
	          else
	           if (lat < -16.697612)
	            {
	            if (poly[423].contains(lat,lng)) return 31;
	            if (poly[424].contains(lat,lng)) return 164;
	            if (poly[425].contains(lat,lng)) return 164;
	            else return 190;
	            }
	           else
	            {
	            if (poly[426].contains(lat,lng)) return 190;
	            else return 164;
	            }
	      else
	       if (lng < -57.458096)
	        if (lat < -18.228182)
	         if (lng < -62.815256)
	          return 190;
	         else
	          if (lng < -60.136676)
	           {
	           if (poly[427].contains(lat,lng)) return 219;
	           else return 190;
	           }
	          else
	           if (lat < -19.914080)
	            {
	            if (poly[428].contains(lat,lng)) return 190;
	            if (poly[429].contains(lat,lng)) return 219;
	            else return 317;
	            }
	           else
	            {
	            if (poly[430].contains(lat,lng)) return 219;
	            if (poly[431].contains(lat,lng)) return 317;
	            else return 190;
	            }
	        else
	         if (lng < -62.815256)
	          return 190;
	         else
	          if (lng < -60.136676)
	           {
	           if (poly[432].contains(lat,lng)) return 363;
	           else return 190;
	           }
	          else
	           if (lat < -16.696387)
	            {
	            if (poly[433].contains(lat,lng)) return 317;
	            if (poly[434].contains(lat,lng)) return 363;
	            else return 190;
	            }
	           else
	            {
	            if (poly[435].contains(lat,lng)) return 190;
	            else return 363;
	            }
	       else
	        if (lat < -20.688748)
	         {
	         if (poly[436].contains(lat,lng)) return 329;
	         else return 317;
	         }
	        else
	         if (lng < -54.536022)
	          {
	          if (poly[437].contains(lat,lng)) return 363;
	          else return 317;
	          }
	         else
	          if (lat < -17.926670)
	           {
	           if (poly[438].contains(lat,lng)) return 329;
	           if (poly[439].contains(lat,lng)) return 363;
	           else return 317;
	           }
	          else
	           if (lng < -53.074986)
	            {
	            if (poly[440].contains(lat,lng)) return 317;
	            if (poly[441].contains(lat,lng)) return 329;
	            else return 363;
	            }
	           else
	            {
	            if (poly[442].contains(lat,lng)) return 363;
	            else return 329;
	            }
	     else
	      return call4(lat,lng);
	    else
	     if (lat < -9.768946)
	      if (lat < -19.135495)
	       {
	       if (poly[443].contains(lat,lng)) return 317;
	       else return 329;
	       }
	      else
	       if (lat < -13.521481)
	        {
	        if (poly[444].contains(lat,lng)) return 363;
	        else return 329;
	        }
	       else
	        {
	        if (poly[445].contains(lat,lng)) return 400;
	        else return 363;
	        }
	     else
	      return 400;
	   else
	    if (lat < -8.837408)
	     if (lat < -15.218693)
	      return 329;
	     else
	      if (lat < -12.028050)
	       {
	       if (poly[446].contains(lat,lng)) return 329;
	       if (poly[447].contains(lat,lng)) return 363;
	       else return 61;
	       }
	      else
	       {
	       if (poly[448].contains(lat,lng)) return 363;
	       if (poly[449].contains(lat,lng)) return 400;
	       else return 61;
	       }
	    else
	     return 400;
	  else
	   if (lat < 8.682077)
	    if (lng < -59.803780)
	     if (lng < -60.922413)
	      if (lng < -69.807697)
	       if (lng < -72.143141)
	        {
	        if (poly[450].contains(lat,lng)) return 397;
	        else return 390;
	        }
	       else
	        {
	        if (poly[451].contains(lat,lng)) return 390;
	        else return 397;
	        }
	      else
	       if (lat < 6.736960)
	        {
	        if (poly[452].contains(lat,lng)) return 298;
	        else return 397;
	        }
	       else
	        return 397;
	     else
	      if (lat < 8.557567)
	       {
	       if (poly[453].contains(lat,lng)) return 298;
	       else return 397;
	       }
	      else
	       return 397;
	    else
	     return 298;
	   else
	    if (lng < -72.666618)
	     {
	     if (poly[454].contains(lat,lng)) return 397;
	     else return 390;
	     }
	    else
	     return 397;
	 else
	  if (lat < 14.392262)
	   if (lng < -64.505745)
	    if (lng < -68.192307)
	     if (lng < -71.116249)
	      if (lng < -71.592949)
	       if (lng < -71.624382)
	        if (lat < 11.022224)
	         {
	         if (poly[455].contains(lat,lng)) return 390;
	         else return 397;
	         }
	        else
	         {
	         if (poly[456].contains(lat,lng)) return 397;
	         else return 390;
	         }
	       else
	        if (lat < 11.013904)
	         return 397;
	        else
	         {
	         if (poly[457].contains(lat,lng)) return 397;
	         else return 390;
	         }
	      else
	       if (lat < 11.051531)
	        return 397;
	       else
	        {
	        if (poly[458].contains(lat,lng)) return 397;
	        else return 390;
	        }
	     else
	      if (lat < 10.845729)
	       return 397;
	      else
	       if (lng < -68.733948)
	        if (lat < 12.385672)
	         {
	         if (poly[459].contains(lat,lng)) return 91;
	         else return 397;
	         }
	        else
	         return 234;
	       else
	        if (lat < 11.998877)
	         if (lat < 11.664527)
	          return 91;
	         else
	          return 397;
	        else
	         return 138;
	    else
	     return 397;
	   else
	    if (lng < -62.224319)
	     return 397;
	    else
	     if (lat < 12.232976)
	      if (lat < 10.020413)
	       return 397;
	      else
	       if (lat < 10.840741)
	        if (lng < -61.843590)
	         if (lat < 10.095178)
	          return 168;
	         else
	          return 397;
	        else
	         return 168;
	       else
	        if (lng < -61.597908)
	         return 86;
	        else
	         return 168;
	     else
	      if (lng < -61.308281)
	       if (lat < 12.529215)
	        return 86;
	       else
	        return 2;
	      else
	       if (lng < -61.113880)
	        return 2;
	       else
	        if (lat < 13.516017)
	         return 228;
	        else
	         return 386;
	  else
	   if (lng < -64.559113)
	    if (lng < -67.837822)
	     if (lat < 19.951086)
	      if (lng < -72.786362)
	       return 19;
	      else
	       if (lat < 17.620874)
	        return 229;
	       else
	        if (lng < -70.836662)
	         if (lng < -71.613358)
	          if (lat < 19.201639)
	           {
	           if (poly[460].contains(lat,lng)) return 229;
	           else return 19;
	           }
	          else
	           {
	           if (poly[461].contains(lat,lng)) return 229;
	           else return 19;
	           }
	         else
	          return 229;
	        else
	         if (lng < -68.320000)
	          return 229;
	         else
	          return 393;
	     else
	      if (lng < -72.619797)
	       if (lng < -74.131775)
	        return 376;
	       else
	        if (lat < 20.501944)
	         return 19;
	        else
	         return 281;
	      else
	       return 103;
	    else
	     if (lng < -65.242737)
	      return 393;
	     else
	      if (lng < -64.833633)
	       return 338;
	      else
	       if (lat < 18.379963)
	        if (lat < 17.795403)
	         return 338;
	        else
	         if (lng < -64.662430)
	          return 338;
	         else
	          return 301;
	       else
	        return 301;
	   else
	    if (lng < -62.932041)
	     if (lng < -64.268768)
	      return 301;
	     else
	      if (lat < 18.130697)
	       if (lat < 17.656101)
	        return 138;
	       else
	        if (lng < -63.012993)
	         {
	         if (poly[462].contains(lat,lng)) return 354;
	         else return 394;
	         }
	        else
	         return 277;
	      else
	       return 176;
	    else
	     if (lng < -61.546261)
	      if (lat < 16.368206)
	       return 277;
	      else
	       if (lng < -62.146420)
	        if (lat < 17.209156)
	         if (lat < 16.956336)
	          return 80;
	         else
	          return 342;
	        else
	         if (lat < 17.652030)
	          return 173;
	         else
	          return 342;
	       else
	        return 204;
	     else
	      if (lat < 15.631809)
	       if (lng < -61.244152)
	        return 1;
	       else
	        return 328;
	      else
	       return 277;
	}

	private static int call7(double lat, double lng)
	{
	 if (lng < -34.790123)
	  if (lng < -45.408802)
	   if (lat < -1.049945)
	    if (lat < -1.595448)
	     if (lat < -1.748470)
	      if (lat < -2.001044)
	       if (lat < -5.164167)
	        if (lat < -13.382072)
	         {
	         if (poly[463].contains(lat,lng)) return 329;
	         else return 171;
	         }
	        else
	         if (lat < -9.273120)
	          if (lng < -47.497873)
	           {
	           if (poly[464].contains(lat,lng)) return 329;
	           else return 61;
	           }
	          else
	           if (lat < -11.327596)
	            {
	            if (poly[465].contains(lat,lng)) return 61;
	            if (poly[466].contains(lat,lng)) return 329;
	            else return 171;
	            }
	           else
	            {
	            if (poly[467].contains(lat,lng)) return 143;
	            if (poly[468].contains(lat,lng)) return 171;
	            else return 61;
	            }
	         else
	          if (lng < -47.497873)
	           if (lat < -7.218644)
	            {
	            if (poly[469].contains(lat,lng)) return 143;
	            if (poly[470].contains(lat,lng)) return 143;
	            if (poly[471].contains(lat,lng)) return 400;
	            else return 61;
	            }
	           else
	            if (lng < -48.542409)
	             {
	             if (poly[472].contains(lat,lng)) return 143;
	             if (poly[473].contains(lat,lng)) return 400;
	             else return 61;
	             }
	            else
	             {
	             if (poly[474].contains(lat,lng)) return 143;
	             if (poly[475].contains(lat,lng)) return 143;
	             if (poly[476].contains(lat,lng)) return 400;
	             if (poly[477].contains(lat,lng)) return 400;
	             else return 61;
	             }
	          else
	           if (lat < -7.218644)
	            {
	            if (poly[478].contains(lat,lng)) return 61;
	            if (poly[479].contains(lat,lng)) return 61;
	            else return 143;
	            }
	           else
	            {
	            if (poly[480].contains(lat,lng)) return 61;
	            else return 143;
	            }
	       else
	        if (lng < -49.284698)
	         return 400;
	        else
	         if (lng < -47.346750)
	          {
	          if (poly[481].contains(lat,lng)) return 143;
	          else return 400;
	          }
	         else
	          {
	          if (poly[482].contains(lat,lng)) return 400;
	          else return 143;
	          }
	      else
	       if (lng < -49.192532)
	        return 400;
	       else
	        {
	        if (poly[483].contains(lat,lng)) return 143;
	        else return 400;
	        }
	     else
	      if (lng < -48.887367)
	       return 400;
	      else
	       {
	       if (poly[484].contains(lat,lng)) return 143;
	       else return 400;
	       }
	    else
	     if (lng < -46.049419)
	      if (lng < -48.279411)
	       return 400;
	      else
	       if (lat < -1.159195)
	        {
	        if (poly[485].contains(lat,lng)) return 143;
	        else return 400;
	        }
	       else
	        return 400;
	     else
	      return 143;
	   else
	    return 400;
	  else
	   if (lat < -7.268889)
	    if (lat < -13.824779)
	     if (lng < -39.656792)
	      if (lat < -17.712378)
	       {
	       if (poly[486].contains(lat,lng)) return 171;
	       else return 329;
	       }
	      else
	       if (lng < -42.532797)
	        {
	        if (poly[487].contains(lat,lng)) return 329;
	        else return 171;
	        }
	       else
	        if (lat < -15.768578)
	         {
	         if (poly[488].contains(lat,lng)) return 329;
	         else return 171;
	         }
	        else
	         {
	         if (poly[489].contains(lat,lng)) return 329;
	         if (poly[490].contains(lat,lng)) return 329;
	         else return 171;
	         }
	     else
	      return 171;
	    else
	     if (lat < -12.650455)
	      return 171;
	     else
	      if (lng < -40.101651)
	       if (lat < -9.959672)
	        {
	        if (poly[491].contains(lat,lng)) return 143;
	        else return 171;
	        }
	       else
	        if (lng < -42.755226)
	         {
	         if (poly[492].contains(lat,lng)) return 171;
	         else return 143;
	         }
	        else
	         if (lat < -8.614281)
	          if (lng < -41.428438)
	           {
	           if (poly[493].contains(lat,lng)) return 171;
	           else return 143;
	           }
	          else
	           {
	           if (poly[494].contains(lat,lng)) return 143;
	           if (poly[495].contains(lat,lng)) return 191;
	           else return 171;
	           }
	         else
	          {
	          if (poly[496].contains(lat,lng)) return 191;
	          else return 143;
	          }
	      else
	       if (lat < -9.959672)
	        {
	        if (poly[497].contains(lat,lng)) return 325;
	        else return 171;
	        }
	       else
	        if (lng < -37.448075)
	         if (lat < -8.614281)
	          if (lng < -38.774863)
	           {
	           if (poly[498].contains(lat,lng)) return 191;
	           if (poly[499].contains(lat,lng)) return 191;
	           else return 171;
	           }
	          else
	           if (lat < -9.286977)
	            {
	            if (poly[500].contains(lat,lng)) return 191;
	            if (poly[501].contains(lat,lng)) return 325;
	            else return 171;
	            }
	           else
	            {
	            if (poly[502].contains(lat,lng)) return 171;
	            if (poly[503].contains(lat,lng)) return 325;
	            else return 191;
	            }
	         else
	          {
	          if (poly[504].contains(lat,lng)) return 143;
	          if (poly[505].contains(lat,lng)) return 171;
	          else return 191;
	          }
	        else
	         if (lat < -8.614281)
	          {
	          if (poly[506].contains(lat,lng)) return 191;
	          else return 325;
	          }
	         else
	          if (lng < -36.121287)
	           {
	           if (poly[507].contains(lat,lng)) return 191;
	           else return 143;
	           }
	          else
	           {
	           if (poly[508].contains(lat,lng)) return 143;
	           else return 191;
	           }
	   else
	    return 143;
	 else
	  if (lng < -15.471467)
	   if (lat < 11.734634)
	    if (lng < -32.382004)
	     return 295;
	    else
	     return 38;
	   else
	    if (lng < -22.669443)
	     return 353;
	    else
	     if (lat < 13.828421)
	      if (lat < 11.974076)
	       return 38;
	      else
	       if (lat < 13.626443)
	        if (lat < 12.513857)
	         {
	         if (poly[509].contains(lat,lng)) return 225;
	         else return 38;
	         }
	        else
	         if (lat < 13.387020)
	          if (lng < -15.510426)
	           {
	           if (poly[510].contains(lat,lng)) return 252;
	           if (poly[511].contains(lat,lng)) return 252;
	           else return 225;
	           }
	          else
	           {
	           if (poly[512].contains(lat,lng)) return 252;
	           else return 225;
	           }
	         else
	          if (lat < 13.488581)
	           return 252;
	          else
	           {
	           if (poly[513].contains(lat,lng)) return 225;
	           else return 252;
	           }
	       else
	        return 225;
	     else
	      if (lat < 19.866785)
	       if (lat < 16.595074)
	        if (lng < -17.465607)
	         return 225;
	        else
	         {
	         if (poly[514].contains(lat,lng)) return 33;
	         else return 225;
	         }
	       else
	        return 33;
	      else
	       if (lat < 20.641302)
	        return 33;
	       else
	        {
	        if (poly[515].contains(lat,lng)) return 29;
	        else return 33;
	        }
	  else
	   if (lng < -13.001760)
	    if (lat < 9.920511)
	     if (lat < 9.104911)
	      if (lat < 8.139237)
	       if (lng < -14.295186)
	        return 156;
	       else
	        return 68;
	      else
	       if (lat < 8.912874)
	        return 68;
	       else
	        {
	        if (poly[516].contains(lat,lng)) return 184;
	        else return 68;
	        }
	     else
	      return 184;
	    else
	     if (lat < 11.075797)
	      if (lng < -15.169497)
	       return 38;
	      else
	       if (lng < -14.918398)
	        if (lat < 10.889210)
	         return 184;
	        else
	         if (lat < 11.046461)
	          {
	          if (poly[517].contains(lat,lng)) return 38;
	          else return 184;
	          }
	         else
	          {
	          if (poly[518].contains(lat,lng)) return 184;
	          else return 38;
	          }
	       else
	        return 184;
	     else
	      if (lat < 12.680789)
	       if (lng < -15.179247)
	        if (lat < 11.882746)
	         return 38;
	        else
	         {
	         if (poly[519].contains(lat,lng)) return 225;
	         else return 38;
	         }
	       else
	        if (lng < -14.090504)
	         {
	         if (poly[520].contains(lat,lng)) return 184;
	         if (poly[521].contains(lat,lng)) return 225;
	         else return 38;
	         }
	        else
	         {
	         if (poly[522].contains(lat,lng)) return 38;
	         if (poly[523].contains(lat,lng)) return 225;
	         else return 184;
	         }
	      else
	       if (lat < 16.691633)
	        if (lat < 13.826571)
	         {
	         if (poly[524].contains(lat,lng)) return 252;
	         else return 225;
	         }
	        else
	         if (lng < -14.983164)
	          {
	          if (poly[525].contains(lat,lng)) return 33;
	          else return 225;
	          }
	         else
	          if (lat < 15.259102)
	           return 225;
	          else
	           if (lng < -13.992462)
	            {
	            if (poly[526].contains(lat,lng)) return 33;
	            else return 225;
	            }
	           else
	            {
	            if (poly[527].contains(lat,lng)) return 225;
	            else return 33;
	            }
	       else
	        if (lat < 21.330809)
	         {
	         if (poly[528].contains(lat,lng)) return 29;
	         else return 33;
	         }
	        else
	         {
	         if (poly[529].contains(lat,lng)) return 33;
	         else return 29;
	         }
	   else
	    return call5(lat,lng);
	}

	private static int call8(double lat, double lng)
	{
	 if (lat < 46.038212)
	  if (lng < -111.043617)
	   if (lat < 37.004261)
	    if (lng < -114.047243)
	     {
	     if (poly[530].contains(lat,lng)) return 9;
	     else return 39;
	     }
	    else
	     if (lng < -112.545430)
	      return 9;
	     else
	      if (lat < 35.542513)
	       {
	       if (poly[531].contains(lat,lng)) return 273;
	       else return 9;
	       }
	      else
	       if (lng < -111.794524)
	        {
	        if (poly[532].contains(lat,lng)) return 273;
	        if (poly[533].contains(lat,lng)) return 273;
	        else return 9;
	        }
	       else
	        if (lat < 36.273387)
	         {
	         if (poly[534].contains(lat,lng)) return 9;
	         if (poly[535].contains(lat,lng)) return 9;
	         else return 273;
	         }
	        else
	         {
	         if (poly[536].contains(lat,lng)) return 9;
	         if (poly[537].contains(lat,lng)) return 9;
	         if (poly[538].contains(lat,lng)) return 9;
	         else return 273;
	         }
	   else
	    if (lng < -116.231369)
	     if (lat < 41.521236)
	      return 39;
	     else
	      if (lng < -118.825245)
	       return 39;
	      else
	       if (lat < 43.779724)
	        {
	        if (poly[539].contains(lat,lng)) return 27;
	        else return 39;
	        }
	       else
	        if (lng < -117.528307)
	         {
	         if (poly[540].contains(lat,lng)) return 27;
	         else return 39;
	         }
	        else
	         {
	         if (poly[541].contains(lat,lng)) return 27;
	         else return 39;
	         }
	    else
	     if (lat < 41.521236)
	      {
	      if (poly[542].contains(lat,lng)) return 39;
	      else return 273;
	      }
	     else
	      if (lng < -113.637493)
	       if (lat < 43.779724)
	        {
	        if (poly[543].contains(lat,lng)) return 39;
	        else return 27;
	        }
	       else
	        if (lng < -114.934431)
	         {
	         if (poly[544].contains(lat,lng)) return 39;
	         else return 27;
	         }
	        else
	         if (lat < 44.908968)
	          return 27;
	         else
	          if (lng < -114.285962)
	           {
	           if (poly[545].contains(lat,lng)) return 27;
	           if (poly[546].contains(lat,lng)) return 273;
	           else return 39;
	           }
	          else
	           {
	           if (poly[547].contains(lat,lng)) return 273;
	           else return 27;
	           }
	      else
	       if (lat < 43.779724)
	        {
	        if (poly[548].contains(lat,lng)) return 27;
	        else return 273;
	        }
	       else
	        {
	        if (poly[549].contains(lat,lng)) return 27;
	        else return 273;
	        }
	  else
	   if (lng < -109.045761)
	    if (lat < 35.257645)
	     if (lng < -109.624168)
	      {
	      if (poly[550].contains(lat,lng)) return 273;
	      else return 9;
	      }
	     else
	      if (lat < 35.165997)
	       {
	       if (poly[551].contains(lat,lng)) return 273;
	       else return 9;
	       }
	      else
	       {
	       if (poly[552].contains(lat,lng)) return 9;
	       else return 273;
	       }
	    else
	     if (lat < 40.647928)
	      if (lat < 37.952786)
	       if (lat < 36.605216)
	        if (lng < -110.044689)
	         if (lat < 35.931430)
	          {
	          if (poly[553].contains(lat,lng)) return 9;
	          if (poly[554].contains(lat,lng)) return 9;
	          else return 273;
	          }
	         else
	          {
	          if (poly[555].contains(lat,lng)) return 9;
	          else return 273;
	          }
	        else
	         {
	         if (poly[556].contains(lat,lng)) return 9;
	         if (poly[557].contains(lat,lng)) return 9;
	         else return 273;
	         }
	       else
	        return 273;
	      else
	       return 273;
	     else
	      return 273;
	   else
	    if (lng < -101.186989)
	     {
	     if (poly[558].contains(lat,lng)) return 273;
	     else return 160;
	     }
	    else
	     if (lat < 40.059488)
	      return 160;
	     else
	      if (lng < -97.257603)
	       if (lat < 43.048850)
	        {
	        if (poly[559].contains(lat,lng)) return 273;
	        else return 160;
	        }
	       else
	        {
	        if (poly[560].contains(lat,lng)) return 273;
	        else return 160;
	        }
	      else
	       return 160;
	 else
	  if (lng < -108.818900)
	   if (lat < 53.016500)
	    if (lng < -114.325294)
	     if (lat < 46.641286)
	      if (lng < -114.595726)
	       {
	       if (poly[561].contains(lat,lng)) return 273;
	       else return 39;
	       }
	      else
	       {
	       if (poly[562].contains(lat,lng)) return 273;
	       if (poly[563].contains(lat,lng)) return 273;
	       else return 39;
	       }
	     else
	      if (lng < -114.616200)
	       if (lng < -118.017661)
	        {
	        if (poly[564].contains(lat,lng)) return 39;
	        if (poly[565].contains(lat,lng)) return 227;
	        else return 45;
	        }
	       else
	        if (lat < 49.828893)
	         if (lng < -116.316931)
	          {
	          if (poly[566].contains(lat,lng)) return 39;
	          if (poly[567].contains(lat,lng)) return 45;
	          if (poly[568].contains(lat,lng)) return 227;
	          else return 253;
	          }
	         else
	          if (lat < 48.235090)
	           {
	           if (poly[569].contains(lat,lng)) return 273;
	           else return 39;
	           }
	          else
	           {
	           if (poly[570].contains(lat,lng)) return 39;
	           if (poly[571].contains(lat,lng)) return 253;
	           if (poly[572].contains(lat,lng)) return 273;
	           else return 227;
	           }
	        else
	         {
	         if (poly[573].contains(lat,lng)) return 45;
	         else return 227;
	         }
	      else
	       if (lat < 46.669704)
	        if (lng < -114.500009)
	         {
	         if (poly[574].contains(lat,lng)) return 39;
	         else return 273;
	         }
	        else
	         if (lng < -114.370506)
	          {
	          if (poly[575].contains(lat,lng)) return 39;
	          else return 273;
	          }
	         else
	          {
	          if (poly[576].contains(lat,lng)) return 273;
	          else return 39;
	          }
	       else
	        {
	        if (poly[577].contains(lat,lng)) return 273;
	        else return 227;
	        }
	    else
	     {
	     if (poly[578].contains(lat,lng)) return 273;
	     if (poly[579].contains(lat,lng)) return 308;
	     else return 227;
	     }
	   else
	    if (lng < -120.159308)
	     {
	     if (poly[580].contains(lat,lng)) return 116;
	     else return 45;
	     }
	    else
	     if (lng < -118.646458)
	      {
	      if (poly[581].contains(lat,lng)) return 116;
	      if (poly[582].contains(lat,lng)) return 227;
	      else return 45;
	      }
	     else
	      {
	      if (poly[583].contains(lat,lng)) return 308;
	      else return 227;
	      }
	  else
	   if (lat < 47.575298)
	    if (lng < -102.432684)
	     if (lng < -103.434273)
	      {
	      if (poly[584].contains(lat,lng)) return 160;
	      else return 273;
	      }
	     else
	      if (lng < -103.093439)
	       {
	       if (poly[585].contains(lat,lng)) return 160;
	       else return 273;
	       }
	      else
	       {
	       if (poly[586].contains(lat,lng)) return 160;
	       else return 273;
	       }
	    else
	     if (lng < -97.880450)
	      if (lng < -100.156567)
	       if (lng < -101.294626)
	        if (lat < 46.806755)
	         {
	         if (poly[587].contains(lat,lng)) return 52;
	         if (poly[588].contains(lat,lng)) return 160;
	         else return 273;
	         }
	        else
	         {
	         if (poly[589].contains(lat,lng)) return 52;
	         if (poly[590].contains(lat,lng)) return 105;
	         if (poly[591].contains(lat,lng)) return 160;
	         if (poly[592].contains(lat,lng)) return 273;
	         else return 71;
	         }
	       else
	        if (lat < 46.806755)
	         {
	         if (poly[593].contains(lat,lng)) return 52;
	         if (poly[594].contains(lat,lng)) return 273;
	         else return 160;
	         }
	        else
	         {
	         if (poly[595].contains(lat,lng)) return 52;
	         if (poly[596].contains(lat,lng)) return 71;
	         if (poly[597].contains(lat,lng)) return 105;
	         else return 160;
	         }
	      else
	       return 160;
	     else
	      return 160;
	   else
	    if (lng < -103.610133)
	     if (lat < 48.997610)
	      {
	      if (poly[598].contains(lat,lng)) return 160;
	      if (poly[599].contains(lat,lng)) return 308;
	      else return 273;
	      }
	     else
	      {
	      if (poly[600].contains(lat,lng)) return 74;
	      else return 308;
	      }
	    else
	     if (lat < 47.674011)
	      if (lng < -102.711964)
	       {
	       if (poly[601].contains(lat,lng)) return 273;
	       else return 160;
	       }
	      else
	       {
	       if (poly[602].contains(lat,lng)) return 273;
	       else return 160;
	       }
	     else
	      {
	      if (poly[603].contains(lat,lng)) return 160;
	      if (poly[604].contains(lat,lng)) return 308;
	      if (poly[605].contains(lat,lng)) return 378;
	      else return 284;
	      }
	}

	private static int call9(double lat, double lng)
	{
	 if (lng < -105.911491)
	  if (lng < -111.455482)
	   if (lat < 27.946997)
	    return 407;
	   else
	    if (lng < -114.366554)
	     return 367;
	    else
	     if (lng < -112.753685)
	      if (lat < 28.002499)
	       {
	       if (poly[606].contains(lat,lng)) return 367;
	       else return 407;
	       }
	      else
	       return 367;
	     else
	      return 402;
	  else
	   if (lng < -109.395576)
	    if (lat < 26.645967)
	     return 407;
	    else
	     return 402;
	   else
	    if (lat < 25.112150)
	     if (lng < -108.038940)
	      return 407;
	     else
	      if (lat < 21.819218)
	       return 407;
	      else
	       {
	       if (poly[607].contains(lat,lng)) return 384;
	       else return 407;
	       }
	    else
	     if (lng < -108.467506)
	      if (lat < 25.528921)
	       return 407;
	      else
	       if (lat < 26.922329)
	        {
	        if (poly[608].contains(lat,lng)) return 402;
	        else return 407;
	        }
	       else
	        {
	        if (poly[609].contains(lat,lng)) return 402;
	        else return 185;
	        }
	     else
	      if (lng < -108.392059)
	       if (lat < 25.175968)
	        return 407;
	       else
	        {
	        if (poly[610].contains(lat,lng)) return 185;
	        else return 407;
	        }
	      else
	       if (lng < -108.243614)
	        if (lat < 25.280951)
	         return 407;
	        else
	         {
	         if (poly[611].contains(lat,lng)) return 185;
	         else return 407;
	         }
	       else
	        {
	        if (poly[612].contains(lat,lng)) return 384;
	        if (poly[613].contains(lat,lng)) return 407;
	        else return 185;
	        }
	 else
	  if (lng < -97.113541)
	   if (lat < 25.335690)
	    if (lat < 24.757231)
	     if (lng < -101.525764)
	      if (lng < -102.506635)
	       if (lng < -104.209063)
	        {
	        if (poly[614].contains(lat,lng)) return 192;
	        if (poly[615].contains(lat,lng)) return 384;
	        else return 407;
	        }
	       else
	        {
	        if (poly[616].contains(lat,lng)) return 384;
	        if (poly[617].contains(lat,lng)) return 407;
	        else return 192;
	        }
	      else
	       {
	       if (poly[618].contains(lat,lng)) return 384;
	       else return 192;
	       }
	     else
	      if (lng < -97.606262)
	       if (lat < 24.653814)
	        if (lng < -97.731384)
	         if (lng < -99.628574)
	          {
	          if (poly[619].contains(lat,lng)) return 384;
	          else return 192;
	          }
	         else
	          {
	          if (poly[620].contains(lat,lng)) return 192;
	          else return 384;
	          }
	        else
	         if (lat < 22.034286)
	          return 192;
	         else
	          return 384;
	       else
	        if (lng < -101.085294)
	         {
	         if (poly[621].contains(lat,lng)) return 384;
	         else return 192;
	         }
	        else
	         return 384;
	      else
	       return 192;
	    else
	     if (lng < -101.189026)
	      if (lng < -101.582217)
	       {
	       if (poly[622].contains(lat,lng)) return 192;
	       else return 384;
	       }
	      else
	       {
	       if (poly[623].contains(lat,lng)) return 192;
	       else return 384;
	       }
	     else
	      return 384;
	   else
	    if (lng < -97.413445)
	     if (lat < 25.453518)
	      return 384;
	     else
	      if (lng < -97.475403)
	       if (lng < -103.288521)
	        if (lat < 27.700886)
	         {
	         if (poly[624].contains(lat,lng)) return 185;
	         else return 384;
	         }
	        else
	         if (lng < -104.600006)
	          {
	          if (poly[625].contains(lat,lng)) return 160;
	          if (poly[626].contains(lat,lng)) return 185;
	          else return 133;
	          }
	         else
	          if (lat < 28.824570)
	           {
	           if (poly[627].contains(lat,lng)) return 133;
	           if (poly[628].contains(lat,lng)) return 133;
	           if (poly[629].contains(lat,lng)) return 374;
	           if (poly[630].contains(lat,lng)) return 384;
	           else return 185;
	           }
	          else
	           {
	           if (poly[631].contains(lat,lng)) return 160;
	           if (poly[632].contains(lat,lng)) return 185;
	           if (poly[633].contains(lat,lng)) return 185;
	           if (poly[634].contains(lat,lng)) return 374;
	           else return 133;
	           }
	       else
	        if (lat < 26.857126)
	         if (lng < -100.381962)
	          return 384;
	         else
	          if (lng < -98.928682)
	           {
	           if (poly[635].contains(lat,lng)) return 160;
	           if (poly[636].contains(lat,lng)) return 384;
	           else return 374;
	           }
	          else
	           if (lng < -98.202043)
	            {
	            if (poly[637].contains(lat,lng)) return 160;
	            if (poly[638].contains(lat,lng)) return 384;
	            else return 374;
	            }
	           else
	            if (lat < 26.155322)
	             {
	             if (poly[639].contains(lat,lng)) return 160;
	             if (poly[640].contains(lat,lng)) return 384;
	             else return 374;
	             }
	            else
	             return 160;
	        else
	         if (lng < -99.285888)
	          if (lng < -101.287204)
	           if (lat < 28.402690)
	            return 384;
	           else
	            if (lng < -102.287863)
	             if (lat < 29.175472)
	              {
	              if (poly[641].contains(lat,lng)) return 160;
	              if (poly[642].contains(lat,lng)) return 384;
	              else return 374;
	              }
	             else
	              {
	              if (poly[643].contains(lat,lng)) return 160;
	              if (poly[644].contains(lat,lng)) return 384;
	              else return 374;
	              }
	            else
	             if (lat < 29.175472)
	              return 384;
	             else
	              if (lng < -101.787533)
	               {
	               if (poly[645].contains(lat,lng)) return 160;
	               if (poly[646].contains(lat,lng)) return 384;
	               else return 374;
	               }
	              else
	               {
	               if (poly[647].contains(lat,lng)) return 160;
	               if (poly[648].contains(lat,lng)) return 384;
	               else return 374;
	               }
	          else
	           if (lat < 28.402690)
	            if (lng < -100.286546)
	             {
	             if (poly[649].contains(lat,lng)) return 160;
	             if (poly[650].contains(lat,lng)) return 374;
	             else return 384;
	             }
	            else
	             if (lat < 27.629908)
	              {
	              if (poly[651].contains(lat,lng)) return 160;
	              if (poly[652].contains(lat,lng)) return 160;
	              if (poly[653].contains(lat,lng)) return 384;
	              else return 374;
	              }
	             else
	              {
	              if (poly[654].contains(lat,lng)) return 160;
	              if (poly[655].contains(lat,lng)) return 384;
	              else return 374;
	              }
	           else
	            {
	            if (poly[656].contains(lat,lng)) return 160;
	            if (poly[657].contains(lat,lng)) return 384;
	            else return 374;
	            }
	         else
	          return 160;
	      else
	       if (lat < 25.878584)
	        if (lat < 25.603125)
	         if (lng < -97.450340)
	          {
	          if (poly[658].contains(lat,lng)) return 374;
	          else return 384;
	          }
	         else
	          {
	          if (poly[659].contains(lat,lng)) return 374;
	          else return 384;
	          }
	        else
	         {
	         if (poly[660].contains(lat,lng)) return 160;
	         else return 374;
	         }
	       else
	        return 160;
	    else
	     if (lat < 25.959148)
	      if (lat < 25.643694)
	       {
	       if (poly[661].contains(lat,lng)) return 374;
	       else return 384;
	       }
	      else
	       {
	       if (poly[662].contains(lat,lng)) return 374;
	       else return 160;
	       }
	     else
	      return 160;
	  else
	   return 160;
	}

	private static int call10(double lat, double lng)
	{
	 if (lat < 44.717964)
	  if (lng < -87.921288)
	   return 160;
	  else
	   if (lng < -84.575844)
	    if (lat < 30.474203)
	     if (lng < -85.625534)
	      return 160;
	     else
	      if (lat < 29.760227)
	       return 165;
	      else
	       if (lng < -84.981323)
	        if (lat < 30.444105)
	         {
	         if (poly[663].contains(lat,lng)) return 165;
	         else return 160;
	         }
	        else
	         {
	         if (poly[664].contains(lat,lng)) return 165;
	         else return 160;
	         }
	       else
	        return 165;
	    else
	     if (lat < 38.929596)
	      if (lat < 36.997822)
	       if (lat < 33.736012)
	        {
	        if (poly[665].contains(lat,lng)) return 165;
	        else return 160;
	        }
	       else
	        if (lng < -86.248566)
	         return 160;
	        else
	         if (lat < 35.366917)
	          {
	          if (poly[666].contains(lat,lng)) return 165;
	          else return 160;
	          }
	         else
	          if (lng < -85.412205)
	           return 160;
	          else
	           if (lat < 36.182369)
	            {
	            if (poly[667].contains(lat,lng)) return 165;
	            else return 160;
	            }
	           else
	            {
	            if (poly[668].contains(lat,lng)) return 110;
	            if (poly[669].contains(lat,lng)) return 160;
	            else return 165;
	            }
	      else
	       if (lng < -87.072258)
	        if (lat < 37.963709)
	         return 160;
	        else
	         if (lat < 38.446652)
	          {
	          if (poly[670].contains(lat,lng)) return 54;
	          if (poly[671].contains(lat,lng)) return 309;
	          else return 160;
	          }
	         else
	          {
	          if (poly[672].contains(lat,lng)) return 54;
	          if (poly[673].contains(lat,lng)) return 89;
	          if (poly[674].contains(lat,lng)) return 160;
	          if (poly[675].contains(lat,lng)) return 160;
	          else return 309;
	          }
	       else
	        if (lng < -85.824051)
	         if (lat < 37.963688)
	          {
	          if (poly[676].contains(lat,lng)) return 90;
	          if (poly[677].contains(lat,lng)) return 130;
	          if (poly[678].contains(lat,lng)) return 165;
	          else return 160;
	          }
	         else
	          if (lng < -86.448154)
	           {
	           if (poly[679].contains(lat,lng)) return 89;
	           if (poly[680].contains(lat,lng)) return 160;
	           if (poly[681].contains(lat,lng)) return 160;
	           if (poly[682].contains(lat,lng)) return 165;
	           if (poly[683].contains(lat,lng)) return 165;
	           if (poly[684].contains(lat,lng)) return 309;
	           if (poly[685].contains(lat,lng)) return 388;
	           else return 90;
	           }
	          else
	           if (lat < 38.446592)
	            {
	            if (poly[686].contains(lat,lng)) return 89;
	            if (poly[687].contains(lat,lng)) return 90;
	            if (poly[688].contains(lat,lng)) return 130;
	            if (poly[689].contains(lat,lng)) return 160;
	            if (poly[690].contains(lat,lng)) return 388;
	            else return 165;
	            }
	           else
	            {
	            if (poly[691].contains(lat,lng)) return 130;
	            else return 89;
	            }
	        else
	         if (lat < 37.963709)
	          {
	          if (poly[692].contains(lat,lng)) return 160;
	          else return 165;
	          }
	         else
	          {
	          if (poly[693].contains(lat,lng)) return 89;
	          if (poly[694].contains(lat,lng)) return 130;
	          if (poly[695].contains(lat,lng)) return 391;
	          else return 165;
	          }
	     else
	      if (lng < -86.466347)
	       if (lat < 41.760455)
	        {
	        if (poly[696].contains(lat,lng)) return 89;
	        if (poly[697].contains(lat,lng)) return 248;
	        if (poly[698].contains(lat,lng)) return 257;
	        else return 160;
	        }
	       else
	        if (lng < -87.351361)
	         return 160;
	        else
	         return 93;
	      else
	       {
	       if (poly[699].contains(lat,lng)) return 89;
	       if (poly[700].contains(lat,lng)) return 165;
	       else return 93;
	       }
	   else
	    if (lat < 41.697075)
	     return 165;
	    else
	     if (lng < -83.325207)
	      if (lat < 41.733951)
	       if (lng < -83.446740)
	        {
	        if (poly[701].contains(lat,lng)) return 165;
	        else return 93;
	        }
	       else
	        return 165;
	      else
	       return 93;
	     else
	      if (lng < -82.407822)
	       if (lng < -83.173058)
	        return 93;
	       else
	        if (lat < 41.829945)
	         if (lng < -82.742973)
	          return 165;
	         else
	          return 238;
	        else
	         {
	         if (poly[702].contains(lat,lng)) return 238;
	         else return 93;
	         }
	      else
	       if (lat < 42.273876)
	        if (lng < -81.861286)
	         return 238;
	        else
	         return 165;
	       else
	        return 238;
	 else
	  if (lng < -86.163531)
	   if (lat < 47.476311)
	    if (lng < -90.438179)
	     return 160;
	    else
	     if (lng < -86.972198)
	      if (lat < 45.340703)
	       if (lng < -87.408328)
	        if (lat < 44.892063)
	         return 160;
	        else
	         if (lng < -87.715370)
	          {
	          if (poly[703].contains(lat,lng)) return 94;
	          if (poly[704].contains(lat,lng)) return 94;
	          if (poly[705].contains(lat,lng)) return 94;
	          else return 160;
	          }
	         else
	          if (lat < 45.154082)
	           {
	           if (poly[706].contains(lat,lng)) return 94;
	           else return 160;
	           }
	          else
	           {
	           if (poly[707].contains(lat,lng)) return 160;
	           else return 94;
	           }
	       else
	        return 160;
	      else
	       if (lat < 46.767605)
	        if (lng < -87.267632)
	         if (lng < -88.852905)
	          {
	          if (poly[708].contains(lat,lng)) return 93;
	          if (poly[709].contains(lat,lng)) return 160;
	          else return 94;
	          }
	         else
	          if (lng < -88.060268)
	           {
	           if (poly[710].contains(lat,lng)) return 93;
	           if (poly[711].contains(lat,lng)) return 160;
	           else return 94;
	           }
	          else
	           {
	           if (poly[712].contains(lat,lng)) return 93;
	           if (poly[713].contains(lat,lng)) return 93;
	           if (poly[714].contains(lat,lng)) return 160;
	           else return 94;
	           }
	        else
	         return 93;
	       else
	        if (lng < -90.385536)
	         return 160;
	        else
	         return 93;
	     else
	      if (lat < 45.424801)
	       return 160;
	      else
	       return 93;
	   else
	    if (lng < -88.234485)
	     if (lng < -88.783119)
	      if (lat < 48.175171)
	       if (lng < -89.307442)
	        if (lng < -89.505821)
	         if (lng < -91.087389)
	          {
	          if (poly[715].contains(lat,lng)) return 223;
	          else return 160;
	          }
	         else
	          {
	          if (poly[716].contains(lat,lng)) return 223;
	          if (poly[717].contains(lat,lng)) return 238;
	          else return 160;
	          }
	        else
	         return 238;
	       else
	        if (lat < 48.074462)
	         return 93;
	        else
	         return 238;
	      else
	       if (lng < -89.998705)
	        if (lat < 51.060221)
	         if (lng < -91.663461)
	          {
	          if (poly[718].contains(lat,lng)) return 160;
	          if (poly[719].contains(lat,lng)) return 223;
	          else return 284;
	          }
	         else
	          {
	          if (poly[720].contains(lat,lng)) return 160;
	          if (poly[721].contains(lat,lng)) return 223;
	          if (poly[722].contains(lat,lng)) return 284;
	          else return 238;
	          }
	        else
	         {
	         if (poly[723].contains(lat,lng)) return 238;
	         else return 284;
	         }
	       else
	        if (lat < 48.269608)
	         return 238;
	        else
	         if (lat < 48.528953)
	          if (lng < -89.151415)
	           {
	           if (poly[724].contains(lat,lng)) return 238;
	           else return 358;
	           }
	          else
	           return 238;
	         else
	          if (lat < 53.162661)
	           {
	           if (poly[725].contains(lat,lng)) return 284;
	           else return 238;
	           }
	          else
	           {
	           if (poly[726].contains(lat,lng)) return 238;
	           else return 284;
	           }
	     else
	      if (lng < -88.434196)
	       if (lat < 48.185020)
	        return 93;
	       else
	        if (lat < 48.613316)
	         return 238;
	        else
	         {
	         if (poly[727].contains(lat,lng)) return 284;
	         else return 238;
	         }
	      else
	       if (lat < 48.517834)
	        if (lng < -88.375648)
	         return 238;
	        else
	         return 93;
	       else
	        if (lat < 48.616454)
	         return 238;
	        else
	         {
	         if (poly[728].contains(lat,lng)) return 335;
	         else return 238;
	         }
	    else
	     return 238;
	  else
	   if (lng < -81.693253)
	    if (lat < 45.828533)
	     if (lng < -83.182182)
	      return 93;
	     else
	      return 238;
	    else
	     if (lng < -83.432907)
	      if (lat < 46.770527)
	       if (lng < -84.597824)
	        if (lng < -84.706245)
	         {
	         if (poly[729].contains(lat,lng)) return 238;
	         else return 93;
	         }
	        else
	         return 93;
	       else
	        if (lng < -84.298393)
	         if (lat < 45.987839)
	          return 93;
	         else
	          {
	          if (poly[730].contains(lat,lng)) return 238;
	          else return 93;
	          }
	        else
	         if (lat < 45.998745)
	          if (lng < -84.256340)
	           return 93;
	          else
	           {
	           if (poly[731].contains(lat,lng)) return 238;
	           else return 93;
	           }
	         else
	          {
	          if (poly[732].contains(lat,lng)) return 238;
	          else return 93;
	          }
	      else
	       return 238;
	     else
	      if (lat < 46.051628)
	       return 238;
	      else
	       if (lng < -82.247681)
	        return 238;
	       else
	        if (lng < -82.117599)
	         return 238;
	        else
	         if (lat < 52.882624)
	          return 137;
	         else
	          return 238;
	   else
	    if (lat < 46.096817)
	     return 238;
	    else
	     if (lat < 52.530595)
	      return 238;
	     else
	      return 137;
	}

	private static int call11(double lat, double lng)
	{
	 if (lat < 49.802902)
	  if (lng < -62.744881)
	   if (lat < 45.034157)
	    if (lng < -66.343018)
	     if (lng < -67.280861)
	      return 165;
	     else
	      if (lng < -66.820839)
	       if (lng < -66.954811)
	        {
	        if (poly[733].contains(lat,lng)) return 77;
	        if (poly[734].contains(lat,lng)) return 77;
	        else return 165;
	        }
	       else
	        return 77;
	      else
	       if (lng < -66.696220)
	        return 77;
	       else
	        return 118;
	    else
	     if (lat < 32.393833)
	      return 256;
	     else
	      return 118;
	   else
	    if (lat < 47.132732)
	     if (lng < -64.981148)
	      if (lng < -66.916946)
	       if (lng < -67.090355)
	        {
	        if (poly[735].contains(lat,lng)) return 77;
	        else return 165;
	        }
	       else
	        return 77;
	      else
	       if (lat < 45.251938)
	        if (lng < -65.740949)
	         return 77;
	        else
	         return 118;
	       else
	        return 77;
	     else
	      if (lat < 46.399082)
	       if (lng < -63.777946)
	        if (lat < 45.387333)
	         return 118;
	        else
	         if (lat < 46.279369)
	          if (lat < 46.006592)
	           {
	           if (poly[736].contains(lat,lng)) return 77;
	           else return 118;
	           }
	          else
	           return 77;
	         else
	          if (lng < -64.516182)
	           return 77;
	          else
	           return 118;
	       else
	        return 118;
	      else
	       if (lng < -64.613220)
	        return 77;
	       else
	        return 118;
	    else
	     if (lat < 47.824421)
	      return 77;
	     else
	      if (lat < 48.065315)
	       if (lng < -65.122518)
	        if (lng < -66.526045)
	         {
	         if (poly[737].contains(lat,lng)) return 77;
	         if (poly[738].contains(lat,lng)) return 77;
	         else return 20;
	         }
	        else
	         if (lng < -65.735591)
	          if (lng < -66.291616)
	           {
	           if (poly[739].contains(lat,lng)) return 20;
	           else return 77;
	           }
	          else
	           return 77;
	         else
	          return 20;
	       else
	        return 77;
	      else
	       return 20;
	  else
	   if (lng < -59.692562)
	    if (lat < 45.657059)
	     return 118;
	    else
	     if (lng < -61.605785)
	      if (lat < 47.599762)
	       return 118;
	      else
	       return 20;
	     else
	      if (lng < -61.394016)
	       return 118;
	      else
	       if (lng < -60.248380)
	        if (lat < 45.826886)
	         if (lng < -60.766365)
	          return 118;
	         else
	          {
	          if (poly[740].contains(lat,lng)) return 81;
	          else return 118;
	          }
	        else
	         {
	         if (poly[741].contains(lat,lng)) return 81;
	         if (poly[742].contains(lat,lng)) return 81;
	         else return 118;
	         }
	       else
	        if (lat < 46.274968)
	         return 81;
	        else
	         return 118;
	   else
	    if (lat < 47.146286)
	     if (lng < -56.166119)
	      return 92;
	     else
	      return 290;
	    else
	     return 290;
	 else
	  if (lng < -56.173176)
	   if (lat < 50.750843)
	    if (lng < -63.099380)
	     return 20;
	    else
	     if (lng < -59.396664)
	      if (lng < -60.008854)
	       if (lng < -60.734821)
	        if (lng < -61.582493)
	         if (lng < -63.049610)
	          return 20;
	         else
	          {
	          if (poly[743].contains(lat,lng)) return 243;
	          if (poly[744].contains(lat,lng)) return 243;
	          else return 20;
	          }
	        else
	         if (lat < 50.210049)
	          return 243;
	         else
	          {
	          if (poly[745].contains(lat,lng)) return 20;
	          else return 243;
	          }
	       else
	        if (lat < 50.259491)
	         return 243;
	        else
	         {
	         if (poly[746].contains(lat,lng)) return 20;
	         else return 243;
	         }
	      else
	       if (lat < 50.403294)
	        return 243;
	       else
	        if (lng < -59.584435)
	         {
	         if (poly[747].contains(lat,lng)) return 20;
	         else return 243;
	         }
	        else
	         if (lat < 50.581497)
	          return 243;
	         else
	          {
	          if (poly[748].contains(lat,lng)) return 20;
	          else return 243;
	          }
	     else
	      if (lng < -59.008266)
	       return 243;
	      else
	       return 290;
	   else
	    if (lng < -57.747738)
	     if (lat < 51.162445)
	      if (lng < -59.102900)
	       {
	       if (poly[749].contains(lat,lng)) return 20;
	       else return 243;
	       }
	      else
	       return 243;
	     else
	      if (lat < 51.296436)
	       if (lng < -58.864053)
	        {
	        if (poly[750].contains(lat,lng)) return 243;
	        else return 20;
	        }
	       else
	        return 243;
	      else
	       if (lat < 51.588661)
	        if (lng < -58.671885)
	         {
	         if (poly[751].contains(lat,lng)) return 243;
	         else return 20;
	         }
	        else
	         if (lat < 51.363617)
	          return 243;
	         else
	          {
	          if (poly[752].contains(lat,lng)) return 20;
	          else return 243;
	          }
	       else
	        if (lng < -62.787630)
	         if (lng < -65.307576)
	          if (lng < -66.567549)
	           {
	           if (poly[753].contains(lat,lng)) return 182;
	           else return 20;
	           }
	          else
	           if (lat < 52.766966)
	            {
	            if (poly[754].contains(lat,lng)) return 182;
	            else return 20;
	            }
	           else
	            {
	            if (poly[755].contains(lat,lng)) return 20;
	            else return 182;
	            }
	         else
	          if (lng < -64.047603)
	           {
	           if (poly[756].contains(lat,lng)) return 182;
	           if (poly[757].contains(lat,lng)) return 182;
	           else return 20;
	           }
	          else
	           {
	           if (poly[758].contains(lat,lng)) return 182;
	           else return 20;
	           }
	        else
	         {
	         if (poly[759].contains(lat,lng)) return 20;
	         else return 182;
	         }
	    else
	     if (lat < 53.630226)
	      if (lat < 51.175003)
	       return 290;
	      else
	       if (lng < -57.608196)
	        if (lat < 51.460896)
	         return 243;
	        else
	         if (lat < 51.589436)
	          {
	          if (poly[760].contains(lat,lng)) return 20;
	          else return 243;
	          }
	         else
	          {
	          if (poly[761].contains(lat,lng)) return 182;
	          else return 20;
	          }
	       else
	        if (lat < 51.506599)
	         if (lng < -57.116951)
	          return 243;
	         else
	          if (lng < -57.096817)
	           {
	           if (poly[762].contains(lat,lng)) return 290;
	           else return 243;
	           }
	          else
	           return 290;
	        else
	         if (lng < -56.478312)
	          if (lat < 53.616500)
	           {
	           if (poly[763].contains(lat,lng)) return 20;
	           if (poly[764].contains(lat,lng)) return 182;
	           if (poly[765].contains(lat,lng)) return 243;
	           else return 290;
	           }
	          else
	           return 182;
	         else
	          return 290;
	     else
	      return 182;
	  else
	   return 290;
	}

	private static int call12(double lat, double lng)
	{
	 if (lat < 48.926083)
	  if (lat < 43.791721)
	   if (lat < 38.159138)
	    if (lng < -13.417682)
	     if (lat < 29.283356)
	      if (lat < 27.131824)
	       return 29;
	      else
	       return 264;
	     else
	      if (lng < -16.466473)
	       if (lng < -25.015833)
	        return 283;
	       else
	        return 63;
	      else
	       if (lng < -15.857750)
	        return 63;
	       else
	        return 264;
	    else
	     if (lat < 35.953278)
	      if (lat < 34.190231)
	       if (lng < 0.701499)
	        if (lng < -6.358091)
	         if (lat < 27.797710)
	          {
	          if (poly[766].contains(lat,lng)) return 33;
	          if (poly[767].contains(lat,lng)) return 181;
	          if (poly[768].contains(lat,lng)) return 343;
	          if (poly[769].contains(lat,lng)) return 380;
	          else return 29;
	          }
	         else
	          {
	          if (poly[770].contains(lat,lng)) return 380;
	          else return 343;
	          }
	        else
	         if (lat < 27.797710)
	          {
	          if (poly[771].contains(lat,lng)) return 33;
	          if (poly[772].contains(lat,lng)) return 33;
	          if (poly[773].contains(lat,lng)) return 380;
	          else return 181;
	          }
	         else
	          if (lng < -2.828296)
	           {
	           if (poly[774].contains(lat,lng)) return 380;
	           else return 343;
	           }
	          else
	           {
	           if (poly[775].contains(lat,lng)) return 343;
	           else return 380;
	           }
	       else
	        {
	        if (poly[776].contains(lat,lng)) return 318;
	        else return 380;
	        }
	      else
	       if (lng < -3.029695)
	        if (lat < 35.928028)
	         {
	         if (poly[777].contains(lat,lng)) return 315;
	         else return 343;
	         }
	        else
	         return 334;
	       else
	        if (lng < -1.689321)
	         if (lng < -2.926722)
	          {
	          if (poly[778].contains(lat,lng)) return 315;
	          else return 343;
	          }
	         else
	          if (lng < -2.913167)
	           return 343;
	          else
	           if (lng < -2.900778)
	            return 343;
	           else
	            {
	            if (poly[779].contains(lat,lng)) return 380;
	            else return 343;
	            }
	        else
	         return 380;
	     else
	      if (lng < -7.828166)
	       return 57;
	      else
	       if (lng < -6.877555)
	        if (lng < -6.950858)
	         {
	         if (poly[780].contains(lat,lng)) return 57;
	         else return 334;
	         }
	        else
	         return 334;
	       else
	        if (lng < -0.623611)
	         if (lng < -5.339639)
	          {
	          if (poly[781].contains(lat,lng)) return 405;
	          else return 334;
	          }
	         else
	          return 334;
	        else
	         return 380;
	   else
	    if (lng < -6.182694)
	     if (lng < -27.040001)
	      return 283;
	     else
	      if (lat < 42.145638)
	       if (lng < -8.984417)
	        return 57;
	       else
	        if (lat < 40.152388)
	         {
	         if (poly[782].contains(lat,lng)) return 334;
	         else return 57;
	         }
	        else
	         if (lng < -7.545764)
	          {
	          if (poly[783].contains(lat,lng)) return 334;
	          else return 57;
	          }
	         else
	          if (lat < 41.149013)
	           {
	           if (poly[784].contains(lat,lng)) return 334;
	           else return 57;
	           }
	          else
	           {
	           if (poly[785].contains(lat,lng)) return 57;
	           else return 334;
	           }
	      else
	       return 334;
	    else
	     if (lat < 40.729195)
	      return 334;
	     else
	      if (lng < 4.614103)
	       if (lng < 3.315139)
	        if (lat < 40.738556)
	         return 334;
	        else
	         if (lng < 1.780389)
	          if (lng < -2.201153)
	           return 334;
	          else
	           if (lng < -0.210382)
	            {
	            if (poly[786].contains(lat,lng)) return 334;
	            else return 297;
	            }
	           else
	            {
	            if (poly[787].contains(lat,lng)) return 136;
	            if (poly[788].contains(lat,lng)) return 297;
	            if (poly[789].contains(lat,lng)) return 297;
	            else return 334;
	            }
	         else
	          {
	          if (poly[790].contains(lat,lng)) return 297;
	          if (poly[791].contains(lat,lng)) return 297;
	          if (poly[792].contains(lat,lng)) return 297;
	          else return 334;
	          }
	       else
	        return 297;
	      else
	       if (lng < 7.064389)
	        return 297;
	       else
	        if (lng < 7.452278)
	         {
	         if (poly[793].contains(lat,lng)) return 261;
	         else return 297;
	         }
	        else
	         {
	         if (poly[794].contains(lat,lng)) return 271;
	         else return 297;
	         }
	  else
	   if (lng < -52.817879)
	    return 290;
	   else
	    if (lng < -2.827945)
	     if (lng < -52.636292)
	      return 290;
	     else
	      return 297;
	    else
	     if (lng < -0.597833)
	      return 297;
	     else
	      if (lat < 43.980424)
	       {
	       if (poly[795].contains(lat,lng)) return 271;
	       else return 297;
	       }
	      else
	       if (lng < 3.518305)
	        return 297;
	       else
	        if (lat < 46.453253)
	         if (lng < 5.576375)
	          return 297;
	         else
	          if (lat < 45.216839)
	           {
	           if (poly[796].contains(lat,lng)) return 271;
	           else return 297;
	           }
	          else
	           if (lng < 6.605410)
	            {
	            if (poly[797].contains(lat,lng)) return 297;
	            else return 172;
	            }
	           else
	            {
	            if (poly[798].contains(lat,lng)) return 172;
	            if (poly[799].contains(lat,lng)) return 271;
	            else return 297;
	            }
	        else
	         if (lng < 5.576375)
	          return 297;
	         else
	          if (lat < 47.689668)
	           {
	           if (poly[800].contains(lat,lng)) return 6;
	           if (poly[801].contains(lat,lng)) return 297;
	           if (poly[802].contains(lat,lng)) return 297;
	           else return 172;
	           }
	          else
	           {
	           if (poly[803].contains(lat,lng)) return 6;
	           else return 297;
	           }
	 else
	  if (lng < -8.989472)
	   if (lng < -10.506306)
	    if (lng < -53.470695)
	     return 290;
	    else
	     return 285;
	   else
	    return 285;
	  else
	   if (lng < 1.759000)
	    if (lng < -4.047028)
	     if (lng < -6.002389)
	      if (lat < 49.976307)
	       return 303;
	      else
	       return 285;
	     else
	      return 303;
	    else
	     if (lat < 50.950080)
	      if (lng < -1.962222)
	       if (lat < 49.514694)
	        if (lng < -2.351278)
	         return 296;
	        else
	         return 139;
	       else
	        if (lat < 49.738609)
	         return 296;
	        else
	         return 303;
	      else
	       if (lng < -1.078694)
	        if (lat < 49.732666)
	         return 297;
	        else
	         return 303;
	       else
	        {
	        if (poly[804].contains(lat,lng)) return 303;
	        if (poly[805].contains(lat,lng)) return 303;
	        if (poly[806].contains(lat,lng)) return 303;
	        if (poly[807].contains(lat,lng)) return 303;
	        else return 297;
	        }
	     else
	      return 303;
	   else
	    if (lat < 52.967224)
	     if (lat < 51.770695)
	      if (lat < 51.505444)
	       if (lng < 4.696722)
	        if (lng < 3.227861)
	         {
	         if (poly[808].contains(lat,lng)) return 254;
	         else return 297;
	         }
	        else
	         if (lat < 50.215763)
	          {
	          if (poly[809].contains(lat,lng)) return 254;
	          else return 297;
	          }
	         else
	          if (lng < 3.962292)
	           {
	           if (poly[810].contains(lat,lng)) return 297;
	           if (poly[811].contains(lat,lng)) return 385;
	           if (poly[812].contains(lat,lng)) return 385;
	           else return 254;
	           }
	          else
	           {
	           if (poly[813].contains(lat,lng)) return 254;
	           if (poly[814].contains(lat,lng)) return 297;
	           else return 385;
	           }
	       else
	        if (lng < 6.165583)
	         if (lat < 50.215763)
	          if (lng < 5.431153)
	           {
	           if (poly[815].contains(lat,lng)) return 297;
	           else return 254;
	           }
	          else
	           if (lat < 49.570923)
	            {
	            if (poly[816].contains(lat,lng)) return 211;
	            if (poly[817].contains(lat,lng)) return 254;
	            else return 297;
	            }
	           else
	            {
	            if (poly[818].contains(lat,lng)) return 6;
	            if (poly[819].contains(lat,lng)) return 211;
	            if (poly[820].contains(lat,lng)) return 297;
	            else return 254;
	            }
	         else
	          if (lng < 5.431153)
	           {
	           if (poly[821].contains(lat,lng)) return 254;
	           else return 385;
	           }
	          else
	           {
	           if (poly[822].contains(lat,lng)) return 6;
	           if (poly[823].contains(lat,lng)) return 6;
	           if (poly[824].contains(lat,lng)) return 6;
	           if (poly[825].contains(lat,lng)) return 254;
	           else return 385;
	           }
	        else
	         if (lat < 50.215763)
	          {
	          if (poly[826].contains(lat,lng)) return 211;
	          if (poly[827].contains(lat,lng)) return 254;
	          if (poly[828].contains(lat,lng)) return 297;
	          else return 6;
	          }
	         else
	          {
	          if (poly[829].contains(lat,lng)) return 254;
	          if (poly[830].contains(lat,lng)) return 385;
	          if (poly[831].contains(lat,lng)) return 385;
	          if (poly[832].contains(lat,lng)) return 385;
	          else return 6;
	          }
	      else
	       if (lng < 4.356167)
	        return 385;
	       else
	        {
	        if (poly[833].contains(lat,lng)) return 6;
	        else return 385;
	        }
	     else
	      if (lng < 5.862928)
	       return 385;
	      else
	       {
	       if (poly[834].contains(lat,lng)) return 6;
	       else return 385;
	       }
	    else
	     if (lng < 6.615556)
	      return 385;
	     else
	      if (lat < 53.470085)
	       {
	       if (poly[835].contains(lat,lng)) return 385;
	       else return 6;
	       }
	      else
	       return 6;
	}

	private static int call13(double lat, double lng)
	{
	 if (lat < 27.038574)
	  if (lat < 23.599949)
	   if (lng < -76.834116)
	    return 376;
	   else
	    if (lng < -73.072296)
	     return 281;
	    else
	     if (lng < -72.729446)
	      return 281;
	     else
	      return 103;
	  else
	   return 281;
	 else
	  if (lat < 41.665817)
	   if (lat < 32.824715)
	    if (lng < -79.721794)
	     return 165;
	    else
	     return 281;
	   else
	    return 165;
	  else
	   if (lng < -71.143646)
	    if (lat < 52.264294)
	     if (lat < 44.366348)
	      if (lat < 44.056385)
	       if (lng < -76.605095)
	        if (lat < 43.433300)
	         if (lat < 43.258678)
	          {
	          if (poly[836].contains(lat,lng)) return 165;
	          else return 238;
	          }
	         else
	          if (lng < -79.675271)
	           return 238;
	          else
	           return 165;
	        else
	         return 238;
	       else
	        return 165;
	      else
	       if (lng < -76.498672)
	        return 238;
	       else
	        if (lat < 44.198559)
	         if (lng < -76.328499)
	          {
	          if (poly[837].contains(lat,lng)) return 165;
	          else return 238;
	          }
	         else
	          return 165;
	        else
	         if (lng < -76.185989)
	          if (lat < 44.210916)
	           if (lng < -76.250479)
	            return 165;
	           else
	            return 238;
	          else
	           return 238;
	         else
	          if (lat < 44.310806)
	           return 165;
	          else
	           {
	           if (poly[838].contains(lat,lng)) return 238;
	           if (poly[839].contains(lat,lng)) return 238;
	           else return 165;
	           }
	     else
	      if (lat < 51.782272)
	       if (lng < -75.912140)
	        if (lat < 51.471649)
	         if (lat < 44.397961)
	          return 238;
	         else
	          if (lat < 47.934805)
	           {
	           if (poly[840].contains(lat,lng)) return 20;
	           else return 238;
	           }
	          else
	           {
	           if (poly[841].contains(lat,lng)) return 238;
	           else return 20;
	           }
	        else
	         if (lng < -79.051132)
	          if (lat < 51.665241)
	           {
	           if (poly[842].contains(lat,lng)) return 137;
	           else return 20;
	           }
	          else
	           return 137;
	         else
	          return 20;
	       else
	        if (lat < 44.368198)
	         return 165;
	        else
	         {
	         if (poly[843].contains(lat,lng)) return 165;
	         if (poly[844].contains(lat,lng)) return 165;
	         if (poly[845].contains(lat,lng)) return 238;
	         if (poly[846].contains(lat,lng)) return 238;
	         else return 20;
	         }
	      else
	       if (lng < -79.098106)
	        return 137;
	       else
	        if (lat < 51.958984)
	         if (lng < -78.947922)
	          if (lat < 51.859096)
	           return 20;
	          else
	           return 137;
	         else
	          {
	          if (poly[847].contains(lat,lng)) return 137;
	          else return 20;
	          }
	        else
	         if (lng < -78.941681)
	          return 137;
	         else
	          return 20;
	    else
	     if (lng < -79.142494)
	      return 137;
	     else
	      if (lat < 52.978027)
	       if (lng < -78.844490)
	        if (lng < -78.920349)
	         return 137;
	        else
	         if (lat < 52.940254)
	          if (lat < 52.762878)
	           {
	           if (poly[848].contains(lat,lng)) return 20;
	           else return 137;
	           }
	          else
	           return 20;
	         else
	          return 137;
	       else
	        if (lat < 52.447598)
	         if (lng < -78.703720)
	          return 137;
	         else
	          if (lat < 52.395023)
	           {
	           if (poly[849].contains(lat,lng)) return 137;
	           else return 20;
	           }
	          else
	           if (lng < -78.558243)
	            return 20;
	           else
	            return 137;
	        else
	         if (lng < -78.775826)
	          if (lat < 52.728059)
	           return 20;
	          else
	           if (lat < 52.776230)
	            {
	            if (poly[850].contains(lat,lng)) return 137;
	            else return 20;
	            }
	           else
	            return 20;
	         else
	          if (lng < -78.753601)
	           if (lat < 52.665272)
	            if (lat < 52.558494)
	             return 20;
	            else
	             {
	             if (poly[851].contains(lat,lng)) return 137;
	             else return 20;
	             }
	           else
	            return 20;
	          else
	           {
	           if (poly[852].contains(lat,lng)) return 137;
	           else return 20;
	           }
	      else
	       if (lat < 53.413036)
	        if (lat < 53.309853)
	         if (lng < -79.004341)
	          return 137;
	         else
	          if (lat < 53.286568)
	           {
	           if (poly[853].contains(lat,lng)) return 137;
	           else return 20;
	           }
	          else
	           if (lng < -78.965593)
	            return 20;
	           else
	            return 137;
	        else
	         if (lng < -78.996750)
	          if (lat < 53.361423)
	           return 137;
	          else
	           {
	           if (poly[854].contains(lat,lng)) return 20;
	           else return 137;
	           }
	         else
	          {
	          if (poly[855].contains(lat,lng)) return 137;
	          else return 20;
	          }
	       else
	        if (lng < -79.100655)
	         if (lat < 53.874668)
	          if (lat < 53.512827)
	           if (lat < 53.449286)
	            return 20;
	           else
	            return 137;
	          else
	           return 137;
	         else
	          return 20;
	        else
	         if (lat < 53.434547)
	          if (lng < -79.082458)
	           return 137;
	          else
	           return 20;
	         else
	          if (lat < 53.544559)
	           {
	           if (poly[856].contains(lat,lng)) return 137;
	           else return 20;
	           }
	          else
	           if (lat < 53.552872)
	            return 20;
	           else
	            {
	            if (poly[857].contains(lat,lng)) return 137;
	            else return 20;
	            }
	   else
	    if (lat < 44.514835)
	     return 165;
	    else
	     if (lng < -70.321770)
	      if (lat < 45.850436)
	       {
	       if (poly[858].contains(lat,lng)) return 20;
	       else return 165;
	       }
	      else
	       return 20;
	     else
	      if (lat < 47.999551)
	       if (lng < -69.687019)
	        if (lat < 46.998252)
	         {
	         if (poly[859].contains(lat,lng)) return 20;
	         else return 165;
	         }
	        else
	         return 20;
	       else
	        if (lat < 44.538010)
	         return 165;
	        else
	         {
	         if (poly[860].contains(lat,lng)) return 20;
	         if (poly[861].contains(lat,lng)) return 20;
	         if (poly[862].contains(lat,lng)) return 165;
	         else return 77;
	         }
	      else
	       return 20;
	}

	private static int call14(double lat, double lng)
	{
	 if (lat < -21.599977)
	  if (lat < -50.882633)
	   if (lng < -72.467056)
	    return 31;
	   else
	    if (lat < -54.523746)
	     if (lng < -68.613205)
	      return 31;
	     else
	      if (lat < -55.351746)
	       if (lng < -66.772804)
	        return 31;
	       else
	        return 69;
	      else
	       if (lat < -55.121574)
	        return 31;
	       else
	        if (lng < -65.110054)
	         if (lat < -54.922653)
	          if (lng < -67.035919)
	           return 31;
	          else
	           if (lng < -66.787865)
	            if (lat < -54.971360)
	             return 31;
	            else
	             return 287;
	           else
	            return 287;
	         else
	          if (lng < -68.253555)
	           if (lat < -54.907707)
	            return 31;
	           else
	            if (lng < -68.609608)
	             {
	             if (poly[863].contains(lat,lng)) return 287;
	             else return 31;
	             }
	            else
	             return 287;
	          else
	           if (lng < -67.756295)
	            if (lat < -54.893145)
	             return 31;
	            else
	             return 287;
	           else
	            if (lat < -54.916042)
	             if (lng < -67.298005)
	              return 31;
	             else
	              return 287;
	            else
	             return 287;
	        else
	         if (lng < -63.806961)
	          return 287;
	         else
	          return 69;
	    else
	     if (lng < -61.327831)
	      if (lng < -72.023773)
	       if (lat < -52.554565)
	        return 31;
	       else
	        if (lat < -52.538841)
	         return 31;
	        else
	         {
	         if (poly[864].contains(lat,lng)) return 85;
	         else return 31;
	         }
	      else
	       if (lng < -71.377541)
	        if (lat < -52.638439)
	         return 31;
	        else
	         {
	         if (poly[865].contains(lat,lng)) return 85;
	         else return 31;
	         }
	       else
	        if (lat < -53.978534)
	         if (lng < -69.057579)
	          return 31;
	         else
	          {
	          if (poly[866].contains(lat,lng)) return 31;
	          else return 287;
	          }
	        else
	         if (lat < -52.456120)
	          if (lng < -69.544449)
	           return 31;
	          else
	           {
	           if (poly[867].contains(lat,lng)) return 31;
	           else return 287;
	           }
	         else
	          if (lng < -68.368202)
	           {
	           if (poly[868].contains(lat,lng)) return 31;
	           else return 85;
	           }
	          else
	           return 82;
	     else
	      if (lng < -38.240654)
	       return 82;
	      else
	       return 69;
	  else
	   if (lat < -44.979031)
	    if (lng < -73.588425)
	     return 31;
	    else
	     if (lat < -45.399536)
	      if (lat < -45.999504)
	       if (lng < -73.586082)
	        return 31;
	       else
	        if (lng < -71.633919)
	         if (lat < -48.441069)
	          {
	          if (poly[869].contains(lat,lng)) return 85;
	          else return 31;
	          }
	         else
	          {
	          if (poly[870].contains(lat,lng)) return 85;
	          else return 31;
	          }
	        else
	         return 85;
	      else
	       if (lng < -72.828827)
	        return 31;
	       else
	        {
	        if (poly[871].contains(lat,lng)) return 31;
	        else return 88;
	        }
	     else
	      if (lng < -73.093870)
	       return 31;
	      else
	       if (lng < -71.297264)
	        {
	        if (poly[872].contains(lat,lng)) return 88;
	        else return 31;
	        }
	       else
	        return 88;
	   else
	    if (lat < -42.249722)
	     if (lat < -44.200775)
	      if (lng < -73.806030)
	       if (lng < -75.153130)
	        return 59;
	       else
	        return 31;
	      else
	       if (lng < -72.698524)
	        return 31;
	       else
	        if (lng < -71.110161)
	         if (lat < -44.523029)
	          {
	          if (poly[873].contains(lat,lng)) return 88;
	          else return 31;
	          }
	         else
	          {
	          if (poly[874].contains(lat,lng)) return 88;
	          else return 31;
	          }
	        else
	         return 88;
	     else
	      if (lat < -43.470894)
	       if (lng < -73.925133)
	        if (lng < -176.192429)
	         return 59;
	        else
	         return 31;
	       else
	        if (lng < -72.978767)
	         return 31;
	        else
	         {
	         if (poly[875].contains(lat,lng)) return 31;
	         else return 88;
	         }
	      else
	       if (lng < -72.823738)
	        return 31;
	       else
	        if (lng < -71.728859)
	         if (lat < -43.211407)
	          {
	          if (poly[876].contains(lat,lng)) return 88;
	          else return 31;
	          }
	         else
	          {
	          if (poly[877].contains(lat,lng)) return 88;
	          else return 31;
	          }
	        else
	         return 88;
	    else
	     return call1(lat,lng);
	 else
	  if (lng < -74.478584)
	   if (lng < -106.292747)
	    if (lng < -148.714951)
	     if (lng < -171.094925)
	      if (lng < -178.235748)
	       return 147;
	      else
	       if (lng < -174.268463)
	        if (lng < -174.490997)
	         if (lat < -19.645048)
	          if (lng < -178.213242)
	           return 147;
	          else
	           return 119;
	         else
	          if (lng < -176.128784)
	           return 5;
	          else
	           if (lng < -174.626297)
	            return 119;
	           else
	            return 282;
	        else
	         return 119;
	       else
	        if (lng < -173.702484)
	         return 119;
	        else
	         if (lat < -13.432207)
	          return 199;
	         else
	          if (lat < -8.553614)
	           return 336;
	          else
	           return 282;
	     else
	      if (lat < -15.829600)
	       if (lng < -157.312134)
	        if (lng < -169.775177)
	         return 372;
	        else
	         return 360;
	       else
	        return 312;
	      else
	       if (lat < -8.982066)
	        if (lng < -163.115570)
	         if (lng < -169.416077)
	          return 44;
	         else
	          return 360;
	        else
	         if (lng < -157.907135)
	          return 360;
	         else
	          if (lng < -154.514053)
	           return 312;
	          else
	           return 237;
	       else
	        if (lat < 16.741133)
	         if (lng < -157.964737)
	          if (lat < 3.847899)
	           if (lng < -159.306065)
	            if (lat < 0.344903)
	             return 237;
	            else
	             return 282;
	           else
	            return 360;
	          else
	           if (lng < -169.517105)
	            return 50;
	           else
	            return 237;
	         else
	          return 237;
	        else
	         return 244;
	    else
	     if (lng < -140.725235)
	      return 312;
	     else
	      if (lng < -138.787460)
	       if (lat < -14.805717)
	        return 312;
	       else
	        return 102;
	      else
	       if (lat < -21.293276)
	        return 305;
	       else
	        if (lat < -17.305042)
	         return 312;
	        else
	         if (lng < -138.593521)
	          return 102;
	         else
	          return 407;
	   else
	    return call2(lat,lng);
	  else
	   if (lng < -49.586945)
	    return call6(lat,lng);
	   else
	    return call7(lat,lng);
	}

	private static int call15(double lat, double lng)
	{
	 if (lng < -133.196884)
	  if (lat < 56.989098)
	   return 23;
	  else
	   if (lat < 58.103729)
	    if (lng < -134.730316)
	     if (lat < 57.768707)
	      return 23;
	     else
	      if (lng < -135.345864)
	       {
	       if (poly[878].contains(lat,lng)) return 23;
	       else return 359;
	       }
	      else
	       return 359;
	    else
	     if (lat < 57.235901)
	      if (lng < -134.142721)
	       return 359;
	      else
	       if (lat < 57.094612)
	        return 23;
	       else
	        {
	        if (poly[879].contains(lat,lng)) return 23;
	        else return 359;
	        }
	     else
	      return 359;
	   else
	    if (lat < 58.711491)
	     if (lng < -134.152863)
	      return 359;
	     else
	      {
	      if (poly[880].contains(lat,lng)) return 45;
	      else return 359;
	      }
	    else
	     if (lng < -139.228378)
	      if (lat < 59.995773)
	       return 98;
	      else
	       if (lat < 60.348695)
	        {
	        if (poly[881].contains(lat,lng)) return 324;
	        else return 98;
	        }
	       else
	        {
	        if (poly[882].contains(lat,lng)) return 330;
	        else return 324;
	        }
	     else
	      if (lng < -136.385616)
	       if (lat < 58.786423)
	        return 359;
	       else
	        if (lat < 58.897247)
	         if (lng < -137.563436)
	          {
	          if (poly[883].contains(lat,lng)) return 98;
	          else return 359;
	          }
	         else
	          return 359;
	        else
	         if (lat < 60.000000)
	          if (lng < -137.492722)
	           if (lat < 58.990978)
	            {
	            if (poly[884].contains(lat,lng)) return 45;
	            if (poly[885].contains(lat,lng)) return 359;
	            else return 98;
	            }
	           else
	            {
	            if (poly[886].contains(lat,lng)) return 45;
	            else return 98;
	            }
	          else
	           if (lat < 58.904221)
	            return 359;
	           else
	            {
	            if (poly[887].contains(lat,lng)) return 45;
	            else return 359;
	            }
	         else
	          {
	          if (poly[888].contains(lat,lng)) return 98;
	          else return 324;
	          }
	      else
	       if (lat < 58.772717)
	        if (lng < -135.007767)
	         return 359;
	        else
	         {
	         if (poly[889].contains(lat,lng)) return 359;
	         else return 45;
	         }
	       else
	        if (lat < 60.000000)
	         if (lng < -135.571940)
	          if (lat < 58.905876)
	           return 359;
	          else
	           {
	           if (poly[890].contains(lat,lng)) return 45;
	           else return 359;
	           }
	         else
	          if (lng < -135.234406)
	           if (lat < 59.346051)
	            return 359;
	           else
	            {
	            if (poly[891].contains(lat,lng)) return 45;
	            else return 359;
	            }
	          else
	           {
	           if (poly[892].contains(lat,lng)) return 45;
	           else return 359;
	           }
	        else
	         return 324;
	 else
	  if (lng < -131.861893)
	   if (lat < 56.090157)
	    if (lat < 54.692081)
	     if (lat < 54.256939)
	      return 45;
	     else
	      return 23;
	    else
	     return 23;
	   else
	    if (lat < 56.685867)
	     return 23;
	    else
	     if (lat < 57.022583)
	      if (lng < -132.248520)
	       return 23;
	      else
	       {
	       if (poly[893].contains(lat,lng)) return 45;
	       else return 23;
	       }
	     else
	      if (lat < 60.000000)
	       {
	       if (poly[894].contains(lat,lng)) return 23;
	       if (poly[895].contains(lat,lng)) return 23;
	       if (poly[896].contains(lat,lng)) return 45;
	       else return 359;
	       }
	      else
	       return 324;
	  else
	   if (lat < 54.818798)
	    if (lng < -130.532215)
	     if (lat < 54.626297)
	      return 45;
	     else
	      if (lng < -130.681854)
	       return 23;
	      else
	       if (lat < 54.778576)
	        if (lng < -130.667728)
	         return 23;
	        else
	         {
	         if (poly[897].contains(lat,lng)) return 45;
	         else return 23;
	         }
	       else
	        return 23;
	    else
	     if (lng < -129.483673)
	      return 45;
	     else
	      if (lng < -110.000000)
	       if (lng < -120.167200)
	        {
	        if (poly[898].contains(lat,lng)) return 116;
	        if (poly[899].contains(lat,lng)) return 116;
	        else return 45;
	        }
	       else
	        if (lng < -120.060618)
	         {
	         if (poly[900].contains(lat,lng)) return 45;
	         else return 116;
	         }
	        else
	         {
	         if (poly[901].contains(lat,lng)) return 227;
	         else return 116;
	         }
	      else
	       {
	       if (poly[902].contains(lat,lng)) return 284;
	       else return 308;
	       }
	   else
	    if (lng < -131.176437)
	     if (lat < 55.709074)
	      if (lat < 55.283344)
	       if (lng < -131.328003)
	        if (lat < 54.982440)
	         return 23;
	        else
	         if (lng < -131.504605)
	          if (lng < -131.685028)
	           return 23;
	          else
	           if (lng < -131.628221)
	            if (lat < 55.162207)
	             return 23;
	            else
	             return 195;
	           else
	            {
	            if (poly[903].contains(lat,lng)) return 23;
	            else return 195;
	            }
	         else
	          if (lat < 55.190212)
	           return 195;
	          else
	           if (lat < 55.254289)
	            {
	            if (poly[904].contains(lat,lng)) return 195;
	            else return 23;
	            }
	           else
	            return 23;
	       else
	        return 23;
	      else
	       return 23;
	     else
	      if (lat < 55.995227)
	       return 23;
	      else
	       if (lat < 60.000000)
	        {
	        if (poly[905].contains(lat,lng)) return 45;
	        else return 23;
	        }
	       else
	        return 324;
	    else
	     if (lng < -129.451582)
	      if (lat < 55.063297)
	       if (lng < -130.219549)
	        {
	        if (poly[906].contains(lat,lng)) return 45;
	        else return 23;
	        }
	       else
	        return 45;
	      else
	       if (lat < 60.000000)
	        if (lng < -129.989868)
	         if (lat < 55.304028)
	          if (lng < -130.503353)
	           return 23;
	          else
	           {
	           if (poly[907].contains(lat,lng)) return 45;
	           else return 23;
	           }
	         else
	          if (lng < -130.927536)
	           if (lat < 55.896952)
	            return 23;
	           else
	            {
	            if (poly[908].contains(lat,lng)) return 45;
	            else return 23;
	            }
	          else
	           {
	           if (poly[909].contains(lat,lng)) return 45;
	           if (poly[910].contains(lat,lng)) return 45;
	           else return 23;
	           }
	        else
	         return 45;
	       else
	        if (lat < 62.070889)
	         return 324;
	        else
	         if (lat < 63.106333)
	          {
	          if (poly[911].contains(lat,lng)) return 142;
	          else return 324;
	          }
	         else
	          {
	          if (poly[912].contains(lat,lng)) return 324;
	          else return 142;
	          }
	     else
	      if (lat < 60.000000)
	       if (lng < -129.253204)
	        return 45;
	       else
	        if (lng < -110.000000)
	         if (lng < -120.000000)
	          if (lng < -124.626602)
	           return 45;
	          else
	           if (lat < 57.409399)
	            if (lng < -122.313301)
	             if (lat < 56.114099)
	              {
	              if (poly[913].contains(lat,lng)) return 116;
	              else return 45;
	              }
	             else
	              {
	              if (poly[914].contains(lat,lng)) return 116;
	              else return 45;
	              }
	            else
	             {
	             if (poly[915].contains(lat,lng)) return 116;
	             else return 45;
	             }
	           else
	            {
	            if (poly[916].contains(lat,lng)) return 116;
	            else return 45;
	            }
	         else
	          return 227;
	        else
	         {
	         if (poly[917].contains(lat,lng)) return 284;
	         else return 308;
	         }
	      else
	       if (lng < -102.008194)
	        if (lng < -115.729888)
	         if (lng < -122.590735)
	          if (lng < -126.021158)
	           if (lat < 62.070889)
	            {
	            if (poly[918].contains(lat,lng)) return 142;
	            if (poly[919].contains(lat,lng)) return 142;
	            else return 324;
	            }
	           else
	            {
	            if (poly[920].contains(lat,lng)) return 324;
	            if (poly[921].contains(lat,lng)) return 324;
	            else return 142;
	            }
	          else
	           {
	           if (poly[922].contains(lat,lng)) return 324;
	           else return 142;
	           }
	         else
	          return 142;
	        else
	         return 142;
	       else
	        return 307;
	}

	private static int call16(double lat, double lng)
	{
	 if (lng < -80.436195)
	  if (lat < 61.762981)
	   if (lat < 60.000000)
	    if (lng < -92.778726)
	     if (lng < -93.769402)
	      if (lat < 59.072639)
	       if (lng < -94.795906)
	        {
	        if (poly[923].contains(lat,lng)) return 307;
	        else return 284;
	        }
	       else
	        {
	        if (poly[924].contains(lat,lng)) return 307;
	        else return 284;
	        }
	      else
	       if (lat < 59.348492)
	        if (lng < -94.781494)
	         {
	         if (poly[925].contains(lat,lng)) return 307;
	         else return 284;
	         }
	        else
	         return 284;
	       else
	        {
	        if (poly[926].contains(lat,lng)) return 307;
	        else return 284;
	        }
	     else
	      if (lng < -93.572128)
	       if (lng < -93.629883)
	        {
	        if (poly[927].contains(lat,lng)) return 307;
	        else return 284;
	        }
	       else
	        if (lat < 58.785896)
	         return 284;
	        else
	         return 307;
	      else
	       if (lng < -93.029205)
	        {
	        if (poly[928].contains(lat,lng)) return 307;
	        else return 284;
	        }
	       else
	        {
	        if (poly[929].contains(lat,lng)) return 307;
	        else return 284;
	        }
	    else
	     return 137;
	   else
	    return 307;
	  else
	   if (lng < -91.862190)
	    return 307;
	   else
	    return 64;
	 else
	  if (lng < -78.626930)
	   return 137;
	  else
	   if (lat < 59.404354)
	    if (lng < -78.293175)
	     if (lat < 58.644310)
	      if (lat < 58.508587)
	       return 137;
	      else
	       if (lng < -78.583992)
	        return 137;
	       else
	        if (lat < 58.579838)
	         if (lng < -78.495689)
	          return 137;
	         else
	          return 20;
	        else
	         {
	         if (poly[930].contains(lat,lng)) return 137;
	         if (poly[931].contains(lat,lng)) return 137;
	         else return 20;
	         }
	     else
	      if (lng < -78.562523)
	       if (lat < 58.697945)
	        return 20;
	       else
	        if (lat < 58.829868)
	         if (lng < -78.599251)
	          return 137;
	         else
	          if (lat < 58.795967)
	           {
	           if (poly[932].contains(lat,lng)) return 137;
	           else return 20;
	           }
	          else
	           return 137;
	        else
	         {
	         if (poly[933].contains(lat,lng)) return 137;
	         else return 20;
	         }
	      else
	       if (lng < -78.530769)
	        if (lat < 58.697453)
	         return 20;
	        else
	         if (lat < 58.741207)
	          {
	          if (poly[934].contains(lat,lng)) return 137;
	          else return 20;
	          }
	         else
	          return 20;
	       else
	        if (lng < -78.524391)
	         return 20;
	        else
	         if (lng < -78.392517)
	          if (lat < 58.753428)
	           return 20;
	          else
	           {
	           if (poly[935].contains(lat,lng)) return 137;
	           else return 20;
	           }
	         else
	          {
	          if (poly[936].contains(lat,lng)) return 137;
	          else return 20;
	          }
	    else
	     if (lat < 58.276955)
	      if (lng < -77.701317)
	       return 137;
	      else
	       if (lat < 58.169769)
	        if (lng < -77.527397)
	         {
	         if (poly[937].contains(lat,lng)) return 20;
	         else return 137;
	         }
	        else
	         {
	         if (poly[938].contains(lat,lng)) return 137;
	         else return 20;
	         }
	       else
	        {
	        if (poly[939].contains(lat,lng)) return 137;
	        if (poly[940].contains(lat,lng)) return 137;
	        else return 20;
	        }
	     else
	      if (lat < 58.411152)
	       if (lng < -78.081505)
	        return 137;
	       else
	        if (lat < 58.337955)
	         {
	         if (poly[941].contains(lat,lng)) return 137;
	         else return 20;
	         }
	        else
	         {
	         if (poly[942].contains(lat,lng)) return 137;
	         else return 20;
	         }
	      else
	       if (lng < -78.153816)
	        if (lat < 59.151792)
	         if (lat < 58.462151)
	          {
	          if (poly[943].contains(lat,lng)) return 137;
	          else return 20;
	          }
	         else
	          if (lat < 59.107185)
	           {
	           if (poly[944].contains(lat,lng)) return 137;
	           else return 20;
	           }
	          else
	           {
	           if (poly[945].contains(lat,lng)) return 137;
	           else return 20;
	           }
	        else
	         {
	         if (poly[946].contains(lat,lng)) return 20;
	         else return 137;
	         }
	       else
	        return 20;
	   else
	    if (lat < 60.825294)
	     if (lat < 60.206890)
	      if (lat < 59.736668)
	       if (lng < -77.730873)
	        if (lat < 59.555698)
	         {
	         if (poly[947].contains(lat,lng)) return 137;
	         else return 20;
	         }
	        else
	         if (lat < 59.722752)
	          return 20;
	         else
	          return 137;
	       else
	        if (lat < 59.713863)
	         {
	         if (poly[948].contains(lat,lng)) return 137;
	         else return 20;
	         }
	        else
	         if (lng < -77.621773)
	          return 20;
	         else
	          return 137;
	      else
	       if (lng < -77.502136)
	        if (lat < 60.128883)
	         if (lat < 59.780479)
	          {
	          if (poly[949].contains(lat,lng)) return 137;
	          else return 20;
	          }
	         else
	          return 20;
	        else
	         if (lng < -77.638252)
	          return 137;
	         else
	          return 20;
	       else
	        if (lat < 59.880760)
	         {
	         if (poly[950].contains(lat,lng)) return 137;
	         else return 20;
	         }
	        else
	         {
	         if (poly[951].contains(lat,lng)) return 137;
	         else return 20;
	         }
	     else
	      if (lng < -77.962517)
	       if (lat < 60.775932)
	        return 137;
	       else
	        if (lng < -78.257248)
	         return 137;
	        else
	         if (lng < -78.080475)
	          {
	          if (poly[952].contains(lat,lng)) return 137;
	          else return 20;
	          }
	         else
	          return 20;
	      else
	       if (lat < 60.274155)
	        if (lng < -77.616859)
	         return 137;
	        else
	         return 20;
	       else
	        {
	        if (poly[953].contains(lat,lng)) return 137;
	        else return 20;
	        }
	    else
	     if (lat < 62.243992)
	      if (lng < -77.801331)
	       if (lat < 61.462588)
	        return 20;
	       else
	        if (lat < 61.679256)
	         return 137;
	        else
	         return 20;
	      else
	       if (lng < -77.609978)
	        if (lat < 61.487820)
	         if (lat < 61.468121)
	          {
	          if (poly[954].contains(lat,lng)) return 137;
	          else return 20;
	          }
	         else
	          return 137;
	        else
	         if (lng < -77.754745)
	          if (lat < 61.636015)
	           return 20;
	          else
	           return 137;
	         else
	          return 20;
	       else
	        return 20;
	     else
	      if (lat < 62.366975)
	       return 20;
	      else
	       if (lng < -78.011292)
	        return 137;
	       else
	        if (lng < -76.111031)
	         if (lat < 62.394833)
	          return 20;
	         else
	          if (lng < -77.859993)
	           if (lat < 62.488289)
	            return 20;
	           else
	            return 137;
	          else
	           {
	           if (poly[955].contains(lat,lng)) return 137;
	           else return 20;
	           }
	        else
	         return 137;
	}

	private static int call17(double lat, double lng)
	{
	 if (lat < 62.597057)
	  if (lat < 57.923771)
	   if (lng < -79.295670)
	    if (lat < 56.077923)
	     if (lat < 54.899916)
	      if (lat < 54.730381)
	       if (lng < -79.520157)
	        if (lng < -81.082413)
	         if (lng < -82.195809)
	          if (lng < -101.762650)
	           if (lat < 54.055759)
	            {
	            if (poly[956].contains(lat,lng)) return 284;
	            else return 308;
	            }
	           else
	            {
	            if (poly[957].contains(lat,lng)) return 308;
	            else return 284;
	            }
	          else
	           {
	           if (poly[958].contains(lat,lng)) return 284;
	           else return 238;
	           }
	         else
	          return 137;
	        else
	         if (lat < 54.483870)
	          if (lat < 54.467125)
	           if (lng < -79.551872)
	            return 137;
	           else
	            if (lat < 54.436700)
	             return 20;
	            else
	             return 137;
	          else
	           if (lat < 54.479338)
	            return 20;
	           else
	            return 137;
	         else
	          if (lng < -79.566742)
	           if (lat < 54.592674)
	            if (lat < 54.558528)
	             return 20;
	            else
	             return 137;
	           else
	            {
	            if (poly[959].contains(lat,lng)) return 137;
	            else return 20;
	            }
	          else
	           return 20;
	       else
	        if (lat < 54.220275)
	         if (lng < -79.430229)
	          return 137;
	         else
	          return 20;
	        else
	         if (lat < 54.247169)
	          if (lng < -79.383024)
	           return 20;
	          else
	           return 137;
	         else
	          {
	          if (poly[960].contains(lat,lng)) return 137;
	          else return 20;
	          }
	      else
	       if (lng < -79.557785)
	        if (lng < -82.204117)
	         {
	         if (poly[961].contains(lat,lng)) return 284;
	         else return 238;
	         }
	        else
	         return 137;
	       else
	        if (lat < 54.771307)
	         {
	         if (poly[962].contains(lat,lng)) return 20;
	         else return 137;
	         }
	        else
	         return 137;
	     else
	      if (lng < -82.258743)
	       if (lng < -90.000000)
	        {
	        if (poly[963].contains(lat,lng)) return 308;
	        else return 284;
	        }
	       else
	        if (lng < -87.477531)
	         return 238;
	        else
	         if (lng < -82.938011)
	          {
	          if (poly[964].contains(lat,lng)) return 137;
	          else return 238;
	          }
	         else
	          {
	          if (poly[965].contains(lat,lng)) return 137;
	          else return 238;
	          }
	      else
	       return 137;
	    else
	     if (lng < -87.587608)
	      if (lng < -92.745041)
	       return 284;
	      else
	       if (lng < -92.435364)
	        return 284;
	       else
	        if (lng < -90.639900)
	         {
	         if (poly[966].contains(lat,lng)) return 307;
	         else return 284;
	         }
	        else
	         if (lng < -88.990593)
	          {
	          if (poly[967].contains(lat,lng)) return 238;
	          else return 284;
	          }
	         else
	          {
	          if (poly[968].contains(lat,lng)) return 307;
	          else return 238;
	          }
	     else
	      return 137;
	   else
	    if (lng < -78.647247)
	     if (lat < 54.938742)
	      if (lat < 54.096584)
	       if (lng < -79.185135)
	        return 137;
	       else
	        {
	        if (poly[969].contains(lat,lng)) return 137;
	        else return 20;
	        }
	      else
	       if (lat < 54.116554)
	        if (lng < -79.182358)
	         return 137;
	        else
	         if (lng < -79.106735)
	          if (lat < 54.102779)
	           return 20;
	          else
	           return 137;
	         else
	          return 20;
	       else
	        if (lat < 54.133778)
	         if (lng < -79.142452)
	          return 20;
	         else
	          return 137;
	        else
	         if (lat < 54.174984)
	          {
	          if (poly[970].contains(lat,lng)) return 137;
	          else return 20;
	          }
	         else
	          {
	          if (poly[971].contains(lat,lng)) return 137;
	          else return 20;
	          }
	     else
	      return 137;
	    else
	     if (lng < -77.671234)
	      if (lat < 55.354782)
	       return 20;
	      else
	       return 137;
	     else
	      if (lat < 56.531151)
	       if (lat < 56.246243)
	        if (lng < -77.144196)
	         if (lat < 55.653851)
	          {
	          if (poly[972].contains(lat,lng)) return 20;
	          else return 137;
	          }
	         else
	          if (lat < 55.716210)
	           return 20;
	          else
	           return 137;
	        else
	         if (lat < 56.183613)
	          {
	          if (poly[973].contains(lat,lng)) return 137;
	          else return 20;
	          }
	         else
	          if (lng < -76.699039)
	           return 20;
	          else
	           return 137;
	       else
	        if (lng < -76.631676)
	         return 137;
	        else
	         return 20;
	      else
	       if (lat < 57.060780)
	        if (lng < -76.620346)
	         return 137;
	        else
	         return 20;
	       else
	        if (lng < -76.866554)
	         if (lat < 57.631952)
	          if (lat < 57.600049)
	           return 20;
	          else
	           return 137;
	         else
	          {
	          if (poly[974].contains(lat,lng)) return 20;
	          else return 137;
	          }
	        else
	         if (lat < 57.202003)
	          if (lng < -76.618283)
	           return 20;
	          else
	           return 137;
	         else
	          if (lat < 57.274315)
	           if (lng < -76.671217)
	            return 20;
	           else
	            return 137;
	          else
	           {
	           if (poly[975].contains(lat,lng)) return 137;
	           else return 20;
	           }
	  else
	   return call16(lat,lng);
	 else
	  if (lng < -85.724396)
	   if (lat < 66.466286)
	    if (lng < -88.233604)
	     return 307;
	    else
	     if (lat < 65.293282)
	      if (lng < -86.900711)
	       if (lat < 63.907318)
	        return 64;
	       else
	        return 307;
	      else
	       return 64;
	     else
	      if (lat < 65.863514)
	       if (lng < -86.271826)
	        return 64;
	       else
	        return 307;
	      else
	       return 307;
	   else
	    if (lng < -96.746651)
	     if (lat < 67.000000)
	      {
	      if (poly[976].contains(lat,lng)) return 108;
	      else return 307;
	      }
	     else
	      return 108;
	    else
	     if (lng < -89.000000)
	      if (lat < 67.000000)
	       {
	       if (poly[977].contains(lat,lng)) return 108;
	       else return 307;
	       }
	      else
	       return 108;
	     else
	      return 307;
	  else
	   if (lng < -78.549866)
	    if (lat < 65.982880)
	     if (lng < -84.512566)
	      return 64;
	     else
	      if (lat < 65.476295)
	       if (lng < -80.147430)
	        return 64;
	       else
	        return 137;
	      else
	       return 137;
	    else
	     if (lng < -84.850848)
	      if (lat < 66.926231)
	       if (lat < 66.086807)
	        return 64;
	       else
	        if (lng < -85.000000)
	         return 307;
	        else
	         return 137;
	      else
	       if (lng < -85.000000)
	        return 307;
	       else
	        return 137;
	     else
	      return 137;
	   else
	    return 137;
	}

	private static int call18(double lat, double lng)
	{
	 if (lng < -68.000000)
	  if (lat < 62.528820)
	   if (lng < -71.511597)
	    return 20;
	   else
	    if (lng < -69.543457)
	     if (lng < -69.876434)
	      if (lng < -70.065117)
	       if (lng < -71.489273)
	        return 20;
	       else
	        if (lat < 61.226833)
	         if (lat < 60.971127)
	          {
	          if (poly[978].contains(lat,lng)) return 137;
	          else return 20;
	          }
	         else
	          if (lng < -70.100906)
	           return 20;
	          else
	           return 137;
	        else
	         return 137;
	      else
	       if (lat < 59.984997)
	        if (lat < 59.961914)
	         return 20;
	        else
	         return 137;
	       else
	        if (lng < -70.025238)
	         if (lat < 60.858704)
	          return 20;
	         else
	          return 137;
	        else
	         return 20;
	     else
	      if (lat < 59.964027)
	       if (lat < 58.782413)
	        if (lng < -69.818451)
	         return 20;
	        else
	         if (lng < -69.782776)
	          return 20;
	         else
	          {
	          if (poly[979].contains(lat,lng)) return 137;
	          else return 20;
	          }
	       else
	        if (lng < -69.770439)
	         if (lng < -69.791435)
	          return 20;
	         else
	          if (lat < 58.928640)
	           return 20;
	          else
	           return 137;
	        else
	         if (lng < -69.700226)
	          if (lat < 58.894062)
	           return 137;
	          else
	           return 20;
	         else
	          if (lat < 59.484098)
	           return 20;
	          else
	           if (lat < 59.897777)
	            {
	            if (poly[980].contains(lat,lng)) return 137;
	            else return 20;
	            }
	           else
	            return 137;
	      else
	       if (lng < -69.763908)
	        if (lat < 60.926550)
	         return 20;
	        else
	         return 137;
	       else
	        if (lng < -69.662239)
	         return 20;
	        else
	         if (lng < -69.626076)
	          return 20;
	         else
	          if (lng < -69.612923)
	           return 20;
	          else
	           if (lat < 60.240005)
	            if (lat < 60.119833)
	             return 20;
	            else
	             return 137;
	           else
	            if (lat < 60.735718)
	             return 20;
	            else
	             return 137;
	    else
	     if (lat < 58.940762)
	      if (lng < -69.177109)
	       if (lng < -69.454346)
	        return 20;
	       else
	        if (lat < 58.903744)
	         if (lng < -69.269768)
	          {
	          if (poly[981].contains(lat,lng)) return 137;
	          else return 20;
	          }
	         else
	          {
	          if (poly[982].contains(lat,lng)) return 137;
	          else return 20;
	          }
	        else
	         return 137;
	      else
	       if (lng < -68.975906)
	        return 20;
	       else
	        {
	        if (poly[983].contains(lat,lng)) return 137;
	        if (poly[984].contains(lat,lng)) return 137;
	        else return 20;
	        }
	     else
	      if (lat < 59.362007)
	       if (lng < -69.343590)
	        if (lng < -69.391823)
	         if (lng < -69.456772)
	          {
	          if (poly[985].contains(lat,lng)) return 137;
	          else return 20;
	          }
	         else
	          if (lat < 59.005432)
	           return 137;
	          else
	           return 20;
	        else
	         if (lat < 59.032063)
	          if (lat < 58.972084)
	           return 20;
	          else
	           return 137;
	         else
	          return 20;
	       else
	        if (lng < -69.095367)
	         if (lng < -69.227913)
	          return 20;
	         else
	          if (lat < 58.967100)
	           if (lng < -69.160313)
	            return 20;
	           else
	            return 137;
	          else
	           {
	           if (poly[986].contains(lat,lng)) return 137;
	           else return 20;
	           }
	        else
	         return 137;
	      else
	       if (lng < -69.363411)
	        return 20;
	       else
	        return 137;
	  else
	   return 137;
	 else
	  if (lat < 60.695583)
	   if (lng < -65.233032)
	    if (lat < 59.097412)
	     if (lat < 58.866348)
	      if (lng < -67.852905)
	       return 20;
	      else
	       if (lat < 58.418457)
	        if (lat < 55.360661)
	         if (lng < -66.542969)
	          {
	          if (poly[987].contains(lat,lng)) return 182;
	          else return 20;
	          }
	         else
	          {
	          if (poly[988].contains(lat,lng)) return 182;
	          else return 20;
	          }
	        else
	         {
	         if (poly[989].contains(lat,lng)) return 166;
	         if (poly[990].contains(lat,lng)) return 166;
	         else return 20;
	         }
	       else
	        if (lat < 58.512039)
	         return 20;
	        else
	         if (lng < -66.467194)
	          if (lng < -67.264587)
	           return 166;
	          else
	           if (lat < 58.731272)
	            return 20;
	           else
	            return 166;
	         else
	          if (lng < -66.127945)
	           {
	           if (poly[991].contains(lat,lng)) return 166;
	           else return 20;
	           }
	          else
	           if (lat < 58.730328)
	            return 20;
	           else
	            {
	            if (poly[992].contains(lat,lng)) return 166;
	            else return 20;
	            }
	     else
	      if (lng < -65.815163)
	       if (lat < 58.990934)
	        if (lng < -65.941536)
	         if (lat < 58.937481)
	          return 20;
	         else
	          return 166;
	        else
	         {
	         if (poly[993].contains(lat,lng)) return 166;
	         else return 20;
	         }
	       else
	        return 166;
	      else
	       if (lng < -65.696472)
	        if (lat < 59.028445)
	         return 20;
	        else
	         {
	         if (poly[994].contains(lat,lng)) return 20;
	         else return 166;
	         }
	       else
	        if (lat < 59.055573)
	         {
	         if (poly[995].contains(lat,lng)) return 166;
	         else return 20;
	         }
	        else
	         return 20;
	    else
	     if (lat < 59.453045)
	      if (lat < 59.336494)
	       if (lng < -65.750923)
	        return 166;
	       else
	        if (lng < -65.671173)
	         if (lat < 59.146431)
	          {
	          if (poly[996].contains(lat,lng)) return 20;
	          else return 166;
	          }
	         else
	          return 20;
	        else
	         if (lng < -65.593872)
	          if (lat < 59.269846)
	           return 20;
	          else
	           return 166;
	         else
	          {
	          if (poly[997].contains(lat,lng)) return 166;
	          else return 20;
	          }
	      else
	       if (lng < -65.590347)
	        return 166;
	       else
	        if (lng < -65.506706)
	         if (lat < 59.385551)
	          {
	          if (poly[998].contains(lat,lng)) return 166;
	          else return 20;
	          }
	         else
	          return 166;
	        else
	         return 20;
	     else
	      if (lng < -65.469658)
	       if (lat < 59.504585)
	        if (lng < -65.523506)
	         return 166;
	        else
	         if (lat < 59.484665)
	          return 20;
	         else
	          return 166;
	       else
	        if (lng < -67.828476)
	         return 166;
	        else
	         return 20;
	      else
	       return 20;
	   else
	    if (lat < 60.372677)
	     if (lng < -64.394348)
	      if (lat < 59.490199)
	       if (lat < 58.196292)
	        if (lat < 54.967976)
	         if (lng < -65.075279)
	          {
	          if (poly[999].contains(lat,lng)) return 20;
	          else return 182;
	          }
	         else
	          {
	          if (poly[1000].contains(lat,lng)) return 20;
	          else return 182;
	          }
	        else
	         {
	         if (poly[1001].contains(lat,lng)) return 182;
	         else return 20;
	         }
	       else
	        if (lng < -65.101463)
	         return 20;
	        else
	         {
	         if (poly[1002].contains(lat,lng)) return 182;
	         if (poly[1003].contains(lat,lng)) return 182;
	         if (poly[1004].contains(lat,lng)) return 182;
	         else return 20;
	         }
	      else
	       if (lng < -65.159363)
	        return 20;
	       else
	        if (lat < 60.227196)
	         if (lng < -64.604942)
	          if (lat < 60.116100)
	           {
	           if (poly[1005].contains(lat,lng)) return 182;
	           else return 20;
	           }
	          else
	           if (lng < -64.945332)
	            return 20;
	           else
	            {
	            if (poly[1006].contains(lat,lng)) return 182;
	            else return 20;
	            }
	         else
	          if (lat < 59.536037)
	           if (lng < -64.447746)
	            {
	            if (poly[1007].contains(lat,lng)) return 20;
	            else return 182;
	            }
	           else
	            {
	            if (poly[1008].contains(lat,lng)) return 182;
	            else return 20;
	            }
	          else
	           return 182;
	        else
	         {
	         if (poly[1009].contains(lat,lng)) return 182;
	         if (poly[1010].contains(lat,lng)) return 182;
	         else return 20;
	         }
	     else
	      if (lat < 59.521731)
	       if (lat < 58.900371)
	        if (lng < -63.322098)
	         if (lat < 58.071928)
	          if (lat < 56.008599)
	           {
	           if (poly[1011].contains(lat,lng)) return 20;
	           else return 182;
	           }
	          else
	           if (lat < 57.040263)
	            {
	            if (poly[1012].contains(lat,lng)) return 182;
	            else return 20;
	            }
	           else
	            {
	            if (poly[1013].contains(lat,lng)) return 20;
	            else return 182;
	            }
	         else
	          {
	          if (poly[1014].contains(lat,lng)) return 20;
	          else return 182;
	          }
	        else
	         return 182;
	       else
	        if (lng < -64.275330)
	         if (lat < 59.032913)
	          {
	          if (poly[1015].contains(lat,lng)) return 20;
	          else return 182;
	          }
	         else
	          if (lat < 59.073945)
	           {
	           if (poly[1016].contains(lat,lng)) return 182;
	           else return 20;
	           }
	          else
	           {
	           if (poly[1017].contains(lat,lng)) return 20;
	           else return 182;
	           }
	        else
	         return 182;
	      else
	       return 182;
	    else
	     return 20;
	  else
	   return 166;
	}

	private static int call19(double lat, double lng)
	{
	 if (lng < -102.000000)
	  if (lat < 64.141777)
	   if (lng < -141.209183)
	    if (lng < -162.000000)
	     return 134;
	    else
	     return 370;
	   else
	    return call15(lat,lng);
	  else
	   if (lng < -113.442200)
	    if (lng < -130.306656)
	     if (lng < -144.013031)
	      if (lng < -162.000000)
	       if (lng < -169.002365)
	        return 365;
	       else
	        return 134;
	      else
	       return 370;
	     else
	      if (lng < -135.198486)
	       if (lng < -136.526718)
	        if (lng < -141.214264)
	         return 370;
	        else
	         return 324;
	       else
	        if (lat < 68.905205)
	         if (lng < -135.763184)
	          {
	          if (poly[1018].contains(lat,lng)) return 324;
	          else return 142;
	          }
	         else
	          if (lat < 67.006071)
	           {
	           if (poly[1019].contains(lat,lng)) return 142;
	           else return 324;
	           }
	          else
	           return 142;
	        else
	         return 142;
	      else
	       if (lat < 68.416974)
	        if (lat < 67.004973)
	         if (lng < -132.752571)
	          {
	          if (poly[1020].contains(lat,lng)) return 142;
	          if (poly[1021].contains(lat,lng)) return 142;
	          if (poly[1022].contains(lat,lng)) return 142;
	          else return 324;
	          }
	         else
	          if (lat < 65.573375)
	           if (lng < -131.529613)
	            {
	            if (poly[1023].contains(lat,lng)) return 142;
	            else return 324;
	            }
	           else
	            {
	            if (poly[1024].contains(lat,lng)) return 324;
	            else return 142;
	            }
	          else
	           {
	           if (poly[1025].contains(lat,lng)) return 324;
	           else return 142;
	           }
	        else
	         {
	         if (poly[1026].contains(lat,lng)) return 142;
	         if (poly[1027].contains(lat,lng)) return 142;
	         else return 56;
	         }
	       else
	        return 142;
	    else
	     if (lat < 70.083939)
	      if (lat < 68.242561)
	       if (lng < -114.096313)
	        if (lng < -114.526794)
	         if (lng < -114.826683)
	          if (lng < -114.954285)
	           {
	           if (poly[1028].contains(lat,lng)) return 142;
	           else return 108;
	           }
	          else
	           if (lat < 66.009722)
	            {
	            if (poly[1029].contains(lat,lng)) return 108;
	            else return 142;
	            }
	           else
	            return 108;
	         else
	          if (lat < 65.964136)
	           {
	           if (poly[1030].contains(lat,lng)) return 108;
	           else return 142;
	           }
	          else
	           return 108;
	        else
	         if (lat < 65.857001)
	          {
	          if (poly[1031].contains(lat,lng)) return 108;
	          else return 142;
	          }
	         else
	          return 108;
	       else
	        if (lat < 65.703211)
	         {
	         if (poly[1032].contains(lat,lng)) return 108;
	         else return 142;
	         }
	        else
	         return 108;
	      else
	       if (lng < -123.791763)
	        return 142;
	       else
	        if (lng < -113.748108)
	         if (lat < 68.929214)
	          if (lng < -121.203401)
	           {
	           if (poly[1033].contains(lat,lng)) return 108;
	           else return 142;
	           }
	          else
	           return 108;
	         else
	          if (lng < -117.239929)
	           if (lat < 69.666817)
	            {
	            if (poly[1034].contains(lat,lng)) return 108;
	            else return 142;
	            }
	           else
	            return 142;
	          else
	           if (lat < 69.653069)
	            return 108;
	           else
	            {
	            if (poly[1035].contains(lat,lng)) return 108;
	            else return 142;
	            }
	        else
	         if (lat < 69.197906)
	          return 108;
	         else
	          {
	          if (poly[1036].contains(lat,lng)) return 142;
	          else return 108;
	          }
	     else
	      return 142;
	   else
	    if (lat < 68.390404)
	     if (lat < 67.008148)
	      if (lat < 66.858467)
	       if (lng < -102.008194)
	        if (lat < 65.481541)
	         {
	         if (poly[1037].contains(lat,lng)) return 108;
	         else return 142;
	         }
	        else
	         return 108;
	       else
	        {
	        if (poly[1038].contains(lat,lng)) return 307;
	        else return 108;
	        }
	      else
	       if (lng < -102.001780)
	        return 108;
	       else
	        return 307;
	     else
	      return 108;
	    else
	     if (lng < -109.570381)
	      if (lat < 72.561844)
	       if (lng < -112.407028)
	        if (lat < 70.034092)
	         {
	         if (poly[1039].contains(lat,lng)) return 142;
	         else return 108;
	         }
	        else
	         return 142;
	       else
	        if (lat < 68.603874)
	         return 108;
	        else
	         if (lat < 70.365860)
	          {
	          if (poly[1040].contains(lat,lng)) return 108;
	          else return 142;
	          }
	         else
	          if (lng < -109.971375)
	           return 142;
	          else
	           return 108;
	      else
	       if (lng < -109.972572)
	        return 142;
	       else
	        if (lat < 72.704750)
	         {
	         if (poly[1041].contains(lat,lng)) return 142;
	         else return 108;
	         }
	        else
	         return 108;
	     else
	      return 108;
	 else
	  if (lat < 68.155983)
	   return call17(lat,lng);
	  else
	   if (lng < -92.515724)
	    if (lat < 72.136444)
	     return 108;
	    else
	     if (lat < 75.642746)
	      if (lat < 74.177071)
	       if (lat < 73.199013)
	        if (lng < -100.585571)
	         return 108;
	        else
	         if (lng < -96.268059)
	          if (lng < -98.208565)
	           if (lat < 72.802119)
	            return 108;
	           else
	            {
	            if (poly[1042].contains(lat,lng)) return 307;
	            if (poly[1043].contains(lat,lng)) return 307;
	            else return 108;
	            }
	          else
	           if (lat < 72.869066)
	            if (lng < -96.998177)
	             {
	             if (poly[1044].contains(lat,lng)) return 307;
	             else return 108;
	             }
	            else
	             if (lat < 72.679507)
	              {
	              if (poly[1045].contains(lat,lng)) return 307;
	              else return 108;
	              }
	             else
	              return 307;
	           else
	            return 307;
	         else
	          if (lng < -95.333183)
	           return 307;
	          else
	           {
	           if (poly[1046].contains(lat,lng)) return 108;
	           else return 307;
	           }
	       else
	        return 307;
	      else
	       if (lng < -96.762569)
	        return 307;
	       else
	        if (lat < 74.746109)
	         if (lng < -96.132774)
	          return 307;
	         else
	          if (lng < -95.229347)
	           if (lat < 74.697929)
	            return 153;
	           else
	            return 307;
	          else
	           return 153;
	        else
	         if (lat < 74.843742)
	          if (lng < -96.103386)
	           return 153;
	          else
	           return 307;
	         else
	          {
	          if (poly[1047].contains(lat,lng)) return 307;
	          if (poly[1048].contains(lat,lng)) return 307;
	          else return 153;
	          }
	     else
	      return 307;
	   else
	    if (lng < -85.000000)
	     if (lng < -89.000000)
	      if (lat < 70.530556)
	       return 108;
	      else
	       if (lat < 70.688119)
	        if (lng < -91.999786)
	         return 108;
	        else
	         return 307;
	       else
	        return 307;
	     else
	      return 307;
	    else
	     return 137;
	}

	private static int call20(double lat, double lng)
	{
	 if (lat < -28.572058)
	  if (lat < -33.408588)
	   return 169;
	  else
	   if (lng < 19.539615)
	    if (lng < 16.478859)
	     {
	     if (poly[1049].contains(lat,lng)) return 189;
	     else return 169;
	     }
	    else
	     {
	     if (poly[1050].contains(lat,lng)) return 189;
	     else return 169;
	     }
	   else
	    if (lng < 25.950998)
	     return 169;
	    else
	     if (lng < 29.156690)
	      if (lat < -30.990323)
	       return 169;
	      else
	       if (lng < 27.553844)
	        {
	        if (poly[1051].contains(lat,lng)) return 339;
	        else return 169;
	        }
	       else
	        if (lat < -29.781190)
	         {
	         if (poly[1052].contains(lat,lng)) return 339;
	         else return 169;
	         }
	        else
	         {
	         if (poly[1053].contains(lat,lng)) return 339;
	         else return 169;
	         }
	     else
	      {
	      if (poly[1054].contains(lat,lng)) return 339;
	      if (poly[1055].contains(lat,lng)) return 339;
	      else return 169;
	      }
	 else
	  if (lng < 29.360781)
	   if (lng < 17.403767)
	    if (lat < -28.037775)
	     if (lng < 17.083324)
	      {
	      if (poly[1056].contains(lat,lng)) return 169;
	      else return 189;
	      }
	     else
	      if (lat < -28.411060)
	       {
	       if (poly[1057].contains(lat,lng)) return 189;
	       else return 169;
	       }
	      else
	       {
	       if (poly[1058].contains(lat,lng)) return 189;
	       else return 169;
	       }
	    else
	     if (lat < -26.993246)
	      return 189;
	     else
	      if (lat < -16.959894)
	       if (lng < 13.142432)
	        {
	        if (poly[1059].contains(lat,lng)) return 299;
	        else return 189;
	        }
	       else
	        {
	        if (poly[1060].contains(lat,lng)) return 299;
	        else return 189;
	        }
	      else
	       return 299;
	   else
	    if (lat < -22.090446)
	     if (lng < 23.382274)
	      if (lat < -25.331252)
	       if (lng < 20.393020)
	        {
	        if (poly[1061].contains(lat,lng)) return 169;
	        else return 189;
	        }
	       else
	        if (lat < -26.951655)
	         return 169;
	        else
	         if (lng < 21.887647)
	          {
	          if (poly[1062].contains(lat,lng)) return 12;
	          else return 169;
	          }
	         else
	          {
	          if (poly[1063].contains(lat,lng)) return 12;
	          else return 169;
	          }
	      else
	       {
	       if (poly[1064].contains(lat,lng)) return 169;
	       if (poly[1065].contains(lat,lng)) return 169;
	       if (poly[1066].contains(lat,lng)) return 189;
	       else return 12;
	       }
	     else
	      if (lat < -25.331252)
	       {
	       if (poly[1067].contains(lat,lng)) return 12;
	       else return 169;
	       }
	      else
	       if (lng < 26.371527)
	        {
	        if (poly[1068].contains(lat,lng)) return 169;
	        if (poly[1069].contains(lat,lng)) return 169;
	        else return 12;
	        }
	       else
	        if (lat < -23.710849)
	         {
	         if (poly[1070].contains(lat,lng)) return 169;
	         else return 12;
	         }
	        else
	         if (lng < 27.866154)
	          {
	          if (poly[1071].contains(lat,lng)) return 169;
	          else return 12;
	          }
	         else
	          {
	          if (poly[1072].contains(lat,lng)) return 12;
	          if (poly[1073].contains(lat,lng)) return 269;
	          else return 169;
	          }
	    else
	     if (lng < 23.382274)
	      if (lat < -18.849641)
	       {
	       if (poly[1074].contains(lat,lng)) return 189;
	       else return 12;
	       }
	      else
	       if (lng < 20.393020)
	        {
	        if (poly[1075].contains(lat,lng)) return 299;
	        else return 189;
	        }
	       else
	        if (lat < -17.229238)
	         {
	         if (poly[1076].contains(lat,lng)) return 12;
	         if (poly[1077].contains(lat,lng)) return 267;
	         if (poly[1078].contains(lat,lng)) return 299;
	         else return 189;
	         }
	        else
	         {
	         if (poly[1079].contains(lat,lng)) return 299;
	         else return 267;
	         }
	     else
	      if (lat < -18.849641)
	       if (lng < 26.371527)
	        {
	        if (poly[1080].contains(lat,lng)) return 269;
	        else return 12;
	        }
	       else
	        if (lat < -20.470044)
	         {
	         if (poly[1081].contains(lat,lng)) return 269;
	         else return 12;
	         }
	        else
	         {
	         if (poly[1082].contains(lat,lng)) return 12;
	         else return 269;
	         }
	      else
	       if (lng < 26.371527)
	        if (lat < -17.229238)
	         if (lng < 24.876900)
	          if (lat < -18.039439)
	           {
	           if (poly[1083].contains(lat,lng)) return 189;
	           if (poly[1084].contains(lat,lng)) return 189;
	           else return 12;
	           }
	          else
	           {
	           if (poly[1085].contains(lat,lng)) return 12;
	           if (poly[1086].contains(lat,lng)) return 12;
	           if (poly[1087].contains(lat,lng)) return 267;
	           if (poly[1088].contains(lat,lng)) return 299;
	           else return 189;
	           }
	         else
	          if (lat < -18.039439)
	           {
	           if (poly[1089].contains(lat,lng)) return 12;
	           else return 269;
	           }
	          else
	           if (lng < 25.624214)
	            {
	            if (poly[1090].contains(lat,lng)) return 12;
	            if (poly[1091].contains(lat,lng)) return 189;
	            if (poly[1092].contains(lat,lng)) return 269;
	            else return 267;
	            }
	           else
	            {
	            if (poly[1093].contains(lat,lng)) return 267;
	            else return 269;
	            }
	        else
	         return 267;
	       else
	        {
	        if (poly[1094].contains(lat,lng)) return 269;
	        else return 267;
	        }
	  else
	   if (lat < -25.719648)
	    if (lng < 32.895973)
	     if (lng < 32.137260)
	      {
	      if (poly[1095].contains(lat,lng)) return 169;
	      if (poly[1096].contains(lat,lng)) return 169;
	      if (poly[1097].contains(lat,lng)) return 364;
	      else return 117;
	      }
	     else
	      if (lat < -26.830089)
	       {
	       if (poly[1098].contains(lat,lng)) return 364;
	       else return 169;
	       }
	      else
	       return 364;
	    else
	     return 364;
	   else
	    if (lat < -20.664242)
	     if (lat < -23.191945)
	      {
	      if (poly[1099].contains(lat,lng)) return 169;
	      else return 364;
	      }
	     else
	      {
	      if (poly[1100].contains(lat,lng)) return 169;
	      if (poly[1101].contains(lat,lng)) return 364;
	      else return 269;
	      }
	    else
	     if (lat < -18.136539)
	      {
	      if (poly[1102].contains(lat,lng)) return 364;
	      else return 269;
	      }
	     else
	      if (lng < 31.533242)
	       {
	       if (poly[1103].contains(lat,lng)) return 267;
	       if (poly[1104].contains(lat,lng)) return 364;
	       else return 269;
	       }
	      else
	       {
	       if (poly[1105].contains(lat,lng)) return 269;
	       else return 364;
	       }
	}

	private static int call21(double lat, double lng)
	{
	 if (lng < 16.192116)
	  if (lat < 2.346989)
	   if (lng < 9.564004)
	    if (lat < 0.668440)
	     return 155;
	    else
	     if (lng < 9.510069)
	      return 201;
	     else
	      if (lat < 1.022095)
	       return 155;
	      else
	       return 201;
	   else
	    if (lng < 11.335724)
	     if (lat < 0.191560)
	      return 155;
	     else
	      {
	      if (poly[1106].contains(lat,lng)) return 155;
	      if (poly[1107].contains(lat,lng)) return 326;
	      else return 201;
	      }
	    else
	     if (lng < 13.763920)
	      if (lat < 0.778424)
	       return 155;
	      else
	       if (lng < 12.549822)
	        {
	        if (poly[1108].contains(lat,lng)) return 326;
	        else return 155;
	        }
	       else
	        {
	        if (poly[1109].contains(lat,lng)) return 154;
	        if (poly[1110].contains(lat,lng)) return 326;
	        else return 155;
	        }
	     else
	      if (lat < 0.778424)
	       {
	       if (poly[1111].contains(lat,lng)) return 154;
	       else return 155;
	       }
	      else
	       if (lng < 14.978018)
	        {
	        if (poly[1112].contains(lat,lng)) return 155;
	        if (poly[1113].contains(lat,lng)) return 326;
	        else return 154;
	        }
	       else
	        {
	        if (poly[1114].contains(lat,lng)) return 214;
	        if (poly[1115].contains(lat,lng)) return 326;
	        else return 154;
	        }
	  else
	   if (lng < 8.341630)
	    if (lat < 4.832398)
	     return 96;
	    else
	     {
	     if (poly[1116].contains(lat,lng)) return 260;
	     else return 96;
	     }
	   else
	    if (lat < 4.686236)
	     if (lng < 8.937982)
	      if (lat < 3.785980)
	       return 201;
	      else
	       return 326;
	     else
	      if (lng < 9.641862)
	       return 326;
	      else
	       {
	       if (poly[1117].contains(lat,lng)) return 214;
	       else return 326;
	       }
	    else
	     if (lat < 9.946423)
	      if (lng < 12.266873)
	       if (lat < 7.316330)
	        if (lng < 10.304251)
	         {
	         if (poly[1118].contains(lat,lng)) return 326;
	         else return 96;
	         }
	        else
	         {
	         if (poly[1119].contains(lat,lng)) return 96;
	         else return 326;
	         }
	       else
	        {
	        if (poly[1120].contains(lat,lng)) return 326;
	        else return 96;
	        }
	      else
	       if (lat < 7.316330)
	        {
	        if (poly[1121].contains(lat,lng)) return 326;
	        else return 214;
	        }
	       else
	        if (lng < 14.229494)
	         {
	         if (poly[1122].contains(lat,lng)) return 96;
	         if (poly[1123].contains(lat,lng)) return 202;
	         else return 326;
	         }
	        else
	         {
	         if (poly[1124].contains(lat,lng)) return 214;
	         if (poly[1125].contains(lat,lng)) return 326;
	         if (poly[1126].contains(lat,lng)) return 326;
	         if (poly[1127].contains(lat,lng)) return 326;
	         else return 202;
	         }
	     else
	      if (lng < 12.266873)
	       {
	       if (poly[1128].contains(lat,lng)) return 260;
	       else return 96;
	       }
	      else
	       if (lat < 12.576517)
	        if (lng < 14.229494)
	         {
	         if (poly[1129].contains(lat,lng)) return 96;
	         if (poly[1130].contains(lat,lng)) return 202;
	         else return 326;
	         }
	        else
	         if (lat < 11.261470)
	          {
	          if (poly[1131].contains(lat,lng)) return 326;
	          else return 202;
	          }
	         else
	          if (lng < 15.210805)
	           if (lat < 11.918994)
	            {
	            if (poly[1132].contains(lat,lng)) return 96;
	            if (poly[1133].contains(lat,lng)) return 202;
	            else return 326;
	            }
	           else
	            {
	            if (poly[1134].contains(lat,lng)) return 96;
	            if (poly[1135].contains(lat,lng)) return 202;
	            else return 326;
	            }
	          else
	           return 202;
	       else
	        if (lng < 14.229494)
	         if (lat < 13.891564)
	          if (lng < 13.248184)
	           {
	           if (poly[1136].contains(lat,lng)) return 260;
	           else return 96;
	           }
	          else
	           {
	           if (poly[1137].contains(lat,lng)) return 202;
	           if (poly[1138].contains(lat,lng)) return 260;
	           if (poly[1139].contains(lat,lng)) return 326;
	           else return 96;
	           }
	         else
	          {
	          if (poly[1140].contains(lat,lng)) return 260;
	          else return 202;
	          }
	        else
	         {
	         if (poly[1141].contains(lat,lng)) return 326;
	         else return 202;
	         }
	 else
	  if (lat < 5.145740)
	   if (lng < 19.554943)
	    if (lat < 2.177799)
	     {
	     if (poly[1142].contains(lat,lng)) return 331;
	     else return 154;
	     }
	    else
	     if (lng < 17.873529)
	      {
	      if (poly[1143].contains(lat,lng)) return 214;
	      else return 154;
	      }
	     else
	      if (lat < 3.661769)
	       {
	       if (poly[1144].contains(lat,lng)) return 214;
	       if (poly[1145].contains(lat,lng)) return 331;
	       else return 154;
	       }
	      else
	       {
	       if (poly[1146].contains(lat,lng)) return 331;
	       else return 214;
	       }
	   else
	    if (lat < 2.177799)
	     {
	     if (poly[1147].contains(lat,lng)) return 352;
	     else return 331;
	     }
	    else
	     if (lng < 21.603165)
	      {
	      if (poly[1148].contains(lat,lng)) return 214;
	      else return 331;
	      }
	     else
	      if (lat < 3.661769)
	       {
	       if (poly[1149].contains(lat,lng)) return 331;
	       if (poly[1150].contains(lat,lng)) return 331;
	       else return 352;
	       }
	      else
	       if (lng < 22.627275)
	        {
	        if (poly[1151].contains(lat,lng)) return 331;
	        if (poly[1152].contains(lat,lng)) return 352;
	        else return 214;
	        }
	       else
	        {
	        if (poly[1153].contains(lat,lng)) return 214;
	        if (poly[1154].contains(lat,lng)) return 331;
	        else return 352;
	        }
	  else
	   if (lat < 9.373837)
	    if (lng < 20.658514)
	     if (lng < 18.425315)
	      {
	      if (poly[1155].contains(lat,lng)) return 214;
	      else return 202;
	      }
	     else
	      if (lat < 7.259788)
	       return 214;
	      else
	       if (lng < 19.541914)
	        {
	        if (poly[1156].contains(lat,lng)) return 214;
	        else return 202;
	        }
	       else
	        {
	        if (poly[1157].contains(lat,lng)) return 202;
	        else return 214;
	        }
	    else
	     if (lat < 9.281783)
	      {
	      if (poly[1158].contains(lat,lng)) return 322;
	      else return 214;
	      }
	     else
	      {
	      if (poly[1159].contains(lat,lng)) return 322;
	      else return 214;
	      }
	   else
	    if (lng < 19.921751)
	     return 202;
	    else
	     if (lat < 12.290224)
	      if (lng < 21.786569)
	       {
	       if (poly[1160].contains(lat,lng)) return 214;
	       else return 202;
	       }
	      else
	       if (lat < 10.832031)
	        {
	        if (poly[1161].contains(lat,lng)) return 202;
	        if (poly[1162].contains(lat,lng)) return 322;
	        if (poly[1163].contains(lat,lng)) return 322;
	        if (poly[1164].contains(lat,lng)) return 322;
	        else return 214;
	        }
	       else
	        {
	        if (poly[1165].contains(lat,lng)) return 214;
	        if (poly[1166].contains(lat,lng)) return 322;
	        else return 202;
	        }
	     else
	      {
	      if (poly[1167].contains(lat,lng)) return 322;
	      else return 202;
	      }
	}

	private static int call22(double lat, double lng)
	{
	 if (lat < 15.206611)
	  if (lng < 23.651386)
	   return call21(lat,lng);
	  else
	   if (lat < 14.893750)
	    if (lng < 27.463421)
	     if (lat < 9.299600)
	      if (lat < 4.254729)
	       return 352;
	      else
	       if (lat < 6.777165)
	        if (lng < 25.557404)
	         if (lat < 5.515947)
	          if (lng < 24.604395)
	           {
	           if (poly[1168].contains(lat,lng)) return 352;
	           else return 214;
	           }
	          else
	           {
	           if (poly[1169].contains(lat,lng)) return 352;
	           else return 214;
	           }
	         else
	          return 214;
	        else
	         if (lat < 5.515947)
	          if (lng < 26.510412)
	           {
	           if (poly[1170].contains(lat,lng)) return 214;
	           else return 352;
	           }
	          else
	           {
	           if (poly[1171].contains(lat,lng)) return 352;
	           if (poly[1172].contains(lat,lng)) return 395;
	           else return 214;
	           }
	         else
	          {
	          if (poly[1173].contains(lat,lng)) return 395;
	          else return 214;
	          }
	       else
	        if (lng < 25.557404)
	         if (lat < 8.038383)
	          {
	          if (poly[1174].contains(lat,lng)) return 395;
	          else return 214;
	          }
	         else
	          {
	          if (poly[1175].contains(lat,lng)) return 322;
	          if (poly[1176].contains(lat,lng)) return 395;
	          else return 214;
	          }
	        else
	         {
	         if (poly[1177].contains(lat,lng)) return 214;
	         else return 395;
	         }
	     else
	      if (lng < 23.697866)
	       if (lat < 9.537375)
	        if (lat < 9.456836)
	         {
	         if (poly[1178].contains(lat,lng)) return 214;
	         else return 322;
	         }
	        else
	         {
	         if (poly[1179].contains(lat,lng)) return 214;
	         else return 322;
	         }
	       else
	        if (lat < 9.667719)
	         {
	         if (poly[1180].contains(lat,lng)) return 214;
	         else return 322;
	         }
	        else
	         {
	         if (poly[1181].contains(lat,lng)) return 214;
	         else return 322;
	         }
	      else
	       {
	       if (poly[1182].contains(lat,lng)) return 395;
	       else return 322;
	       }
	    else
	     if (lat < 5.008428)
	      if (lng < 34.052362)
	       if (lng < 30.757892)
	        if (lat < 2.109143)
	         {
	         if (poly[1183].contains(lat,lng)) return 179;
	         else return 352;
	         }
	        else
	         if (lng < 29.110656)
	          {
	          if (poly[1184].contains(lat,lng)) return 395;
	          else return 352;
	          }
	         else
	          {
	          if (poly[1185].contains(lat,lng)) return 179;
	          if (poly[1186].contains(lat,lng)) return 395;
	          else return 352;
	          }
	       else
	        if (lat < 2.109143)
	         {
	         if (poly[1187].contains(lat,lng)) return 122;
	         if (poly[1188].contains(lat,lng)) return 352;
	         else return 179;
	         }
	        else
	         if (lng < 32.405127)
	          if (lat < 3.558785)
	           {
	           if (poly[1189].contains(lat,lng)) return 352;
	           if (poly[1190].contains(lat,lng)) return 352;
	           if (poly[1191].contains(lat,lng)) return 395;
	           if (poly[1192].contains(lat,lng)) return 395;
	           else return 179;
	           }
	          else
	           {
	           if (poly[1193].contains(lat,lng)) return 179;
	           if (poly[1194].contains(lat,lng)) return 179;
	           if (poly[1195].contains(lat,lng)) return 352;
	           else return 395;
	           }
	         else
	          {
	          if (poly[1196].contains(lat,lng)) return 122;
	          if (poly[1197].contains(lat,lng)) return 395;
	          else return 179;
	          }
	      else
	       if (lng < 37.346833)
	        if (lat < 2.109143)
	         {
	         if (poly[1198].contains(lat,lng)) return 179;
	         else return 122;
	         }
	        else
	         if (lng < 35.699598)
	          {
	          if (poly[1199].contains(lat,lng)) return 179;
	          if (poly[1200].contains(lat,lng)) return 179;
	          if (poly[1201].contains(lat,lng)) return 395;
	          else return 122;
	          }
	         else
	          {
	          if (poly[1202].contains(lat,lng)) return 350;
	          if (poly[1203].contains(lat,lng)) return 395;
	          else return 122;
	          }
	       else
	        {
	        if (poly[1204].contains(lat,lng)) return 350;
	        else return 122;
	        }
	     else
	      if (lng < 37.916084)
	       if (lng < 32.689753)
	        {
	        if (poly[1205].contains(lat,lng)) return 322;
	        else return 395;
	        }
	       else
	        if (lat < 9.951089)
	         if (lng < 35.302918)
	          if (lat < 7.479758)
	           {
	           if (poly[1206].contains(lat,lng)) return 350;
	           if (poly[1207].contains(lat,lng)) return 350;
	           else return 395;
	           }
	          else
	           if (lng < 33.996336)
	            if (lat < 8.715424)
	             if (lng < 33.343044)
	              {
	              if (poly[1208].contains(lat,lng)) return 350;
	              else return 395;
	              }
	             else
	              {
	              if (poly[1209].contains(lat,lng)) return 350;
	              else return 395;
	              }
	            else
	             {
	             if (poly[1210].contains(lat,lng)) return 322;
	             else return 395;
	             }
	           else
	            {
	            if (poly[1211].contains(lat,lng)) return 322;
	            if (poly[1212].contains(lat,lng)) return 395;
	            else return 350;
	            }
	         else
	          {
	          if (poly[1213].contains(lat,lng)) return 395;
	          if (poly[1214].contains(lat,lng)) return 395;
	          else return 350;
	          }
	        else
	         if (lng < 35.302918)
	          {
	          if (poly[1215].contains(lat,lng)) return 350;
	          if (poly[1216].contains(lat,lng)) return 395;
	          else return 322;
	          }
	         else
	          {
	          if (poly[1217].contains(lat,lng)) return 149;
	          if (poly[1218].contains(lat,lng)) return 322;
	          else return 350;
	          }
	      else
	       {
	       if (poly[1219].contains(lat,lng)) return 350;
	       else return 149;
	       }
	   else
	    if (lng < 36.483905)
	     {
	     if (poly[1220].contains(lat,lng)) return 149;
	     else return 322;
	     }
	    else
	     return 149;
	 else
	  if (lng < 36.903550)
	   if (lng < 14.859665)
	    {
	    if (poly[1221].contains(lat,lng)) return 202;
	    else return 260;
	    }
	   else
	    if (lng < 24.002661)
	     {
	     if (poly[1222].contains(lat,lng)) return 322;
	     else return 202;
	     }
	    else
	     {
	     if (poly[1223].contains(lat,lng)) return 149;
	     else return 322;
	     }
	  else
	   return 149;
	}

	private static int call23(double lat, double lng)
	{
	 if (lat < -8.129065)
	  if (lng < 35.916821)
	   if (lng < 33.705704)
	    if (lat < -15.608835)
	     return call20(lat,lng);
	    else
	     if (lng < 13.264205)
	      return 299;
	     else
	      if (lng < 24.082119)
	       if (lat < -11.397731)
	        {
	        if (poly[1224].contains(lat,lng)) return 267;
	        else return 299;
	        }
	       else
	        if (lng < 18.673162)
	         return 299;
	        else
	         if (lng < 21.377640)
	          return 299;
	         else
	          if (lat < -9.763398)
	           {
	           if (poly[1225].contains(lat,lng)) return 267;
	           if (poly[1226].contains(lat,lng)) return 352;
	           else return 299;
	           }
	          else
	           {
	           if (poly[1227].contains(lat,lng)) return 352;
	           else return 299;
	           }
	      else
	       if (lat < -13.998108)
	        {
	        if (poly[1228].contains(lat,lng)) return 206;
	        if (poly[1229].contains(lat,lng)) return 206;
	        if (poly[1230].contains(lat,lng)) return 267;
	        else return 364;
	        }
	       else
	        if (lng < 30.774246)
	         if (lat < -8.224360)
	          if (lng < 27.428183)
	           if (lat < -11.111234)
	            if (lng < 25.755151)
	             {
	             if (poly[1231].contains(lat,lng)) return 352;
	             else return 267;
	             }
	            else
	             {
	             if (poly[1232].contains(lat,lng)) return 352;
	             else return 267;
	             }
	           else
	            {
	            if (poly[1233].contains(lat,lng)) return 267;
	            else return 352;
	            }
	          else
	           if (lat < -11.111234)
	            if (lng < 29.101214)
	             if (lat < -12.554671)
	              {
	              if (poly[1234].contains(lat,lng)) return 352;
	              else return 267;
	              }
	             else
	              {
	              if (poly[1235].contains(lat,lng)) return 352;
	              else return 267;
	              }
	            else
	             {
	             if (poly[1236].contains(lat,lng)) return 352;
	             else return 267;
	             }
	           else
	            {
	            if (poly[1237].contains(lat,lng)) return 267;
	            else return 352;
	            }
	         else
	          {
	          if (poly[1238].contains(lat,lng)) return 17;
	          else return 352;
	          }
	        else
	         if (lat < -11.063586)
	          if (lat < -12.530847)
	           {
	           if (poly[1239].contains(lat,lng)) return 267;
	           if (poly[1240].contains(lat,lng)) return 267;
	           if (poly[1241].contains(lat,lng)) return 267;
	           else return 206;
	           }
	          else
	           {
	           if (poly[1242].contains(lat,lng)) return 267;
	           else return 206;
	           }
	         else
	          if (lat < -9.596325)
	           {
	           if (poly[1243].contains(lat,lng)) return 17;
	           if (poly[1244].contains(lat,lng)) return 17;
	           if (poly[1245].contains(lat,lng)) return 267;
	           else return 206;
	           }
	          else
	           if (lng < 32.239975)
	            {
	            if (poly[1246].contains(lat,lng)) return 17;
	            else return 267;
	            }
	           else
	            {
	            if (poly[1247].contains(lat,lng)) return 206;
	            if (poly[1248].contains(lat,lng)) return 206;
	            if (poly[1249].contains(lat,lng)) return 267;
	            else return 17;
	            }
	   else
	    if (lat < -19.090635)
	     return 364;
	    else
	     if (lat < -13.609850)
	      if (lat < -16.350243)
	       {
	       if (poly[1250].contains(lat,lng)) return 206;
	       else return 364;
	       }
	      else
	       if (lat < -14.980046)
	        {
	        if (poly[1251].contains(lat,lng)) return 206;
	        else return 364;
	        }
	       else
	        {
	        if (poly[1252].contains(lat,lng)) return 206;
	        if (poly[1253].contains(lat,lng)) return 206;
	        else return 364;
	        }
	     else
	      if (lat < -10.869457)
	       {
	       if (poly[1254].contains(lat,lng)) return 206;
	       if (poly[1255].contains(lat,lng)) return 364;
	       else return 17;
	       }
	      else
	       {
	       if (poly[1256].contains(lat,lng)) return 206;
	       else return 17;
	       }
	  else
	   if (lng < 40.443222)
	    if (lat < -16.279331)
	     if (lat < -46.609653)
	      return 169;
	     else
	      return 364;
	    else
	     if (lat < -10.481017)
	      if (lat < -12.512221)
	       return 364;
	      else
	       {
	       if (poly[1257].contains(lat,lng)) return 17;
	       else return 364;
	       }
	     else
	      return 17;
	   else
	    if (lng < 40.842995)
	     return 364;
	    else
	     return 101;
	 else
	  if (lng < 39.598442)
	   if (lng < 24.421398)
	    if (lat < -5.927235)
	     if (lng < 12.664001)
	      if (lat < -6.026062)
	       if (lng < 12.560946)
	        if (lat < -6.060813)
	         return 299;
	        else
	         return 331;
	       else
	        return 299;
	      else
	       return 331;
	     else
	      if (lng < 12.869216)
	       if (lat < -6.012101)
	        return 299;
	       else
	        if (lat < -5.979688)
	         if (lng < 12.722283)
	          return 331;
	         else
	          return 299;
	        else
	         return 331;
	      else
	       if (lng < 12.944152)
	        return 299;
	       else
	        if (lng < 18.682775)
	         if (lng < 15.813463)
	          return 299;
	         else
	          if (lng < 17.248119)
	           {
	           if (poly[1258].contains(lat,lng)) return 331;
	           else return 299;
	           }
	          else
	           {
	           if (poly[1259].contains(lat,lng)) return 299;
	           else return 331;
	           }
	        else
	         if (lng < 21.552087)
	          {
	          if (poly[1260].contains(lat,lng)) return 299;
	          if (poly[1261].contains(lat,lng)) return 352;
	          else return 331;
	          }
	         else
	          {
	          if (poly[1262].contains(lat,lng)) return 299;
	          else return 352;
	          }
	    else
	     if (lng < 17.529885)
	      if (lat < -5.841628)
	       if (lng < 12.922298)
	        if (lng < 12.876115)
	         {
	         if (poly[1263].contains(lat,lng)) return 299;
	         else return 331;
	         }
	        else
	         return 331;
	       else
	        if (lng < 13.986273)
	         {
	         if (poly[1264].contains(lat,lng)) return 331;
	         else return 299;
	         }
	        else
	         {
	         if (poly[1265].contains(lat,lng)) return 299;
	         else return 331;
	         }
	      else
	       if (lng < 8.838093)
	        return 155;
	       else
	        if (lng < 13.183989)
	         if (lat < -3.315885)
	          if (lng < 11.905854)
	           {
	           if (poly[1266].contains(lat,lng)) return 155;
	           else return 154;
	           }
	          else
	           if (lat < -4.578756)
	            {
	            if (poly[1267].contains(lat,lng)) return 154;
	            if (poly[1268].contains(lat,lng)) return 154;
	            if (poly[1269].contains(lat,lng)) return 331;
	            else return 299;
	            }
	           else
	            {
	            if (poly[1270].contains(lat,lng)) return 155;
	            if (poly[1271].contains(lat,lng)) return 155;
	            if (poly[1272].contains(lat,lng)) return 299;
	            else return 154;
	            }
	         else
	          {
	          if (poly[1273].contains(lat,lng)) return 154;
	          else return 155;
	          }
	        else
	         if (lat < -3.315885)
	          if (lng < 15.356937)
	           if (lat < -4.578756)
	            {
	            if (poly[1274].contains(lat,lng)) return 154;
	            if (poly[1275].contains(lat,lng)) return 154;
	            else return 331;
	            }
	           else
	            {
	            if (poly[1276].contains(lat,lng)) return 331;
	            if (poly[1277].contains(lat,lng)) return 331;
	            else return 154;
	            }
	          else
	           {
	           if (poly[1278].contains(lat,lng)) return 154;
	           else return 331;
	           }
	         else
	          if (lng < 15.356937)
	           {
	           if (poly[1279].contains(lat,lng)) return 154;
	           else return 155;
	           }
	          else
	           {
	           if (poly[1280].contains(lat,lng)) return 331;
	           else return 154;
	           }
	     else
	      if (lng < 20.975641)
	       {
	       if (poly[1281].contains(lat,lng)) return 352;
	       else return 331;
	       }
	      else
	       {
	       if (poly[1282].contains(lat,lng)) return 331;
	       else return 352;
	       }
	   else
	    if (lat < -5.718678)
	     if (lng < 30.744639)
	      {
	      if (poly[1283].contains(lat,lng)) return 17;
	      else return 352;
	      }
	     else
	      return 17;
	    else
	     if (lng < 39.221806)
	      if (lng < 30.895958)
	       if (lat < -2.078556)
	        if (lng < 27.658678)
	         return 352;
	        else
	         if (lat < -3.898617)
	          {
	          if (poly[1284].contains(lat,lng)) return 83;
	          if (poly[1285].contains(lat,lng)) return 352;
	          else return 17;
	          }
	         else
	          if (lng < 29.277318)
	           {
	           if (poly[1286].contains(lat,lng)) return 83;
	           if (poly[1287].contains(lat,lng)) return 293;
	           else return 352;
	           }
	          else
	           if (lat < -2.988586)
	            {
	            if (poly[1288].contains(lat,lng)) return 83;
	            else return 17;
	            }
	           else
	            if (lng < 30.086638)
	             {
	             if (poly[1289].contains(lat,lng)) return 293;
	             else return 83;
	             }
	            else
	             {
	             if (poly[1290].contains(lat,lng)) return 17;
	             if (poly[1291].contains(lat,lng)) return 293;
	             else return 83;
	             }
	       else
	        if (lng < 27.658678)
	         return 352;
	        else
	         if (lng < 29.277318)
	          {
	          if (poly[1292].contains(lat,lng)) return 293;
	          else return 352;
	          }
	         else
	          if (lng < 30.086638)
	           {
	           if (poly[1293].contains(lat,lng)) return 293;
	           if (poly[1294].contains(lat,lng)) return 352;
	           else return 179;
	           }
	          else
	           {
	           if (poly[1295].contains(lat,lng)) return 17;
	           if (poly[1296].contains(lat,lng)) return 179;
	           else return 293;
	           }
	      else
	       if (lat < -5.085639)
	        return 17;
	       else
	        {
	        if (poly[1297].contains(lat,lng)) return 122;
	        if (poly[1298].contains(lat,lng)) return 179;
	        else return 17;
	        }
	     else
	      if (lat < -5.404505)
	       return 17;
	      else
	       return 122;
	  else
	   if (lat < -4.865081)
	    return 17;
	   else
	    if (lat < -1.742535)
	     return 122;
	    else
	     if (lng < 41.567944)
	      {
	      if (poly[1299].contains(lat,lng)) return 250;
	      else return 122;
	      }
	     else
	      return 250;
	}

	private static int call24(double lat, double lng)
	{
	 if (lat < 24.936501)
	  if (lng < 39.488415)
	   if (lng < 36.894085)
	    if (lng < 36.208694)
	     if (lng < 35.794861)
	      if (lng < 35.183971)
	       if (lng < 24.002352)
	        if (lng < 15.818398)
	         {
	         if (poly[1300].contains(lat,lng)) return 202;
	         if (poly[1301].contains(lat,lng)) return 202;
	         if (poly[1302].contains(lat,lng)) return 333;
	         if (poly[1303].contains(lat,lng)) return 380;
	         else return 260;
	         }
	        else
	         {
	         if (poly[1304].contains(lat,lng)) return 260;
	         if (poly[1305].contains(lat,lng)) return 322;
	         if (poly[1306].contains(lat,lng)) return 333;
	         else return 202;
	         }
	       else
	        {
	        if (poly[1307].contains(lat,lng)) return 322;
	        if (poly[1308].contains(lat,lng)) return 333;
	        else return 112;
	        }
	      else
	       if (lat < 23.146889)
	        if (lng < 35.623360)
	         {
	         if (poly[1309].contains(lat,lng)) return 112;
	         else return 322;
	         }
	        else
	         return 322;
	       else
	        return 112;
	     else
	      if (lat < 22.840429)
	       return 322;
	      else
	       return 112;
	    else
	     return 322;
	   else
	    if (lat < 21.996111)
	     if (lat < 18.791805)
	      if (lat < 18.003084)
	       if (lat < 16.518745)
	        {
	        if (poly[1310].contains(lat,lng)) return 322;
	        else return 149;
	        }
	       else
	        {
	        if (poly[1311].contains(lat,lng)) return 322;
	        else return 149;
	        }
	      else
	       return 322;
	     else
	      if (lng < 38.176304)
	       return 322;
	      else
	       return 163;
	    else
	     return 163;
	  else
	   if (lng < 41.542473)
	    if (lat < 16.547777)
	     return 149;
	    else
	     return 163;
	   else
	    if (lng < 42.777195)
	     return 163;
	    else
	     if (lat < 16.674110)
	      {
	      if (poly[1312].contains(lat,lng)) return 215;
	      else return 163;
	      }
	     else
	      {
	      if (poly[1313].contains(lat,lng)) return 215;
	      else return 163;
	      }
	 else
	  if (lng < 33.699028)
	   if (lng < 30.985861)
	    if (lng < 23.294111)
	     if (lat < 33.904083)
	      if (lng < 11.598278)
	       if (lat < 33.582180)
	        if (lng < 10.811750)
	         if (lat < 29.259340)
	          {
	          if (poly[1314].contains(lat,lng)) return 380;
	          else return 333;
	          }
	         else
	          {
	          if (poly[1315].contains(lat,lng)) return 333;
	          if (poly[1316].contains(lat,lng)) return 380;
	          else return 318;
	          }
	        else
	         if (lat < 33.168999)
	          if (lat < 32.523556)
	           {
	           if (poly[1317].contains(lat,lng)) return 318;
	           else return 333;
	           }
	          else
	           {
	           if (poly[1318].contains(lat,lng)) return 333;
	           else return 318;
	           }
	         else
	          return 318;
	       else
	        return 318;
	      else
	       return 333;
	     else
	      if (lng < 11.122278)
	       if (lng < 8.473444)
	        if (lat < 35.242054)
	         {
	         if (poly[1319].contains(lat,lng)) return 380;
	         else return 318;
	         }
	        else
	         {
	         if (poly[1320].contains(lat,lng)) return 318;
	         else return 380;
	         }
	       else
	        return 318;
	      else
	       if (lng < 11.342472)
	        return 318;
	       else
	        if (lat < 35.669319)
	         return 271;
	        else
	         return 302;
	    else
	     if (lat < 32.220139)
	      if (lng < 25.150612)
	       {
	       if (poly[1321].contains(lat,lng)) return 112;
	       else return 333;
	       }
	      else
	       return 112;
	     else
	      return 152;
	   else
	    if (lat < 31.601473)
	     return 112;
	    else
	     return 174;
	  else
	   if (lat < 27.445555)
	    if (lng < 34.956427)
	     return 112;
	    else
	     return 163;
	   else
	    if (lat < 27.855917)
	     if (lng < 34.318772)
	      return 112;
	     else
	      return 163;
	    else
	     if (lat < 28.084833)
	      if (lng < 34.444985)
	       return 112;
	      else
	       return 163;
	     else
	      if (lng < 34.903873)
	       if (lat < 32.529386)
	        if (lat < 29.200893)
	         {
	         if (poly[1322].contains(lat,lng)) return 112;
	         else return 163;
	         }
	        else
	         {
	         if (poly[1323].contains(lat,lng)) return 112;
	         if (poly[1324].contains(lat,lng)) return 255;
	         if (poly[1325].contains(lat,lng)) return 344;
	         else return 8;
	         }
	       else
	        return 174;
	      else
	       if (lng < 39.051479)
	        if (lat < 31.948417)
	         if (lng < 36.977676)
	          if (lat < 30.016625)
	           {
	           if (poly[1326].contains(lat,lng)) return 8;
	           if (poly[1327].contains(lat,lng)) return 193;
	           else return 163;
	           }
	          else
	           {
	           if (poly[1328].contains(lat,lng)) return 193;
	           if (poly[1329].contains(lat,lng)) return 344;
	           else return 8;
	           }
	         else
	          {
	          if (poly[1330].contains(lat,lng)) return 193;
	          else return 163;
	          }
	        else
	         if (lng < 36.977676)
	          if (lat < 33.880208)
	           if (lng < 35.940775)
	            if (lat < 32.914313)
	             {
	             if (poly[1331].contains(lat,lng)) return 193;
	             if (poly[1332].contains(lat,lng)) return 344;
	             if (poly[1333].contains(lat,lng)) return 368;
	             else return 8;
	             }
	            else
	             {
	             if (poly[1334].contains(lat,lng)) return 8;
	             if (poly[1335].contains(lat,lng)) return 368;
	             if (poly[1336].contains(lat,lng)) return 368;
	             else return 404;
	             }
	           else
	            {
	            if (poly[1337].contains(lat,lng)) return 193;
	            if (poly[1338].contains(lat,lng)) return 404;
	            if (poly[1339].contains(lat,lng)) return 404;
	            else return 368;
	            }
	          else
	           {
	           if (poly[1340].contains(lat,lng)) return 404;
	           else return 368;
	           }
	         else
	          {
	          if (poly[1341].contains(lat,lng)) return 28;
	          if (poly[1342].contains(lat,lng)) return 28;
	          if (poly[1343].contains(lat,lng)) return 163;
	          if (poly[1344].contains(lat,lng)) return 368;
	          else return 193;
	          }
	       else
	        {
	        if (poly[1345].contains(lat,lng)) return 163;
	        if (poly[1346].contains(lat,lng)) return 193;
	        if (poly[1347].contains(lat,lng)) return 193;
	        if (poly[1348].contains(lat,lng)) return 368;
	        else return 28;
	        }
	}

	private static int call25(double lat, double lng)
	{
	 if (lng < 15.805111)
	  if (lat < 41.371582)
	   if (lat < 37.543915)
	    if (lng < 11.135750)
	     if (lng < 8.672028)
	      if (lat < 36.824917)
	       {
	       if (poly[1349].contains(lat,lng)) return 318;
	       else return 380;
	       }
	      else
	       {
	       if (poly[1350].contains(lat,lng)) return 318;
	       else return 380;
	       }
	     else
	      return 318;
	    else
	     if (lng < 12.881139)
	      return 271;
	     else
	      if (lat < 36.082027)
	       return 302;
	      else
	       return 271;
	   else
	    return 271;
	  else
	   if (lat < 43.746029)
	    if (lng < 15.459583)
	     if (lng < 10.098917)
	      if (lng < 9.561556)
	       return 297;
	      else
	       return 271;
	     else
	      if (lng < 15.305507)
	       return 271;
	      else
	       if (lat < 42.829397)
	        return 30;
	       else
	        return 271;
	    else
	     if (lat < 42.126751)
	      return 271;
	     else
	      return 30;
	   else
	    if (lng < 13.188644)
	     if (lng < 8.430425)
	      {
	      if (poly[1351].contains(lat,lng)) return 297;
	      else return 271;
	      }
	     else
	      if (lng < 9.857306)
	       return 271;
	      else
	       {
	       if (poly[1352].contains(lat,lng)) return 313;
	       else return 271;
	       }
	    else
	     return 30;
	 else
	  if (lat < 39.179638)
	   if (lng < 17.213722)
	    return 271;
	   else
	    return 152;
	  else
	   if (lng < 17.754278)
	    if (lat < 42.806278)
	     if (lat < 41.951027)
	      return 271;
	     else
	      return 30;
	    else
	     if (lng < 16.180721)
	      return 30;
	     else
	      if (lat < 43.099861)
	       if (lng < 17.201416)
	        return 30;
	       else
	        {
	        if (poly[1353].contains(lat,lng)) return 348;
	        else return 30;
	        }
	      else
	       if (lat < 43.236000)
	        if (lng < 17.194445)
	         return 30;
	        else
	         {
	         if (poly[1354].contains(lat,lng)) return 348;
	         else return 30;
	         }
	       else
	        {
	        if (poly[1355].contains(lat,lng)) return 348;
	        else return 30;
	        }
	   else
	    if (lat < 40.728896)
	     if (lat < 39.251167)
	      return 152;
	     else
	      if (lng < 19.436611)
	       if (lng < 19.305916)
	        if (lng < 18.513445)
	         return 271;
	        else
	         return 75;
	       else
	        if (lat < 39.873859)
	         return 152;
	        else
	         return 75;
	      else
	       if (lng < 19.598167)
	        if (lat < 39.905056)
	         return 152;
	        else
	         return 75;
	       else
	        {
	        if (poly[1356].contains(lat,lng)) return 75;
	        else return 152;
	        }
	    else
	     if (lng < 18.530390)
	      if (lng < 17.975721)
	       if (lat < 42.791241)
	        return 30;
	       else
	        {
	        if (poly[1357].contains(lat,lng)) return 348;
	        else return 30;
	        }
	      else
	       if (lat < 42.622113)
	        {
	        if (poly[1358].contains(lat,lng)) return 30;
	        if (poly[1359].contains(lat,lng)) return 348;
	        else return 212;
	        }
	       else
	        if (lng < 18.417428)
	         {
	         if (poly[1360].contains(lat,lng)) return 348;
	         else return 30;
	         }
	        else
	         {
	         if (poly[1361].contains(lat,lng)) return 212;
	         else return 348;
	         }
	     else
	      if (lng < 20.358187)
	       if (lng < 18.576824)
	        if (lat < 42.610511)
	         if (lat < 42.431325)
	          return 212;
	         else
	          {
	          if (poly[1362].contains(lat,lng)) return 348;
	          else return 212;
	          }
	        else
	         if (lat < 42.642925)
	          {
	          if (poly[1363].contains(lat,lng)) return 348;
	          else return 212;
	          }
	         else
	          if (lat < 42.735220)
	           {
	           if (poly[1364].contains(lat,lng)) return 348;
	           else return 212;
	           }
	          else
	           {
	           if (poly[1365].contains(lat,lng)) return 348;
	           else return 212;
	           }
	       else
	        if (lat < 42.465448)
	         {
	         if (poly[1366].contains(lat,lng)) return 209;
	         if (poly[1367].contains(lat,lng)) return 212;
	         if (poly[1368].contains(lat,lng)) return 212;
	         else return 75;
	         }
	        else
	         if (lng < 19.467505)
	          if (lat < 43.333724)
	           {
	           if (poly[1369].contains(lat,lng)) return 348;
	           if (poly[1370].contains(lat,lng)) return 348;
	           else return 212;
	           }
	          else
	           {
	           if (poly[1371].contains(lat,lng)) return 209;
	           if (poly[1372].contains(lat,lng)) return 209;
	           if (poly[1373].contains(lat,lng)) return 209;
	           if (poly[1374].contains(lat,lng)) return 212;
	           if (poly[1375].contains(lat,lng)) return 212;
	           else return 348;
	           }
	         else
	          if (lat < 43.333724)
	           {
	           if (poly[1376].contains(lat,lng)) return 75;
	           if (poly[1377].contains(lat,lng)) return 209;
	           else return 212;
	           }
	          else
	           {
	           if (poly[1378].contains(lat,lng)) return 212;
	           if (poly[1379].contains(lat,lng)) return 348;
	           if (poly[1380].contains(lat,lng)) return 348;
	           else return 209;
	           }
	      else
	       if (lat < 42.374481)
	        if (lng < 21.975260)
	         if (lat < 41.551688)
	          {
	          if (poly[1381].contains(lat,lng)) return 75;
	          if (poly[1382].contains(lat,lng)) return 152;
	          else return 337;
	          }
	         else
	          if (lng < 21.166723)
	           {
	           if (poly[1383].contains(lat,lng)) return 75;
	           if (poly[1384].contains(lat,lng)) return 209;
	           else return 337;
	           }
	          else
	           {
	           if (poly[1385].contains(lat,lng)) return 337;
	           else return 209;
	           }
	        else
	         if (lat < 41.551688)
	          {
	          if (poly[1386].contains(lat,lng)) return 337;
	          if (poly[1387].contains(lat,lng)) return 340;
	          else return 152;
	          }
	         else
	          {
	          if (poly[1388].contains(lat,lng)) return 209;
	          if (poly[1389].contains(lat,lng)) return 340;
	          else return 337;
	          }
	       else
	        if (lng < 21.975260)
	         return 209;
	        else
	         if (lat < 43.288240)
	          {
	          if (poly[1390].contains(lat,lng)) return 340;
	          else return 209;
	          }
	         else
	          {
	          if (poly[1391].contains(lat,lng)) return 209;
	          if (poly[1392].contains(lat,lng)) return 266;
	          else return 340;
	          }
	}

	private static int call26(double lat, double lng)
	{
	 if (lat < 49.603168)
	  if (lng < 24.644997)
	   if (lat < 46.189445)
	    if (lng < 22.047057)
	     if (lng < 20.748087)
	      {
	      if (poly[1393].contains(lat,lng)) return 198;
	      if (poly[1394].contains(lat,lng)) return 198;
	      if (poly[1395].contains(lat,lng)) return 266;
	      else return 209;
	      }
	     else
	      {
	      if (poly[1396].contains(lat,lng)) return 266;
	      else return 209;
	      }
	    else
	     {
	     if (poly[1397].contains(lat,lng)) return 209;
	     if (poly[1398].contains(lat,lng)) return 340;
	     else return 266;
	     }
	   else
	    if (lat < 46.192638)
	     if (lng < 20.503389)
	      {
	      if (poly[1399].contains(lat,lng)) return 266;
	      else return 198;
	      }
	     else
	      if (lng < 20.511267)
	       {
	       if (poly[1400].contains(lat,lng)) return 266;
	       else return 198;
	       }
	      else
	       {
	       if (poly[1401].contains(lat,lng)) return 266;
	       else return 198;
	       }
	    else
	     if (lng < 22.047057)
	      if (lat < 47.897903)
	       {
	       if (poly[1402].contains(lat,lng)) return 266;
	       else return 198;
	       }
	      else
	       if (lng < 20.748087)
	        if (lat < 48.750536)
	         {
	         if (poly[1403].contains(lat,lng)) return 198;
	         else return 162;
	         }
	        else
	         {
	         if (poly[1404].contains(lat,lng)) return 186;
	         else return 162;
	         }
	       else
	        {
	        if (poly[1405].contains(lat,lng)) return 186;
	        if (poly[1406].contains(lat,lng)) return 198;
	        else return 162;
	        }
	     else
	      if (lat < 47.897903)
	       {
	       if (poly[1407].contains(lat,lng)) return 198;
	       else return 266;
	       }
	      else
	       if (lng < 23.346027)
	        if (lat < 48.750536)
	         {
	         if (poly[1408].contains(lat,lng)) return 162;
	         if (poly[1409].contains(lat,lng)) return 198;
	         if (poly[1410].contains(lat,lng)) return 266;
	         if (poly[1411].contains(lat,lng)) return 275;
	         else return 387;
	         }
	        else
	         {
	         if (poly[1412].contains(lat,lng)) return 162;
	         if (poly[1413].contains(lat,lng)) return 275;
	         if (poly[1414].contains(lat,lng)) return 387;
	         if (poly[1415].contains(lat,lng)) return 387;
	         else return 186;
	         }
	       else
	        {
	        if (poly[1416].contains(lat,lng)) return 266;
	        if (poly[1417].contains(lat,lng)) return 275;
	        if (poly[1418].contains(lat,lng)) return 275;
	        else return 387;
	        }
	  else
	   if (lat < 44.848141)
	    return 266;
	   else
	    if (lng < 27.390221)
	     if (lat < 47.225655)
	      return 266;
	     else
	      if (lng < 26.017609)
	       {
	       if (poly[1419].contains(lat,lng)) return 275;
	       else return 266;
	       }
	      else
	       if (lat < 48.414412)
	        if (lng < 26.703915)
	         {
	         if (poly[1420].contains(lat,lng)) return 266;
	         if (poly[1421].contains(lat,lng)) return 294;
	         else return 275;
	         }
	        else
	         if (lat < 47.820033)
	          {
	          if (poly[1422].contains(lat,lng)) return 294;
	          else return 266;
	          }
	         else
	          {
	          if (poly[1423].contains(lat,lng)) return 266;
	          if (poly[1424].contains(lat,lng)) return 275;
	          if (poly[1425].contains(lat,lng)) return 275;
	          if (poly[1426].contains(lat,lng)) return 275;
	          if (poly[1427].contains(lat,lng)) return 275;
	          else return 294;
	          }
	       else
	        {
	        if (poly[1428].contains(lat,lng)) return 294;
	        if (poly[1429].contains(lat,lng)) return 294;
	        if (poly[1430].contains(lat,lng)) return 294;
	        else return 275;
	        }
	    else
	     if (lat < 47.225655)
	      if (lng < 28.762833)
	       if (lat < 46.036898)
	        {
	        if (poly[1431].contains(lat,lng)) return 266;
	        if (poly[1432].contains(lat,lng)) return 275;
	        else return 294;
	        }
	       else
	        {
	        if (poly[1433].contains(lat,lng)) return 294;
	        else return 266;
	        }
	      else
	       if (lat < 46.036898)
	        {
	        if (poly[1434].contains(lat,lng)) return 266;
	        if (poly[1435].contains(lat,lng)) return 294;
	        else return 275;
	        }
	       else
	        if (lng < 29.449139)
	         {
	         if (poly[1436].contains(lat,lng)) return 275;
	         else return 294;
	         }
	        else
	         {
	         if (poly[1437].contains(lat,lng)) return 294;
	         else return 275;
	         }
	     else
	      if (lng < 28.762833)
	       {
	       if (poly[1438].contains(lat,lng)) return 266;
	       if (poly[1439].contains(lat,lng)) return 275;
	       else return 294;
	       }
	      else
	       {
	       if (poly[1440].contains(lat,lng)) return 294;
	       else return 275;
	       }
	 else
	  if (lng < 19.892860)
	   if (lat < 54.454834)
	    if (lng < 19.662167)
	     return 186;
	    else
	     {
	     if (poly[1441].contains(lat,lng)) return 175;
	     else return 186;
	     }
	   else
	    return 175;
	  else
	   if (lng < 25.014153)
	    if (lat < 52.221153)
	     if (lng < 22.453506)
	      return 186;
	     else
	      if (lat < 50.912161)
	       {
	       if (poly[1442].contains(lat,lng)) return 275;
	       else return 186;
	       }
	      else
	       if (lng < 23.733829)
	        {
	        if (poly[1443].contains(lat,lng)) return 275;
	        if (poly[1444].contains(lat,lng)) return 399;
	        else return 186;
	        }
	       else
	        {
	        if (poly[1445].contains(lat,lng)) return 186;
	        if (poly[1446].contains(lat,lng)) return 399;
	        else return 275;
	        }
	    else
	     if (lng < 22.453506)
	      {
	      if (poly[1447].contains(lat,lng)) return 186;
	      else return 175;
	      }
	     else
	      if (lat < 53.530146)
	       {
	       if (poly[1448].contains(lat,lng)) return 186;
	       else return 399;
	       }
	      else
	       if (lng < 23.733829)
	        {
	        if (poly[1449].contains(lat,lng)) return 175;
	        if (poly[1450].contains(lat,lng)) return 186;
	        if (poly[1451].contains(lat,lng)) return 399;
	        else return 53;
	        }
	       else
	        {
	        if (poly[1452].contains(lat,lng)) return 399;
	        else return 53;
	        }
	   else
	    if (lat < 52.221153)
	     if (lng < 27.574799)
	      {
	      if (poly[1453].contains(lat,lng)) return 399;
	      else return 275;
	      }
	     else
	      {
	      if (poly[1454].contains(lat,lng)) return 275;
	      else return 399;
	      }
	    else
	     {
	     if (poly[1455].contains(lat,lng)) return 53;
	     else return 399;
	     }
	}

	private static int call27(double lat, double lng)
	{
	 if (lng < 30.135445)
	  if (lng < 19.449118)
	   if (lat < 46.068307)
	    if (lng < 16.237659)
	     if (lat < 44.530358)
	      {
	      if (poly[1456].contains(lat,lng)) return 348;
	      else return 30;
	      }
	     else
	      {
	      if (poly[1457].contains(lat,lng)) return 30;
	      else return 348;
	      }
	    else
	     if (lng < 17.843388)
	      {
	      if (poly[1458].contains(lat,lng)) return 198;
	      if (poly[1459].contains(lat,lng)) return 348;
	      else return 30;
	      }
	     else
	      if (lat < 45.135153)
	       if (lng < 18.646253)
	        {
	        if (poly[1460].contains(lat,lng)) return 348;
	        else return 30;
	        }
	       else
	        if (lat < 44.668577)
	         {
	         if (poly[1461].contains(lat,lng)) return 348;
	         else return 209;
	         }
	        else
	         {
	         if (poly[1462].contains(lat,lng)) return 30;
	         if (poly[1463].contains(lat,lng)) return 30;
	         if (poly[1464].contains(lat,lng)) return 348;
	         else return 209;
	         }
	      else
	       if (lng < 18.646253)
	        {
	        if (poly[1465].contains(lat,lng)) return 198;
	        if (poly[1466].contains(lat,lng)) return 348;
	        if (poly[1467].contains(lat,lng)) return 348;
	        else return 30;
	        }
	       else
	        if (lat < 45.601730)
	         {
	         if (poly[1468].contains(lat,lng)) return 30;
	         else return 209;
	         }
	        else
	         {
	         if (poly[1469].contains(lat,lng)) return 30;
	         if (poly[1470].contains(lat,lng)) return 198;
	         else return 209;
	         }
	   else
	    if (lat < 46.877918)
	     {
	     if (poly[1471].contains(lat,lng)) return 30;
	     if (poly[1472].contains(lat,lng)) return 177;
	     if (poly[1473].contains(lat,lng)) return 276;
	     else return 198;
	     }
	    else
	     if (lat < 50.858528)
	      if (lat < 48.868223)
	       if (lng < 17.780503)
	        if (lat < 47.873071)
	         {
	         if (poly[1474].contains(lat,lng)) return 162;
	         if (poly[1475].contains(lat,lng)) return 276;
	         else return 198;
	         }
	        else
	         {
	         if (poly[1476].contains(lat,lng)) return 198;
	         if (poly[1477].contains(lat,lng)) return 220;
	         if (poly[1478].contains(lat,lng)) return 276;
	         else return 162;
	         }
	       else
	        {
	        if (poly[1479].contains(lat,lng)) return 198;
	        else return 162;
	        }
	      else
	       if (lng < 17.780503)
	        if (lat < 49.863376)
	         {
	         if (poly[1480].contains(lat,lng)) return 162;
	         else return 220;
	         }
	        else
	         if (lng < 16.946196)
	          {
	          if (poly[1481].contains(lat,lng)) return 220;
	          if (poly[1482].contains(lat,lng)) return 220;
	          else return 186;
	          }
	         else
	          {
	          if (poly[1483].contains(lat,lng)) return 220;
	          else return 186;
	          }
	       else
	        if (lat < 49.863376)
	         {
	         if (poly[1484].contains(lat,lng)) return 186;
	         if (poly[1485].contains(lat,lng)) return 220;
	         else return 162;
	         }
	        else
	         {
	         if (poly[1486].contains(lat,lng)) return 220;
	         else return 186;
	         }
	     else
	      return 186;
	  else
	   return call26(lat,lng);
	 else
	  if (lng < 32.191639)
	   if (lat < 46.578388)
	    return 275;
	   else
	    if (lat < 53.219612)
	     if (lat < 49.899000)
	      return 275;
	     else
	      if (lat < 51.559306)
	       {
	       if (poly[1487].contains(lat,lng)) return 399;
	       else return 275;
	       }
	      else
	       if (lng < 31.163542)
	        {
	        if (poly[1488].contains(lat,lng)) return 275;
	        else return 399;
	        }
	       else
	        {
	        if (poly[1489].contains(lat,lng)) return 183;
	        if (poly[1490].contains(lat,lng)) return 275;
	        else return 399;
	        }
	    else
	     {
	     if (poly[1491].contains(lat,lng)) return 183;
	     else return 399;
	     }
	  else
	   if (lng < 33.260555)
	    if (lat < 46.068832)
	     if (lat < 45.807167)
	      return 197;
	     else
	      return 275;
	    else
	     if (lat < 46.515057)
	      return 275;
	     else
	      if (lat < 52.369362)
	       {
	       if (poly[1492].contains(lat,lng)) return 183;
	       else return 275;
	       }
	      else
	       {
	       if (poly[1493].contains(lat,lng)) return 399;
	       else return 183;
	       }
	   else
	    if (lng < 37.223877)
	     if (lat < 45.442917)
	      if (lng < 36.583805)
	       {
	       if (poly[1494].contains(lat,lng)) return 183;
	       else return 197;
	       }
	      else
	       {
	       if (poly[1495].contains(lat,lng)) return 197;
	       else return 183;
	       }
	     else
	      if (lat < 45.476776)
	       return 197;
	      else
	       if (lat < 48.130928)
	        if (lng < 35.246422)
	         if (lat < 46.282494)
	          if (lng < 34.253489)
	           {
	           if (poly[1496].contains(lat,lng)) return 197;
	           else return 275;
	           }
	          else
	           {
	           if (poly[1497].contains(lat,lng)) return 146;
	           if (poly[1498].contains(lat,lng)) return 197;
	           else return 275;
	           }
	         else
	          if (lng < 34.253489)
	           {
	           if (poly[1499].contains(lat,lng)) return 146;
	           else return 275;
	           }
	          else
	           if (lat < 47.206711)
	            {
	            if (poly[1500].contains(lat,lng)) return 275;
	            else return 146;
	            }
	           else
	            {
	            if (poly[1501].contains(lat,lng)) return 146;
	            else return 275;
	            }
	        else
	         if (lng < 36.235149)
	          {
	          if (poly[1502].contains(lat,lng)) return 275;
	          else return 146;
	          }
	         else
	          {
	          if (poly[1503].contains(lat,lng)) return 275;
	          if (poly[1504].contains(lat,lng)) return 275;
	          else return 146;
	          }
	       else
	        if (lat < 51.485033)
	         {
	         if (poly[1505].contains(lat,lng)) return 183;
	         else return 275;
	         }
	        else
	         {
	         if (poly[1506].contains(lat,lng)) return 275;
	         else return 183;
	         }
	    else
	     if (lat < 46.770611)
	      return 183;
	     else
	      if (lng < 40.207390)
	       if (lat < 50.804874)
	        if (lat < 48.787743)
	         if (lng < 38.715633)
	          {
	          if (poly[1507].contains(lat,lng)) return 183;
	          if (poly[1508].contains(lat,lng)) return 183;
	          else return 275;
	          }
	         else
	          {
	          if (poly[1509].contains(lat,lng)) return 275;
	          else return 183;
	          }
	        else
	         if (lng < 38.715633)
	          {
	          if (poly[1510].contains(lat,lng)) return 275;
	          else return 183;
	          }
	         else
	          {
	          if (poly[1511].contains(lat,lng)) return 275;
	          else return 183;
	          }
	       else
	        return 183;
	      else
	       if (lat < 50.804874)
	        if (lat < 48.787743)
	         {
	         if (poly[1512].contains(lat,lng)) return 99;
	         else return 183;
	         }
	        else
	         {
	         if (poly[1513].contains(lat,lng)) return 183;
	         else return 99;
	         }
	       else
	        {
	        if (poly[1514].contains(lat,lng)) return 99;
	        else return 183;
	        }
	}

	private static int call28(double lat, double lng)
	{
	 if (lng < 12.734445)
	  if (lng < 11.344611)
	   if (lat < 54.996582)
	    if (lat < 54.084915)
	     if (lat < 49.066696)
	      if (lat < 47.805332)
	       if (lat < 46.995804)
	        if (lng < 11.324862)
	         if (lng < 9.479653)
	          {
	          if (poly[1515].contains(lat,lng)) return 172;
	          else return 271;
	          }
	         else
	          if (lat < 45.598902)
	           return 271;
	          else
	           if (lng < 10.402257)
	            {
	            if (poly[1516].contains(lat,lng)) return 271;
	            if (poly[1517].contains(lat,lng)) return 271;
	            if (poly[1518].contains(lat,lng)) return 271;
	            if (poly[1519].contains(lat,lng)) return 276;
	            if (poly[1520].contains(lat,lng)) return 276;
	            else return 172;
	            }
	           else
	            {
	            if (poly[1521].contains(lat,lng)) return 172;
	            if (poly[1522].contains(lat,lng)) return 172;
	            if (poly[1523].contains(lat,lng)) return 172;
	            if (poly[1524].contains(lat,lng)) return 276;
	            else return 271;
	            }
	        else
	         {
	         if (poly[1525].contains(lat,lng)) return 276;
	         else return 271;
	         }
	       else
	        if (lng < 8.721278)
	         {
	         if (poly[1526].contains(lat,lng)) return 6;
	         if (poly[1527].contains(lat,lng)) return 6;
	         else return 172;
	         }
	        else
	         if (lng < 10.032945)
	          if (lng < 9.377111)
	           {
	           if (poly[1528].contains(lat,lng)) return 172;
	           else return 6;
	           }
	          else
	           {
	           if (poly[1529].contains(lat,lng)) return 6;
	           if (poly[1530].contains(lat,lng)) return 78;
	           if (poly[1531].contains(lat,lng)) return 172;
	           else return 276;
	           }
	         else
	          {
	          if (poly[1532].contains(lat,lng)) return 6;
	          if (poly[1533].contains(lat,lng)) return 172;
	          else return 276;
	          }
	      else
	       {
	       if (poly[1534].contains(lat,lng)) return 297;
	       else return 6;
	       }
	     else
	      return 6;
	    else
	     if (lat < 54.716026)
	      return 6;
	     else
	      if (lng < 10.012129)
	       if (lng < 8.598500)
	        return 6;
	       else
	        {
	        if (poly[1535].contains(lat,lng)) return 6;
	        else return 319;
	        }
	      else
	       return 319;
	   else
	    if (lng < 8.569390)
	     if (lat < 57.082884)
	      if (lat < 55.045019)
	       return 6;
	      else
	       return 319;
	     else
	      return 355;
	    else
	     return 319;
	  else
	   if (lat < 57.108582)
	    if (lat < 54.485249)
	     if (lat < 45.546391)
	      return 271;
	     else
	      if (lat < 50.416000)
	       if (lat < 47.744141)
	        if (lat < 47.095196)
	         {
	         if (poly[1536].contains(lat,lng)) return 276;
	         if (poly[1537].contains(lat,lng)) return 276;
	         else return 271;
	         }
	        else
	         if (lng < 12.253805)
	          {
	          if (poly[1538].contains(lat,lng)) return 6;
	          else return 276;
	          }
	         else
	          {
	          if (poly[1539].contains(lat,lng)) return 6;
	          else return 276;
	          }
	       else
	        {
	        if (poly[1540].contains(lat,lng)) return 220;
	        else return 6;
	        }
	      else
	       return 6;
	    else
	     if (lng < 12.237861)
	      return 319;
	     else
	      if (lat < 55.698891)
	       return 319;
	      else
	       if (lat < 56.120609)
	        if (lng < 12.635111)
	         {
	         if (poly[1541].contains(lat,lng)) return 375;
	         else return 319;
	         }
	        else
	         return 375;
	       else
	        return 375;
	   else
	    return 375;
	 else
	  if (lat < 54.839138)
	   if (lng < 16.111889)
	    if (lat < 45.246277)
	     if (lng < 15.525750)
	      return 30;
	     else
	      {
	      if (poly[1542].contains(lat,lng)) return 348;
	      else return 30;
	      }
	    else
	     if (lat < 49.406637)
	      if (lat < 46.871498)
	       if (lng < 13.369805)
	        if (lat < 45.721138)
	         return 271;
	        else
	         {
	         if (poly[1543].contains(lat,lng)) return 276;
	         else return 271;
	         }
	       else
	        if (lng < 14.740847)
	         if (lat < 46.058887)
	          if (lng < 14.055326)
	           {
	           if (poly[1544].contains(lat,lng)) return 30;
	           if (poly[1545].contains(lat,lng)) return 177;
	           else return 271;
	           }
	          else
	           {
	           if (poly[1546].contains(lat,lng)) return 177;
	           else return 30;
	           }
	         else
	          {
	          if (poly[1547].contains(lat,lng)) return 271;
	          if (poly[1548].contains(lat,lng)) return 276;
	          else return 177;
	          }
	        else
	         {
	         if (poly[1549].contains(lat,lng)) return 30;
	         if (poly[1550].contains(lat,lng)) return 276;
	         else return 177;
	         }
	      else
	       if (lng < 14.423167)
	        if (lat < 48.139068)
	         {
	         if (poly[1551].contains(lat,lng)) return 276;
	         else return 6;
	         }
	        else
	         if (lng < 13.578806)
	          {
	          if (poly[1552].contains(lat,lng)) return 220;
	          if (poly[1553].contains(lat,lng)) return 276;
	          else return 6;
	          }
	         else
	          {
	          if (poly[1554].contains(lat,lng)) return 6;
	          if (poly[1555].contains(lat,lng)) return 220;
	          else return 276;
	          }
	       else
	        {
	        if (poly[1556].contains(lat,lng)) return 276;
	        else return 220;
	        }
	     else
	      if (lat < 54.279383)
	       if (lng < 13.715182)
	        if (lat < 50.721819)
	         {
	         if (poly[1557].contains(lat,lng)) return 6;
	         else return 220;
	         }
	        else
	         return 6;
	       else
	        if (lat < 54.141001)
	         if (lat < 51.058887)
	          if (lng < 14.913535)
	           {
	           if (poly[1558].contains(lat,lng)) return 6;
	           if (poly[1559].contains(lat,lng)) return 186;
	           else return 220;
	           }
	          else
	           {
	           if (poly[1560].contains(lat,lng)) return 6;
	           if (poly[1561].contains(lat,lng)) return 220;
	           else return 186;
	           }
	         else
	          if (lng < 14.358836)
	           if (lat < 53.741001)
	            if (lat < 53.088120)
	             {
	             if (poly[1562].contains(lat,lng)) return 186;
	             else return 6;
	             }
	            else
	             {
	             if (poly[1563].contains(lat,lng)) return 186;
	             else return 6;
	             }
	           else
	            if (lng < 14.092113)
	             return 6;
	            else
	             if (lat < 53.759693)
	              return 6;
	             else
	              if (lng < 14.224361)
	               {
	               if (poly[1564].contains(lat,lng)) return 186;
	               else return 6;
	               }
	              else
	               return 186;
	          else
	           if (lat < 53.467436)
	            if (lat < 52.747607)
	             {
	             if (poly[1565].contains(lat,lng)) return 6;
	             else return 186;
	             }
	            else
	             {
	             if (poly[1566].contains(lat,lng)) return 6;
	             else return 186;
	             }
	           else
	            return 186;
	        else
	         if (lng < 13.834655)
	          return 6;
	         else
	          return 186;
	      else
	       return 6;
	   else
	    return call27(lat,lng);
	  else
	   if (lng < 16.781500)
	    if (lat < 55.673500)
	     if (lng < 12.792528)
	      return 319;
	     else
	      if (lat < 55.318626)
	       return 319;
	      else
	       return 375;
	    else
	     return 375;
	   else
	    if (lng < 19.362333)
	     return 375;
	    else
	     if (lng < 23.271139)
	      if (lat < 57.761696)
	       if (lat < 55.724445)
	        {
	        if (poly[1567].contains(lat,lng)) return 53;
	        if (poly[1568].contains(lat,lng)) return 53;
	        else return 175;
	        }
	       else
	        {
	        if (poly[1569].contains(lat,lng)) return 53;
	        else return 332;
	        }
	      else
	       return 49;
	     else
	      if (lng < 24.279055)
	       if (lat < 57.167540)
	        {
	        if (poly[1570].contains(lat,lng)) return 53;
	        else return 332;
	        }
	       else
	        return 49;
	      else
	       if (lng < 33.739070)
	        if (lng < 29.009063)
	         if (lng < 26.644059)
	          if (lat < 56.576389)
	           {
	           if (poly[1571].contains(lat,lng)) return 332;
	           if (poly[1572].contains(lat,lng)) return 399;
	           if (poly[1573].contains(lat,lng)) return 399;
	           else return 53;
	           }
	          else
	           {
	           if (poly[1574].contains(lat,lng)) return 49;
	           else return 332;
	           }
	         else
	          if (lat < 56.576389)
	           if (lng < 27.826561)
	            {
	            if (poly[1575].contains(lat,lng)) return 53;
	            if (poly[1576].contains(lat,lng)) return 332;
	            else return 399;
	            }
	           else
	            {
	            if (poly[1577].contains(lat,lng)) return 332;
	            if (poly[1578].contains(lat,lng)) return 399;
	            else return 183;
	            }
	          else
	           {
	           if (poly[1579].contains(lat,lng)) return 49;
	           if (poly[1580].contains(lat,lng)) return 332;
	           else return 183;
	           }
	        else
	         {
	         if (poly[1581].contains(lat,lng)) return 399;
	         else return 183;
	         }
	       else
	        return 183;
	}

	private static int call29(double lat, double lng)
	{
	 if (lat < 35.812000)
	  if (lat < 16.225750)
	   if (lat < -0.790142)
	    return call23(lat,lng);
	   else
	    if (lng < 40.641304)
	     return call22(lat,lng);
	    else
	     if (lat < 14.911166)
	      if (lat < 12.995916)
	       if (lat < 12.845361)
	        if (lat < 4.667450)
	         if (lng < 42.239388)
	          {
	          if (poly[1582].contains(lat,lng)) return 122;
	          if (poly[1583].contains(lat,lng)) return 250;
	          if (poly[1584].contains(lat,lng)) return 250;
	          else return 350;
	          }
	         else
	          if (lat < -0.372982)
	           return 250;
	          else
	           {
	           if (poly[1585].contains(lat,lng)) return 350;
	           else return 250;
	           }
	        else
	         if (lat < 11.730194)
	          {
	          if (poly[1586].contains(lat,lng)) return 250;
	          if (poly[1587].contains(lat,lng)) return 350;
	          else return 311;
	          }
	         else
	          {
	          if (poly[1588].contains(lat,lng)) return 311;
	          if (poly[1589].contains(lat,lng)) return 350;
	          else return 149;
	          }
	       else
	        if (lng < 42.010485)
	         {
	         if (poly[1590].contains(lat,lng)) return 149;
	         else return 350;
	         }
	        else
	         return 149;
	      else
	       if (lng < 42.204277)
	        if (lng < 41.916477)
	         if (lat < 14.207822)
	          {
	          if (poly[1591].contains(lat,lng)) return 350;
	          else return 149;
	          }
	         else
	          return 149;
	        else
	         return 149;
	       else
	        if (lat < 13.649540)
	         if (lng < 42.592335)
	          return 149;
	         else
	          if (lat < 13.136590)
	           return 149;
	          else
	           return 215;
	        else
	         return 215;
	     else
	      if (lng < 41.297417)
	       return 149;
	      else
	       if (lat < 15.769584)
	        return 215;
	       else
	        if (lng < 42.415333)
	         return 163;
	        else
	         return 215;
	  else
	   return call24(lat,lng);
	 else
	  if (lat < 44.202000)
	   if (lng < 23.592333)
	    return call25(lat,lng);
	   else
	    if (lat < 37.252693)
	     if (lng < 27.216833)
	      return 152;
	     else
	      if (lng < 28.246389)
	       if (lat < 36.484890)
	        return 152;
	       else
	        if (lat < 36.736694)
	         if (lat < 36.640251)
	          if (lng < 27.874916)
	           if (lat < 36.533819)
	            return 152;
	           else
	            return 205;
	          else
	           return 205;
	         else
	          return 205;
	        else
	         if (lng < 27.357195)
	          if (lat < 36.917999)
	           return 152;
	          else
	           {
	           if (poly[1592].contains(lat,lng)) return 152;
	           else return 205;
	           }
	         else
	          return 205;
	      else
	       if (lng < 29.370945)
	        return 205;
	       else
	        if (lng < 30.410055)
	         if (lng < 29.647083)
	          if (lat < 36.115612)
	           return 152;
	          else
	           if (lng < 29.513166)
	            if (lat < 36.188656)
	             return 152;
	            else
	             return 205;
	           else
	            {
	            if (poly[1593].contains(lat,lng)) return 152;
	            else return 205;
	            }
	         else
	          return 205;
	        else
	         if (lng < 35.659962)
	          return 205;
	         else
	          if (lng < 35.722252)
	           return 205;
	          else
	           if (lng < 39.460669)
	            {
	            if (poly[1594].contains(lat,lng)) return 368;
	            else return 205;
	            }
	           else
	            {
	            if (poly[1595].contains(lat,lng)) return 28;
	            if (poly[1596].contains(lat,lng)) return 205;
	            if (poly[1597].contains(lat,lng)) return 205;
	            else return 368;
	            }
	    else
	     if (lat < 38.613083)
	      if (lng < 26.165445)
	       return 152;
	      else
	       if (lat < 37.476891)
	        if (lng < 27.104305)
	         return 152;
	        else
	         if (lng < 27.201663)
	          return 205;
	         else
	          if (lng < 42.327971)
	           {
	           if (poly[1598].contains(lat,lng)) return 368;
	           else return 205;
	           }
	          else
	           {
	           if (poly[1599].contains(lat,lng)) return 28;
	           else return 205;
	           }
	       else
	        if (lng < 26.569361)
	         if (lat < 37.694279)
	          return 152;
	         else
	          if (lng < 26.294640)
	           if (lat < 38.379166)
	            return 205;
	           else
	            return 152;
	          else
	           return 205;
	        else
	         if (lat < 37.814610)
	          if (lng < 26.808306)
	           return 152;
	          else
	           if (lng < 27.069723)
	            {
	            if (poly[1600].contains(lat,lng)) return 205;
	            else return 152;
	            }
	           else
	            return 205;
	         else
	          return 205;
	     else
	      if (lat < 39.563000)
	       if (lng < 25.845306)
	        return 152;
	       else
	        if (lat < 38.929779)
	         return 205;
	        else
	         if (lat < 39.393970)
	          if (lng < 26.603834)
	           if (lat < 39.307919)
	            {
	            if (poly[1601].contains(lat,lng)) return 205;
	            else return 152;
	            }
	           else
	            if (lng < 26.501052)
	             return 152;
	            else
	             return 205;
	          else
	           if (lat < 39.053222)
	            if (lng < 26.709736)
	             return 152;
	            else
	             return 205;
	           else
	            return 205;
	         else
	          return 205;
	      else
	       if (lat < 40.575138)
	        if (lng < 25.700861)
	         if (lng < 25.448389)
	          return 152;
	         else
	          if (lat < 40.286988)
	           return 152;
	          else
	           return 205;
	        else
	         return 205;
	       else
	        if (lng < 28.677111)
	         if (lat < 42.107613)
	          if (lng < 25.151501)
	           if (lat < 41.008026)
	            return 152;
	           else
	            {
	            if (poly[1602].contains(lat,lng)) return 340;
	            else return 152;
	            }
	          else
	           if (lat < 40.677166)
	            return 205;
	           else
	            if (lng < 26.914306)
	             if (lng < 26.032903)
	              {
	              if (poly[1603].contains(lat,lng)) return 340;
	              else return 152;
	              }
	             else
	              if (lat < 41.392389)
	               {
	               if (poly[1604].contains(lat,lng)) return 205;
	               if (poly[1605].contains(lat,lng)) return 340;
	               else return 152;
	               }
	              else
	               {
	               if (poly[1606].contains(lat,lng)) return 152;
	               if (poly[1607].contains(lat,lng)) return 205;
	               else return 340;
	               }
	            else
	             {
	             if (poly[1608].contains(lat,lng)) return 340;
	             else return 205;
	             }
	         else
	          {
	          if (poly[1609].contains(lat,lng)) return 266;
	          else return 340;
	          }
	        else
	         if (lng < 29.149529)
	          return 205;
	         else
	          if (lat < 42.101807)
	           {
	           if (poly[1610].contains(lat,lng)) return 47;
	           else return 205;
	           }
	          else
	           {
	           if (poly[1611].contains(lat,lng)) return 183;
	           else return 47;
	           }
	  else
	   return call28(lat,lng);
	}

	private static int call30(double lat, double lng)
	{
	 if (lat < 63.396389)
	  if (lng < 21.009916)
	   if (lat < 59.977943)
	    if (lng < 17.966583)
	     if (lng < 10.975583)
	      return 355;
	     else
	      if (lat < 58.863640)
	       return 375;
	      else
	       if (lng < 12.329235)
	        if (lng < 11.114695)
	         if (lat < 58.967918)
	          return 375;
	         else
	          return 355;
	        else
	         if (lat < 58.893585)
	          if (lng < 11.183945)
	           return 375;
	          else
	           {
	           if (poly[1612].contains(lat,lng)) return 355;
	           else return 375;
	           }
	         else
	          if (lng < 11.471500)
	           if (lat < 58.996334)
	            if (lng < 11.169000)
	             return 375;
	            else
	             {
	             if (poly[1613].contains(lat,lng)) return 355;
	             else return 375;
	             }
	           else
	            {
	            if (poly[1614].contains(lat,lng)) return 375;
	            else return 355;
	            }
	          else
	           {
	           if (poly[1615].contains(lat,lng)) return 375;
	           else return 355;
	           }
	       else
	        return 375;
	    else
	     if (lng < 19.327723)
	      return 375;
	     else
	      return 188;
	   else
	    if (lng < 18.917583)
	     if (lng < 17.174500)
	      if (lng < 9.462450)
	       return 355;
	      else
	       if (lng < 12.874028)
	        if (lat < 61.360279)
	         {
	         if (poly[1616].contains(lat,lng)) return 375;
	         else return 355;
	         }
	        else
	         {
	         if (poly[1617].contains(lat,lng)) return 375;
	         else return 355;
	         }
	       else
	        return 375;
	     else
	      return 375;
	    else
	     if (lat < 60.488861)
	      return 188;
	     else
	      if (lng < 19.269813)
	       return 375;
	      else
	       return 323;
	  else
	   if (lng < 21.913445)
	    if (lat < 60.574001)
	     if (lat < 60.311028)
	      if (lng < 21.548611)
	       if (lng < 21.075195)
	        return 188;
	       else
	        return 323;
	      else
	       if (lat < 58.498943)
	        return 49;
	       else
	        return 323;
	     else
	      if (lng < 21.119917)
	       return 188;
	      else
	       return 323;
	    else
	     return 323;
	   else
	    if (lng < 23.687056)
	     if (lat < 59.805805)
	      if (lat < 59.306194)
	       return 49;
	      else
	       return 323;
	     else
	      return 323;
	    else
	     if (lng < 26.170805)
	      if (lat < 59.687946)
	       return 49;
	      else
	       return 323;
	     else
	      if (lng < 27.435055)
	       if (lat < 60.110668)
	        if (lat < 59.587564)
	         return 49;
	        else
	         return 183;
	       else
	        return 323;
	      else
	       if (lng < 28.407444)
	        if (lat < 60.044083)
	         if (lat < 59.470806)
	          {
	          if (poly[1618].contains(lat,lng)) return 183;
	          else return 49;
	          }
	         else
	          return 183;
	        else
	         if (lat < 60.457390)
	          return 183;
	         else
	          if (lng < 27.854750)
	           if (lat < 60.525665)
	            if (lng < 27.745832)
	             return 323;
	            else
	             return 183;
	           else
	            {
	            if (poly[1619].contains(lat,lng)) return 183;
	            else return 323;
	            }
	          else
	           if (lat < 60.485054)
	            return 183;
	           else
	            {
	            if (poly[1620].contains(lat,lng)) return 323;
	            else return 183;
	            }
	       else
	        if (lat < 60.750500)
	         return 183;
	        else
	         {
	         if (poly[1621].contains(lat,lng)) return 323;
	         else return 183;
	         }
	 else
	  if (lat < 66.493835)
	   if (lng < 21.873028)
	    if (lng < 12.043694)
	     return 355;
	    else
	     if (lat < 65.745277)
	      if (lng < 20.798334)
	       if (lng < 14.575282)
	        if (lng < 12.227278)
	         if (lat < 63.632717)
	          {
	          if (poly[1622].contains(lat,lng)) return 375;
	          else return 355;
	          }
	         else
	          return 355;
	        else
	         if (lat < 65.221085)
	          {
	          if (poly[1623].contains(lat,lng)) return 375;
	          else return 355;
	          }
	         else
	          if (lng < 12.453417)
	           return 355;
	          else
	           {
	           if (poly[1624].contains(lat,lng)) return 375;
	           else return 355;
	           }
	       else
	        return 375;
	      else
	       if (lat < 63.464695)
	        return 323;
	       else
	        return 375;
	     else
	      if (lng < 13.186111)
	       return 355;
	      else
	       {
	       if (poly[1625].contains(lat,lng)) return 375;
	       else return 355;
	       }
	   else
	    if (lng < 33.387779)
	     if (lng < 22.965139)
	      if (lat < 65.340164)
	       if (lng < 21.957138)
	        return 375;
	       else
	        return 323;
	      else
	       return 375;
	     else
	      if (lng < 24.163279)
	       if (lat < 65.677864)
	        if (lat < 64.456893)
	         return 323;
	        else
	         return 375;
	       else
	        if (lng < 23.574472)
	         return 375;
	        else
	         if (lat < 65.717720)
	          return 375;
	         else
	          if (lat < 65.759140)
	           if (lng < 23.981722)
	            return 323;
	           else
	            return 375;
	          else
	           {
	           if (poly[1626].contains(lat,lng)) return 323;
	           else return 375;
	           }
	      else
	       if (lng < 25.360001)
	        return 323;
	       else
	        {
	        if (poly[1627].contains(lat,lng)) return 183;
	        else return 323;
	        }
	    else
	     return 183;
	  else
	   if (lat < 69.757027)
	    if (lng < 15.314777)
	     return 355;
	    else
	     if (lng < 32.329250)
	      if (lng < 16.755695)
	       if (lat < 67.925449)
	        {
	        if (poly[1628].contains(lat,lng)) return 375;
	        else return 355;
	        }
	       else
	        return 355;
	      else
	       if (lng < 18.256083)
	        if (lat < 68.558932)
	         {
	         if (poly[1629].contains(lat,lng)) return 375;
	         else return 355;
	         }
	        else
	         return 355;
	       else
	        if (lng < 26.171008)
	         if (lng < 19.024416)
	          if (lat < 68.584389)
	           if (lng < 18.417805)
	            {
	            if (poly[1630].contains(lat,lng)) return 355;
	            else return 375;
	            }
	           else
	            {
	            if (poly[1631].contains(lat,lng)) return 355;
	            else return 375;
	            }
	          else
	           return 355;
	         else
	          if (lng < 22.597712)
	           if (lng < 20.811064)
	            {
	            if (poly[1632].contains(lat,lng)) return 323;
	            if (poly[1633].contains(lat,lng)) return 375;
	            else return 355;
	            }
	           else
	            {
	            if (poly[1634].contains(lat,lng)) return 355;
	            if (poly[1635].contains(lat,lng)) return 375;
	            else return 323;
	            }
	          else
	           if (lng < 24.384360)
	            {
	            if (poly[1636].contains(lat,lng)) return 355;
	            if (poly[1637].contains(lat,lng)) return 375;
	            else return 323;
	            }
	           else
	            {
	            if (poly[1638].contains(lat,lng)) return 355;
	            else return 323;
	            }
	        else
	         if (lng < 30.945389)
	          if (lng < 28.558198)
	           {
	           if (poly[1639].contains(lat,lng)) return 183;
	           if (poly[1640].contains(lat,lng)) return 183;
	           else return 323;
	           }
	          else
	           if (lat < 68.125431)
	            {
	            if (poly[1641].contains(lat,lng)) return 323;
	            else return 183;
	            }
	           else
	            {
	            if (poly[1642].contains(lat,lng)) return 183;
	            if (poly[1643].contains(lat,lng)) return 183;
	            if (poly[1644].contains(lat,lng)) return 323;
	            if (poly[1645].contains(lat,lng)) return 323;
	            if (poly[1646].contains(lat,lng)) return 323;
	            else return 355;
	            }
	         else
	          return 183;
	     else
	      return 183;
	   else
	    if (lat < 76.436279)
	     if (lng < 21.929611)
	      if (lat < 70.395920)
	       return 355;
	      else
	       return 111;
	     else
	      if (lat < 70.096054)
	       if (lng < 30.098833)
	        if (lng < 28.792934)
	         if (lng < 22.034249)
	          return 355;
	         else
	          {
	          if (poly[1647].contains(lat,lng)) return 323;
	          else return 355;
	          }
	        else
	         return 355;
	       else
	        if (lng < 30.400282)
	         return 355;
	        else
	         if (lng < 30.856945)
	          {
	          if (poly[1648].contains(lat,lng)) return 183;
	          else return 355;
	          }
	         else
	          return 183;
	      else
	       return 355;
	    else
	     return 111;
	}

	private static int call31(double lat, double lng)
	{
	 if (lng < 97.198914)
	  if (lng < 93.360947)
	   if (lng < 92.525970)
	    if (lng < 57.844944)
	     if (lat < 12.250028)
	      if (lng < 43.600809)
	       if (lng < 43.416973)
	        if (lat < 11.495500)
	         if (lng < 43.254696)
	          {
	          if (poly[1649].contains(lat,lng)) return 311;
	          else return 250;
	          }
	         else
	          return 250;
	        else
	         return 311;
	       else
	        return 250;
	      else
	       if (lng < 51.284752)
	        return 250;
	       else
	        return 215;
	     else
	      if (lng < 54.530529)
	       if (lat < 12.714472)
	        if (lng < 43.389330)
	         if (lat < 12.619930)
	          return 215;
	         else
	          return 311;
	        else
	         return 215;
	       else
	        if (lat < 13.289861)
	         return 215;
	        else
	         if (lng < 48.864807)
	          {
	          if (poly[1650].contains(lat,lng)) return 163;
	          if (poly[1651].contains(lat,lng)) return 163;
	          else return 215;
	          }
	         else
	          {
	          if (poly[1652].contains(lat,lng)) return 163;
	          if (poly[1653].contains(lat,lng)) return 357;
	          else return 215;
	          }
	      else
	       return 357;
	    else
	     return 371;
	   else
	    if (lng < 93.133308)
	     return 371;
	    else
	     return 40;
	  else
	   if (lat < 14.848945)
	    if (lng < 93.385139)
	     return 40;
	    else
	     return 371;
	   else
	    return 40;
	 else
	  if (lng < 98.162170)
	   if (lat < 17.270240)
	    return 40;
	   else
	    {
	    if (poly[1654].contains(lat,lng)) return 270;
	    else return 40;
	    }
	  else
	   if (lat < 12.230139)
	    if (lng < 98.690804)
	     return 40;
	    else
	     if (lng < 102.511497)
	      if (lng < 99.668724)
	       if (lat < 11.815278)
	        {
	        if (poly[1655].contains(lat,lng)) return 270;
	        else return 40;
	        }
	       else
	        {
	        if (poly[1656].contains(lat,lng)) return 270;
	        else return 40;
	        }
	      else
	       return 270;
	     else
	      if (lng < 102.610809)
	       return 270;
	      else
	       if (lng < 103.077553)
	        if (lng < 102.919525)
	         {
	         if (poly[1657].contains(lat,lng)) return 251;
	         else return 270;
	         }
	        else
	         return 251;
	       else
	        if (lng < 103.105614)
	         return 251;
	        else
	         {
	         if (poly[1658].contains(lat,lng)) return 24;
	         else return 251;
	         }
	   else
	    if (lng < 98.564003)
	     if (lat < 13.640472)
	      return 40;
	     else
	      if (lat < 15.706820)
	       if (lat < 15.385889)
	        {
	        if (poly[1659].contains(lat,lng)) return 270;
	        else return 40;
	        }
	       else
	        {
	        if (poly[1660].contains(lat,lng)) return 270;
	        else return 40;
	        }
	      else
	       if (lat < 16.056181)
	        {
	        if (poly[1661].contains(lat,lng)) return 270;
	        else return 40;
	        }
	       else
	        {
	        if (poly[1662].contains(lat,lng)) return 270;
	        else return 40;
	        }
	    else
	     if (lng < 99.497317)
	      if (lat < 14.394400)
	       if (lat < 12.461389)
	        if (lng < 98.613220)
	         return 40;
	        else
	         {
	         if (poly[1663].contains(lat,lng)) return 270;
	         else return 40;
	         }
	       else
	        {
	        if (poly[1664].contains(lat,lng)) return 270;
	        else return 40;
	        }
	      else
	       if (lat < 15.666673)
	        {
	        if (poly[1665].contains(lat,lng)) return 40;
	        else return 270;
	        }
	       else
	        if (lat < 16.048701)
	         {
	         if (poly[1666].contains(lat,lng)) return 40;
	         else return 270;
	         }
	        else
	         {
	         if (poly[1667].contains(lat,lng)) return 40;
	         else return 270;
	         }
	     else
	      if (lng < 100.840173)
	       if (lat < 13.177694)
	        return 270;
	       else
	        {
	        if (poly[1668].contains(lat,lng)) return 265;
	        else return 270;
	        }
	      else
	       if (lng < 102.244331)
	        if (lat < 12.585389)
	         return 270;
	        else
	         if (lat < 16.076319)
	          return 270;
	         else
	          if (lat < 17.821784)
	           {
	           if (poly[1669].contains(lat,lng)) return 265;
	           else return 270;
	           }
	          else
	           {
	           if (poly[1670].contains(lat,lng)) return 265;
	           else return 270;
	           }
	       else
	        if (lat < 14.587611)
	         if (lng < 106.532585)
	          if (lng < 104.388458)
	           if (lat < 13.408875)
	            {
	            if (poly[1671].contains(lat,lng)) return 251;
	            else return 270;
	            }
	           else
	            {
	            if (poly[1672].contains(lat,lng)) return 251;
	            else return 270;
	            }
	          else
	           if (lat < 13.408875)
	            return 251;
	           else
	            if (lng < 105.460522)
	             {
	             if (poly[1673].contains(lat,lng)) return 265;
	             if (poly[1674].contains(lat,lng)) return 270;
	             else return 251;
	             }
	            else
	             {
	             if (poly[1675].contains(lat,lng)) return 251;
	             if (poly[1676].contains(lat,lng)) return 270;
	             else return 265;
	             }
	         else
	          if (lat < 12.279220)
	           {
	           if (poly[1677].contains(lat,lng)) return 24;
	           else return 251;
	           }
	          else
	           {
	           if (poly[1678].contains(lat,lng)) return 265;
	           else return 251;
	           }
	        else
	         if (lat < 17.077430)
	          if (lng < 104.715237)
	           return 270;
	          else
	           if (lat < 15.832521)
	            {
	            if (poly[1679].contains(lat,lng)) return 270;
	            else return 265;
	            }
	           else
	            {
	            if (poly[1680].contains(lat,lng)) return 24;
	            if (poly[1681].contains(lat,lng)) return 270;
	            else return 265;
	            }
	         else
	          if (lng < 104.681789)
	           if (lat < 18.322340)
	            {
	            if (poly[1682].contains(lat,lng)) return 270;
	            else return 265;
	            }
	           else
	            {
	            if (poly[1683].contains(lat,lng)) return 24;
	            if (poly[1684].contains(lat,lng)) return 270;
	            if (poly[1685].contains(lat,lng)) return 270;
	            else return 265;
	            }
	          else
	           if (lat < 18.322340)
	            {
	            if (poly[1686].contains(lat,lng)) return 265;
	            if (poly[1687].contains(lat,lng)) return 270;
	            else return 24;
	            }
	           else
	            {
	            if (poly[1688].contains(lat,lng)) return 265;
	            else return 24;
	            }
	}

	private static int call32(double lat, double lng)
	{
	 if (lat < 30.415028)
	  if (lng < 48.877104)
	   if (lat < 29.472639)
	    if (lng < 48.431473)
	     {
	     if (poly[1689].contains(lat,lng)) return 28;
	     if (poly[1690].contains(lat,lng)) return 163;
	     else return 231;
	     }
	    else
	     return 163;
	   else
	    if (lng < 47.971863)
	     if (lng < 44.344163)
	      {
	      if (poly[1691].contains(lat,lng)) return 163;
	      else return 28;
	      }
	     else
	      {
	      if (poly[1692].contains(lat,lng)) return 231;
	      else return 28;
	      }
	    else
	     if (lat < 29.903723)
	      return 231;
	     else
	      if (lng < 48.015431)
	       if (lat < 29.993876)
	        return 28;
	       else
	        return 231;
	      else
	       {
	       if (poly[1693].contains(lat,lng)) return 28;
	       if (poly[1694].contains(lat,lng)) return 55;
	       else return 231;
	       }
	  else
	   return 55;
	 else
	  if (lng < 48.533806)
	   if (lat < 41.016529)
	    if (lat < 39.785526)
	     if (lat < 37.350129)
	      if (lat < 33.882578)
	       if (lng < 45.866446)
	        {
	        if (poly[1695].contains(lat,lng)) return 55;
	        if (poly[1696].contains(lat,lng)) return 55;
	        else return 28;
	        }
	       else
	        if (lat < 32.148803)
	         {
	         if (poly[1697].contains(lat,lng)) return 55;
	         else return 28;
	         }
	        else
	         {
	         if (poly[1698].contains(lat,lng)) return 28;
	         if (poly[1699].contains(lat,lng)) return 28;
	         else return 55;
	         }
	      else
	       if (lng < 45.866446)
	        if (lat < 35.616353)
	         {
	         if (poly[1700].contains(lat,lng)) return 55;
	         else return 28;
	         }
	        else
	         if (lng < 44.532765)
	          {
	          if (poly[1701].contains(lat,lng)) return 205;
	          else return 28;
	          }
	         else
	          if (lat < 36.483241)
	           {
	           if (poly[1702].contains(lat,lng)) return 55;
	           else return 28;
	           }
	          else
	           {
	           if (poly[1703].contains(lat,lng)) return 28;
	           if (poly[1704].contains(lat,lng)) return 205;
	           else return 55;
	           }
	       else
	        {
	        if (poly[1705].contains(lat,lng)) return 28;
	        else return 55;
	        }
	     else
	      if (lng < 45.866446)
	       if (lng < 44.532765)
	        {
	        if (poly[1706].contains(lat,lng)) return 55;
	        else return 205;
	        }
	       else
	        if (lat < 38.567827)
	         {
	         if (poly[1707].contains(lat,lng)) return 205;
	         else return 55;
	         }
	        else
	         if (lng < 45.199605)
	          {
	          if (poly[1708].contains(lat,lng)) return 48;
	          if (poly[1709].contains(lat,lng)) return 55;
	          if (poly[1710].contains(lat,lng)) return 205;
	          else return 51;
	          }
	         else
	          {
	          if (poly[1711].contains(lat,lng)) return 48;
	          if (poly[1712].contains(lat,lng)) return 55;
	          else return 51;
	          }
	      else
	       if (lng < 47.200126)
	        if (lat < 38.567827)
	         return 55;
	        else
	         if (lng < 46.533286)
	          if (lat < 39.176677)
	           {
	           if (poly[1713].contains(lat,lng)) return 48;
	           if (poly[1714].contains(lat,lng)) return 55;
	           else return 51;
	           }
	          else
	           {
	           if (poly[1715].contains(lat,lng)) return 48;
	           else return 51;
	           }
	         else
	          {
	          if (poly[1716].contains(lat,lng)) return 48;
	          if (poly[1717].contains(lat,lng)) return 48;
	          if (poly[1718].contains(lat,lng)) return 48;
	          if (poly[1719].contains(lat,lng)) return 48;
	          if (poly[1720].contains(lat,lng)) return 48;
	          if (poly[1721].contains(lat,lng)) return 48;
	          if (poly[1722].contains(lat,lng)) return 55;
	          else return 51;
	          }
	       else
	        {
	        if (poly[1723].contains(lat,lng)) return 55;
	        else return 51;
	        }
	    else
	     if (lng < 45.244499)
	      if (lng < 44.698987)
	       {
	       if (poly[1724].contains(lat,lng)) return 48;
	       else return 205;
	       }
	      else
	       {
	       if (poly[1725].contains(lat,lng)) return 51;
	       else return 48;
	       }
	     else
	      {
	      if (poly[1726].contains(lat,lng)) return 51;
	      if (poly[1727].contains(lat,lng)) return 51;
	      if (poly[1728].contains(lat,lng)) return 51;
	      else return 48;
	      }
	   else
	    if (lat < 42.932384)
	     if (lng < 45.318278)
	      if (lat < 41.466140)
	       if (lng < 43.506565)
	        {
	        if (poly[1729].contains(lat,lng)) return 47;
	        if (poly[1730].contains(lat,lng)) return 48;
	        else return 205;
	        }
	       else
	        if (lng < 45.296813)
	         {
	         if (poly[1731].contains(lat,lng)) return 47;
	         if (poly[1732].contains(lat,lng)) return 51;
	         if (poly[1733].contains(lat,lng)) return 51;
	         else return 48;
	         }
	        else
	         if (lat < 41.035907)
	          {
	          if (poly[1734].contains(lat,lng)) return 51;
	          else return 48;
	          }
	         else
	          {
	          if (poly[1735].contains(lat,lng)) return 47;
	          else return 51;
	          }
	      else
	       {
	       if (poly[1736].contains(lat,lng)) return 183;
	       else return 47;
	       }
	     else
	      if (lat < 41.030472)
	       {
	       if (poly[1737].contains(lat,lng)) return 48;
	       else return 51;
	       }
	      else
	       if (lng < 46.926042)
	        if (lat < 41.981428)
	         if (lng < 46.122160)
	          {
	          if (poly[1738].contains(lat,lng)) return 47;
	          else return 51;
	          }
	         else
	          {
	          if (poly[1739].contains(lat,lng)) return 47;
	          if (poly[1740].contains(lat,lng)) return 183;
	          else return 51;
	          }
	        else
	         {
	         if (poly[1741].contains(lat,lng)) return 47;
	         else return 183;
	         }
	       else
	        {
	        if (poly[1742].contains(lat,lng)) return 51;
	        else return 183;
	        }
	    else
	     return 183;
	  else
	   if (lat < 30.502083)
	    return 55;
	   else
	    if (lat < 38.510531)
	     {
	     if (poly[1743].contains(lat,lng)) return 51;
	     else return 55;
	     }
	    else
	     return 51;
	}

	private static int call33(double lat, double lng)
	{
	 if (lng < 68.199081)
	  if (lng < 65.196175)
	   if (lat < 42.795555)
	    if (lng < 61.743199)
	     if (lat < 40.639486)
	      return 76;
	     else
	      if (lng < 60.016710)
	       {
	       if (poly[1744].contains(lat,lng)) return 76;
	       else return 167;
	       }
	      else
	       {
	       if (poly[1745].contains(lat,lng)) return 167;
	       else return 76;
	       }
	    else
	     {
	     if (poly[1746].contains(lat,lng)) return 167;
	     else return 76;
	     }
	   else
	    if (lng < 58.915638)
	     return 167;
	    else
	     {
	     if (poly[1747].contains(lat,lng)) return 239;
	     else return 167;
	     }
	  else
	   if (lat < 38.926109)
	    {
	    if (poly[1748].contains(lat,lng)) return 208;
	    else return 167;
	    }
	   else
	    if (lat < 41.598787)
	     if (lng < 66.697628)
	      {
	      if (poly[1749].contains(lat,lng)) return 246;
	      if (poly[1750].contains(lat,lng)) return 246;
	      if (poly[1751].contains(lat,lng)) return 347;
	      else return 167;
	      }
	     else
	      if (lat < 40.262448)
	       if (lng < 67.448355)
	        {
	        if (poly[1752].contains(lat,lng)) return 208;
	        if (poly[1753].contains(lat,lng)) return 208;
	        if (poly[1754].contains(lat,lng)) return 246;
	        if (poly[1755].contains(lat,lng)) return 246;
	        else return 167;
	        }
	       else
	        {
	        if (poly[1756].contains(lat,lng)) return 167;
	        if (poly[1757].contains(lat,lng)) return 167;
	        if (poly[1758].contains(lat,lng)) return 167;
	        if (poly[1759].contains(lat,lng)) return 246;
	        else return 208;
	        }
	      else
	       {
	       if (poly[1760].contains(lat,lng)) return 167;
	       if (poly[1761].contains(lat,lng)) return 167;
	       if (poly[1762].contains(lat,lng)) return 167;
	       if (poly[1763].contains(lat,lng)) return 347;
	       else return 246;
	       }
	    else
	     {
	     if (poly[1764].contains(lat,lng)) return 167;
	     if (poly[1765].contains(lat,lng)) return 239;
	     else return 347;
	     }
	 else
	  if (lng < 73.132278)
	   if (lat < 41.377441)
	    if (lng < 70.665680)
	     if (lat < 39.930429)
	      if (lng < 69.432381)
	       {
	       if (poly[1766].contains(lat,lng)) return 230;
	       if (poly[1767].contains(lat,lng)) return 246;
	       if (poly[1768].contains(lat,lng)) return 246;
	       else return 208;
	       }
	      else
	       {
	       if (poly[1769].contains(lat,lng)) return 230;
	       if (poly[1770].contains(lat,lng)) return 230;
	       else return 208;
	       }
	     else
	      if (lng < 69.432381)
	       if (lat < 40.653935)
	        {
	        if (poly[1771].contains(lat,lng)) return 230;
	        if (poly[1772].contains(lat,lng)) return 246;
	        else return 208;
	        }
	       else
	        {
	        if (poly[1773].contains(lat,lng)) return 208;
	        if (poly[1774].contains(lat,lng)) return 347;
	        else return 246;
	        }
	      else
	       if (lat < 40.653935)
	        {
	        if (poly[1775].contains(lat,lng)) return 230;
	        if (poly[1776].contains(lat,lng)) return 230;
	        if (poly[1777].contains(lat,lng)) return 246;
	        if (poly[1778].contains(lat,lng)) return 246;
	        else return 208;
	        }
	       else
	        {
	        if (poly[1779].contains(lat,lng)) return 246;
	        if (poly[1780].contains(lat,lng)) return 246;
	        else return 208;
	        }
	    else
	     if (lat < 39.930429)
	      {
	      if (poly[1781].contains(lat,lng)) return 208;
	      if (poly[1782].contains(lat,lng)) return 208;
	      if (poly[1783].contains(lat,lng)) return 246;
	      else return 230;
	      }
	     else
	      if (lng < 71.898979)
	       if (lat < 40.653935)
	        {
	        if (poly[1784].contains(lat,lng)) return 208;
	        if (poly[1785].contains(lat,lng)) return 208;
	        if (poly[1786].contains(lat,lng)) return 246;
	        if (poly[1787].contains(lat,lng)) return 246;
	        else return 230;
	        }
	       else
	        {
	        if (poly[1788].contains(lat,lng)) return 208;
	        if (poly[1789].contains(lat,lng)) return 208;
	        if (poly[1790].contains(lat,lng)) return 230;
	        if (poly[1791].contains(lat,lng)) return 230;
	        if (poly[1792].contains(lat,lng)) return 230;
	        else return 246;
	        }
	      else
	       if (lat < 40.653935)
	        {
	        if (poly[1793].contains(lat,lng)) return 246;
	        else return 230;
	        }
	       else
	        {
	        if (poly[1794].contains(lat,lng)) return 246;
	        if (poly[1795].contains(lat,lng)) return 246;
	        else return 230;
	        }
	   else
	    if (lng < 70.665680)
	     {
	     if (poly[1796].contains(lat,lng)) return 230;
	     if (poly[1797].contains(lat,lng)) return 347;
	     else return 246;
	     }
	    else
	     if (lat < 42.824453)
	      if (lng < 71.898979)
	       if (lat < 42.100947)
	        {
	        if (poly[1798].contains(lat,lng)) return 246;
	        if (poly[1799].contains(lat,lng)) return 246;
	        if (poly[1800].contains(lat,lng)) return 246;
	        if (poly[1801].contains(lat,lng)) return 347;
	        else return 230;
	        }
	       else
	        {
	        if (poly[1802].contains(lat,lng)) return 246;
	        if (poly[1803].contains(lat,lng)) return 347;
	        if (poly[1804].contains(lat,lng)) return 347;
	        else return 230;
	        }
	      else
	       {
	       if (poly[1805].contains(lat,lng)) return 347;
	       else return 230;
	       }
	     else
	      {
	      if (poly[1806].contains(lat,lng)) return 230;
	      else return 347;
	      }
	  else
	   if (lat < 41.059696)
	    if (lng < 77.047279)
	     if (lng < 75.089779)
	      if (lat < 39.771557)
	       {
	       if (poly[1807].contains(lat,lng)) return 208;
	       if (poly[1808].contains(lat,lng)) return 230;
	       else return 4;
	       }
	      else
	       {
	       if (poly[1809].contains(lat,lng)) return 4;
	       else return 230;
	       }
	     else
	      {
	      if (poly[1810].contains(lat,lng)) return 230;
	      else return 4;
	      }
	    else
	     {
	     if (poly[1811].contains(lat,lng)) return 230;
	     else return 4;
	     }
	   else
	    {
	    if (poly[1812].contains(lat,lng)) return 347;
	    else return 230;
	    }
	}

	private static int call34(double lat, double lng)
	{
	 if (lng < 58.290222)
	  if (lat < 26.078861)
	   if (lng < 55.584751)
	    if (lat < 25.587223)
	     return 321;
	    else
	     if (lng < 54.774807)
	      return 55;
	     else
	      return 321;
	   else
	    if (lng < 56.555802)
	     if (lng < 55.795612)
	      return 321;
	     else
	      if (lat < 24.983110)
	       if (lng < 55.957863)
	        {
	        if (poly[1813].contains(lat,lng)) return 357;
	        else return 321;
	        }
	       else
	        {
	        if (poly[1814].contains(lat,lng)) return 321;
	        else return 357;
	        }
	      else
	       if (lng < 55.973804)
	        return 321;
	       else
	        {
	        if (poly[1815].contains(lat,lng)) return 321;
	        else return 357;
	        }
	    else
	     return 55;
	  else
	   if (lat < 26.387972)
	    if (lng < 55.316555)
	     return 55;
	    else
	     if (lng < 56.537693)
	      return 357;
	     else
	      return 55;
	   else
	    if (lat < 27.152056)
	     return 55;
	    else
	     if (lat < 38.272583)
	      {
	      if (poly[1816].contains(lat,lng)) return 76;
	      else return 55;
	      }
	     else
	      if (lat < 41.272024)
	       {
	       if (poly[1817].contains(lat,lng)) return 120;
	       if (poly[1818].contains(lat,lng)) return 120;
	       if (poly[1819].contains(lat,lng)) return 167;
	       else return 76;
	       }
	      else
	       if (lng < 56.211527)
	        {
	        if (poly[1820].contains(lat,lng)) return 76;
	        if (poly[1821].contains(lat,lng)) return 76;
	        if (poly[1822].contains(lat,lng)) return 76;
	        if (poly[1823].contains(lat,lng)) return 167;
	        else return 120;
	        }
	       else
	        {
	        if (poly[1824].contains(lat,lng)) return 167;
	        else return 76;
	        }
	 else
	  if (lat < 24.905527)
	   if (lng < 67.436974)
	    return 210;
	   else
	    {
	    if (poly[1825].contains(lat,lng)) return 371;
	    else return 210;
	    }
	  else
	   if (lat < 25.612417)
	    if (lng < 59.965057)
	     return 55;
	    else
	     if (lng < 66.206192)
	      if (lng < 61.677770)
	       {
	       if (poly[1826].contains(lat,lng)) return 210;
	       else return 55;
	       }
	      else
	       return 210;
	     else
	      if (lng < 66.530281)
	       return 210;
	      else
	       {
	       if (poly[1827].contains(lat,lng)) return 371;
	       else return 210;
	       }
	   else
	    if (lat < 38.483418)
	     if (lng < 68.402641)
	      if (lat < 31.834249)
	       if (lng < 68.168892)
	        if (lng < 63.229557)
	         if (lat < 28.723333)
	          {
	          if (poly[1828].contains(lat,lng)) return 210;
	          if (poly[1829].contains(lat,lng)) return 210;
	          else return 55;
	          }
	         else
	          {
	          if (poly[1830].contains(lat,lng)) return 210;
	          if (poly[1831].contains(lat,lng)) return 366;
	          else return 55;
	          }
	        else
	         {
	         if (poly[1832].contains(lat,lng)) return 55;
	         if (poly[1833].contains(lat,lng)) return 366;
	         else return 210;
	         }
	       else
	        {
	        if (poly[1834].contains(lat,lng)) return 366;
	        else return 210;
	        }
	      else
	       if (lng < 66.684303)
	        if (lat < 37.932888)
	         if (lng < 62.487263)
	          if (lat < 34.883569)
	           {
	           if (poly[1835].contains(lat,lng)) return 366;
	           else return 55;
	           }
	          else
	           if (lng < 60.388742)
	            {
	            if (poly[1836].contains(lat,lng)) return 76;
	            else return 55;
	            }
	           else
	            if (lat < 36.408228)
	             if (lng < 61.438003)
	              {
	              if (poly[1837].contains(lat,lng)) return 76;
	              if (poly[1838].contains(lat,lng)) return 366;
	              else return 55;
	              }
	             else
	              {
	              if (poly[1839].contains(lat,lng)) return 366;
	              else return 76;
	              }
	            else
	             {
	             if (poly[1840].contains(lat,lng)) return 55;
	             else return 76;
	             }
	         else
	          if (lat < 34.883569)
	           return 366;
	          else
	           if (lng < 64.585783)
	            {
	            if (poly[1841].contains(lat,lng)) return 366;
	            else return 76;
	            }
	           else
	            {
	            if (poly[1842].contains(lat,lng)) return 167;
	            if (poly[1843].contains(lat,lng)) return 366;
	            else return 76;
	            }
	        else
	         {
	         if (poly[1844].contains(lat,lng)) return 167;
	         else return 76;
	         }
	       else
	        if (lat < 38.195000)
	         {
	         if (poly[1845].contains(lat,lng)) return 167;
	         if (poly[1846].contains(lat,lng)) return 208;
	         else return 366;
	         }
	        else
	         {
	         if (poly[1847].contains(lat,lng)) return 208;
	         else return 167;
	         }
	     else
	      if (lat < 35.410908)
	       if (lat < 30.511663)
	        {
	        if (poly[1848].contains(lat,lng)) return 371;
	        else return 210;
	        }
	       else
	        if (lng < 73.050777)
	         if (lat < 32.961285)
	          {
	          if (poly[1849].contains(lat,lng)) return 366;
	          else return 210;
	          }
	         else
	          {
	          if (poly[1850].contains(lat,lng)) return 210;
	          else return 366;
	          }
	        else
	         if (lat < 32.961285)
	          if (lng < 75.374846)
	           if (lat < 31.736474)
	            {
	            if (poly[1851].contains(lat,lng)) return 371;
	            else return 210;
	            }
	           else
	            {
	            if (poly[1852].contains(lat,lng)) return 371;
	            if (poly[1853].contains(lat,lng)) return 371;
	            else return 210;
	            }
	          else
	           {
	           if (poly[1854].contains(lat,lng)) return 210;
	           else return 371;
	           }
	         else
	          if (lng < 75.374846)
	           {
	           if (poly[1855].contains(lat,lng)) return 371;
	           else return 210;
	           }
	          else
	           {
	           if (poly[1856].contains(lat,lng)) return 210;
	           else return 371;
	           }
	      else
	       if (lng < 70.989082)
	        if (lat < 36.947163)
	         return 366;
	        else
	         if (lng < 69.695862)
	          {
	          if (poly[1857].contains(lat,lng)) return 366;
	          else return 208;
	          }
	         else
	          {
	          if (poly[1858].contains(lat,lng)) return 208;
	          else return 366;
	          }
	       else
	        if (lng < 74.343998)
	         if (lng < 72.666540)
	          if (lat < 36.947163)
	           {
	           if (poly[1859].contains(lat,lng)) return 208;
	           if (poly[1860].contains(lat,lng)) return 210;
	           else return 366;
	           }
	          else
	           {
	           if (poly[1861].contains(lat,lng)) return 208;
	           else return 366;
	           }
	         else
	          {
	          if (poly[1862].contains(lat,lng)) return 208;
	          if (poly[1863].contains(lat,lng)) return 210;
	          else return 366;
	          }
	        else
	         if (lng < 76.021456)
	          if (lat < 36.947163)
	           {
	           if (poly[1864].contains(lat,lng)) return 4;
	           if (poly[1865].contains(lat,lng)) return 4;
	           if (poly[1866].contains(lat,lng)) return 4;
	           if (poly[1867].contains(lat,lng)) return 4;
	           else return 210;
	           }
	          else
	           if (lng < 75.182727)
	            if (lat < 37.715290)
	             if (lng < 74.763362)
	              {
	              if (poly[1868].contains(lat,lng)) return 4;
	              if (poly[1869].contains(lat,lng)) return 208;
	              if (poly[1870].contains(lat,lng)) return 210;
	              else return 366;
	              }
	             else
	              {
	              if (poly[1871].contains(lat,lng)) return 208;
	              if (poly[1872].contains(lat,lng)) return 210;
	              if (poly[1873].contains(lat,lng)) return 210;
	              if (poly[1874].contains(lat,lng)) return 366;
	              else return 4;
	              }
	            else
	             {
	             if (poly[1875].contains(lat,lng)) return 208;
	             else return 4;
	             }
	           else
	            {
	            if (poly[1876].contains(lat,lng)) return 210;
	            else return 4;
	            }
	         else
	          {
	          if (poly[1877].contains(lat,lng)) return 210;
	          if (poly[1878].contains(lat,lng)) return 210;
	          if (poly[1879].contains(lat,lng)) return 210;
	          else return 4;
	          }
	    else
	     return call33(lat,lng);
	}

	private static int call35(double lat, double lng)
	{
	 if (lng < 59.411304)
	  if (lat < 44.892151)
	   if (lng < 55.998606)
	    {
	    if (poly[1880].contains(lat,lng)) return 167;
	    else return 120;
	    }
	   else
	    return 167;
	  else
	   if (lat < 48.806938)
	    if (lng < 58.607960)
	     if (lng < 54.658043)
	      {
	      if (poly[1881].contains(lat,lng)) return 240;
	      else return 120;
	      }
	     else
	      {
	      if (poly[1882].contains(lat,lng)) return 167;
	      if (poly[1883].contains(lat,lng)) return 239;
	      if (poly[1884].contains(lat,lng)) return 240;
	      else return 120;
	      }
	    else
	     if (lat < 45.436764)
	      if (lng < 59.239082)
	       if (lat < 44.974435)
	        return 167;
	       else
	        {
	        if (poly[1885].contains(lat,lng)) return 239;
	        else return 167;
	        }
	      else
	       {
	       if (poly[1886].contains(lat,lng)) return 239;
	       else return 167;
	       }
	     else
	      if (lat < 45.622234)
	       return 239;
	      else
	       {
	       if (poly[1887].contains(lat,lng)) return 240;
	       else return 239;
	       }
	   else
	    if (lat < 54.830132)
	     if (lat < 51.818535)
	      if (lng < 56.993151)
	       {
	       if (poly[1888].contains(lat,lng)) return 240;
	       else return 178;
	       }
	      else
	       {
	       if (poly[1889].contains(lat,lng)) return 240;
	       else return 178;
	       }
	     else
	      return 178;
	    else
	     return 178;
	 else
	  if (lng < 68.111374)
	   if (lat < 45.737469)
	    if (lng < 60.200966)
	     return 239;
	    else
	     if (lng < 61.121097)
	      {
	      if (poly[1890].contains(lat,lng)) return 167;
	      else return 239;
	      }
	     else
	      {
	      if (poly[1891].contains(lat,lng)) return 347;
	      else return 239;
	      }
	   else
	    if (lat < 53.295397)
	     if (lng < 63.761339)
	      if (lat < 49.516433)
	       {
	       if (poly[1892].contains(lat,lng)) return 240;
	       if (poly[1893].contains(lat,lng)) return 347;
	       else return 239;
	       }
	      else
	       if (lng < 61.586322)
	        if (lat < 51.405915)
	         {
	         if (poly[1894].contains(lat,lng)) return 239;
	         if (poly[1895].contains(lat,lng)) return 240;
	         else return 178;
	         }
	        else
	         {
	         if (poly[1896].contains(lat,lng)) return 239;
	         if (poly[1897].contains(lat,lng)) return 239;
	         else return 178;
	         }
	       else
	        if (lat < 51.405915)
	         {
	         if (poly[1898].contains(lat,lng)) return 178;
	         if (poly[1899].contains(lat,lng)) return 240;
	         else return 239;
	         }
	        else
	         {
	         if (poly[1900].contains(lat,lng)) return 178;
	         else return 239;
	         }
	     else
	      if (lat < 49.516433)
	       {
	       if (poly[1901].contains(lat,lng)) return 240;
	       if (poly[1902].contains(lat,lng)) return 347;
	       else return 239;
	       }
	      else
	       if (lng < 65.936357)
	        return 239;
	       else
	        if (lat < 51.405915)
	         {
	         if (poly[1903].contains(lat,lng)) return 239;
	         else return 347;
	         }
	        else
	         {
	         if (poly[1904].contains(lat,lng)) return 347;
	         else return 239;
	         }
	    else
	     if (lng < 63.761339)
	      if (lat < 57.074361)
	       if (lng < 61.586322)
	        {
	        if (poly[1905].contains(lat,lng)) return 239;
	        if (poly[1906].contains(lat,lng)) return 239;
	        else return 178;
	        }
	       else
	        {
	        if (poly[1907].contains(lat,lng)) return 239;
	        else return 178;
	        }
	      else
	       return 178;
	     else
	      if (lat < 57.074361)
	       if (lng < 65.936357)
	        {
	        if (poly[1908].contains(lat,lng)) return 178;
	        else return 239;
	        }
	       else
	        {
	        if (poly[1909].contains(lat,lng)) return 178;
	        if (poly[1910].contains(lat,lng)) return 239;
	        else return 347;
	        }
	      else
	       return 178;
	  else
	   if (lat < 52.562395)
	    return 347;
	   else
	    if (lng < 72.905144)
	     if (lat < 56.707860)
	      if (lng < 70.508259)
	       {
	       if (poly[1911].contains(lat,lng)) return 347;
	       if (poly[1912].contains(lat,lng)) return 379;
	       if (poly[1913].contains(lat,lng)) return 379;
	       else return 178;
	       }
	      else
	       if (lat < 54.635128)
	        {
	        if (poly[1914].contains(lat,lng)) return 379;
	        else return 347;
	        }
	       else
	        if (lng < 71.706701)
	         if (lat < 55.671494)
	          {
	          if (poly[1915].contains(lat,lng)) return 178;
	          if (poly[1916].contains(lat,lng)) return 347;
	          else return 379;
	          }
	         else
	          {
	          if (poly[1917].contains(lat,lng)) return 379;
	          else return 178;
	          }
	        else
	         return 379;
	     else
	      if (lng < 70.508259)
	       {
	       if (poly[1918].contains(lat,lng)) return 379;
	       if (poly[1919].contains(lat,lng)) return 379;
	       else return 178;
	       }
	      else
	       if (lat < 58.780592)
	        if (lng < 71.706701)
	         {
	         if (poly[1920].contains(lat,lng)) return 379;
	         if (poly[1921].contains(lat,lng)) return 379;
	         else return 178;
	         }
	        else
	         {
	         if (poly[1922].contains(lat,lng)) return 379;
	         else return 178;
	         }
	       else
	        return 178;
	    else
	     if (lat < 56.707860)
	      if (lng < 75.302029)
	       if (lat < 54.635128)
	        if (lng < 74.103586)
	         {
	         if (poly[1923].contains(lat,lng)) return 379;
	         else return 347;
	         }
	        else
	         {
	         if (poly[1924].contains(lat,lng)) return 379;
	         else return 347;
	         }
	       else
	        {
	        if (poly[1925].contains(lat,lng)) return 95;
	        if (poly[1926].contains(lat,lng)) return 95;
	        if (poly[1927].contains(lat,lng)) return 95;
	        else return 379;
	        }
	      else
	       if (lat < 54.635128)
	        {
	        if (poly[1928].contains(lat,lng)) return 95;
	        if (poly[1929].contains(lat,lng)) return 379;
	        else return 347;
	        }
	       else
	        if (lng < 76.500471)
	         if (lat < 55.671494)
	          {
	          if (poly[1930].contains(lat,lng)) return 379;
	          if (poly[1931].contains(lat,lng)) return 379;
	          else return 95;
	          }
	         else
	          {
	          if (poly[1932].contains(lat,lng)) return 379;
	          if (poly[1933].contains(lat,lng)) return 379;
	          else return 95;
	          }
	        else
	         return 95;
	     else
	      if (lng < 75.302029)
	       {
	       if (poly[1934].contains(lat,lng)) return 95;
	       if (poly[1935].contains(lat,lng)) return 95;
	       if (poly[1936].contains(lat,lng)) return 178;
	       else return 379;
	       }
	      else
	       {
	       if (poly[1937].contains(lat,lng)) return 178;
	       if (poly[1938].contains(lat,lng)) return 178;
	       if (poly[1939].contains(lat,lng)) return 379;
	       if (poly[1940].contains(lat,lng)) return 379;
	       else return 95;
	       }
	}

	private static int call36(double lat, double lng)
	{
	 if (lng < 50.371387)
	  if (lat < 46.054779)
	   if (lng < 48.941555)
	    if (lng < 47.589027)
	     if (lat < 45.179359)
	      return 183;
	     else
	      {
	      if (poly[1941].contains(lat,lng)) return 99;
	      else return 183;
	      }
	    else
	     return 99;
	   else
	    if (lng < 49.562946)
	     return 99;
	    else
	     return 120;
	  else
	   if (lat < 46.392056)
	    if (lng < 49.248165)
	     if (lng < 49.218388)
	      if (lng < 47.174988)
	       {
	       if (poly[1942].contains(lat,lng)) return 183;
	       else return 99;
	       }
	      else
	       if (lat < 46.315418)
	        return 99;
	       else
	        {
	        if (poly[1943].contains(lat,lng)) return 120;
	        if (poly[1944].contains(lat,lng)) return 120;
	        else return 99;
	        }
	     else
	      if (lat < 46.295479)
	       if (lng < 49.234390)
	        return 99;
	       else
	        if (lat < 46.220064)
	         return 99;
	        else
	         return 120;
	      else
	       return 120;
	    else
	     return 120;
	   else
	    if (lat < 51.408128)
	     if (lng < 46.785236)
	      if (lat < 48.900092)
	       if (lng < 44.992161)
	        {
	        if (poly[1945].contains(lat,lng)) return 99;
	        else return 183;
	        }
	       else
	        {
	        if (poly[1946].contains(lat,lng)) return 115;
	        if (poly[1947].contains(lat,lng)) return 183;
	        else return 99;
	        }
	      else
	       {
	       if (poly[1948].contains(lat,lng)) return 115;
	       if (poly[1949].contains(lat,lng)) return 115;
	       else return 99;
	       }
	     else
	      if (lat < 48.900092)
	       if (lng < 48.578312)
	        {
	        if (poly[1950].contains(lat,lng)) return 115;
	        if (poly[1951].contains(lat,lng)) return 120;
	        if (poly[1952].contains(lat,lng)) return 120;
	        if (poly[1953].contains(lat,lng)) return 183;
	        if (poly[1954].contains(lat,lng)) return 183;
	        else return 99;
	        }
	       else
	        {
	        if (poly[1955].contains(lat,lng)) return 99;
	        if (poly[1956].contains(lat,lng)) return 99;
	        if (poly[1957].contains(lat,lng)) return 115;
	        else return 120;
	        }
	      else
	       if (lng < 48.578312)
	        {
	        if (poly[1958].contains(lat,lng)) return 99;
	        if (poly[1959].contains(lat,lng)) return 99;
	        else return 115;
	        }
	       else
	        {
	        if (poly[1960].contains(lat,lng)) return 115;
	        else return 99;
	        }
	    else
	     if (lat < 51.434435)
	      {
	      if (poly[1961].contains(lat,lng)) return 115;
	      else return 99;
	      }
	     else
	      if (lat < 54.516670)
	       if (lng < 46.785236)
	        if (lng < 44.992161)
	         {
	         if (poly[1962].contains(lat,lng)) return 183;
	         else return 99;
	         }
	        else
	         {
	         if (poly[1963].contains(lat,lng)) return 183;
	         else return 99;
	         }
	       else
	        if (lng < 48.578312)
	         if (lat < 52.975553)
	          {
	          if (poly[1964].contains(lat,lng)) return 157;
	          if (poly[1965].contains(lat,lng)) return 157;
	          if (poly[1966].contains(lat,lng)) return 157;
	          if (poly[1967].contains(lat,lng)) return 183;
	          else return 99;
	          }
	         else
	          {
	          if (poly[1968].contains(lat,lng)) return 157;
	          if (poly[1969].contains(lat,lng)) return 157;
	          else return 183;
	          }
	        else
	         if (lat < 52.975553)
	          {
	          if (poly[1970].contains(lat,lng)) return 99;
	          if (poly[1971].contains(lat,lng)) return 183;
	          else return 157;
	          }
	         else
	          {
	          if (poly[1972].contains(lat,lng)) return 157;
	          else return 183;
	          }
	      else
	       if (lat < 60.475559)
	        if (lng < 46.785236)
	         {
	         if (poly[1973].contains(lat,lng)) return 99;
	         if (poly[1974].contains(lat,lng)) return 99;
	         if (poly[1975].contains(lat,lng)) return 99;
	         if (poly[1976].contains(lat,lng)) return 99;
	         if (poly[1977].contains(lat,lng)) return 99;
	         else return 183;
	         }
	        else
	         if (lat < 57.496115)
	          if (lng < 48.578312)
	           if (lat < 56.006392)
	            return 183;
	           else
	            if (lng < 47.681774)
	             {
	             if (poly[1978].contains(lat,lng)) return 183;
	             if (poly[1979].contains(lat,lng)) return 183;
	             if (poly[1980].contains(lat,lng)) return 183;
	             if (poly[1981].contains(lat,lng)) return 183;
	             else return 99;
	             }
	            else
	             {
	             if (poly[1982].contains(lat,lng)) return 183;
	             else return 99;
	             }
	          else
	           {
	           if (poly[1983].contains(lat,lng)) return 183;
	           else return 99;
	           }
	         else
	          if (lng < 48.578312)
	           if (lat < 58.985837)
	            {
	            if (poly[1984].contains(lat,lng)) return 183;
	            if (poly[1985].contains(lat,lng)) return 183;
	            else return 99;
	            }
	           else
	            {
	            if (poly[1986].contains(lat,lng)) return 99;
	            else return 183;
	            }
	          else
	           {
	           if (poly[1987].contains(lat,lng)) return 183;
	           else return 99;
	           }
	       else
	        {
	        if (poly[1988].contains(lat,lng)) return 99;
	        else return 183;
	        }
	 else
	  if (lng < 54.574997)
	   if (lat < 46.904415)
	    return 120;
	   else
	    if (lat < 51.766613)
	     if (lat < 50.015274)
	      if (lng < 52.473192)
	       {
	       if (poly[1989].contains(lat,lng)) return 115;
	       else return 120;
	       }
	      else
	       if (lat < 48.459845)
	        return 120;
	       else
	        if (lng < 53.524095)
	         {
	         if (poly[1990].contains(lat,lng)) return 120;
	         else return 115;
	         }
	        else
	         {
	         if (poly[1991].contains(lat,lng)) return 115;
	         if (poly[1992].contains(lat,lng)) return 120;
	         else return 240;
	         }
	     else
	      if (lng < 52.473192)
	       {
	       if (poly[1993].contains(lat,lng)) return 99;
	       if (poly[1994].contains(lat,lng)) return 178;
	       else return 115;
	       }
	      else
	       if (lng < 53.524095)
	        {
	        if (poly[1995].contains(lat,lng)) return 178;
	        else return 115;
	        }
	       else
	        {
	        if (poly[1996].contains(lat,lng)) return 178;
	        if (poly[1997].contains(lat,lng)) return 240;
	        else return 115;
	        }
	    else
	     if (lat < 54.677216)
	      if (lng < 52.473192)
	       if (lat < 53.221914)
	        {
	        if (poly[1998].contains(lat,lng)) return 99;
	        if (poly[1999].contains(lat,lng)) return 178;
	        else return 157;
	        }
	       else
	        if (lng < 51.422290)
	         {
	         if (poly[2000].contains(lat,lng)) return 157;
	         else return 183;
	         }
	        else
	         {
	         if (poly[2001].contains(lat,lng)) return 178;
	         if (poly[2002].contains(lat,lng)) return 178;
	         if (poly[2003].contains(lat,lng)) return 183;
	         else return 157;
	         }
	      else
	       {
	       if (poly[2004].contains(lat,lng)) return 157;
	       if (poly[2005].contains(lat,lng)) return 157;
	       if (poly[2006].contains(lat,lng)) return 183;
	       else return 178;
	       }
	     else
	      if (lat < 60.555832)
	       if (lat < 57.616524)
	        if (lng < 52.473192)
	         if (lat < 56.146870)
	          {
	          if (poly[2007].contains(lat,lng)) return 99;
	          if (poly[2008].contains(lat,lng)) return 99;
	          if (poly[2009].contains(lat,lng)) return 157;
	          else return 183;
	          }
	         else
	          if (lng < 51.422290)
	           {
	           if (poly[2010].contains(lat,lng)) return 157;
	           if (poly[2011].contains(lat,lng)) return 157;
	           if (poly[2012].contains(lat,lng)) return 157;
	           if (poly[2013].contains(lat,lng)) return 183;
	           if (poly[2014].contains(lat,lng)) return 183;
	           else return 99;
	           }
	          else
	           {
	           if (poly[2015].contains(lat,lng)) return 157;
	           if (poly[2016].contains(lat,lng)) return 183;
	           else return 99;
	           }
	        else
	         if (lat < 56.146870)
	          if (lng < 53.524095)
	           {
	           if (poly[2017].contains(lat,lng)) return 157;
	           if (poly[2018].contains(lat,lng)) return 157;
	           if (poly[2019].contains(lat,lng)) return 157;
	           if (poly[2020].contains(lat,lng)) return 157;
	           if (poly[2021].contains(lat,lng)) return 157;
	           if (poly[2022].contains(lat,lng)) return 178;
	           else return 183;
	           }
	          else
	           {
	           if (poly[2023].contains(lat,lng)) return 157;
	           if (poly[2024].contains(lat,lng)) return 183;
	           if (poly[2025].contains(lat,lng)) return 183;
	           if (poly[2026].contains(lat,lng)) return 183;
	           else return 178;
	           }
	         else
	          if (lng < 53.524095)
	           {
	           if (poly[2027].contains(lat,lng)) return 183;
	           if (poly[2028].contains(lat,lng)) return 183;
	           if (poly[2029].contains(lat,lng)) return 183;
	           else return 157;
	           }
	          else
	           if (lat < 56.881697)
	            {
	            if (poly[2030].contains(lat,lng)) return 178;
	            if (poly[2031].contains(lat,lng)) return 183;
	            else return 157;
	            }
	           else
	            {
	            if (poly[2032].contains(lat,lng)) return 178;
	            else return 157;
	            }
	       else
	        if (lng < 52.473192)
	         {
	         if (poly[2033].contains(lat,lng)) return 157;
	         if (poly[2034].contains(lat,lng)) return 178;
	         if (poly[2035].contains(lat,lng)) return 183;
	         else return 99;
	         }
	        else
	         if (lat < 59.086178)
	          {
	          if (poly[2036].contains(lat,lng)) return 99;
	          if (poly[2037].contains(lat,lng)) return 178;
	          else return 157;
	          }
	         else
	          {
	          if (poly[2038].contains(lat,lng)) return 178;
	          else return 99;
	          }
	      else
	       {
	       if (poly[2039].contains(lat,lng)) return 178;
	       else return 183;
	       }
	  else
	   if (lat < 60.853325)
	    return call35(lat,lng);
	   else
	    if (lng < 63.351297)
	     if (lng < 58.963147)
	      {
	      if (poly[2040].contains(lat,lng)) return 183;
	      else return 178;
	      }
	     else
	      if (lat < 63.643887)
	       {
	       if (poly[2041].contains(lat,lng)) return 183;
	       else return 178;
	       }
	      else
	       {
	       if (poly[2042].contains(lat,lng)) return 178;
	       else return 183;
	       }
	    else
	     return 178;
	}

	private static int call37(double lat, double lng)
	{
	 if (lng < 90.261749)
	  if (lat < 23.821917)
	   if (lng < 88.803276)
	    {
	    if (poly[2043].contains(lat,lng)) return 371;
	    else return 150;
	    }
	   else
	    return 150;
	  else
	   if (lat < 23.984638)
	    if (lng < 88.775749)
	     {
	     if (poly[2044].contains(lat,lng)) return 150;
	     else return 371;
	     }
	    else
	     return 150;
	   else
	    if (lng < 77.840919)
	     {
	     if (poly[2045].contains(lat,lng)) return 210;
	     if (poly[2046].contains(lat,lng)) return 371;
	     else return 4;
	     }
	    else
	     if (lat < 26.631945)
	      if (lng < 85.956075)
	       {
	       if (poly[2047].contains(lat,lng)) return 268;
	       else return 371;
	       }
	      else
	       if (lng < 88.108912)
	        {
	        if (poly[2048].contains(lat,lng)) return 150;
	        if (poly[2049].contains(lat,lng)) return 150;
	        if (poly[2050].contains(lat,lng)) return 268;
	        else return 371;
	        }
	       else
	        if (lat < 25.308291)
	         if (lng < 89.185331)
	          {
	          if (poly[2051].contains(lat,lng)) return 371;
	          if (poly[2052].contains(lat,lng)) return 371;
	          else return 150;
	          }
	         else
	          {
	          if (poly[2053].contains(lat,lng)) return 371;
	          else return 150;
	          }
	        else
	         if (lng < 89.185331)
	          if (lat < 25.970118)
	           {
	           if (poly[2054].contains(lat,lng)) return 371;
	           if (poly[2055].contains(lat,lng)) return 371;
	           else return 150;
	           }
	          else
	           if (lng < 88.647121)
	            {
	            if (poly[2056].contains(lat,lng)) return 150;
	            if (poly[2057].contains(lat,lng)) return 268;
	            else return 371;
	            }
	           else
	            {
	            if (poly[2058].contains(lat,lng)) return 371;
	            else return 150;
	            }
	         else
	          {
	          if (poly[2059].contains(lat,lng)) return 150;
	          else return 371;
	          }
	     else
	      if (lng < 85.793788)
	       if (lat < 31.074458)
	        if (lng < 81.817354)
	         if (lat < 28.853201)
	          {
	          if (poly[2060].contains(lat,lng)) return 268;
	          else return 371;
	          }
	         else
	          if (lng < 79.829137)
	           {
	           if (poly[2061].contains(lat,lng)) return 4;
	           else return 371;
	           }
	          else
	           if (lat < 29.963830)
	            {
	            if (poly[2062].contains(lat,lng)) return 268;
	            else return 371;
	            }
	           else
	            {
	            if (poly[2063].contains(lat,lng)) return 268;
	            if (poly[2064].contains(lat,lng)) return 371;
	            else return 4;
	            }
	        else
	         if (lat < 28.853201)
	          if (lng < 83.805571)
	           {
	           if (poly[2065].contains(lat,lng)) return 371;
	           else return 268;
	           }
	          else
	           if (lat < 27.742573)
	            {
	            if (poly[2066].contains(lat,lng)) return 371;
	            else return 268;
	            }
	           else
	            {
	            if (poly[2067].contains(lat,lng)) return 406;
	            else return 268;
	            }
	         else
	          if (lng < 83.805571)
	           if (lat < 29.963830)
	            {
	            if (poly[2068].contains(lat,lng)) return 406;
	            else return 268;
	            }
	           else
	            {
	            if (poly[2069].contains(lat,lng)) return 4;
	            if (poly[2070].contains(lat,lng)) return 268;
	            else return 406;
	            }
	          else
	           {
	           if (poly[2071].contains(lat,lng)) return 268;
	           else return 406;
	           }
	       else
	        if (lng < 81.817354)
	         if (lat < 33.295715)
	          if (lng < 79.829137)
	           if (lat < 32.185086)
	            {
	            if (poly[2072].contains(lat,lng)) return 371;
	            else return 4;
	            }
	           else
	            {
	            if (poly[2073].contains(lat,lng)) return 371;
	            else return 4;
	            }
	          else
	           return 4;
	         else
	          {
	          if (poly[2074].contains(lat,lng)) return 371;
	          else return 4;
	          }
	        else
	         if (lat < 33.295715)
	          {
	          if (poly[2075].contains(lat,lng)) return 406;
	          else return 4;
	          }
	         else
	          {
	          if (poly[2076].contains(lat,lng)) return 406;
	          else return 4;
	          }
	      else
	       if (lat < 31.074458)
	        if (lng < 88.027768)
	         if (lat < 28.853201)
	          if (lng < 86.910778)
	           {
	           if (poly[2077].contains(lat,lng)) return 371;
	           if (poly[2078].contains(lat,lng)) return 406;
	           else return 268;
	           }
	          else
	           {
	           if (poly[2079].contains(lat,lng)) return 371;
	           if (poly[2080].contains(lat,lng)) return 406;
	           else return 268;
	           }
	         else
	          return 406;
	        else
	         if (lat < 28.853201)
	          if (lng < 89.144759)
	           if (lat < 27.742573)
	            if (lng < 88.586264)
	             {
	             if (poly[2081].contains(lat,lng)) return 268;
	             if (poly[2082].contains(lat,lng)) return 268;
	             else return 371;
	             }
	            else
	             {
	             if (poly[2083].contains(lat,lng)) return 371;
	             if (poly[2084].contains(lat,lng)) return 406;
	             else return 62;
	             }
	           else
	            {
	            if (poly[2085].contains(lat,lng)) return 268;
	            if (poly[2086].contains(lat,lng)) return 371;
	            else return 406;
	            }
	          else
	           {
	           if (poly[2087].contains(lat,lng)) return 371;
	           if (poly[2088].contains(lat,lng)) return 406;
	           else return 62;
	           }
	         else
	          return 406;
	       else
	        return 406;
	 else
	  if (lng < 90.577446)
	   if (lat < 23.760834)
	    return 150;
	   else
	    if (lat < 25.189353)
	     {
	     if (poly[2089].contains(lat,lng)) return 371;
	     else return 150;
	     }
	    else
	     if (lat < 26.903528)
	      if (lng < 90.416695)
	       {
	       if (poly[2090].contains(lat,lng)) return 62;
	       else return 371;
	       }
	      else
	       {
	       if (poly[2091].contains(lat,lng)) return 62;
	       else return 371;
	       }
	     else
	      {
	      if (poly[2092].contains(lat,lng)) return 406;
	      else return 62;
	      }
	  else
	   if (lng < 92.486359)
	    if (lat < 24.867944)
	     if (lng < 90.759941)
	      return 150;
	     else
	      if (lng < 91.623150)
	       {
	       if (poly[2093].contains(lat,lng)) return 371;
	       else return 150;
	       }
	      else
	       {
	       if (poly[2094].contains(lat,lng)) return 371;
	       else return 150;
	       }
	    else
	     if (lat < 25.202389)
	      if (lng < 91.261497)
	       {
	       if (poly[2095].contains(lat,lng)) return 371;
	       else return 150;
	       }
	      else
	       {
	       if (poly[2096].contains(lat,lng)) return 371;
	       else return 150;
	       }
	     else
	      if (lat < 30.359680)
	       if (lat < 27.781034)
	        {
	        if (poly[2097].contains(lat,lng)) return 62;
	        if (poly[2098].contains(lat,lng)) return 406;
	        else return 371;
	        }
	       else
	        {
	        if (poly[2099].contains(lat,lng)) return 62;
	        if (poly[2100].contains(lat,lng)) return 371;
	        else return 406;
	        }
	      else
	       return 406;
	   else
	    if (lng < 99.836250)
	     if (lat < 29.481194)
	      if (lng < 96.161304)
	       if (lat < 26.463306)
	        if (lng < 94.323832)
	         {
	         if (poly[2101].contains(lat,lng)) return 40;
	         else return 371;
	         }
	        else
	         {
	         if (poly[2102].contains(lat,lng)) return 371;
	         else return 40;
	         }
	       else
	        if (lng < 94.323832)
	         {
	         if (poly[2103].contains(lat,lng)) return 406;
	         else return 371;
	         }
	        else
	         if (lat < 27.972250)
	          {
	          if (poly[2104].contains(lat,lng)) return 40;
	          else return 371;
	          }
	         else
	          {
	          if (poly[2105].contains(lat,lng)) return 406;
	          if (poly[2106].contains(lat,lng)) return 406;
	          if (poly[2107].contains(lat,lng)) return 406;
	          else return 371;
	          }
	      else
	       if (lat < 26.463306)
	        if (lng < 97.998777)
	         {
	         if (poly[2108].contains(lat,lng)) return 403;
	         else return 40;
	         }
	        else
	         if (lat < 24.954362)
	          {
	          if (poly[2109].contains(lat,lng)) return 40;
	          else return 403;
	          }
	         else
	          {
	          if (poly[2110].contains(lat,lng)) return 40;
	          else return 403;
	          }
	       else
	        if (lng < 97.998777)
	         if (lat < 27.972250)
	          {
	          if (poly[2111].contains(lat,lng)) return 371;
	          else return 40;
	          }
	         else
	          if (lng < 97.080041)
	           {
	           if (poly[2112].contains(lat,lng)) return 406;
	           else return 371;
	           }
	          else
	           {
	           if (poly[2113].contains(lat,lng)) return 371;
	           if (poly[2114].contains(lat,lng)) return 406;
	           else return 40;
	           }
	        else
	         if (lat < 27.972250)
	          {
	          if (poly[2115].contains(lat,lng)) return 403;
	          else return 40;
	          }
	         else
	          {
	          if (poly[2116].contains(lat,lng)) return 40;
	          if (poly[2117].contains(lat,lng)) return 406;
	          else return 403;
	          }
	     else
	      if (lng < 96.161304)
	       return 406;
	      else
	       if (lat < 32.499083)
	        {
	        if (poly[2118].contains(lat,lng)) return 406;
	        else return 403;
	        }
	       else
	        if (lng < 97.998777)
	         {
	         if (poly[2119].contains(lat,lng)) return 403;
	         else return 406;
	         }
	        else
	         {
	         if (poly[2120].contains(lat,lng)) return 406;
	         else return 403;
	         }
	    else
	     return 403;
	}

	private static int call38(double lat, double lng)
	{
	 if (lat < 45.366638)
	  if (lng < 81.689583)
	   if (lat < 40.441805)
	    return 4;
	   else
	    if (lat < 42.904222)
	     if (lng < 79.694248)
	      if (lat < 41.673013)
	       {
	       if (poly[2121].contains(lat,lng)) return 230;
	       else return 4;
	       }
	      else
	       {
	       if (poly[2122].contains(lat,lng)) return 4;
	       if (poly[2123].contains(lat,lng)) return 347;
	       else return 230;
	       }
	     else
	      {
	      if (poly[2124].contains(lat,lng)) return 230;
	      if (poly[2125].contains(lat,lng)) return 347;
	      else return 4;
	      }
	    else
	     if (lng < 79.694248)
	      {
	      if (poly[2126].contains(lat,lng)) return 230;
	      else return 347;
	      }
	     else
	      if (lat < 44.135430)
	       {
	       if (poly[2127].contains(lat,lng)) return 347;
	       else return 4;
	       }
	      else
	       {
	       if (poly[2128].contains(lat,lng)) return 4;
	       else return 347;
	       }
	  else
	   if (lat < 40.441805)
	    {
	    if (poly[2129].contains(lat,lng)) return 406;
	    else return 4;
	    }
	   else
	    if (lng < 85.779781)
	     if (lat < 42.904222)
	      {
	      if (poly[2130].contains(lat,lng)) return 406;
	      else return 4;
	      }
	     else
	      if (lng < 83.734682)
	       if (lat < 44.135430)
	        {
	        if (poly[2131].contains(lat,lng)) return 406;
	        if (poly[2132].contains(lat,lng)) return 406;
	        else return 4;
	        }
	       else
	        {
	        if (poly[2133].contains(lat,lng)) return 347;
	        if (poly[2134].contains(lat,lng)) return 406;
	        else return 4;
	        }
	      else
	       {
	       if (poly[2135].contains(lat,lng)) return 4;
	       else return 406;
	       }
	    else
	     return 406;
	 else
	  if (lat < 56.834160)
	   if (lat < 49.999054)
	    if (lng < 87.312668)
	     if (lng < 82.505791)
	      {
	      if (poly[2136].contains(lat,lng)) return 406;
	      else return 347;
	      }
	     else
	      if (lng < 84.909229)
	       {
	       if (poly[2137].contains(lat,lng)) return 406;
	       else return 347;
	       }
	      else
	       if (lat < 47.682846)
	        {
	        if (poly[2138].contains(lat,lng)) return 347;
	        else return 406;
	        }
	       else
	        if (lng < 86.110949)
	         {
	         if (poly[2139].contains(lat,lng)) return 379;
	         if (poly[2140].contains(lat,lng)) return 406;
	         else return 347;
	         }
	        else
	         {
	         if (poly[2141].contains(lat,lng)) return 379;
	         if (poly[2142].contains(lat,lng)) return 406;
	         else return 347;
	         }
	    else
	     if (lat < 47.682846)
	      return 406;
	     else
	      if (lng < 88.591324)
	       if (lat < 48.840950)
	        {
	        if (poly[2143].contains(lat,lng)) return 121;
	        if (poly[2144].contains(lat,lng)) return 121;
	        else return 406;
	        }
	       else
	        {
	        if (poly[2145].contains(lat,lng)) return 379;
	        if (poly[2146].contains(lat,lng)) return 406;
	        else return 121;
	        }
	      else
	       if (lat < 48.840950)
	        {
	        if (poly[2147].contains(lat,lng)) return 406;
	        if (poly[2148].contains(lat,lng)) return 406;
	        else return 121;
	        }
	       else
	        {
	        if (poly[2149].contains(lat,lng)) return 289;
	        if (poly[2150].contains(lat,lng)) return 289;
	        if (poly[2151].contains(lat,lng)) return 379;
	        else return 121;
	        }
	   else
	    if (lng < 83.784447)
	     if (lat < 53.416607)
	      if (lng < 80.741680)
	       {
	       if (poly[2152].contains(lat,lng)) return 95;
	       if (poly[2153].contains(lat,lng)) return 347;
	       else return 379;
	       }
	      else
	       {
	       if (poly[2154].contains(lat,lng)) return 379;
	       else return 347;
	       }
	     else
	      if (lng < 80.741680)
	       {
	       if (poly[2155].contains(lat,lng)) return 379;
	       else return 95;
	       }
	      else
	       {
	       if (poly[2156].contains(lat,lng)) return 379;
	       else return 95;
	       }
	    else
	     if (lat < 53.416607)
	      if (lng < 86.827213)
	       {
	       if (poly[2157].contains(lat,lng)) return 87;
	       if (poly[2158].contains(lat,lng)) return 347;
	       else return 379;
	       }
	      else
	       if (lat < 51.707830)
	        {
	        if (poly[2159].contains(lat,lng)) return 379;
	        else return 289;
	        }
	       else
	        if (lng < 88.348597)
	         {
	         if (poly[2160].contains(lat,lng)) return 87;
	         if (poly[2161].contains(lat,lng)) return 289;
	         else return 379;
	         }
	        else
	         {
	         if (poly[2162].contains(lat,lng)) return 289;
	         if (poly[2163].contains(lat,lng)) return 379;
	         else return 87;
	         }
	     else
	      if (lng < 86.827213)
	       if (lat < 55.125383)
	        if (lng < 85.305830)
	         {
	         if (poly[2164].contains(lat,lng)) return 87;
	         if (poly[2165].contains(lat,lng)) return 379;
	         else return 95;
	         }
	        else
	         {
	         if (poly[2166].contains(lat,lng)) return 379;
	         else return 87;
	         }
	       else
	        if (lng < 85.305830)
	         {
	         if (poly[2167].contains(lat,lng)) return 87;
	         else return 95;
	         }
	        else
	         {
	         if (poly[2168].contains(lat,lng)) return 95;
	         else return 87;
	         }
	      else
	       if (lat < 55.125383)
	        if (lng < 88.348597)
	         {
	         if (poly[2169].contains(lat,lng)) return 379;
	         else return 87;
	         }
	        else
	         {
	         if (poly[2170].contains(lat,lng)) return 289;
	         else return 87;
	         }
	       else
	        if (lng < 88.348597)
	         {
	         if (poly[2171].contains(lat,lng)) return 95;
	         else return 87;
	         }
	        else
	         {
	         if (poly[2172].contains(lat,lng)) return 95;
	         if (poly[2173].contains(lat,lng)) return 95;
	         if (poly[2174].contains(lat,lng)) return 289;
	         else return 87;
	         }
	  else
	   if (lng < 83.784447)
	    {
	    if (poly[2175].contains(lat,lng)) return 178;
	    else return 95;
	    }
	   else
	    if (lat < 60.415409)
	     {
	     if (poly[2176].contains(lat,lng)) return 95;
	     else return 289;
	     }
	    else
	     if (lng < 86.827213)
	      if (lat < 62.206034)
	       {
	       if (poly[2177].contains(lat,lng)) return 95;
	       if (poly[2178].contains(lat,lng)) return 289;
	       else return 178;
	       }
	      else
	       {
	       if (poly[2179].contains(lat,lng)) return 289;
	       else return 178;
	       }
	     else
	      return 289;
	}

	private static int call39(double lat, double lng)
	{
	 if (lat < 63.996658)
	  if (lng < 89.869980)
	   return call38(lat,lng);
	  else
	   if (lat < 47.884361)
	    if (lng < 89.987708)
	     {
	     if (poly[2180].contains(lat,lng)) return 121;
	     else return 406;
	     }
	    else
	     if (lng < 98.586925)
	      if (lat < 41.700666)
	       {
	       if (poly[2181].contains(lat,lng)) return 403;
	       else return 406;
	       }
	      else
	       if (lng < 94.287316)
	        {
	        if (poly[2182].contains(lat,lng)) return 121;
	        else return 406;
	        }
	       else
	        if (lat < 44.792514)
	         {
	         if (poly[2183].contains(lat,lng)) return 123;
	         if (poly[2184].contains(lat,lng)) return 403;
	         if (poly[2185].contains(lat,lng)) return 406;
	         else return 121;
	         }
	        else
	         {
	         if (poly[2186].contains(lat,lng)) return 123;
	         else return 121;
	         }
	     else
	      if (lat < 41.700666)
	       if (lng < 102.886533)
	        if (lat < 38.608819)
	         if (lng < 100.736729)
	          if (lat < 37.062895)
	           {
	           if (poly[2187].contains(lat,lng)) return 406;
	           else return 403;
	           }
	          else
	           {
	           if (poly[2188].contains(lat,lng)) return 406;
	           else return 403;
	           }
	         else
	          return 403;
	        else
	         return 403;
	       else
	        {
	        if (poly[2189].contains(lat,lng)) return 123;
	        else return 403;
	        }
	      else
	       {
	       if (poly[2190].contains(lat,lng)) return 403;
	       if (poly[2191].contains(lat,lng)) return 403;
	       else return 123;
	       }
	   else
	    if (lng < 98.528061)
	     if (lat < 55.940510)
	      if (lng < 94.199020)
	       {
	       if (poly[2192].contains(lat,lng)) return 289;
	       else return 121;
	       }
	      else
	       if (lat < 51.912436)
	        if (lng < 96.363541)
	         {
	         if (poly[2193].contains(lat,lng)) return 121;
	         else return 289;
	         }
	        else
	         if (lat < 49.898398)
	          {
	          if (poly[2194].contains(lat,lng)) return 121;
	          if (poly[2195].contains(lat,lng)) return 289;
	          else return 123;
	          }
	         else
	          {
	          if (poly[2196].contains(lat,lng)) return 121;
	          if (poly[2197].contains(lat,lng)) return 121;
	          if (poly[2198].contains(lat,lng)) return 123;
	          else return 289;
	          }
	       else
	        if (lng < 96.363541)
	         {
	         if (poly[2199].contains(lat,lng)) return 144;
	         else return 289;
	         }
	        else
	         {
	         if (poly[2200].contains(lat,lng)) return 144;
	         else return 289;
	         }
	     else
	      {
	      if (poly[2201].contains(lat,lng)) return 144;
	      else return 289;
	      }
	    else
	     if (lat < 55.940510)
	      if (lng < 102.857101)
	       if (lat < 51.912436)
	        {
	        if (poly[2202].contains(lat,lng)) return 121;
	        if (poly[2203].contains(lat,lng)) return 144;
	        if (poly[2204].contains(lat,lng)) return 289;
	        else return 123;
	        }
	       else
	        {
	        if (poly[2205].contains(lat,lng)) return 123;
	        if (poly[2206].contains(lat,lng)) return 289;
	        else return 144;
	        }
	      else
	       {
	       if (poly[2207].contains(lat,lng)) return 123;
	       else return 144;
	       }
	     else
	      if (lng < 102.857101)
	       if (lat < 59.968584)
	        if (lng < 100.692581)
	         {
	         if (poly[2208].contains(lat,lng)) return 289;
	         else return 144;
	         }
	        else
	         {
	         if (poly[2209].contains(lat,lng)) return 289;
	         else return 144;
	         }
	       else
	        return 289;
	      else
	       if (lat < 59.968584)
	        if (lng < 105.021622)
	         {
	         if (poly[2210].contains(lat,lng)) return 144;
	         if (poly[2211].contains(lat,lng)) return 144;
	         else return 289;
	         }
	        else
	         {
	         if (poly[2212].contains(lat,lng)) return 289;
	         if (poly[2213].contains(lat,lng)) return 289;
	         else return 144;
	         }
	       else
	        if (lng < 105.021622)
	         {
	         if (poly[2214].contains(lat,lng)) return 144;
	         else return 289;
	         }
	        else
	         if (lat < 61.982621)
	          {
	          if (poly[2215].contains(lat,lng)) return 289;
	          if (poly[2216].contains(lat,lng)) return 289;
	          else return 144;
	          }
	         else
	          {
	          if (poly[2217].contains(lat,lng)) return 144;
	          else return 289;
	          }
	 else
	  if (lng < 79.087059)
	   return 178;
	  else
	   if (lng < 86.031097)
	    if (lat < 64.275269)
	     {
	     if (poly[2218].contains(lat,lng)) return 289;
	     else return 178;
	     }
	    else
	     if (lng < 82.559078)
	      if (lat < 66.954357)
	       return 178;
	      else
	       if (lng < 80.823069)
	        {
	        if (poly[2219].contains(lat,lng)) return 289;
	        else return 178;
	        }
	       else
	        if (lat < 68.293901)
	         {
	         if (poly[2220].contains(lat,lng)) return 289;
	         else return 178;
	         }
	        else
	         {
	         if (poly[2221].contains(lat,lng)) return 289;
	         if (poly[2222].contains(lat,lng)) return 289;
	         else return 178;
	         }
	     else
	      if (lat < 66.954357)
	       if (lng < 84.295088)
	        {
	        if (poly[2223].contains(lat,lng)) return 289;
	        if (poly[2224].contains(lat,lng)) return 289;
	        if (poly[2225].contains(lat,lng)) return 289;
	        else return 178;
	        }
	       else
	        {
	        if (poly[2226].contains(lat,lng)) return 178;
	        if (poly[2227].contains(lat,lng)) return 178;
	        if (poly[2228].contains(lat,lng)) return 178;
	        else return 289;
	        }
	      else
	       {
	       if (poly[2229].contains(lat,lng)) return 178;
	       if (poly[2230].contains(lat,lng)) return 178;
	       if (poly[2231].contains(lat,lng)) return 178;
	       else return 289;
	       }
	   else
	    if (lng < 96.608620)
	     return 289;
	    else
	     if (lng < 101.897381)
	      return 289;
	     else
	      if (lat < 66.815052)
	       if (lng < 104.541761)
	        return 289;
	       else
	        if (lat < 65.405855)
	         {
	         if (poly[2232].contains(lat,lng)) return 141;
	         else return 289;
	         }
	        else
	         {
	         if (poly[2233].contains(lat,lng)) return 289;
	         else return 141;
	         }
	      else
	       {
	       if (poly[2234].contains(lat,lng)) return 141;
	       else return 289;
	       }
	}

	private static int call40(double lat, double lng)
	{
	 if (lat < 22.521084)
	  if (lng < 91.211945)
	   if (lng < 89.199837)
	    if (lng < 88.912415)
	     return 371;
	    else
	     {
	     if (poly[2235].contains(lat,lng)) return 150;
	     if (poly[2236].contains(lat,lng)) return 150;
	     else return 371;
	     }
	   else
	    return 150;
	  else
	   if (lng < 93.530998)
	    if (lat < 20.499527)
	     return 40;
	    else
	     if (lng < 92.143913)
	      return 150;
	     else
	      if (lat < 20.634556)
	       if (lng < 92.329613)
	        return 150;
	       else
	        return 40;
	      else
	       if (lng < 92.359863)
	        if (lat < 20.724388)
	         return 150;
	        else
	         if (lat < 20.864358)
	          {
	          if (poly[2237].contains(lat,lng)) return 150;
	          else return 40;
	          }
	         else
	          {
	          if (poly[2238].contains(lat,lng)) return 150;
	          else return 40;
	          }
	       else
	        if (lat < 20.758194)
	         return 40;
	        else
	         {
	         if (poly[2239].contains(lat,lng)) return 150;
	         if (poly[2240].contains(lat,lng)) return 371;
	         else return 40;
	         }
	   else
	    if (lng < 93.814003)
	     return 40;
	    else
	     if (lng < 106.830833)
	      if (lng < 105.908585)
	       if (lng < 104.991447)
	        if (lng < 101.285751)
	         if (lat < 20.463194)
	          if (lng < 100.581413)
	           if (lng < 99.968803)
	            {
	            if (poly[2241].contains(lat,lng)) return 270;
	            else return 40;
	            }
	           else
	            {
	            if (poly[2242].contains(lat,lng)) return 40;
	            if (poly[2243].contains(lat,lng)) return 270;
	            else return 265;
	            }
	          else
	           {
	           if (poly[2244].contains(lat,lng)) return 270;
	           else return 265;
	           }
	         else
	          if (lng < 97.549877)
	           return 40;
	          else
	           if (lng < 99.417814)
	            {
	            if (poly[2245].contains(lat,lng)) return 403;
	            else return 40;
	            }
	           else
	            if (lat < 21.492139)
	             {
	             if (poly[2246].contains(lat,lng)) return 265;
	             if (poly[2247].contains(lat,lng)) return 403;
	             if (poly[2248].contains(lat,lng)) return 403;
	             if (poly[2249].contains(lat,lng)) return 403;
	             else return 40;
	             }
	            else
	             {
	             if (poly[2250].contains(lat,lng)) return 40;
	             if (poly[2251].contains(lat,lng)) return 40;
	             if (poly[2252].contains(lat,lng)) return 40;
	             if (poly[2253].contains(lat,lng)) return 265;
	             else return 403;
	             }
	        else
	         if (lat < 20.096361)
	          {
	          if (poly[2254].contains(lat,lng)) return 24;
	          else return 265;
	          }
	         else
	          if (lng < 103.138599)
	           if (lat < 21.308722)
	            {
	            if (poly[2255].contains(lat,lng)) return 24;
	            if (poly[2256].contains(lat,lng)) return 403;
	            else return 265;
	            }
	           else
	            if (lng < 102.212175)
	             {
	             if (poly[2257].contains(lat,lng)) return 24;
	             if (poly[2258].contains(lat,lng)) return 265;
	             else return 403;
	             }
	            else
	             {
	             if (poly[2259].contains(lat,lng)) return 265;
	             if (poly[2260].contains(lat,lng)) return 403;
	             if (poly[2261].contains(lat,lng)) return 403;
	             else return 24;
	             }
	          else
	           if (lat < 21.308722)
	            {
	            if (poly[2262].contains(lat,lng)) return 265;
	            else return 24;
	            }
	           else
	            {
	            if (poly[2263].contains(lat,lng)) return 403;
	            else return 24;
	            }
	       else
	        return 24;
	      else
	       if (lat < 21.023890)
	        return 24;
	       else
	        {
	        if (poly[2264].contains(lat,lng)) return 403;
	        else return 24;
	        }
	     else
	      if (lat < 20.985666)
	       return 24;
	      else
	       {
	       if (poly[2265].contains(lat,lng)) return 403;
	       else return 24;
	       }
	 else
	  if (lat < 35.516972)
	   if (lat < 23.445417)
	    if (lng < 90.600334)
	     if (lng < 88.997475)
	      if (lat < 23.199751)
	       {
	       if (poly[2266].contains(lat,lng)) return 150;
	       else return 371;
	       }
	      else
	       {
	       if (poly[2267].contains(lat,lng)) return 150;
	       else return 371;
	       }
	     else
	      return 150;
	    else
	     if (lng < 91.172333)
	      return 150;
	     else
	      if (lng < 91.944140)
	       if (lat < 22.756250)
	        return 150;
	       else
	        {
	        if (poly[2268].contains(lat,lng)) return 371;
	        else return 150;
	        }
	      else
	       if (lng < 99.563530)
	        if (lng < 93.402527)
	         if (lng < 92.532311)
	          {
	          if (poly[2269].contains(lat,lng)) return 371;
	          else return 150;
	          }
	         else
	          if (lat < 23.386499)
	           {
	           if (poly[2270].contains(lat,lng)) return 40;
	           else return 371;
	           }
	          else
	           {
	           if (poly[2271].contains(lat,lng)) return 40;
	           else return 371;
	           }
	        else
	         {
	         if (poly[2272].contains(lat,lng)) return 403;
	         if (poly[2273].contains(lat,lng)) return 403;
	         else return 40;
	         }
	       else
	        if (lng < 102.914339)
	         {
	         if (poly[2274].contains(lat,lng)) return 24;
	         else return 403;
	         }
	        else
	         if (lng < 103.939563)
	          {
	          if (poly[2275].contains(lat,lng)) return 24;
	          else return 403;
	          }
	         else
	          if (lng < 105.562853)
	           {
	           if (poly[2276].contains(lat,lng)) return 24;
	           else return 403;
	           }
	          else
	           {
	           if (poly[2277].contains(lat,lng)) return 24;
	           else return 403;
	           }
	   else
	    return call37(lat,lng);
	  else
	   if (lat < 70.332947)
	    if (lat < 69.633446)
	     return call39(lat,lng);
	    else
	     if (lng < 80.035687)
	      {
	      if (poly[2278].contains(lat,lng)) return 289;
	      else return 178;
	      }
	     else
	      return 289;
	   else
	    if (lng < 80.879807)
	     if (lat < 72.596179)
	      if (lng < 78.340698)
	       return 178;
	      else
	       if (lat < 72.380585)
	        if (lng < 80.798584)
	         if (lng < 79.569641)
	          {
	          if (poly[2279].contains(lat,lng)) return 289;
	          if (poly[2280].contains(lat,lng)) return 289;
	          else return 178;
	          }
	         else
	          {
	          if (poly[2281].contains(lat,lng)) return 178;
	          if (poly[2282].contains(lat,lng)) return 178;
	          else return 289;
	          }
	        else
	         return 289;
	       else
	        return 289;
	     else
	      return 289;
	    else
	     return 289;
	}

	private static int call41(double lat, double lng)
	{
	 if (lat < 3.416222)
	  if (lng < 100.976219)
	   if (lat < -9.221187)
	    if (lat < -38.738110)
	     return 236;
	    else
	     if (lng < 47.654308)
	      if (lat < -14.360188)
	       return 101;
	      else
	       if (lng < 45.292950)
	        if (lng < 44.538223)
	         return 218;
	        else
	         return 351;
	       else
	        return 66;
	     else
	      if (lng < 50.483780)
	       if (lat < -11.945433)
	        return 101;
	       else
	        return 66;
	      else
	       if (lat < -12.134610)
	        if (lng < 59.558319)
	         if (lng < 55.845039)
	          return 274;
	         else
	          return 233;
	        else
	         if (lng < 63.500179)
	          return 233;
	         else
	          if (lng < 77.598808)
	           return 236;
	          else
	           return 97;
	       else
	        if (lng < 51.192341)
	         return 66;
	        else
	         if (lng < 56.632473)
	          return 233;
	         else
	          return 97;
	   else
	    if (lng < 95.690170)
	     if (lng < 71.417809)
	      if (lng < 55.534912)
	       if (lng < 46.997968)
	        return 250;
	       else
	        return 66;
	      else
	       if (lng < 56.279507)
	        return 66;
	       else
	        return 13;
	     else
	      if (lng < 73.223663)
	       if (lng < 72.493164)
	        return 13;
	       else
	        return 382;
	      else
	       if (lng < 73.596863)
	        return 382;
	       else
	        return 26;
	    else
	     return 26;
	  else
	   if (lng < 104.306503)
	    if (lat < 1.155111)
	     return 26;
	    else
	     if (lat < 2.307417)
	      if (lng < 103.268196)
	       if (lng < 101.791115)
	        return 26;
	       else
	        if (lat < 1.412778)
	         return 26;
	        else
	         if (lng < 102.076376)
	          return 26;
	         else
	          if (lng < 102.501221)
	           if (lat < 1.600691)
	            return 26;
	           else
	            return 327;
	          else
	           return 327;
	      else
	       if (lat < 1.256694)
	        if (lng < 103.842331)
	         return 316;
	        else
	         return 26;
	       else
	        if (lat < 1.471278)
	         if (lng < 103.785408)
	          if (lng < 103.439392)
	           return 327;
	          else
	           {
	           if (poly[2283].contains(lat,lng)) return 327;
	           else return 316;
	           }
	         else
	          if (lng < 104.009645)
	           if (lat < 1.425806)
	            return 316;
	           else
	            if (lng < 103.865644)
	             return 316;
	            else
	             return 327;
	          else
	           {
	           if (poly[2284].contains(lat,lng)) return 316;
	           else return 327;
	           }
	        else
	         return 327;
	     else
	      return 327;
	   else
	    if (lng < 105.744698)
	     if (lat < 0.248667)
	      if (lat < -10.415778)
	       return 124;
	      else
	       return 26;
	     else
	      if (lat < 2.424417)
	       return 26;
	      else
	       if (lng < 104.546280)
	        return 327;
	       else
	        return 26;
	    else
	     return 26;
	 else
	  if (lat < 11.100528)
	   if (lng < 98.435860)
	    if (lng < 93.224335)
	     if (lng < 73.641914)
	      if (lng < 73.104500)
	       if (lng < 72.966393)
	        if (lng < 72.647163)
	         if (lng < 51.413029)
	          {
	          if (poly[2285].contains(lat,lng)) return 350;
	          else return 250;
	          }
	         else
	          return 371;
	        else
	         return 382;
	       else
	        if (lat < 7.030305)
	         return 382;
	        else
	         return 371;
	      else
	       if (lat < 6.978889)
	        return 382;
	       else
	        return 371;
	     else
	      if (lat < 9.259306)
	       if (lng < 79.442337)
	        return 371;
	       else
	        if (lng < 81.881279)
	         return 288;
	        else
	         return 371;
	      else
	       if (lng < 79.898308)
	        if (lng < 79.785637)
	         if (lng < 73.684830)
	          return 371;
	         else
	          if (lng < 79.729614)
	           {
	           if (poly[2286].contains(lat,lng)) return 288;
	           else return 371;
	           }
	          else
	           if (lat < 9.683722)
	            return 288;
	           else
	            return 371;
	        else
	         if (lat < 9.762139)
	          return 288;
	         else
	          return 371;
	       else
	        if (lng < 80.825242)
	         return 288;
	        else
	         return 371;
	    else
	     if (lat < 9.784583)
	      if (lng < 97.912476)
	       if (lat < 5.904417)
	        return 26;
	       else
	        if (lng < 93.954697)
	         return 371;
	        else
	         return 270;
	      else
	       if (lat < 9.011639)
	        if (lat < 4.860407)
	         return 26;
	        else
	         return 270;
	       else
	        if (lng < 98.166420)
	         return 40;
	        else
	         return 270;
	     else
	      return 40;
	   else
	    if (lat < 8.416445)
	     if (lat < 6.729889)
	      if (lng < 99.937164)
	       if (lat < 6.471167)
	        if (lng < 99.359053)
	         return 26;
	        else
	         return 327;
	       else
	        return 270;
	      else
	       if (lat < 5.479667)
	        return 327;
	       else
	        if (lng < 102.092140)
	         if (lng < 101.014652)
	          {
	          if (poly[2287].contains(lat,lng)) return 270;
	          if (poly[2288].contains(lat,lng)) return 270;
	          else return 327;
	          }
	         else
	          {
	          if (poly[2289].contains(lat,lng)) return 327;
	          if (poly[2290].contains(lat,lng)) return 327;
	          else return 270;
	          }
	        else
	         return 327;
	     else
	      return 270;
	    else
	     if (lng < 100.128609)
	      if (lng < 99.227821)
	       if (lng < 98.555359)
	        if (lat < 9.908667)
	         return 270;
	        else
	         return 40;
	       else
	        if (lat < 10.941639)
	         if (lng < 98.630722)
	          if (lat < 10.036747)
	           {
	           if (poly[2291].contains(lat,lng)) return 40;
	           else return 270;
	           }
	          else
	           return 40;
	         else
	          {
	          if (poly[2292].contains(lat,lng)) return 40;
	          if (poly[2293].contains(lat,lng)) return 40;
	          else return 270;
	          }
	        else
	         if (lng < 98.707916)
	          return 40;
	         else
	          {
	          if (poly[2294].contains(lat,lng)) return 270;
	          else return 40;
	          }
	      else
	       return 270;
	     else
	      if (lng < 103.821335)
	       if (lat < 9.317778)
	        if (lat < 8.900347)
	         return 24;
	        else
	         return 270;
	       else
	        return 251;
	      else
	       if (lat < 10.323584)
	        return 24;
	       else
	        if (lng < 106.207001)
	         if (lng < 104.334137)
	          if (lng < 104.078867)
	           if (lat < 10.498389)
	            return 24;
	           else
	            return 251;
	          else
	           return 251;
	         else
	          {
	          if (poly[2295].contains(lat,lng)) return 251;
	          else return 24;
	          }
	        else
	         return 24;
	  else
	   return call31(lat,lng);
	}

	private static int call42(double lat, double lng)
	{
	 if (lat < -11.020500)
	  if (lng < 152.860504)
	   if (lat < -21.504999)
	    if (lat < -38.222610)
	     if (lat < -40.198944)
	      return 113;
	     else
	      if (lat < -39.442554)
	       if (lng < 146.662109)
	        if (lng < 144.138580)
	         return 43;
	        else
	         return 126;
	       else
	        return 113;
	      else
	       return 126;
	    else
	     if (lng < 150.240891)
	      if (lat < -28.553888)
	       if (lng < 139.718369)
	        return 67;
	       else
	        if (lat < -34.009640)
	         if (lng < 141.000077)
	          {
	          if (poly[2296].contains(lat,lng)) return 126;
	          else return 67;
	          }
	         else
	          if (lat < -37.546555)
	           return 126;
	          else
	           if (lng < 145.620484)
	            if (lng < 143.310280)
	             if (lat < -35.778097)
	              return 126;
	             else
	              if (lng < 142.155178)
	               {
	               if (poly[2297].contains(lat,lng)) return 207;
	               else return 126;
	               }
	              else
	               if (lat < -34.893868)
	                return 126;
	               else
	                if (lng < 142.732729)
	                 {
	                 if (poly[2298].contains(lat,lng)) return 207;
	                 else return 126;
	                 }
	                else
	                 {
	                 if (poly[2299].contains(lat,lng)) return 207;
	                 else return 126;
	                 }
	            else
	             {
	             if (poly[2300].contains(lat,lng)) return 207;
	             else return 126;
	             }
	           else
	            if (lng < 147.930687)
	             {
	             if (poly[2301].contains(lat,lng)) return 207;
	             else return 126;
	             }
	            else
	             {
	             if (poly[2302].contains(lat,lng)) return 126;
	             else return 207;
	             }
	        else
	         if (lng < 150.149612)
	          {
	          if (poly[2303].contains(lat,lng)) return 67;
	          if (poly[2304].contains(lat,lng)) return 128;
	          if (poly[2305].contains(lat,lng)) return 235;
	          else return 207;
	          }
	         else
	          {
	          if (poly[2306].contains(lat,lng)) return 235;
	          else return 207;
	          }
	      else
	       if (lng < 141.004157)
	        {
	        if (poly[2307].contains(lat,lng)) return 67;
	        if (poly[2308].contains(lat,lng)) return 235;
	        else return 10;
	        }
	       else
	        return 235;
	     else
	      if (lat < -28.264166)
	       if (lat < -32.203251)
	        return 207;
	       else
	        if (lng < 152.468948)
	         {
	         if (poly[2309].contains(lat,lng)) return 235;
	         else return 207;
	         }
	        else
	         if (lat < -32.180173)
	          return 207;
	         else
	          {
	          if (poly[2310].contains(lat,lng)) return 235;
	          else return 207;
	          }
	      else
	       return 235;
	   else
	    if (lng < 136.992950)
	     return 10;
	    else
	     if (lng < 148.434326)
	      if (lng < 139.075974)
	       if (lng < 137.104355)
	        return 10;
	       else
	        if (lng < 138.000479)
	         {
	         if (poly[2311].contains(lat,lng)) return 235;
	         else return 10;
	         }
	        else
	         return 235;
	      else
	       return 235;
	     else
	      if (lat < -20.465250)
	       return 235;
	      else
	       if (lng < 149.064468)
	        if (lng < 148.871002)
	         return 235;
	        else
	         if (lat < -20.333471)
	          if (lng < 148.937225)
	           return 235;
	          else
	           return 213;
	         else
	          if (lng < 148.893860)
	           if (lat < -20.085666)
	            return 213;
	           else
	            return 235;
	          else
	           return 235;
	       else
	        if (lng < 149.111053)
	         return 235;
	        else
	         return 170;
	  else
	   if (lat < -21.188667)
	    if (lng < 171.450439)
	     if (lng < 159.045700)
	      if (lat < -28.160110)
	       if (lng < 153.483002)
	        if (lat < -29.370111)
	         return 207;
	        else
	         {
	         if (poly[2312].contains(lat,lng)) return 235;
	         else return 207;
	         }
	       else
	        if (lng < 153.639252)
	         if (lat < -28.222924)
	          return 207;
	         else
	          if (lat < -28.177389)
	           return 207;
	          else
	           {
	           if (poly[2313].contains(lat,lng)) return 235;
	           else return 207;
	           }
	        else
	         return 60;
	      else
	       return 235;
	     else
	      if (lat < -44.594396)
	       return 377;
	      else
	       if (lat < -28.992390)
	        if (lat < -41.865051)
	         return 377;
	        else
	         if (lng < 159.111282)
	          return 3;
	         else
	          return 224;
	       else
	        return 369;
	    else
	     return 377;
	   else
	    if (lng < 177.058060)
	     if (lat < -16.968111)
	      if (lng < 167.830368)
	       return 369;
	      else
	       if (lng < 170.234772)
	        return 232;
	       else
	        return 147;
	     else
	      if (lat < -12.518985)
	       return 232;
	      else
	       if (lng < 154.281662)
	        return 170;
	       else
	        if (lng < 168.843506)
	         return 304;
	        else
	         return 147;
	    else
	     return 147;
	 else
	  if (lng < 151.866272)
	   if (lng < 144.064911)
	    if (lng < 142.089691)
	     if (lat < -2.608639)
	      if (lat < -7.098861)
	       if (lng < 139.172470)
	        if (lng < 136.760635)
	         return 10;
	        else
	         return 131;
	       else
	        if (lng < 140.487747)
	         return 131;
	        else
	         if (lng < 141.021774)
	          return 131;
	         else
	          if (lng < 142.019409)
	           {
	           if (poly[2314].contains(lat,lng)) return 235;
	           else return 170;
	           }
	          else
	           if (lat < -9.177556)
	            return 170;
	           else
	            return 235;
	      else
	       if (lng < 138.932388)
	        return 131;
	       else
	        {
	        if (poly[2315].contains(lat,lng)) return 170;
	        else return 131;
	        }
	     else
	      return 131;
	    else
	     if (lat < -9.199111)
	      if (lat < -9.361917)
	       return 235;
	      else
	       if (lng < 142.294937)
	        return 235;
	       else
	        if (lng < 142.315720)
	         if (lat < -9.204348)
	          return 170;
	         else
	          return 235;
	        else
	         return 170;
	     else
	      return 170;
	   else
	    return 170;
	  else
	   if (lng < 158.697495)
	    if (lng < 157.121201)
	     if (lng < 154.718277)
	      return 170;
	     else
	      if (lat < -6.962555)
	       return 304;
	      else
	       if (lng < 155.963562)
	        return 170;
	       else
	        if (lat < -6.589611)
	         return 304;
	        else
	         return 170;
	    else
	     return 304;
	   else
	    if (lng < 161.563782)
	     if (lat < -4.621028)
	      return 304;
	     else
	      return 170;
	    else
	     if (lat < -9.702111)
	      return 304;
	     else
	      if (lng < 174.908386)
	       if (lng < 166.945282)
	        if (lng < 162.752884)
	         return 304;
	        else
	         return 398;
	       else
	        return 148;
	      else
	       if (lat < -5.641972)
	        return 65;
	       else
	        return 148;
	}

	private static int call43(double lat, double lng)
	{
	 if (lat < 12.112695)
	  if (lat < 5.898222)
	   if (lng < 118.087585)
	    if (lng < 108.407944)
	     return 26;
	    else
	     if (lng < 117.061142)
	      if (lng < 109.267975)
	       if (lat < 1.751361)
	        return 320;
	       else
	        return 26;
	      else
	       if (lat < 5.047167)
	        if (lat < 2.083333)
	         if (lng < 111.293198)
	          {
	          if (poly[2316].contains(lat,lng)) return 320;
	          else return 286;
	          }
	         else
	          if (lng < 114.177170)
	           if (lng < 112.735184)
	            {
	            if (poly[2317].contains(lat,lng)) return 320;
	            else return 286;
	            }
	           else
	            {
	            if (poly[2318].contains(lat,lng)) return 286;
	            if (poly[2319].contains(lat,lng)) return 383;
	            else return 320;
	            }
	          else
	           {
	           if (poly[2320].contains(lat,lng)) return 286;
	           if (poly[2321].contains(lat,lng)) return 320;
	           if (poly[2322].contains(lat,lng)) return 320;
	           else return 383;
	           }
	        else
	         if (lng < 111.416031)
	          return 286;
	         else
	          if (lng < 114.238586)
	           {
	           if (poly[2323].contains(lat,lng)) return 217;
	           else return 286;
	           }
	          else
	           if (lat < 3.565250)
	            {
	            if (poly[2324].contains(lat,lng)) return 383;
	            else return 286;
	            }
	           else
	            if (lng < 115.649864)
	             if (lat < 4.306208)
	              {
	              if (poly[2325].contains(lat,lng)) return 217;
	              if (poly[2326].contains(lat,lng)) return 217;
	              if (poly[2327].contains(lat,lng)) return 383;
	              else return 286;
	              }
	             else
	              if (lng < 114.944225)
	               {
	               if (poly[2328].contains(lat,lng)) return 286;
	               if (poly[2329].contains(lat,lng)) return 286;
	               else return 217;
	               }
	              else
	               {
	               if (poly[2330].contains(lat,lng)) return 217;
	               if (poly[2331].contains(lat,lng)) return 217;
	               else return 286;
	               }
	            else
	             {
	             if (poly[2332].contains(lat,lng)) return 286;
	             else return 383;
	             }
	       else
	        return 286;
	     else
	      if (lat < 4.147695)
	       return 383;
	      else
	       if (lng < 117.585808)
	        {
	        if (poly[2333].contains(lat,lng)) return 383;
	        else return 286;
	        }
	       else
	        {
	        if (poly[2334].contains(lat,lng)) return 383;
	        else return 286;
	        }
	   else
	    if (lng < 119.737030)
	     if (lat < 4.564611)
	      if (lat < 2.383722)
	       return 383;
	      else
	       return 286;
	     else
	      if (lng < 119.267502)
	       return 286;
	      else
	       return 279;
	    else
	     if (lat < 1.079167)
	      return 383;
	     else
	      return 279;
	  else
	   if (lng < 118.420753)
	    if (lat < 7.363417)
	     return 286;
	    else
	     if (lng < 109.288948)
	      return 24;
	     else
	      return 279;
	   else
	    return 279;
	 else
	  if (lat < 21.782278)
	   if (lat < 20.860584)
	    if (lng < 116.730553)
	     if (lat < 16.916646)
	      if (lng < 107.697029)
	       if (lat < 16.150742)
	        if (lat < 14.131718)
	         {
	         if (poly[2335].contains(lat,lng)) return 251;
	         else return 24;
	         }
	        else
	         if (lat < 15.141230)
	          {
	          if (poly[2336].contains(lat,lng)) return 251;
	          if (poly[2337].contains(lat,lng)) return 265;
	          else return 24;
	          }
	         else
	          {
	          if (poly[2338].contains(lat,lng)) return 265;
	          else return 24;
	          }
	       else
	        return 24;
	      else
	       return 24;
	     else
	      if (lng < 107.735809)
	       return 24;
	      else
	       if (lat < 20.156473)
	        return 403;
	       else
	        if (lng < 110.588860)
	         return 406;
	        else
	         return 125;
	    else
	     return 279;
	   else
	    if (lng < 108.123749)
	     if (lat < 21.495306)
	      return 24;
	     else
	      if (lng < 108.035889)
	       {
	       if (poly[2339].contains(lat,lng)) return 403;
	       else return 24;
	       }
	      else
	       return 403;
	    else
	     if (lng < 111.446281)
	      if (lat < 21.392139)
	       return 406;
	      else
	       if (lng < 109.072281)
	        return 403;
	       else
	        if (lng < 109.930391)
	         if (lat < 21.424911)
	          if (lng < 109.566605)
	           return 403;
	          else
	           return 406;
	         else
	          {
	          if (poly[2340].contains(lat,lng)) return 406;
	          if (poly[2341].contains(lat,lng)) return 406;
	          else return 403;
	          }
	        else
	         if (lng < 110.797249)
	          return 406;
	         else
	          {
	          if (poly[2342].contains(lat,lng)) return 403;
	          else return 406;
	          }
	     else
	      return 403;
	  else
	   if (lng < 114.403358)
	    if (lng < 113.660530)
	     if (lng < 113.414276)
	      if (lng < 113.043556)
	       if (lng < 111.681794)
	        if (lat < 23.833925)
	         if (lng < 108.477303)
	          return 403;
	         else
	          if (lng < 108.520538)
	           return 403;
	          else
	           if (lat < 22.694863)
	            if (lng < 111.101901)
	             {
	             if (poly[2343].contains(lat,lng)) return 406;
	             else return 403;
	             }
	            else
	             {
	             if (poly[2344].contains(lat,lng)) return 406;
	             else return 403;
	             }
	           else
	            {
	            if (poly[2345].contains(lat,lng)) return 291;
	            else return 403;
	            }
	        else
	         if (lng < 109.822357)
	          if (lng < 109.584435)
	           {
	           if (poly[2346].contains(lat,lng)) return 291;
	           else return 403;
	           }
	          else
	           {
	           if (poly[2347].contains(lat,lng)) return 291;
	           else return 403;
	           }
	         else
	          if (lng < 110.303545)
	           {
	           if (poly[2348].contains(lat,lng)) return 291;
	           else return 403;
	           }
	          else
	           {
	           if (poly[2349].contains(lat,lng)) return 291;
	           else return 403;
	           }
	       else
	        if (lat < 21.980417)
	         return 403;
	        else
	         if (lat < 23.501723)
	          if (lat < 22.741070)
	           {
	           if (poly[2350].contains(lat,lng)) return 291;
	           if (poly[2351].contains(lat,lng)) return 291;
	           else return 403;
	           }
	          else
	           {
	           if (poly[2352].contains(lat,lng)) return 403;
	           else return 291;
	           }
	         else
	          {
	          if (poly[2353].contains(lat,lng)) return 403;
	          else return 291;
	          }
	      else
	       return 291;
	     else
	      if (lat < 22.222334)
	       if (lng < 113.526558)
	        return 291;
	       else
	        if (lat < 22.137777)
	         {
	         if (poly[2354].contains(lat,lng)) return 291;
	         else return 132;
	         }
	        else
	         if (lat < 22.163389)
	          if (lng < 113.541180)
	           return 132;
	          else
	           return 291;
	         else
	          {
	          if (poly[2355].contains(lat,lng)) return 291;
	          else return 132;
	          }
	      else
	       return 291;
	    else
	     if (lat < 22.147278)
	      return 291;
	     else
	      if (lng < 114.271446)
	       if (lat < 22.232944)
	        if (lng < 113.808304)
	         return 291;
	        else
	         return 15;
	       else
	        if (lat < 22.356306)
	         return 15;
	        else
	         if (lng < 113.816559)
	          return 291;
	         else
	          {
	          if (poly[2356].contains(lat,lng)) return 291;
	          else return 15;
	          }
	      else
	       if (lat < 22.550333)
	        return 15;
	       else
	        return 291;
	   else
	    if (lng < 118.214226)
	     return 291;
	    else
	     if (lat < 24.452055)
	      return 125;
	     else
	      if (lng < 119.613945)
	       if (lat < 24.531528)
	        if (lng < 118.517749)
	         return 125;
	        else
	         return 291;
	       else
	        return 291;
	      else
	       if (lng < 119.899170)
	        return 291;
	       else
	        return 125;
	}

	private static int call44(double lat, double lng)
	{
	 if (lng < 119.924309)
	  if (lat < 39.324333)
	   if (lng < 111.212129)
	    if (lat < 39.289394)
	     if (lng < 109.199136)
	      return 403;
	     else
	      if (lat < 37.278850)
	       {
	       if (poly[2357].contains(lat,lng)) return 403;
	       else return 291;
	       }
	      else
	       {
	       if (poly[2358].contains(lat,lng)) return 291;
	       else return 403;
	       }
	    else
	     {
	     if (poly[2359].contains(lat,lng)) return 291;
	     else return 403;
	     }
	   else
	    return 291;
	  else
	   if (lat < 43.386189)
	    if (lng < 114.824286)
	     if (lng < 111.005214)
	      {
	      if (poly[2360].contains(lat,lng)) return 123;
	      else return 403;
	      }
	     else
	      if (lat < 41.355261)
	       {
	       if (poly[2361].contains(lat,lng)) return 403;
	       else return 291;
	       }
	      else
	       if (lng < 112.914750)
	        {
	        if (poly[2362].contains(lat,lng)) return 123;
	        if (poly[2363].contains(lat,lng)) return 291;
	        else return 403;
	        }
	       else
	        {
	        if (poly[2364].contains(lat,lng)) return 403;
	        else return 291;
	        }
	    else
	     return 291;
	   else
	    if (lat < 51.353809)
	     if (lng < 109.385003)
	      if (lat < 47.369999)
	       return 123;
	      else
	       if (lat < 49.361904)
	        {
	        if (poly[2365].contains(lat,lng)) return 141;
	        if (poly[2366].contains(lat,lng)) return 141;
	        else return 123;
	        }
	       else
	        if (lng < 108.285572)
	         {
	         if (poly[2367].contains(lat,lng)) return 123;
	         if (poly[2368].contains(lat,lng)) return 144;
	         else return 141;
	         }
	        else
	         {
	         if (poly[2369].contains(lat,lng)) return 123;
	         if (poly[2370].contains(lat,lng)) return 144;
	         if (poly[2371].contains(lat,lng)) return 144;
	         if (poly[2372].contains(lat,lng)) return 144;
	         else return 141;
	         }
	     else
	      if (lng < 114.654656)
	       if (lat < 47.369999)
	        if (lng < 112.019829)
	         {
	         if (poly[2373].contains(lat,lng)) return 203;
	         if (poly[2374].contains(lat,lng)) return 291;
	         else return 123;
	         }
	        else
	         {
	         if (poly[2375].contains(lat,lng)) return 123;
	         if (poly[2376].contains(lat,lng)) return 291;
	         else return 203;
	         }
	       else
	        if (lng < 112.019829)
	         {
	         if (poly[2377].contains(lat,lng)) return 141;
	         if (poly[2378].contains(lat,lng)) return 203;
	         else return 123;
	         }
	        else
	         {
	         if (poly[2379].contains(lat,lng)) return 123;
	         if (poly[2380].contains(lat,lng)) return 141;
	         else return 203;
	         }
	      else
	       if (lat < 47.369999)
	        if (lng < 117.289482)
	         {
	         if (poly[2381].contains(lat,lng)) return 291;
	         else return 203;
	         }
	        else
	         if (lat < 45.378094)
	          return 291;
	         else
	          if (lng < 118.606896)
	           {
	           if (poly[2382].contains(lat,lng)) return 291;
	           else return 203;
	           }
	          else
	           {
	           if (poly[2383].contains(lat,lng)) return 203;
	           else return 291;
	           }
	       else
	        if (lng < 117.289482)
	         {
	         if (poly[2384].contains(lat,lng)) return 141;
	         if (poly[2385].contains(lat,lng)) return 291;
	         else return 203;
	         }
	        else
	         if (lat < 49.361904)
	          {
	          if (poly[2386].contains(lat,lng)) return 203;
	          else return 291;
	          }
	         else
	          {
	          if (poly[2387].contains(lat,lng)) return 141;
	          else return 291;
	          }
	    else
	     if (lng < 119.135536)
	      if (lat < 57.605097)
	       if (lng < 113.160839)
	        {
	        if (poly[2388].contains(lat,lng)) return 141;
	        if (poly[2389].contains(lat,lng)) return 141;
	        else return 144;
	        }
	       else
	        if (lat < 54.479453)
	         if (lng < 116.148188)
	          if (lat < 52.916631)
	           {
	           if (poly[2390].contains(lat,lng)) return 144;
	           else return 141;
	           }
	          else
	           {
	           if (poly[2391].contains(lat,lng)) return 144;
	           else return 141;
	           }
	         else
	          {
	          if (poly[2392].contains(lat,lng)) return 144;
	          else return 141;
	          }
	        else
	         if (lng < 116.148188)
	          {
	          if (poly[2393].contains(lat,lng)) return 141;
	          if (poly[2394].contains(lat,lng)) return 141;
	          else return 144;
	          }
	         else
	          if (lat < 56.042275)
	           {
	           if (poly[2395].contains(lat,lng)) return 144;
	           else return 141;
	           }
	          else
	           {
	           if (poly[2396].contains(lat,lng)) return 144;
	           else return 141;
	           }
	      else
	       if (lng < 113.160839)
	        if (lat < 60.730740)
	         if (lng < 110.173491)
	          {
	          if (poly[2397].contains(lat,lng)) return 141;
	          else return 144;
	          }
	         else
	          {
	          if (poly[2398].contains(lat,lng)) return 141;
	          else return 144;
	          }
	        else
	         if (lng < 110.173491)
	          if (lat < 62.293562)
	           {
	           if (poly[2399].contains(lat,lng)) return 141;
	           else return 144;
	           }
	          else
	           {
	           if (poly[2400].contains(lat,lng)) return 144;
	           else return 141;
	           }
	         else
	          {
	          if (poly[2401].contains(lat,lng)) return 144;
	          else return 141;
	          }
	       else
	        if (lat < 60.730740)
	         if (lng < 116.148188)
	          {
	          if (poly[2402].contains(lat,lng)) return 144;
	          else return 141;
	          }
	         else
	          if (lat < 59.167918)
	           if (lng < 117.641862)
	            {
	            if (poly[2403].contains(lat,lng)) return 141;
	            else return 144;
	            }
	           else
	            {
	            if (poly[2404].contains(lat,lng)) return 144;
	            else return 141;
	            }
	          else
	           {
	           if (poly[2405].contains(lat,lng)) return 144;
	           else return 141;
	           }
	        else
	         return 141;
	     else
	      {
	      if (poly[2406].contains(lat,lng)) return 291;
	      else return 141;
	      }
	 else
	  if (lat < 40.517445)
	   return 291;
	  else
	   if (lat < 51.358552)
	    {
	    if (poly[2407].contains(lat,lng)) return 141;
	    else return 291;
	    }
	   else
	    {
	    if (poly[2408].contains(lat,lng)) return 291;
	    else return 141;
	    }
	}

	private static int call45(double lat, double lng)
	{
	 if (lng < 139.207809)
	  if (lat < 40.005638)
	   if (lng < 130.923218)
	    if (lat < 38.612446)
	     if (lng < 128.353561)
	      {
	      if (poly[2409].contains(lat,lng)) return 151;
	      else return 11;
	      }
	     else
	      return 11;
	    else
	     return 151;
	   else
	    return 280;
	  else
	   if (lng < 134.959961)
	    if (lat < 42.777332)
	     if (lng < 130.674866)
	      if (lng < 129.803268)
	       if (lat < 42.677834)
	        if (lat < 41.341736)
	         return 151;
	        else
	         if (lng < 128.633190)
	          {
	          if (poly[2410].contains(lat,lng)) return 151;
	          else return 42;
	          }
	         else
	          {
	          if (poly[2411].contains(lat,lng)) return 42;
	          else return 151;
	          }
	       else
	        {
	        if (poly[2412].contains(lat,lng)) return 151;
	        else return 42;
	        }
	      else
	       if (lat < 42.171638)
	        return 151;
	       else
	        {
	        if (poly[2413].contains(lat,lng)) return 37;
	        if (poly[2414].contains(lat,lng)) return 42;
	        else return 151;
	        }
	     else
	      return 37;
	    else
	     if (lat < 42.920139)
	      if (lng < 131.110046)
	       if (lng < 129.869827)
	        {
	        if (poly[2415].contains(lat,lng)) return 151;
	        else return 42;
	        }
	       else
	        if (lng < 130.261398)
	         {
	         if (poly[2416].contains(lat,lng)) return 42;
	         else return 151;
	         }
	        else
	         {
	         if (poly[2417].contains(lat,lng)) return 37;
	         else return 42;
	         }
	      else
	       return 37;
	     else
	      if (lat < 43.057888)
	       if (lng < 131.134811)
	        if (lng < 130.143417)
	         {
	         if (poly[2418].contains(lat,lng)) return 151;
	         else return 42;
	         }
	        else
	         if (lat < 42.957943)
	          {
	          if (poly[2419].contains(lat,lng)) return 37;
	          else return 42;
	          }
	         else
	          {
	          if (poly[2420].contains(lat,lng)) return 37;
	          else return 42;
	          }
	       else
	        return 37;
	      else
	       if (lat < 50.754833)
	        if (lat < 46.906361)
	         if (lng < 131.211536)
	          {
	          if (poly[2421].contains(lat,lng)) return 37;
	          if (poly[2422].contains(lat,lng)) return 37;
	          if (poly[2423].contains(lat,lng)) return 37;
	          if (poly[2424].contains(lat,lng)) return 37;
	          else return 42;
	          }
	         else
	          if (lat < 44.982124)
	           {
	           if (poly[2425].contains(lat,lng)) return 42;
	           if (poly[2426].contains(lat,lng)) return 42;
	           if (poly[2427].contains(lat,lng)) return 42;
	           if (poly[2428].contains(lat,lng)) return 42;
	           else return 37;
	           }
	          else
	           if (lng < 133.085749)
	            {
	            if (poly[2429].contains(lat,lng)) return 37;
	            else return 42;
	            }
	           else
	            {
	            if (poly[2430].contains(lat,lng)) return 42;
	            if (poly[2431].contains(lat,lng)) return 42;
	            else return 37;
	            }
	        else
	         if (lng < 131.211536)
	          if (lat < 48.830597)
	           {
	           if (poly[2432].contains(lat,lng)) return 37;
	           else return 42;
	           }
	          else
	           if (lng < 129.337324)
	            {
	            if (poly[2433].contains(lat,lng)) return 42;
	            else return 141;
	            }
	           else
	            if (lat < 49.792715)
	             {
	             if (poly[2434].contains(lat,lng)) return 37;
	             if (poly[2435].contains(lat,lng)) return 42;
	             else return 141;
	             }
	            else
	             {
	             if (poly[2436].contains(lat,lng)) return 37;
	             else return 141;
	             }
	         else
	          if (lat < 48.830597)
	           if (lng < 133.085749)
	            {
	            if (poly[2437].contains(lat,lng)) return 42;
	            else return 37;
	            }
	           else
	            {
	            if (poly[2438].contains(lat,lng)) return 42;
	            else return 37;
	            }
	          else
	           {
	           if (poly[2439].contains(lat,lng)) return 141;
	           else return 37;
	           }
	       else
	        if (lat < 54.603306)
	         if (lng < 131.211536)
	          {
	          if (poly[2440].contains(lat,lng)) return 141;
	          else return 37;
	          }
	         else
	          if (lat < 52.679070)
	           if (lng < 133.085749)
	            {
	            if (poly[2441].contains(lat,lng)) return 141;
	            else return 37;
	            }
	           else
	            {
	            if (poly[2442].contains(lat,lng)) return 141;
	            if (poly[2443].contains(lat,lng)) return 141;
	            else return 37;
	            }
	          else
	           if (lng < 133.085749)
	            {
	            if (poly[2444].contains(lat,lng)) return 141;
	            if (poly[2445].contains(lat,lng)) return 141;
	            else return 37;
	            }
	           else
	            if (lat < 53.641188)
	             if (lng < 134.022855)
	              {
	              if (poly[2446].contains(lat,lng)) return 141;
	              else return 37;
	              }
	             else
	              {
	              if (poly[2447].contains(lat,lng)) return 141;
	              else return 37;
	              }
	            else
	             return 37;
	        else
	         if (lng < 131.211536)
	          {
	          if (poly[2448].contains(lat,lng)) return 141;
	          else return 37;
	          }
	         else
	          if (lat < 56.527542)
	           if (lng < 133.085749)
	            if (lat < 55.565424)
	             {
	             if (poly[2449].contains(lat,lng)) return 141;
	             else return 37;
	             }
	            else
	             {
	             if (poly[2450].contains(lat,lng)) return 141;
	             if (poly[2451].contains(lat,lng)) return 141;
	             if (poly[2452].contains(lat,lng)) return 141;
	             else return 37;
	             }
	           else
	            return 37;
	          else
	           if (lng < 133.085749)
	            if (lat < 57.489660)
	             {
	             if (poly[2453].contains(lat,lng)) return 141;
	             if (poly[2454].contains(lat,lng)) return 141;
	             else return 37;
	             }
	            else
	             {
	             if (poly[2455].contains(lat,lng)) return 141;
	             else return 37;
	             }
	           else
	            return 37;
	   else
	    return 37;
	 else
	  if (lat < 45.463501)
	   return 280;
	  else
	   if (lat < 46.286667)
	    return 36;
	   else
	    return 37;
	}

	private static int call46(double lat, double lng)
	{
	 if (lng < 149.472534)
	  if (lng < 129.612976)
	   return 141;
	  else
	   if (lat < 72.331413)
	    if (lat < 71.397003)
	     if (lat < 71.260391)
	      if (lng < 147.165802)
	       if (lng < 146.329132)
	        if (lng < 138.052246)
	         if (lat < 64.856085)
	          if (lng < 133.832611)
	           if (lat < 61.653932)
	            if (lng < 131.722794)
	             {
	             if (poly[2456].contains(lat,lng)) return 104;
	             else return 141;
	             }
	            else
	             if (lat < 60.052855)
	              if (lng < 132.777702)
	               {
	               if (poly[2457].contains(lat,lng)) return 37;
	               if (poly[2458].contains(lat,lng)) return 104;
	               else return 141;
	               }
	              else
	               {
	               if (poly[2459].contains(lat,lng)) return 104;
	               else return 37;
	               }
	             else
	              {
	              if (poly[2460].contains(lat,lng)) return 141;
	              if (poly[2461].contains(lat,lng)) return 141;
	              else return 104;
	              }
	           else
	            {
	            if (poly[2462].contains(lat,lng)) return 37;
	            if (poly[2463].contains(lat,lng)) return 104;
	            if (poly[2464].contains(lat,lng)) return 104;
	            if (poly[2465].contains(lat,lng)) return 104;
	            else return 141;
	            }
	          else
	           if (lat < 61.653932)
	            if (lng < 135.942429)
	             {
	             if (poly[2466].contains(lat,lng)) return 37;
	             if (poly[2467].contains(lat,lng)) return 141;
	             else return 104;
	             }
	            else
	             {
	             if (poly[2468].contains(lat,lng)) return 104;
	             else return 37;
	             }
	           else
	            {
	            if (poly[2469].contains(lat,lng)) return 141;
	            else return 104;
	            }
	         else
	          if (lng < 133.832611)
	           if (lat < 68.058238)
	            {
	            if (poly[2470].contains(lat,lng)) return 104;
	            if (poly[2471].contains(lat,lng)) return 141;
	            else return 37;
	            }
	           else
	            {
	            if (poly[2472].contains(lat,lng)) return 37;
	            else return 141;
	            }
	          else
	           {
	           if (poly[2473].contains(lat,lng)) return 104;
	           else return 37;
	           }
	        else
	         if (lat < 60.739159)
	          if (lng < 138.438873)
	           if (lat < 60.680275)
	            {
	            if (poly[2474].contains(lat,lng)) return 104;
	            else return 37;
	            }
	           else
	            {
	            if (poly[2475].contains(lat,lng)) return 37;
	            else return 104;
	            }
	          else
	           {
	           if (poly[2476].contains(lat,lng)) return 46;
	           else return 37;
	           }
	         else
	          if (lat < 65.999775)
	           if (lng < 142.190689)
	            if (lat < 63.369467)
	             if (lng < 140.121468)
	              {
	              if (poly[2477].contains(lat,lng)) return 37;
	              if (poly[2478].contains(lat,lng)) return 140;
	              else return 104;
	              }
	             else
	              {
	              if (poly[2479].contains(lat,lng)) return 104;
	              if (poly[2480].contains(lat,lng)) return 140;
	              else return 37;
	              }
	            else
	             {
	             if (poly[2481].contains(lat,lng)) return 37;
	             if (poly[2482].contains(lat,lng)) return 46;
	             if (poly[2483].contains(lat,lng)) return 140;
	             else return 104;
	             }
	           else
	            if (lat < 63.369467)
	             if (lng < 144.259911)
	              {
	              if (poly[2484].contains(lat,lng)) return 140;
	              else return 37;
	              }
	             else
	              if (lat < 62.054313)
	               {
	               if (poly[2485].contains(lat,lng)) return 46;
	               if (poly[2486].contains(lat,lng)) return 46;
	               if (poly[2487].contains(lat,lng)) return 46;
	               if (poly[2488].contains(lat,lng)) return 140;
	               else return 37;
	               }
	              else
	               {
	               if (poly[2489].contains(lat,lng)) return 37;
	               if (poly[2490].contains(lat,lng)) return 37;
	               if (poly[2491].contains(lat,lng)) return 140;
	               else return 46;
	               }
	            else
	             {
	             if (poly[2492].contains(lat,lng)) return 140;
	             else return 46;
	             }
	          else
	           if (lng < 142.190689)
	            if (lat < 68.630083)
	             {
	             if (poly[2493].contains(lat,lng)) return 46;
	             else return 37;
	             }
	            else
	             {
	             if (poly[2494].contains(lat,lng)) return 46;
	             if (poly[2495].contains(lat,lng)) return 46;
	             if (poly[2496].contains(lat,lng)) return 46;
	             else return 37;
	             }
	           else
	            {
	            if (poly[2497].contains(lat,lng)) return 37;
	            if (poly[2498].contains(lat,lng)) return 37;
	            if (poly[2499].contains(lat,lng)) return 37;
	            else return 46;
	            }
	       else
	        if (lat < 59.279970)
	         return 37;
	        else
	         if (lat < 59.638329)
	          {
	          if (poly[2500].contains(lat,lng)) return 46;
	          else return 37;
	          }
	         else
	          if (lat < 60.272491)
	           if (lng < 146.376343)
	            {
	            if (poly[2501].contains(lat,lng)) return 46;
	            else return 37;
	            }
	           else
	            {
	            if (poly[2502].contains(lat,lng)) return 46;
	            else return 37;
	            }
	          else
	           {
	           if (poly[2503].contains(lat,lng)) return 37;
	           else return 46;
	           }
	      else
	       return 46;
	     else
	      if (lng < 138.020813)
	       return 37;
	      else
	       {
	       if (poly[2504].contains(lat,lng)) return 46;
	       else return 37;
	       }
	    else
	     if (lng < 137.560638)
	      if (lng < 130.047699)
	       return 141;
	      else
	       return 37;
	     else
	      if (lng < 138.896896)
	       return 37;
	      else
	       if (lng < 147.388789)
	        {
	        if (poly[2505].contains(lat,lng)) return 46;
	        else return 37;
	        }
	       else
	        return 46;
	   else
	    return 37;
	 else
	  if (lat < 70.611664)
	   if (lat < 69.316170)
	    if (lng < 155.597702)
	     return 46;
	    else
	     if (lat < 61.465637)
	      if (lng < 162.131085)
	       if (lat < 60.482445)
	        return 373;
	       else
	        return 46;
	      else
	       return 373;
	     else
	      if (lng < 161.149689)
	       if (lat < 68.347848)
	        if (lat < 64.906742)
	         return 46;
	        else
	         if (lng < 158.373695)
	          {
	          if (poly[2506].contains(lat,lng)) return 365;
	          if (poly[2507].contains(lat,lng)) return 365;
	          else return 46;
	          }
	         else
	          {
	          if (poly[2508].contains(lat,lng)) return 365;
	          else return 46;
	          }
	       else
	        return 46;
	      else
	       if (lng < 174.513611)
	        if (lat < 65.153413)
	         if (lng < 167.831650)
	          if (lng < 164.490669)
	           if (lat < 63.309525)
	            {
	            if (poly[2509].contains(lat,lng)) return 46;
	            else return 373;
	            }
	           else
	            if (lng < 162.820179)
	             {
	             if (poly[2510].contains(lat,lng)) return 365;
	             if (poly[2511].contains(lat,lng)) return 373;
	             if (poly[2512].contains(lat,lng)) return 373;
	             if (poly[2513].contains(lat,lng)) return 373;
	             if (poly[2514].contains(lat,lng)) return 373;
	             else return 46;
	             }
	            else
	             if (lat < 64.231469)
	              {
	              if (poly[2515].contains(lat,lng)) return 46;
	              if (poly[2516].contains(lat,lng)) return 46;
	              if (poly[2517].contains(lat,lng)) return 46;
	              else return 373;
	              }
	             else
	              {
	              if (poly[2518].contains(lat,lng)) return 46;
	              if (poly[2519].contains(lat,lng)) return 365;
	              else return 373;
	              }
	          else
	           {
	           if (poly[2520].contains(lat,lng)) return 365;
	           else return 373;
	           }
	         else
	          if (lng < 171.172630)
	           if (lat < 63.309525)
	            {
	            if (poly[2521].contains(lat,lng)) return 365;
	            else return 373;
	            }
	           else
	            {
	            if (poly[2522].contains(lat,lng)) return 373;
	            else return 365;
	            }
	          else
	           {
	           if (poly[2523].contains(lat,lng)) return 365;
	           else return 373;
	           }
	        else
	         {
	         if (poly[2524].contains(lat,lng)) return 46;
	         else return 365;
	         }
	       else
	        return 365;
	   else
	    if (lng < 162.089188)
	     return 46;
	    else
	     if (lng < 162.534729)
	      {
	      if (poly[2525].contains(lat,lng)) return 365;
	      if (poly[2526].contains(lat,lng)) return 365;
	      else return 46;
	      }
	     else
	      return 365;
	  else
	   if (lng < 151.096558)
	    if (lat < 72.170552)
	     return 46;
	    else
	     return 37;
	   else
	    if (lng < 152.310165)
	     return 46;
	    else
	     if (lng < 160.792831)
	      if (lat < 71.090889)
	       return 46;
	      else
	       return 37;
	     else
	      if (lng < 162.522812)
	       return 46;
	      else
	       return 365;
	}

	private static int call47(double lat, double lng)
	{
	 if (lng < 122.943253)
	  if (lat < 63.856384)
	   if (lng < 120.943222)
	    return call44(lat,lng);
	   else
	    if (lat < 40.943722)
	     return 291;
	    else
	     if (lat < 47.698626)
	      if (lat < 46.097607)
	       if (lat < 43.520664)
	        return 291;
	       else
	        if (lat < 44.809135)
	         {
	         if (poly[2527].contains(lat,lng)) return 42;
	         else return 291;
	         }
	        else
	         {
	         if (poly[2528].contains(lat,lng)) return 42;
	         else return 291;
	         }
	      else
	       {
	       if (poly[2529].contains(lat,lng)) return 42;
	       else return 291;
	       }
	     else
	      if (lat < 52.302201)
	       {
	       if (poly[2530].contains(lat,lng)) return 42;
	       else return 291;
	       }
	      else
	       {
	       if (poly[2531].contains(lat,lng)) return 291;
	       else return 141;
	       }
	  else
	   if (lng < 113.919746)
	    if (lat < 74.684387)
	     if (lng < 112.725800)
	      if (lat < 74.049721)
	       if (lat < 64.333396)
	        {
	        if (poly[2532].contains(lat,lng)) return 141;
	        if (poly[2533].contains(lat,lng)) return 289;
	        else return 144;
	        }
	       else
	        if (lng < 109.130819)
	         if (lat < 69.854431)
	          if (lng < 108.236649)
	           {
	           if (poly[2534].contains(lat,lng)) return 289;
	           else return 141;
	           }
	          else
	           {
	           if (poly[2535].contains(lat,lng)) return 289;
	           else return 141;
	           }
	         else
	          return 289;
	        else
	         if (lat < 69.191558)
	          return 141;
	         else
	          if (lat < 71.620640)
	           {
	           if (poly[2536].contains(lat,lng)) return 289;
	           else return 141;
	           }
	          else
	           {
	           if (poly[2537].contains(lat,lng)) return 141;
	           else return 289;
	           }
	      else
	       if (lng < 111.420525)
	        {
	        if (poly[2538].contains(lat,lng)) return 141;
	        else return 289;
	        }
	       else
	        if (lat < 74.602180)
	         return 141;
	        else
	         return 289;
	     else
	      return 141;
	    else
	     return 289;
	   else
	    return 141;
	 else
	  if (lat < 37.646000)
	   return 11;
	  else
	   if (lat < 73.348557)
	    if (lat < 39.418335)
	     if (lng < 125.381859)
	      if (lng < 124.768219)
	       if (lat < 37.982666)
	        return 11;
	       else
	        if (lng < 123.197807)
	         return 291;
	        else
	         return 151;
	      else
	       return 151;
	     else
	      if (lat < 37.681999)
	       return 11;
	      else
	       if (lng < 126.167000)
	        return 151;
	       else
	        if (lng < 126.666692)
	         if (lat < 37.749195)
	          return 11;
	         else
	          if (lng < 126.336586)
	           if (lat < 37.823339)
	            return 11;
	           else
	            return 151;
	          else
	           if (lng < 126.516444)
	            {
	            if (poly[2539].contains(lat,lng)) return 11;
	            else return 151;
	            }
	           else
	            {
	            if (poly[2540].contains(lat,lng)) return 11;
	            else return 151;
	            }
	        else
	         if (lat < 38.331139)
	          if (lng < 127.382829)
	           {
	           if (poly[2541].contains(lat,lng)) return 151;
	           else return 11;
	           }
	          else
	           {
	           if (poly[2542].contains(lat,lng)) return 151;
	           else return 11;
	           }
	         else
	          return 151;
	    else
	     if (lat < 39.607334)
	      if (lng < 123.096695)
	       return 291;
	      else
	       return 151;
	     else
	      if (lat < 39.931973)
	       if (lng < 124.255466)
	        if (lng < 123.756470)
	         return 291;
	        else
	         {
	         if (poly[2543].contains(lat,lng)) return 151;
	         else return 291;
	         }
	       else
	        return 151;
	      else
	       if (lat < 44.516262)
	        if (lat < 39.975224)
	         if (lng < 124.335096)
	          {
	          if (poly[2544].contains(lat,lng)) return 291;
	          else return 151;
	          }
	         else
	          return 151;
	        else
	         if (lat < 42.245743)
	          if (lng < 125.203182)
	           {
	           if (poly[2545].contains(lat,lng)) return 291;
	           else return 151;
	           }
	          else
	           if (lat < 41.110483)
	            {
	            if (poly[2546].contains(lat,lng)) return 42;
	            if (poly[2547].contains(lat,lng)) return 291;
	            else return 151;
	            }
	           else
	            if (lng < 126.333147)
	             {
	             if (poly[2548].contains(lat,lng)) return 151;
	             if (poly[2549].contains(lat,lng)) return 291;
	             else return 42;
	             }
	            else
	             {
	             if (poly[2550].contains(lat,lng)) return 151;
	             else return 42;
	             }
	         else
	          if (lng < 125.203182)
	           if (lat < 43.381002)
	            {
	            if (poly[2551].contains(lat,lng)) return 42;
	            if (poly[2552].contains(lat,lng)) return 42;
	            else return 291;
	            }
	           else
	            {
	            if (poly[2553].contains(lat,lng)) return 291;
	            if (poly[2554].contains(lat,lng)) return 291;
	            else return 42;
	            }
	          else
	           {
	           if (poly[2555].contains(lat,lng)) return 291;
	           else return 42;
	           }
	       else
	        if (lat < 53.560860)
	         if (lat < 46.995642)
	          {
	          if (poly[2556].contains(lat,lng)) return 291;
	          else return 42;
	          }
	         else
	          if (lat < 50.278251)
	           if (lng < 125.203182)
	            {
	            if (poly[2557].contains(lat,lng)) return 291;
	            else return 42;
	            }
	           else
	            {
	            if (poly[2558].contains(lat,lng)) return 141;
	            if (poly[2559].contains(lat,lng)) return 291;
	            if (poly[2560].contains(lat,lng)) return 291;
	            if (poly[2561].contains(lat,lng)) return 291;
	            else return 42;
	            }
	          else
	           if (lng < 125.203182)
	            if (lat < 51.919555)
	             {
	             if (poly[2562].contains(lat,lng)) return 291;
	             else return 42;
	             }
	            else
	             {
	             if (poly[2563].contains(lat,lng)) return 42;
	             if (poly[2564].contains(lat,lng)) return 291;
	             else return 141;
	             }
	           else
	            if (lat < 51.919555)
	             if (lng < 126.333147)
	              {
	              if (poly[2565].contains(lat,lng)) return 291;
	              else return 42;
	              }
	             else
	              {
	              if (poly[2566].contains(lat,lng)) return 141;
	              else return 42;
	              }
	            else
	             {
	             if (poly[2567].contains(lat,lng)) return 42;
	             else return 141;
	             }
	        else
	         return 141;
	   else
	    return 141;
	}

	private static int call48(double lat, double lng)
	{
	 if (lat < 0.572694)
	  if (lng < 135.281830)
	   if (lat < -6.556556)
	    if (lat < -13.113000)
	     if (lng < 125.490166)
	      return 35;
	     else
	      if (lng < 128.250809)
	       if (lat < -31.300000)
	        {
	        if (poly[2568].contains(lat,lng)) return 35;
	        else return 79;
	        }
	       else
	        return 35;
	      else
	       if (lat < -32.235695)
	        return 67;
	       else
	        if (lng < 128.470978)
	         if (lat < -31.300000)
	          return 79;
	         else
	          return 35;
	        else
	         if (lat < -14.760972)
	          if (lng < 129.259827)
	           if (lat < -31.300000)
	            {
	            if (poly[2569].contains(lat,lng)) return 67;
	            else return 79;
	            }
	           else
	            {
	            if (poly[2570].contains(lat,lng)) return 35;
	            if (poly[2571].contains(lat,lng)) return 67;
	            else return 10;
	            }
	          else
	           if (lat < -25.998917)
	            {
	            if (poly[2572].contains(lat,lng)) return 10;
	            else return 67;
	            }
	           else
	            return 10;
	         else
	          return 10;
	    else
	     if (lng < 125.647530)
	      if (lng < 119.216080)
	       if (lng < 116.696442)
	        if (lat < -8.059556)
	         if (lng < 114.601669)
	          if (lng < 113.425163)
	           return 26;
	          else
	           {
	           if (poly[2573].contains(lat,lng)) return 383;
	           if (poly[2574].contains(lat,lng)) return 383;
	           else return 26;
	           }
	         else
	          return 383;
	        else
	         return 26;
	       else
	        return 383;
	      else
	       if (lng < 124.019058)
	        return 383;
	       else
	        if (lat < -8.467278)
	         if (lat < -8.529388)
	          if (lng < 124.463577)
	           if (lat < -9.174916)
	            {
	            if (poly[2575].contains(lat,lng)) return 383;
	            else return 345;
	            }
	           else
	            return 383;
	          else
	           {
	           if (poly[2576].contains(lat,lng)) return 383;
	           else return 345;
	           }
	         else
	          return 383;
	        else
	         if (lng < 125.131531)
	          return 383;
	         else
	          return 345;
	     else
	      if (lat < -8.267805)
	       if (lat < -10.911028)
	        return 10;
	       else
	        if (lng < 127.345337)
	         return 345;
	        else
	         if (lng < 130.894257)
	          return 131;
	         else
	          return 10;
	      else
	       return 131;
	   else
	    if (lng < 125.011559)
	     if (lng < 121.025780)
	      if (lng < 115.766197)
	       if (lng < 108.610085)
	        if (lat < -2.501333)
	         return 26;
	        else
	         if (lng < 107.250053)
	          return 26;
	         else
	          return 320;
	       else
	        if (lng < 110.174026)
	         if (lng < 108.740776)
	          if (lat < -2.157319)
	           return 26;
	          else
	           return 320;
	         else
	          return 320;
	        else
	         if (lat < -5.426000)
	          return 26;
	         else
	          if (lat < -4.685056)
	           return 383;
	          else
	           if (lng < 113.894443)
	            if (lng < 113.424698)
	             return 320;
	            else
	             {
	             if (poly[2577].contains(lat,lng)) return 383;
	             else return 320;
	             }
	           else
	            if (lat < -1.807882)
	             {
	             if (poly[2578].contains(lat,lng)) return 320;
	             else return 383;
	             }
	            else
	             {
	             if (poly[2579].contains(lat,lng)) return 383;
	             if (poly[2580].contains(lat,lng)) return 383;
	             else return 320;
	             }
	      else
	       return 383;
	     else
	      if (lng < 124.280525)
	       return 383;
	      else
	       if (lat < -1.783278)
	        if (lat < -5.320944)
	         return 383;
	        else
	         return 131;
	       else
	        if (lat < -1.577778)
	         return 131;
	        else
	         return 383;
	    else
	     return 131;
	  else
	   return call42(lat,lng);
	 else
	  if (lat < 25.986084)
	   if (lng < 120.461304)
	    return call43(lat,lng);
	   else
	    if (lat < 9.828222)
	     if (lng < 125.889748)
	      if (lat < 5.184916)
	       if (lat < 3.805333)
	        return 383;
	       else
	        return 279;
	      else
	       return 279;
	     else
	      if (lat < 6.124361)
	       if (lng < 129.160172)
	        if (lat < 2.645555)
	         return 131;
	        else
	         if (lng < 126.592941)
	          return 279;
	         else
	          return 383;
	       else
	        if (lng < 172.137970)
	         if (lng < 163.034882)
	          if (lat < 1.072750)
	           return 131;
	          else
	           if (lng < 157.167328)
	            if (lng < 153.587921)
	             if (lat < 4.946306)
	              return 187;
	             else
	              return 216;
	            else
	             if (lng < 153.821915)
	              return 216;
	             else
	              return 222;
	           else
	            return 247;
	         else
	          return 18;
	        else
	         return 148;
	      else
	       if (lng < 168.183884)
	        if (lng < 147.031219)
	         if (lng < 126.601524)
	          return 279;
	         else
	          if (lng < 134.723724)
	           return 187;
	          else
	           return 216;
	        else
	         if (lng < 151.978775)
	          return 216;
	         else
	          if (lng < 160.718369)
	           return 222;
	          else
	           if (lng < 166.271835)
	            return 18;
	           else
	            if (lng < 167.775497)
	             return 292;
	            else
	             return 18;
	       else
	        return 18;
	    else
	     if (lat < 12.150167)
	      if (lng < 126.146303)
	       return 279;
	      else
	       if (lng < 139.795227)
	        return 216;
	       else
	        return 18;
	     else
	      if (lat < 13.240611)
	       return 279;
	      else
	       if (lng < 123.547638)
	        if (lat < 21.901806)
	         return 279;
	        else
	         if (lng < 122.000443)
	          return 125;
	         else
	          return 280;
	       else
	        if (lat < 24.215279)
	         if (lng < 124.421890)
	          if (lat < 14.100528)
	           return 279;
	          else
	           return 280;
	         else
	          if (lat < 16.374584)
	           if (lat < 14.655167)
	            if (lng < 145.294525)
	             if (lat < 13.879847)
	              return 106;
	             else
	              return 389;
	            else
	             return 18;
	           else
	            return 389;
	          else
	           if (lng < 145.852493)
	            return 389;
	           else
	            return 135;
	        else
	         return 280;
	  else
	   if (lat < 35.268307)
	    if (lng < 129.254519)
	     if (lng < 125.483109)
	      if (lat < 29.498695)
	       if (lng < 120.600304)
	        if (lng < 119.822388)
	         if (lng < 111.265633)
	          if (lng < 109.646695)
	           if (lng < 109.557594)
	            if (lat < 26.971592)
	             {
	             if (poly[2581].contains(lat,lng)) return 403;
	             else return 291;
	             }
	            else
	             if (lat < 28.235144)
	              {
	              if (poly[2582].contains(lat,lng)) return 403;
	              else return 291;
	              }
	             else
	              {
	              if (poly[2583].contains(lat,lng)) return 403;
	              else return 291;
	              }
	           else
	            {
	            if (poly[2584].contains(lat,lng)) return 403;
	            else return 291;
	            }
	          else
	           if (lng < 110.248316)
	            {
	            if (poly[2585].contains(lat,lng)) return 403;
	            else return 291;
	            }
	           else
	            {
	            if (poly[2586].contains(lat,lng)) return 403;
	            else return 291;
	            }
	         else
	          return 291;
	        else
	         if (lat < 26.386000)
	          if (lng < 119.958138)
	           if (lat < 26.236944)
	            return 125;
	           else
	            return 291;
	          else
	           return 125;
	         else
	          return 291;
	       else
	        return 291;
	      else
	       if (lat < 30.407972)
	        if (lng < 121.665970)
	         if (lng < 108.901655)
	          if (lat < 29.583147)
	           {
	           if (poly[2587].contains(lat,lng)) return 291;
	           else return 403;
	           }
	          else
	           {
	           if (poly[2588].contains(lat,lng)) return 291;
	           else return 403;
	           }
	         else
	          return 291;
	        else
	         return 291;
	       else
	        if (lng < 122.269943)
	         if (lng < 111.023788)
	          if (lat < 33.487884)
	           if (lng < 109.104965)
	            {
	            if (poly[2589].contains(lat,lng)) return 291;
	            else return 403;
	            }
	           else
	            if (lat < 31.947928)
	             {
	             if (poly[2590].contains(lat,lng)) return 403;
	             else return 291;
	             }
	            else
	             if (lng < 110.064377)
	              {
	              if (poly[2591].contains(lat,lng)) return 291;
	              if (poly[2592].contains(lat,lng)) return 291;
	              else return 403;
	              }
	             else
	              {
	              if (poly[2593].contains(lat,lng)) return 403;
	              if (poly[2594].contains(lat,lng)) return 403;
	              else return 291;
	              }
	          else
	           {
	           if (poly[2595].contains(lat,lng)) return 291;
	           else return 403;
	           }
	         else
	          return 291;
	        else
	         if (lng < 122.833557)
	          return 291;
	         else
	          return 11;
	     else
	      if (lat < 34.365654)
	       if (lat < 33.161221)
	        return 280;
	       else
	        if (lng < 127.405281)
	         return 11;
	        else
	         return 280;
	      else
	       return 11;
	    else
	     return 280;
	   else
	    if (lng < 127.463112)
	     return call47(lat,lng);
	    else
	     if (lat < 58.451778)
	      if (lng < 141.592331)
	       return call45(lat,lng);
	      else
	       if (lng < 150.940918)
	        if (lng < 145.820892)
	         if (lat < 45.523140)
	          if (lat < 43.575165)
	           return 280;
	          else
	           if (lng < 145.383667)
	            return 140;
	           else
	            return 280;
	         else
	          return 36;
	        else
	         return 140;
	       else
	        if (lng < 168.134003)
	         if (lat < 50.771973)
	          return 140;
	         else
	          if (lat < 50.924026)
	           if (lng < 156.508676)
	            return 140;
	           else
	            return 373;
	          else
	           return 373;
	        else
	         return 158;
	     else
	      return call46(lat,lng);
	}

	private static int call49(double lat, double lng)
	{
	 if (lat < 53.945271)
	  if (lat < 21.405190)
	   return call14(lat,lng);
	  else
	   if (lng < -79.740791)
	    if (lng < -93.328217)
	     if (lng < -123.840132)
	      if (lng < -128.572205)
	       if (lng < -133.190201)
	        if (lng < -175.332275)
	         if (lat < 28.219814)
	          return 196;
	         else
	          return 158;
	        else
	         if (lng < -169.662872)
	          if (lat < 26.074541)
	           return 244;
	          else
	           return 158;
	         else
	          if (lng < -166.086777)
	           return 134;
	          else
	           return 244;
	       else
	        return 45;
	      else
	       if (lat < 48.818356)
	        if (lat < 46.964504)
	         return 39;
	        else
	         if (lng < -124.737381)
	          return 45;
	         else
	          {
	          if (poly[2596].contains(lat,lng)) return 45;
	          else return 39;
	          }
	       else
	        return 45;
	     else
	      if (lat < 29.948254)
	       return call9(lat,lng);
	      else
	       if (lng < -122.747757)
	        if (lat < 48.698986)
	         if (lat < 48.259938)
	          return 39;
	         else
	          if (lng < -123.223061)
	           if (lng < -123.263084)
	            return 45;
	           else
	            if (lat < 48.555286)
	             return 39;
	            else
	             return 45;
	          else
	           return 39;
	        else
	         if (lng < -123.124390)
	          return 45;
	         else
	          if (lat < 48.930873)
	           if (lng < -123.040512)
	            if (lat < 48.708994)
	             return 39;
	            else
	             return 45;
	           else
	            return 39;
	          else
	           if (lat < 49.001335)
	            if (lng < -123.022484)
	             return 39;
	            else
	             if (lat < 48.988731)
	              return 39;
	             else
	              {
	              if (poly[2597].contains(lat,lng)) return 45;
	              else return 39;
	              }
	           else
	            return 45;
	       else
	        if (lng < -121.419121)
	         if (lat < 48.744881)
	          return 39;
	         else
	          if (lat < 49.001666)
	           {
	           if (poly[2598].contains(lat,lng)) return 45;
	           else return 39;
	           }
	          else
	           return 45;
	        else
	         if (lat < 34.080765)
	          if (lng < -118.299171)
	           return 39;
	          else
	           if (lng < -114.431386)
	            if (lat < 30.494665)
	             return 367;
	            else
	             if (lng < -117.236336)
	              if (lat < 32.542791)
	               return 39;
	              else
	               return 194;
	             else
	              if (lng < -116.797958)
	               if (lat < 31.821316)
	                return 194;
	               else
	                {
	                if (poly[2599].contains(lat,lng)) return 39;
	                else return 194;
	                }
	              else
	               if (lat < 32.287715)
	                {
	                if (poly[2600].contains(lat,lng)) return 194;
	                if (poly[2601].contains(lat,lng)) return 402;
	                else return 367;
	                }
	               else
	                {
	                if (poly[2602].contains(lat,lng)) return 39;
	                if (poly[2603].contains(lat,lng)) return 194;
	                if (poly[2604].contains(lat,lng)) return 367;
	                if (poly[2605].contains(lat,lng)) return 402;
	                else return 9;
	                }
	           else
	            if (lng < -103.053351)
	             if (lng < -109.046668)
	              if (lat < 29.996002)
	               if (lng < -113.569499)
	                return 367;
	               else
	                return 402;
	              else
	               {
	               if (poly[2606].contains(lat,lng)) return 9;
	               if (poly[2607].contains(lat,lng)) return 273;
	               else return 402;
	               }
	             else
	              if (lng < -106.050009)
	               if (lat < 32.014509)
	                if (lng < -107.548338)
	                 {
	                 if (poly[2608].contains(lat,lng)) return 185;
	                 if (poly[2609].contains(lat,lng)) return 273;
	                 if (poly[2610].contains(lat,lng)) return 402;
	                 else return 133;
	                 }
	                else
	                 {
	                 if (poly[2611].contains(lat,lng)) return 185;
	                 if (poly[2612].contains(lat,lng)) return 273;
	                 else return 133;
	                 }
	               else
	                return 273;
	              else
	               if (lat < 32.014509)
	                if (lng < -104.551680)
	                 if (lat < 30.981381)
	                  if (lng < -105.300845)
	                   {
	                   if (poly[2613].contains(lat,lng)) return 185;
	                   if (poly[2614].contains(lat,lng)) return 273;
	                   else return 133;
	                   }
	                  else
	                   {
	                   if (poly[2615].contains(lat,lng)) return 160;
	                   if (poly[2616].contains(lat,lng)) return 185;
	                   if (poly[2617].contains(lat,lng)) return 273;
	                   else return 133;
	                   }
	                 else
	                  {
	                  if (poly[2618].contains(lat,lng)) return 160;
	                  if (poly[2619].contains(lat,lng)) return 185;
	                  if (poly[2620].contains(lat,lng)) return 273;
	                  else return 133;
	                  }
	                else
	                 {
	                 if (poly[2621].contains(lat,lng)) return 273;
	                 else return 160;
	                 }
	               else
	                {
	                if (poly[2622].contains(lat,lng)) return 160;
	                else return 273;
	                }
	            else
	             return 160;
	         else
	          return call8(lat,lng);
	    else
	     if (lat < 29.404121)
	      if (lat < 24.892271)
	       if (lat < 22.911360)
	        if (lng < -84.369110)
	         if (lng < -86.791000)
	          if (lng < -89.633240)
	           return 32;
	          else
	           if (lng < -87.539192)
	            {
	            if (poly[2623].contains(lat,lng)) return 401;
	            else return 32;
	            }
	           else
	            return 401;
	         else
	          return 376;
	        else
	         return 376;
	       else
	        if (lat < 23.236698)
	         return 376;
	        else
	         if (lat < 24.000000)
	          if (lng < -80.852767)
	           return 376;
	          else
	           return 281;
	         else
	          return 165;
	      else
	       if (lng < -89.328438)
	        return 160;
	       else
	        if (lng < -88.995483)
	         return 160;
	        else
	         return 165;
	     else
	      return call10(lat,lng);
	   else
	    if (lng < -67.827522)
	     return call13(lat,lng);
	    else
	     if (lng < -55.273384)
	      return call11(lat,lng);
	     else
	      return call12(lat,lng);
	 else
	  if (lng < -73.973289)
	   return call19(lat,lng);
	  else
	   if (lng < -54.004189)
	    if (lat < 63.944897)
	     if (lng < -62.987293)
	      return call18(lat,lng);
	     else
	      return 182;
	    else
	     if (lat < 67.921890)
	      if (lng < -68.000000)
	       return 137;
	      else
	       return 166;
	     else
	      if (lng < -61.024975)
	       if (lat < 70.351471)
	        if (lng < -68.000000)
	         return 137;
	        else
	         return 166;
	       else
	        if (lng < -68.000000)
	         if (lat < 71.776749)
	          return 137;
	         else
	          if (lat < 79.070419)
	           return 306;
	          else
	           return 137;
	        else
	         if (lat < 77.519371)
	          if (lng < -63.736012)
	           if (lng < -65.006592)
	            return 306;
	           else
	            if (lat < 76.134232)
	             return 306;
	            else
	             {
	             if (poly[2624].contains(lat,lng)) return 16;
	             else return 306;
	             }
	          else
	           return 16;
	         else
	          if (lat < 81.847073)
	           if (lat < 80.684196)
	            if (lat < 79.171616)
	             if (lng < -66.601120)
	              return 306;
	             else
	              {
	              if (poly[2625].contains(lat,lng)) return 306;
	              else return 16;
	              }
	            else
	             return 16;
	           else
	            if (lng < -63.681331)
	             if (lat < 81.121711)
	              if (lng < -65.997282)
	               return 16;
	              else
	               return 166;
	             else
	              return 166;
	            else
	             return 16;
	          else
	           return 166;
	      else
	       return 16;
	   else
	    if (lng < -24.546524)
	     return 16;
	    else
	     if (lng < 4.692500)
	      if (lat < 62.400749)
	       if (lat < 57.850334)
	        if (lat < 55.540638)
	         if (lng < -8.195111)
	          return 285;
	         else
	          if (lng < -5.434611)
	           if (lng < -6.107806)
	            if (lat < 55.253502)
	             if (lng < -7.151459)
	              {
	              if (poly[2626].contains(lat,lng)) return 303;
	              if (poly[2627].contains(lat,lng)) return 303;
	              else return 285;
	              }
	             else
	              {
	              if (poly[2628].contains(lat,lng)) return 285;
	              if (poly[2629].contains(lat,lng)) return 285;
	              else return 303;
	              }
	            else
	             if (lng < -6.953667)
	              return 285;
	             else
	              return 303;
	           else
	            return 303;
	          else
	           if (lng < -5.059361)
	            return 303;
	           else
	            if (lng < -4.311500)
	             if (lat < 54.419724)
	              return 242;
	             else
	              return 303;
	            else
	             return 303;
	        else
	         return 303;
	       else
	        if (lat < 60.995361)
	         return 303;
	        else
	         if (lng < -6.258611)
	          return 361;
	         else
	          return 355;
	      else
	       if (lat < 76.736557)
	        if (lat < 66.534630)
	         return 21;
	        else
	         if (lat < 75.423607)
	          if (lat < 72.348299)
	           if (lat < 70.672104)
	            if (lat < 70.257696)
	             if (lng < -22.101221)
	              return 16;
	             else
	              return 21;
	            else
	             return 73;
	           else
	            if (lng < -22.033676)
	             if (lng < -24.400325)
	              {
	              if (poly[2630].contains(lat,lng)) return 73;
	              else return 16;
	              }
	             else
	              {
	              if (poly[2631].contains(lat,lng)) return 16;
	              else return 73;
	              }
	            else
	             if (lng < -21.532724)
	              return 73;
	             else
	              return 111;
	          else
	           return 16;
	         else
	          if (lat < 76.432144)
	           if (lng < -21.152466)
	            if (lat < 76.351425)
	             if (lat < 76.295341)
	              {
	              if (poly[2632].contains(lat,lng)) return 16;
	              else return 25;
	              }
	             else
	              if (lng < -22.892117)
	               {
	               if (poly[2633].contains(lat,lng)) return 25;
	               else return 16;
	               }
	              else
	               return 25;
	            else
	             if (lng < -22.886763)
	              {
	              if (poly[2634].contains(lat,lng)) return 25;
	              else return 16;
	              }
	             else
	              return 25;
	           else
	            if (lat < 75.841735)
	             {
	             if (poly[2635].contains(lat,lng)) return 25;
	             else return 16;
	             }
	            else
	             return 25;
	          else
	           if (lng < -22.866570)
	            {
	            if (poly[2636].contains(lat,lng)) return 25;
	            else return 16;
	            }
	           else
	            return 25;
	       else
	        if (lat < 78.174644)
	         if (lng < -22.771178)
	          {
	          if (poly[2637].contains(lat,lng)) return 25;
	          else return 16;
	          }
	         else
	          return 25;
	        else
	         if (lat < 79.653531)
	          if (lng < -19.217758)
	           if (lat < 78.355278)
	            if (lng < -22.759196)
	             {
	             if (poly[2638].contains(lat,lng)) return 25;
	             else return 16;
	             }
	            else
	             return 25;
	           else
	            if (lat < 78.890169)
	             if (lng < -22.723715)
	              {
	              if (poly[2639].contains(lat,lng)) return 25;
	              else return 16;
	              }
	             else
	              return 25;
	            else
	             if (lng < -22.673079)
	              {
	              if (poly[2640].contains(lat,lng)) return 25;
	              else return 16;
	              }
	             else
	              if (lat < 79.360726)
	               return 25;
	              else
	               {
	               if (poly[2641].contains(lat,lng)) return 16;
	               else return 25;
	               }
	          else
	           return 25;
	         else
	          return 16;
	     else
	      return 355;
	}

    public static class TzPolygon {

        double[] pts;

        TzPolygon(double ... D)
        {
            pts = D;
        }

        TzPolygon(String s)
        {
			String parts[] = s.split(",[\\s]*");
            pts = new double[parts.length];
            for (int i=0; i < parts.length; i++)
                pts[i] = Double.valueOf(parts[i]);
        }

        public boolean contains(double testy, double testx)
        {
            boolean inside = false;
            int n = pts.length;
            double yj = pts[n-2];
            double xj = pts[n-1];
            for (int i = 0; i < n; ) {
                double yi = pts[i++];
                double xi = pts[i++];
                if ( ((yi>testy) != (yj>testy)) && (testx < (xj-xi) * (testy-yi) / (yj-yi) + xi - 0.0001))
                    inside = !inside;
                xj = xi;
                yj = yi;
            }
            return inside;
        }
    }


	private static TzPolygon[] poly = initPolyArray();

	static TzPolygon[] initPolyArray()
    {
        poly = new TzPolygon[2642];
    
		TimezoneMapperInit1.init1(poly);
		TimezoneMapperInit1.init2(poly);
		TimezoneMapperInit1.init3(poly);
		TimezoneMapperInit1.init4(poly);
		TimezoneMapperInit1.init5(poly);
		TimezoneMapperInit1.init6(poly);
		TimezoneMapperInit1.init7(poly);
		TimezoneMapperInit1.init8(poly);
		TimezoneMapperInit1.init9(poly);
		TimezoneMapperInit1.init10(poly);
		TimezoneMapperInit2.init11(poly);
		TimezoneMapperInit2.init12(poly);
		TimezoneMapperInit2.init13(poly);
		TimezoneMapperInit2.init14(poly);
		TimezoneMapperInit2.init15(poly);
		TimezoneMapperInit2.init16(poly);
		TimezoneMapperInit2.init17(poly);
		TimezoneMapperInit2.init18(poly);
		TimezoneMapperInit2.init19(poly);
		TimezoneMapperInit2.init20(poly);
		TimezoneMapperInit3.init21(poly);
		TimezoneMapperInit3.init22(poly);
		TimezoneMapperInit3.init23(poly);
		TimezoneMapperInit3.init24(poly);
		TimezoneMapperInit3.init25(poly);
		TimezoneMapperInit3.init26(poly);
		TimezoneMapperInit3.init27(poly);
		return poly;
	}
}

