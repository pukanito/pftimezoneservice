/*
 This file is part of PFTimezoneService.

 PFTimezoneService is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PFTimezoneService is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with PFTimezoneService.  If not, see <http://www.gnu.org/licenses/>.

 The original code was written by Tim Cooper:   tim@edval.com.au
 https://sites.google.com/a/edval.biz/www/mapping-lat-lng-s-to-timezones
 This code is available under GPL:   https://www.gnu.org/copyleft/gpl.html

 The original code has been modified to perform on Android.
 Copyright 2015 Yves Han.
*/

package pukanito.at.gmail.com.pftimezoneservice;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

/**
 * Simple service that converts latitude/longitude to a time zone ID.
 */
public class TZService extends IntentService {

    public TZService() {
        super("TZService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            try {
                final ResultReceiver receiver = intent.getParcelableExtra("receiver");
                try {
                    final double latitude = intent.getDoubleExtra("latitude", 53.47722);
                    final double longitude = intent.getDoubleExtra("longitude", 0.0);
                    final String tzId = TimezoneMapper.latLngToTimezoneString(latitude, longitude);
                    final Bundle result = new Bundle();
                    result.putString("tzid", tzId);
                    receiver.send(0, result);
                } catch (Exception e) {
                    Log.d("TZService", "Exception: " + e.getMessage());
                    final Bundle result = new Bundle();
                    result.putString("tzid", "unknown");
                    result.putString("error", e.getMessage());
                    receiver.send(1, result);
                }
            }
            catch (Exception e) {
                Log.d("TZService", "Exception: " + e.getMessage());
            }
        }
    }
}
