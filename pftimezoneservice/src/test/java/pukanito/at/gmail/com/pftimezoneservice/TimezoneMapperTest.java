package pukanito.at.gmail.com.pftimezoneservice;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TimezoneMapperTest {

    @Test
    public void test()
    {
        assertEquals("Australia/Sydney", TimezoneMapper.latLngToTimezoneString(-33, 151));
        assertEquals("Europe/Helsinki", TimezoneMapper.latLngToTimezoneString(60.173154, 24.940936));
        assertEquals("Europe/London", TimezoneMapper.latLngToTimezoneString(51.516007, -0.121644));
        assertEquals("Europe/Paris", TimezoneMapper.latLngToTimezoneString(48.856696, 2.352077));
        assertEquals("Europe/Tallinn", TimezoneMapper.latLngToTimezoneString(59.438442, 24.753463));
        assertEquals("Asia/Riyadh", TimezoneMapper.latLngToTimezoneString(21.518043, 39.191228));
        assertEquals("America/New_York", TimezoneMapper.latLngToTimezoneString(35.03217, -85.19392));
        assertEquals("Australia/Brisbane", TimezoneMapper.latLngToTimezoneString(-28.019981, 153.428073));
        assertEquals("Europe/Helsinki", TimezoneMapper.latLngToTimezoneString(65.012197, 25.471152));
    }
}